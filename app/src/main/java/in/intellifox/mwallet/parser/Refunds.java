package in.intellifox.mwallet.parser;

import java.util.ArrayList;

/**
 * Created by Raju on 09/10/2015.
 */
public class Refunds {


    private ArrayList<Refund> mRefunds;

    public ArrayList<Refund> getRefunds() {
        return mRefunds;
    }

    public void setRefunds(ArrayList<Refund> refunds) {
        this.mRefunds = refunds;
    }
}
