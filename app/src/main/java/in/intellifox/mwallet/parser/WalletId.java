package in.intellifox.mwallet.parser;

/**
 * Created by Raju on 08/10/2015.
 */
public class WalletId {

    private String AccountType;
    private String isPrimary;
    private String balance;
    private String walletId;


    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }
    public String getAccountType() {
        return AccountType;
    }
    public void setAccountType(String accountType) {
        AccountType = accountType;
    }
    public String getIsPrimary() {
        return isPrimary;
    }
    public void setIsPrimary(String isPrimary) {
        this.isPrimary = isPrimary;
    }


    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }
}
