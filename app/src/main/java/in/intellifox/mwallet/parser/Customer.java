package in.intellifox.mwallet.parser;

import in.intellifox.mwallet.utilities.Utils;

/**
 * Created by Raju on 09/10/2015.
 */
public class Customer {


    private String FirstName;

    private String LastName;

    private String MobileNumber;
    private String CardNumber;


    public String getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(String cardNumber) {
        CardNumber = cardNumber;
    }

    public String getFirstName ()
    {     if(FirstName==null || FirstName.equalsIgnoreCase("null") || FirstName.isEmpty())
        return "";

        return Utils.decodeString(FirstName);
    }

    public void setFirstName (String FirstName)
    {
        this.FirstName = FirstName;
    }

    public String getLastName ()
    {
        if(LastName==null || LastName.equalsIgnoreCase("null") || LastName.isEmpty())
            return "";

        return Utils.decodeString(LastName);
    }

    public void setLastName (String LastName)
    {
        this.LastName = LastName;
    }

    public String getMobileNumber ()
    {
        return MobileNumber;
    }

    public void setMobileNumber (String MobileNumber)
    {
        this.MobileNumber = MobileNumber;
    }

}
