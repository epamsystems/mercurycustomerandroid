package in.intellifox.mwallet.parser;

/**
 * Created by Raju on 05/10/2015.
 */
public class RequestDetails {

    private String Address;
    private String Amount;
    private String ATMId;
    private String AccountType;
    private String BankName;
    private String CardNumber;
    private String Country;
    private String CVVNumber;
    private String DateOfBirth;
    private String ExpiryDate;
    private String EmailId;
    private String Favourite;
    private String FirstName;
    private String LastName;
    private String MobileNumber;
    private String MerchantId;
    private String Mpin;
    private String NewTpin;
    private String NewMpin;
    private String OldTpin;
    private String OldMpin;
    private String PinCode;
    private String ProfilePic;
    private String ResponseURL;
    private String ResponseVar;
    private String RegionName;
    private String RegionID;
    private String Remark;
    private String ReceiverMobileNumber;
    private String ReceiverEmailId;
    private String SenderMobileNumber;
    private String StartDate;
    private String Status;
    private String Tpin;
    private String TxnId;
    private String WalletId;
    private KYCDocuments kycDocuments;


    public KYCDocuments getKYCDocuments(){return kycDocuments;}

    public void setKYCDocuments(KYCDocuments kycDocuments)
    {
        this.kycDocuments = kycDocuments;
    }


    public String getAddress() {
        return Address;
    }
    public void setAddress(String Address) {
        this.Address = Address;
    }
    public String getAmount() {
        return Amount;
    }
    public void setAmount(String Amount) {
        this.Amount = Amount;
    }
    public String getATMId() {
        return ATMId;
    }
    public void setATMId(String ATMId) {
        this.ATMId = ATMId;
    }
    public String getAccountType() {
        return AccountType;
    }
    public void setAccountType(String AccountType) {
        this.AccountType = AccountType;
    }
    public String getBankName() {
        return BankName;
    }
    public void setBankName(String BankName) {
        this.BankName = BankName;
    }
    public String getCardNumber() {
        return CardNumber;
    }
    public void setCardNumber(String CardNumber) {
        this.CardNumber = CardNumber;
    }
    public String getCountry() {
        return Country;
    }
    public void setCountry(String Country) {
        this.Country = Country;
    }
    public String getCVVNumber() {
        return CVVNumber;
    }
    public void setCVVNumber(String CVVNumber) {
        this.CVVNumber = CVVNumber;
    }
    public String getDateOfBirth() {
        return DateOfBirth;
    }
    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }
    public String getExpiryDate()
    {
        return ExpiryDate;
    }

    public void setExpiryDate(String ExpiryDate)
    {
        this.ExpiryDate = ExpiryDate;
    }
    public String getEmailId() {
        return EmailId;
    }
    public void setEmailId(String EmailId)
    {
        this.EmailId = EmailId;
    }
    public String getFavourite()
    {
        return Favourite;
    }
    public void setFavourite(String Favourite)
    {
        this.Favourite = Favourite;
    }

    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }
    public String getLastName() {
        return LastName;
    }
    public void setLastName(String LastName) {
        this.LastName = LastName;
    }
    public String getMobileNumber() {
        return MobileNumber;
    }
    public void setMobileNumber(String MobileNumber) {
        this.MobileNumber = MobileNumber;
    }
    public String getMerchantId() {
        return MerchantId;
    }
    public void setMerchantId(String MerchantId) {
        this.MerchantId = MerchantId;
    }
    public String getMpin() {
        return Mpin;
    }
    public void setMpin(String Mpin) {
        this.Mpin = Mpin;
    }
    public String getNewTpin() {
        return NewTpin;
    }
    public void setNewTpin(String NewTpin) {
        this.NewTpin = NewTpin;
    }
    public String getNewMpin() {
        return NewMpin;
    }
    public void setNewMpin(String NewMpin) {
        this.NewMpin = NewMpin;
    }
    public String getOldTpin() {
        return OldTpin;
    }
    public void setOldTpin(String OldTpin) {
        this.OldTpin = OldTpin;
    }
    public String getOldMpin() {
        return OldMpin;
    }
    public void setOldMpin(String OldMpin) {
        this.OldMpin = OldMpin;
    }
    public String getPinCode()
    {
        return PinCode;
    }

    public void setPinCode(String PinCode)
    {
        this.PinCode = PinCode;
    }
    public String getProfilePic() {
        return ProfilePic;
    }
    public void setProfilePic(String ProfilePic)
    {
        this.ProfilePic = ProfilePic;
    }
    public String getResponseURL()
    {
        return ResponseURL;
    }
    public void setResponseURL(String ResponseURL)
    {
        this.ResponseURL = ResponseURL;
    }

    public String getResponseVar()
    {
        return ResponseVar;
    }

    public void setResponseVar(String ResponseVar)
    {
        this.ResponseVar = ResponseVar;
    }
    public String getRegionName() {
        return RegionName;
    }
    public void setRegionName(String RegionName)
    {
        this.RegionName = RegionName;
    }
    public String getRegionID()
    {
        return RegionID;
    }
    public void setRegionID(String RegionID)
    {
        this.RegionID = RegionID;
    }

    public String getRemark() {
        return Remark;
    }
    public void setRemark(String Remark) {
        this.Remark = Remark;
    }
    public String getReceiverMobileNumber() {
        return ReceiverMobileNumber;
    }
    public void setReceiverMobileNumber(String ReceiverMobileNumber) {
        this.ReceiverMobileNumber = ReceiverMobileNumber;
    }
    public String getReceiverEmailId() {
        return ReceiverEmailId;
    }
    public void setReceiverEmailId(String ReceiverEmailId) {
        this.ReceiverEmailId = ReceiverEmailId;
    }
    public String getSenderMobileNumber() {
        return SenderMobileNumber;
    }
    public void setSenderMobileNumber(String SenderMobileNumber) {
        this.SenderMobileNumber = SenderMobileNumber;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String StartDate) {
        this.StartDate = StartDate;
    }
    public String getStatus() {
        return Status;
    }
    public void setStatus(String Status) {
        this.Status = Status;
    }
    public String getTpin() {
        return Tpin;
    }
    public void setTpin(String Tpin) {
        this.Tpin = Tpin;
    }
    public String getTxnId() {
        return TxnId;
    }
    public void setTxnId(String TxnId) {
        this.TxnId = TxnId;
    }
    public String getWalletId() {
        return WalletId;
    }
    public void setWalletId(String WalletId) {
        this.WalletId = WalletId;
    }


}
