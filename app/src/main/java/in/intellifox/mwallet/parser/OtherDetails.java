package in.intellifox.mwallet.parser;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Raju on 09/10/2015.
 */
public class OtherDetails {

    private ArrayList<Customer> Customer;
    private ArrayList<Bank> bank;
    private ArrayList<Merchant> Merchant;
    private ArrayList<Agent> agents;


    private ArrayList<Refunds> refunds;

    public ArrayList<Bank> getBank() {
        return bank;
    }

    public void setBank(ArrayList<Bank> bank) {
        this.bank = bank;
    }

    public ArrayList<Merchant> getMerchant() {
        return Merchant;
    }

    public void setMerchant(ArrayList<Merchant> Merchant) {
        this.Merchant = Merchant;
    }

    public ArrayList<Customer> getCustomer() {
        return Customer;
    }

    public void setCustomer(ArrayList<Customer> customer) {
        Customer = customer;
    }


    public void setAgents(ArrayList<Agent> agents) {

        this.agents = agents;
    }

    public ArrayList<Agent> getAgents() {

        return agents;
    }

    public ArrayList<Refunds> getRefunds() {
        return refunds;
    }

    public void setRefunds(ArrayList<Refunds> refunds) {
        this.refunds = refunds;
    }


}
