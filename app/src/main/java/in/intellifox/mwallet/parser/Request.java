package in.intellifox.mwallet.parser;

/**
 * Created by Raju on 05/10/2015.
 */
public class Request {

    private String RequestType;
    private String UserType;

    public String getRequestType() {
        return RequestType;
    }
    public void setRequestType(String requestType) {
        RequestType = requestType;
    }
    public String getUserType() {
        return UserType;
    }
    public void setUserType(String userType) {
        UserType = userType;
    }
}
