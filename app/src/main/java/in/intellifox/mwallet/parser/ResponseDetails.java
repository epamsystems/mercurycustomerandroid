package in.intellifox.mwallet.parser;

import java.io.Serializable;

/**
 * Created by Raju on 05/10/2015.
 */
public class ResponseDetails implements Serializable{

    private String securitykey;


    private String firstName;
    private String firstTime;
    private String lastName;
    private String changeMPin;
    private String changeTPin;
    private String lastLogin;
    private String lastTransaction;
    private String agentId;
    private String errorCode;
    private String reason;
    private String message;
    private String balance;
    private String txnId;
    private String amount;
    private String commission;
    private String fee;
    private String ChangeLastName;
    private String ChangeRequestID;
    private String BankAccountLinked;
    private String ChangeFirstName;
    private String transactiondate;
    private String OTP;
    private String AppUpdateFlag;
    private Profile profile;
    private Wallet wallet;
    private MoneyRequests MoneyRequests;
    private Documents documents;
    private Beneficiarys beneficiarys;

    private Regions regions;
    private Banks banks;
    private Transactions Transactions;

    private Favourites Favourites;
    private Cards Cards;

    //This is implemented for Oneclick
    private String OneClilckFlag ;
    private String OneClilckAmount ;


    private  String cashbackAmount;

    public String getOneClilckFlag() {
        return OneClilckFlag;
    }

    public void setOneClilckFlag(String oneClilckFlag) {
        OneClilckFlag = oneClilckFlag;
    }

    public String getOneClilckAmount() {
        return OneClilckAmount;
    }

    public void setOneClilckAmount(String oneClilckAmount) {
        OneClilckAmount = oneClilckAmount;
    }

    public String getAppUpdateFlag() {
        return AppUpdateFlag;
    }

    public void setAppUpdateFlag(String appUpdateFlag) {
        AppUpdateFlag = appUpdateFlag;
    }

    public String getSecuritykey() {
        return securitykey;
    }

    public void setSecuritykey(String securitykey) {
        this.securitykey = securitykey;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public Beneficiarys getBeneficiarys() {
        return beneficiarys;
    }

    public void setBeneficiarys(Beneficiarys beneficiarys) {
        this.beneficiarys = beneficiarys;
    }

    public Cards getCards ()
    {
        return Cards;
    }

    public void setCards(Cards Cards)
    {
        this.Cards = Cards;
    }

    public Favourites getFavourites ()
    {
        return Favourites;
    }

    public void setFavourites(Favourites Favourites)
    {
        this.Favourites = Favourites;
    }


    public Transactions getTransactions ()
    {
        return Transactions;
    }

    public void setTransactions (Transactions Transactions)
    {
        this.Transactions = Transactions;
    }


    public Profile getProfile(){return profile;}

    public void setProfile(Profile profile)
    {
        this.profile = profile;
    }


    public Banks getBanks ()
    {
        return banks;
    }

    public void setBanks (Banks banks)
    {
        this.banks = banks;
    }

    public Regions getRegions(){return regions;}

    public void setRegions(Regions regions)
    {
        this.regions = regions;
    }

    public Documents getDocuments(){return documents;}

    public void setDocuments(Documents documents)
    {
        this.documents = documents;
    }

    public Wallet getWallet(){return wallet;}

    public void setWallet(Wallet wallet)
    {
        this.wallet = wallet;
    }
    public MoneyRequests getMoneyRequests(){return MoneyRequests;}

    public void setMoneyRequests(MoneyRequests MoneyRequests)
    {
        this.MoneyRequests = MoneyRequests;
    }

    public String getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    public String getReason() {
        return reason;
    }
    public void setReason(String reason) {
        this.reason = reason;
    }
    public String getSecurityKey() {
        return securitykey;
    }
    public void setSecurityKey(String securitykey) {
        this.securitykey = securitykey;
    }


    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getChangeMPin() {
        return changeMPin;
    }
    public void setChangeMPin(String changeMPin) {
        this.changeMPin = changeMPin;
    }

    public String getChangeTPin(){ return changeTPin; }

    public void setChangeTPin(String changeTPin){this.changeTPin = changeTPin; }


    public String getLastLogin(){return lastLogin;}
    public void setLastLogin(String lastLogin){this.lastLogin=lastLogin;}

    public String getLastTransaction(){return lastTransaction;}
    public  void setLastTransaction(String lastTransaction){this.lastTransaction=lastTransaction;}


    public String getAgentId() {
        return agentId;
    }
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }


    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }
    public String getTxnId() {
        return txnId;
    }
    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }


    public String getAmount() {
        return amount;
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }
    public String getCommission() {
        return commission;
    }
    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getFee() {
        return fee;
    }
    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getFirstTime() {
        return firstTime;
    }
    public void setFirstTime(String firstTime) {
        this.firstTime = firstTime;
    }
    public String getTransactionDate(){return transactiondate;}
    public void setTransactionDate(String transactiondate){this.transactiondate =transactiondate;}

    public String getChangeFirstName(){return ChangeFirstName;}
    public void setChangeFirstName(String transactiondate){this.ChangeFirstName =ChangeFirstName;}

    public String getChangeLastName(){return ChangeLastName;}
    public void setChangeLastName(String transactiondate){this.ChangeLastName =ChangeLastName;}

    public String getChangeRequestID(){return ChangeRequestID;}
    public void setChangeRequestID(String ChangeRequestID){this.ChangeRequestID =ChangeRequestID;}

    public String getBankAccountLinked(){return BankAccountLinked;}
    public void setBankAccountLinked(String BankAccountLinked){this.BankAccountLinked =BankAccountLinked;}

    public String getOTP(){return OTP;}
    public void setOTP(String OTP){this.OTP =OTP;}

    public String getCashbackAmount() {
        return cashbackAmount;
    }

    public void setCashbackAmount(String cashbackAmount) {
        this.cashbackAmount = cashbackAmount;
    }





}
