package in.intellifox.mwallet.parser;

import org.jdom2.Document;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Raju on 08/10/2015.
 */
public class KYCDocuments implements Serializable {

    private ArrayList<in.intellifox.mwallet.parser.Document> documents;

    public ArrayList<in.intellifox.mwallet.parser.Document> getDocument() {
        return documents;
    }

    public void setDocument(ArrayList<in.intellifox.mwallet.parser.Document> documents) {
        this.documents = documents;
    }
}
