package in.intellifox.mwallet.parser;

/**
 * Created by Raju on 08/10/2015.
 */
public class MoneyRequest {

    private String amount;
    private String id;
    private String mobileNumber;
    private String firstName;
    private String lastName;
    private String txnDate;

    private String MoneyRequest;

    public String getamount() {
        return amount;
    }
    public void setamount(String amount) {
        this.amount = amount;
    }
    public String getid() {
        return id;
    }
    public void setid(String id) {
        this.id = id;
    }

    public String getmobileNumber() {
        return mobileNumber;
    }
    public void setmobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getfirstName() {
        return firstName;
    }
    public void setfirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getlastName() {
        return lastName;
    }
    public void setlastName(String lastName) {
        this.lastName = lastName;
    }

    public String gettxnDate() {
        return txnDate;
    }
    public void settxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getMoneyRequest() {
        return MoneyRequest;
    }

    public void setMoneyRequest(String MoneyRequest) {
        this.MoneyRequest = MoneyRequest;
    }

}
