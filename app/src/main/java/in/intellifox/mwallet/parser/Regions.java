package in.intellifox.mwallet.parser;

import java.util.ArrayList;

/**
 * Created by Raju on 08/10/2015.
 */
public class Regions {


    private ArrayList<Region> region;

    public ArrayList<Region> getRegion() {
        return region;
    }

    public void setRegion(ArrayList<Region> region) {
        this.region = region;
    }
}
