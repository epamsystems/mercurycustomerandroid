package in.intellifox.mwallet.parser;


public class MpsXml {


    private Header header;
    private Request request;
    private Response response;
    private ResponseDetails responsedetails;
    private RequestDetails requestdetails;

    public Header getHeader() {
        return header;
    }
    public void setHeader(Header header) {
        this.header = header;
    }
    public Request getRequest() {
        return request;
    }
    public void setRequest(Request request) {
        this.request = request;
    }
    public Response getResponse() {
        return response;
    }
    public void setResponse(Response response) {
        this.response = response;
    }
    public ResponseDetails getResponsedetails() {
        return responsedetails;
    }
    public void setResponsedetails(ResponseDetails responsedetails) {
        this.responsedetails = responsedetails;
    }

    public RequestDetails getRequestDetails() {
        return requestdetails;
    }
    public void setRequestDetails(RequestDetails requestdetails) {
        this.requestdetails = requestdetails;
    }
}
