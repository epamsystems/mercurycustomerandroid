package in.intellifox.mwallet.parser;

/**
 * Created by Raju on 08/10/2015.
 */
public class Region {



    private String parentId;
    private String level;
    private String id;
    private String name;

    private String Region;


    public String getparentId() {
        return parentId;
    }
    public void setparentId(String parentId) {
        this.parentId = parentId;
    }

    public String getname() {
        return name;
    }
    public void setname(String name) {
        this.name = name;
    }
    public String getid() {
        return id;
    }
    public void setid(String id) {
        this.id = id;
    }

    public String getlevel() {
        return level;
    }
    public void setlevel(String level) {
        this.level = level;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }
}
