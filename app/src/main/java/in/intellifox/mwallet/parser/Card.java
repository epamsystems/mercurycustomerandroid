package in.intellifox.mwallet.parser;

/**
 * Created by Raju on 09/10/2015.
 */
public class Card {

    private String number;
    private String expiryDate;
    private String type;

    private String bankName;


    private Transactions transaction;

//    private OtherDetails OtherDetails;

    public Transactions getTransactions() {
        return transaction;
    }

    public void setTransactions(Transactions transaction) {
        this.transaction = transaction;
    }



    public String getnumber() {
        return number;
    }
    public void setnumber(String number) {
        this.number = number;
    }
    public String getexpiryDate() {
        return expiryDate;
    }
    public void setexpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getbankName() {
        return bankName;
    }
    public void setbankName(String bankName) {
        this.bankName = bankName;
    }


    public String gettype() {
        return type;
    }
    public void settype(String type) {
        this.type = type;
    }



}
