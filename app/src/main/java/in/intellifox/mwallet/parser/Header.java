package in.intellifox.mwallet.parser;

/**
 * Created by Raju on 05/10/2015.
 */
public class Header {

    private String ChanelId;
    private String Timestamp;
    private String SessionId;
    private String ServiceProvider;

    public String getChanelId() {
        return ChanelId;
    }
    public void setChanelId(String chanelId) {
        ChanelId = chanelId;
    }
    public String getTimestamp() {
        return Timestamp;
    }
    public void setTimestamp(String timestamp) {
        Timestamp = timestamp;
    }
    public String getSessionId() {
        return SessionId;
    }
    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }
    public String getServiceProvider() {
        return ServiceProvider;
    }
    public void setServiceProvider(String serviceProvider) {
        ServiceProvider = serviceProvider;
    }
}
