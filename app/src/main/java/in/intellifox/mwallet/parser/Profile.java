package in.intellifox.mwallet.parser;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by Raju on 08/10/2015.
 */
public class Profile implements Serializable{

    private String LastName;

    private String Pincode;

    private String CustomerId;

    private String CustomerType;

    private String Address;

    private String Gender;

    private String FirstName;

    private String EmailId;

    private String AlternateAddress;

    private String MobileNumber;

    private String City;

    private String birthDate;
    private String state;
    private Bitmap profile_bitmap;


    public Bitmap getProfile_bitmap() {
        return profile_bitmap;
    }

    public void setProfile_bitmap(Bitmap profile_bitmap) {
        this.profile_bitmap = profile_bitmap;
    }



    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getCity(){return City;}

    public byte[] profileImage = null;

    public byte[] getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(byte[] profileImage) {
        this.profileImage = profileImage;
    }

    public void setCity(String City){
        this.City = City;
    }




    public String getLastName ()
    {
        return LastName;
    }

    public void setLastName (String LastName)
    {
        this.LastName = LastName;
    }

    public String getPincode ()
    {
        return Pincode;
    }

    public void setPincode (String Pincode)
    {
        this.Pincode = Pincode;
    }



    public String getCustomerId()
    {
        return CustomerId;
    }

    public void setCustomerId (String CustomerId)
    {
        this.CustomerId = CustomerId;
    }

    public String getCustomerType ()
    {
        return CustomerType;
    }

    public void setCustomerType (String CustomerType)
    {
        this.CustomerType = CustomerType;
    }

    public String getAddress ()
    {
        return Address;
    }

    public void setAddress (String Address)
    {
        this.Address = Address;
    }

    public String getFirstName ()
    {
        return FirstName;
    }

    public void setFirstName (String FirstName)
    {
        this.FirstName = FirstName;
    }


    public String getEmailId ()
    {
        return EmailId;
    }

    public void setEmailId (String EmailId)
    {
        this.EmailId = EmailId;
    }



    public String getAlternateAddress ()
    {
        return AlternateAddress;
    }

    public void setAlternateAddress (String AlternateAddress)
    {
        this.AlternateAddress = AlternateAddress;
    }

    public String getMobileNumber ()
    {
        return MobileNumber;
    }

    public void setMobileNumber (String MobileNumber)
    {
        this.MobileNumber = MobileNumber;
    }

    @Override
    public String toString()
    {
        return "[ LastName = "+LastName+", Pincode = "+Pincode+", CustomerId = "+CustomerId+", CustomerType = "+CustomerType+", Address = "+Address+", FirstName = "+FirstName+",City = "+City+", EmailId = "+EmailId+", AlternateAddress = "+AlternateAddress+", MobileNumber = "+MobileNumber+"]";
    }

}
