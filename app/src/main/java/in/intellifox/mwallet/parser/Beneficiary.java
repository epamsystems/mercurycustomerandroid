package in.intellifox.mwallet.parser;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Raju on 08/10/2015.
 */
public class Beneficiary implements Serializable{

    private String beneficiaryid;

    private String nickName;

    private String type;

    private String value;

    private String pan;


    public String getBeneficiaryid() {
        return beneficiaryid;
    }

    public void setBeneficiaryid(String beneficiaryid) {
        this.beneficiaryid = beneficiaryid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }
}
