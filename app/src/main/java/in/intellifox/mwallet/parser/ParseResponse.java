package in.intellifox.mwallet.parser;

import android.util.Log;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ParseResponse {

    public MpsXml parseXML(InputSource xml) {
        SAXBuilder builder = new SAXBuilder();
        MpsXml mpsxml = new MpsXml();
        try {
            Log.d("TAG", "xml 131=" + xml);
            Document document = (Document) builder.build(xml);
            Log.d("TAG", "document A131=" + document);
            Element rootNode = document.getRootElement();
            List listHeader = rootNode.getChildren("Header");
            Element nodeHeader = (Element) listHeader.get(0);
            Header head = new Header();
            head.setChanelId(nodeHeader.getChildText("ChannelId"));
            head.setTimestamp(nodeHeader.getChildText("Timestamp"));
            head.setSessionId(nodeHeader.getChildText("SessionId"));
            head.setServiceProvider(nodeHeader.getChildText("ServiceProvider"));
            mpsxml.setHeader(head);

            List listRequest = rootNode.getChildren("Request");

            Element nodeRequest = (Element) listRequest.get(0);

            Request request = new Request();
            request.setRequestType(nodeRequest.getChildText("RequestType"));
            request.setUserType(nodeRequest.getChildText("UserType"));
            mpsxml.setRequest(request);

            List listResponse = rootNode.getChildren("Response");

            Element nodeResponse = (Element) listResponse.get(0);
            Response response = new Response();
            response.setResponseType(nodeResponse.getChildText("ResponseType"));
            mpsxml.setResponse(response);

            List listResponseDetails = rootNode.getChildren("ResponseDetails");
            Element nodeResponseDetails = (Element) listResponseDetails.get(0);
            ResponseDetails responsedetails = new ResponseDetails();

            responsedetails.setSecurityKey(nodeResponseDetails.getChildText("SecurityKey"));
            responsedetails.setChangeMPin(nodeResponseDetails.getChildText("ChangeMpin"));
            responsedetails.setChangeTPin(nodeResponseDetails.getChildText("ChangeTpin"));
            responsedetails.setFirstName(nodeResponseDetails.getChildText("FirstName"));
            responsedetails.setLastName(nodeResponseDetails.getChildText("LastName"));
            responsedetails.setChangeRequestID(nodeResponseDetails.getChildText("RequestID"));
            responsedetails.setLastLogin(nodeResponseDetails.getChildText("LastLogin"));
            responsedetails.setLastTransaction(nodeResponseDetails.getChildText("LastTransaction"));
            responsedetails.setTransactionDate(nodeResponseDetails.getChildText("TxnDate"));
            responsedetails.setBankAccountLinked(nodeResponseDetails.getChildText("BankAccountLinked"));
            responsedetails.setErrorCode(nodeResponseDetails.getChildText("ErrorCode"));
            responsedetails.setReason(nodeResponseDetails.getChildText("Reason"));
            responsedetails.setMessage(nodeResponseDetails.getChildText("Message"));
            responsedetails.setBalance(nodeResponseDetails.getChildText("Balance"));
            responsedetails.setTxnId(nodeResponseDetails.getChildText("TxnId"));
            responsedetails.setAmount(nodeResponseDetails.getChildText("Amount"));
            responsedetails.setAgentId(nodeResponseDetails.getChildText("AgentId"));
            responsedetails.setCommission(nodeResponseDetails.getChildText("Commission"));
            responsedetails.setFee(nodeResponseDetails.getChildText("Fee"));
            responsedetails.setOTP(nodeResponseDetails.getChildText("OTP"));
            responsedetails.setAppUpdateFlag(nodeResponseDetails.getChildText("AppUpdateFlag"));
            responsedetails.setOneClilckFlag(nodeResponseDetails.getChildText("OneClickFlag"));
            responsedetails.setOneClilckAmount(nodeResponseDetails.getChildText("OneClickAmount"));
            responsedetails.setCashbackAmount(nodeResponseDetails.getChildText("CashBackValue"));

            List listprofile = nodeResponseDetails.getChildren("Profile");
            try {
                Element nodeprofiledetails = (Element) listprofile.get(0);

                Profile profiledetails = new Profile();
                profiledetails.setFirstName(nodeprofiledetails.getChildText("FirstName"));
                profiledetails.setLastName(nodeprofiledetails.getChildText("LastName"));
                profiledetails.setCustomerId(nodeprofiledetails.getChildText("CustomerId"));
                profiledetails.setCustomerType(nodeprofiledetails.getChildText("CustomerType"));
                profiledetails.setAddress(nodeprofiledetails.getChildText("Address"));
                profiledetails.setAlternateAddress(nodeprofiledetails.getChildText("AlternateAddress"));
                profiledetails.setPincode(nodeprofiledetails.getChildText("Pincode"));
                profiledetails.setCity(nodeprofiledetails.getChildText("City"));
                profiledetails.setEmailId(nodeprofiledetails.getChildText("EmailId"));
                profiledetails.setMobileNumber(nodeprofiledetails.getChildText("MobileNumber"));

                // listprofile.add(profiledetails);
                responsedetails.setProfile(profiledetails);
            } catch (Exception e) {
                e.printStackTrace();
            }
            List listWall = nodeResponseDetails.getChildren("Wallets");
            try {
                Element nodeWall = (Element) listWall.get(0);
                List listWallet = nodeWall.getChildren("Wallet");
                ArrayList<WalletId> WalletList = new ArrayList<WalletId>();
                for (int i = 0; i < listWallet.size(); i++) {

                    Element nodeWallet = (Element) listWallet.get(i);
                    WalletId wallet = new WalletId();
                    wallet.setAccountType(nodeWallet.getAttributeValue("AccountType"));
                    wallet.setIsPrimary(nodeWallet.getAttributeValue("isPrimary"));
                    wallet.setWalletId(nodeWallet.getAttributeValue("walletId"));
                    wallet.setBalance(nodeWallet.getAttributeValue("balance"));
                    WalletList.add(wallet);
                }
                Wallet wal = new Wallet();
                wal.setWalletId(WalletList);
                responsedetails.setWallet(wal);
            } catch (Exception e) {
                e.printStackTrace();
            }

            List listBeneficiary = nodeResponseDetails.getChildren("Beneficiaries");
            try {
                Element nodeBeneficiarys = (Element) listBeneficiary.get(0);
                List listBeneficiarys = nodeBeneficiarys.getChildren("Beneficiary");
                ArrayList<Beneficiary> beneficiaryList = new ArrayList<Beneficiary>();
                for (int i = 0; i < listBeneficiarys.size(); i++) {
                    Element nodeBeneficiary = (Element) listBeneficiarys.get(i);
                    Beneficiary beneficiary = new Beneficiary();
                    beneficiary.setBeneficiaryid(nodeBeneficiary.getAttributeValue("id"));
                    beneficiary.setNickName(nodeBeneficiary.getAttributeValue("nickname"));
                    beneficiary.setType(nodeBeneficiary.getAttributeValue("type"));
                    beneficiary.setValue(nodeBeneficiary.getAttributeValue("value"));
                    beneficiary.setPan(nodeBeneficiary.getAttributeValue("PAN"));
                    beneficiaryList.add(beneficiary);
                }
                Beneficiarys beneficiarys = new Beneficiarys();
                beneficiarys.setBeneficiary(beneficiaryList);
                responsedetails.setBeneficiarys(beneficiarys);
            } catch (Exception e) {
                e.printStackTrace();
            }
            List transactionlist = nodeResponseDetails.getChildren("Transactions");
            Transactions transactions = new Transactions();
            try {
                Element nodetransactiondetails = (Element) transactionlist.get(0);
                List listTransactionDetails = nodetransactiondetails.getChildren("Transaction");

                ArrayList<Transaction> transactionList = new ArrayList<Transaction>();
                for (int i = 0; i < listTransactionDetails.size(); i++) {
                    Element nodeTransaction = (Element) listTransactionDetails.get(i);
                    Transaction transaction = new Transaction();

                    transaction.setId(nodeTransaction.getAttributeValue("id"));
                    transaction.setTxnType(nodeTransaction.getAttributeValue("txnType"));
                    transaction.setAmount(nodeTransaction.getAttributeValue("amount"));
                    transaction.setType(nodeTransaction.getAttributeValue("type"));
                    transaction.setTxnDate(nodeTransaction.getAttributeValue("txnDate"));
                    transaction.setRemark(nodeTransaction.getAttributeValue("remark"));

                    List otherDetails = nodeTransaction.getChildren("OtherDetails");

                    try {
                        OtherDetails otherDetails1 = new OtherDetails();
                        Element otherDetail = (Element) otherDetails.get(0);
                        try {
                            List customersList = otherDetail.getChildren("Customer");
                            ArrayList<Customer> customerdetails = new ArrayList<Customer>();
                            for (int j = 0; j < customersList.size(); j++) {
                                Element customer = (Element) customersList.get(j);

                                Customer cus = new Customer();
                                cus.setFirstName(customer.getChildText("FirstName"));
                                cus.setLastName(customer.getChildText("LastName"));
                                cus.setMobileNumber(customer.getChildText("MobileNumber"));
                                cus.setCardNumber(customer.getChildText("CardNumber"));

                                // Log.v("TAG",firstName+""+lastName+""+mobileNumber);
                                customerdetails.add(cus);
                            }
                            otherDetails1.setCustomer(customerdetails);
                        } catch (Exception e) {
                        }
                        try {
                            List bankList = otherDetail.getChildren("Bank");
                            ArrayList<Bank> bankDetails = new ArrayList<>();
                            for (int j = 0; j < bankList.size(); j++) {
                                Element bank = (Element) bankList.get(j);
                                Bank bank1 = new Bank();
                                bank1.setBankName(bank.getChildText("BankName"));
                                bank1.setAccountNumber(bank.getChildText("AccountNumber"));

                                bankDetails.add(bank1);
                            }
                            otherDetails1.setBank(bankDetails);
                        } catch (Exception e) {
                        }

                        try {
                            List merchantList = otherDetail.getChildren("Merchant");
                            ArrayList<Merchant> arrayMerchant = new ArrayList<>();
                            for (int j = 0; j < merchantList.size(); j++) {
                                Element emerchante = (Element) merchantList.get(j);
                                Merchant merchant = new Merchant();
                                merchant.setFirstName(emerchante.getChildText("FirstName"));
                                merchant.setLastName(emerchante.getChildText("LastName"));
                                merchant.setMobileNumber(emerchante.getChildText("MobileNumber"));

                                arrayMerchant.add(merchant);
                            }
                            otherDetails1.setMerchant(arrayMerchant);
                        } catch (Exception e) {
                        }

                        try {
                            List agentList = otherDetail.getChildren("Agent");
                            ArrayList<Agent> arrayAgent = new ArrayList<>();
                            for (int j = 0; j < agentList.size(); j++) {
                                Element emerchante = (Element) agentList.get(j);
                                Agent agent = new Agent();
                                agent.setFirstName(emerchante.getChildText("FirstName"));
                                agent.setLastName(emerchante.getChildText("LastName"));
                                agent.setMobileNumber(emerchante.getChildText("MobileNumber"));


                                arrayAgent.add(agent);
                            }
                            otherDetails1.setAgents(arrayAgent);
                        } catch (Exception e) {
                            Log.v("TAG", " Exception in Agent");
                            e.printStackTrace();
                        }
                        try {
                            List refundsList = otherDetail.getChildren("Refunds");
                            ArrayList<Refunds> refunds = new ArrayList<>();
                            for (int j = 0; j < refundsList.size(); j++) {
                                Element refundsElement = (Element) refundsList.get(j);
                                Refunds refunds1 = new Refunds();
                                List refundList = refundsElement.getChildren("Refund");
                                ArrayList<Refund> refund = new ArrayList<>();
                                for (int k = 0; k < refundList.size(); k++) {
                                    Element refundElement = (Element) refundList.get(k);
                                    Refund refundObject = new Refund();
                                    refundObject.setAmount(refundElement.getAttributeValue("amount"));
                                    refundObject.setId(refundElement.getAttributeValue("id"));
                                    refundObject.setTxnDate(refundElement.getAttributeValue("txnDate"));
                                    refundObject.setTxnType(refundElement.getAttributeValue("txnType"));
                                    refund.add(refundObject);
                                }
                                refunds1.setRefunds(refund);
                                refunds.add(refunds1);
                            }
                            otherDetails1.setRefunds(refunds);
                        } catch (Exception e) {
                        }
                        transaction.setOtherDetails(otherDetails1);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    transactionList.add(transaction);
                }

                transactions.setTransaction(transactionList);

                responsedetails.setTransactions(transactions);

            } catch (Exception e) {
                e.printStackTrace();
            }


//            List listCards = nodeResponseDetails.getChildren("Cards");
//            try {
//                Element nodecards = (Element) listCards.get(0);
//                List listCard = nodecards.getChildren("Card");
//
//                ArrayList<Card> listCardarray = new ArrayList<Card>();
//                for (int i = 0; i < listCard.size(); i++) {
//                    Element nodeCard = (Element) listCard.get(i);
//                    Card card = new Card();
//                    card.setexpiryDate(nodeCard.getAttributeValue("expiryDate"));
//                    card.setnumber(nodeCard.getAttributeValue("number"));
//                    card.setbankName(nodeCard.getAttributeValue("bankName"));
//                    card.settype(nodeCard.getAttributeValue("type"));
//
//                    List transactionlistcards = nodeCard.getChildren("Transactions");
//                    Transactions transactions1 = new Transactions();
//                    try {
//                        Element nodetransactiondetails = (Element) transactionlistcards.get(0);
//                        List listTransactionDetails = nodetransactiondetails.getChildren("Transaction");
//
//                        ArrayList<Transaction> transactionList = new ArrayList<Transaction>();
//
//                        for (int j = 0; j < listTransactionDetails.size(); j++) {
//
//                            Element nodeTransaction = (Element) listTransactionDetails.get(i);
//                            Transaction transaction = new Transaction();
//
//                            transaction.setId(nodeTransaction.getAttributeValue("id"));
//                            transaction.setTxnType(nodeTransaction.getAttributeValue("txnType"));
//                            transaction.setAmount(nodeTransaction.getAttributeValue("amount"));
//                            transaction.setType(nodeTransaction.getAttributeValue("type"));
//                            transaction.setTxnDate(nodeTransaction.getAttributeValue("txnDate"));
//                            transaction.setRemark(nodeTransaction.getAttributeValue("remark"));
//
//                            List otherDetails = nodeTransaction.getChildren("OtherDetails");
//
//                            try {
//                                OtherDetails otherDetails1 = new OtherDetails();
//                                Element otherDetail = (Element) otherDetails.get(0);
//                                List customersList = otherDetail.getChildren("Customer");
//                                ArrayList<Customer> customerdetails = new ArrayList<Customer>();
//                                for (int k = 0; k < customersList.size(); k++) {
//                                    Element customer = (Element) customersList.get(k);
//
//                                    Customer cus = new Customer();
//                                    cus.setFirstName(customer.getChildText("FirstName"));
//                                    cus.setLastName(customer.getChildText("LastName"));
//                                    cus.setMobileNumber(customer.getChildText("MobileNumber"));
//
//
//                                    // Log.v("TAG",firstName+""+lastName+""+mobileNumber);
//                                    customerdetails.add(cus);
//                                }
//                                otherDetails1.setCustomer(customerdetails);
//
//                                List bankList = otherDetail.getChildren("Bank");
//                                ArrayList<Bank> bankDetails = new ArrayList<>();
//                                for (int l = 0; l < bankList.size(); l++) {
//                                    Element bank = (Element) bankList.get(l);
//                                    Bank bank1 = new Bank();
//                                    bank1.setBankName(bank.getChildText("BankName"));
//                                    bank1.setAccountNumber(bank.getChildText("AccountNumber"));
//
//                                    bankDetails.add(bank1);
//                                }
//                                otherDetails1.setBank(bankDetails);
//
//
//                                List merchantList = otherDetail.getChildren("Merchant");
//                                ArrayList<Merchant> arrayMerchant = new ArrayList<>();
//                                for (int n = 0; n < merchantList.size(); n++) {
//                                    Element emerchante = (Element) merchantList.get(n);
//                                    Merchant merchant = new Merchant();
//                                    merchant.setFirstName(emerchante.getChildText("FirstName"));
//                                    merchant.setLastName(emerchante.getChildText("LastName"));
//                                    merchant.setMobileNumber(emerchante.getChildText("MobileNumber"));
//
//                                    arrayMerchant.add(merchant);
//                                }
//                                otherDetails1.setMerchant(arrayMerchant);
//
//                                List refundsList = otherDetail.getChildren("Refunds");
//                                ArrayList<Refunds> refunds = new ArrayList<>();
//                                for (int m = 0; m < refundsList.size(); m++) {
//                                    Element refundsElement = (Element) refundsList.get(m);
//                                    Refunds refunds1 = new Refunds();
//                                    List refundList = refundsElement.getChildren("Refund");
//                                    ArrayList<Refund> refund = new ArrayList<>();
//                                    for (int k = 0; k < refundList.size(); k++) {
//                                        Element refundElement = (Element) refundList.get(k);
//                                        Refund refundObject = new Refund();
//                                        refundObject.setAmount(refundElement.getAttributeValue("amount"));
//                                        refundObject.setId(refundElement.getAttributeValue("id"));
//                                        refundObject.setTxnDate(refundElement.getAttributeValue("txnDate"));
//                                        refundObject.setTxnType(refundElement.getAttributeValue("txnType"));
//                                        refund.add(refundObject);
//                                    }
//                                    refunds1.setRefunds(refund);
//                                    refunds.add(refunds1);
//                                }
//                                otherDetails1.setRefunds(refunds);
//                                transaction.setOtherDetails(otherDetails1);
//
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                            transactionList.add(transaction);
//                        }
//                        transactions1.setTransaction(transactionList);
//                        card.setTransactions(transactions1);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    listCardarray.add(card);
//                }
//                Cards cards = new Cards();
//                cards.setCard(listCardarray);
//                responsedetails.setCards(cards);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            List listCards = nodeResponseDetails.getChildren("Cards");
            try {
                Element nodecards = (Element) listCards.get(0);
                List listCard = nodecards.getChildren("Card");

                ArrayList<Card1> listCardarray = new ArrayList<Card1>();
                for (int i = 0; i < listCard.size(); i++) {
                    Element nodeCard = (Element) listCard.get(i);
                    Card1 card = new Card1();
                    card.setToken(nodeCard.getAttributeValue("token"));
                    card.setNumber(nodeCard.getAttributeValue("number"));
                    card.setScheme(nodeCard.getAttributeValue("scheme"));
                    card.setType(nodeCard.getAttributeValue("type"));
                    card.setCardHolderName(nodeCard.getAttributeValue("cardHolderName"));
                    card.setIsPrimary(nodeCard.getAttributeValue("isPrimary"));
                    card.setNickName(nodeCard.getAttributeValue("nickName"));
                    listCardarray.add(card);
                }
                Cards cards = new Cards();
                cards.setCard(listCardarray);
                responsedetails.setCards(cards);
            } catch (Exception e) {
                e.printStackTrace();
            }
            List listFavourites = nodeResponseDetails.getChildren("Favourites");
            try {
                Element nodeFavourites = (Element) listFavourites.get(0);
                List listFavourite = nodeFavourites.getChildren("Favourite");

                ArrayList<Favourite> listFavouritearray = new ArrayList<Favourite>();
                for (int i = 0; i < listFavourite.size(); i++) {
                    Element nodeFavourite = (Element) listFavourite.get(i);
                    Favourite favourite = new Favourite();
                    favourite.setAmount(nodeFavourite.getAttributeValue("Amount"));
                    favourite.setLastTransactionDate(nodeFavourite.getAttributeValue("LastTransactionDate"));
                    favourite.setMobileNumber(nodeFavourite.getAttributeValue("MobileNumber"));
                    favourite.setFirstName(nodeFavourite.getAttributeValue("FirstName"));
                    favourite.setLastName(nodeFavourite.getAttributeValue("LastName"));
                    favourite.setEmailId(nodeFavourite.getAttributeValue("EmailId"));

                    listFavouritearray.add(favourite);
                }
                Favourites favourites = new Favourites();
                favourites.setFavourite(listFavouritearray);
                responsedetails.setFavourites(favourites);
            } catch (Exception e) {
                e.printStackTrace();
            }

            List listMoneyRequests = nodeResponseDetails.getChildren("MoneyRequests");
            try {
                Element nodemoneyReq = (Element) listMoneyRequests.get(0);
                List listMoneyRequest = nodemoneyReq.getChildren("MoneyRequest");

                ArrayList<MoneyRequest> listMoneyRequestList = new ArrayList<MoneyRequest>();
                for (int i = 0; i < listMoneyRequest.size(); i++) {
                    Element nodeRequests = (Element) listMoneyRequest.get(i);
                    MoneyRequest Request = new MoneyRequest();
                    Request.setid(nodeRequests.getAttributeValue("id"));
                    Request.setamount(nodeRequests.getAttributeValue("amount"));
                    Request.setmobileNumber(nodeRequests.getAttributeValue("mobileNumber"));
                    Request.setfirstName(nodeRequests.getAttributeValue("firstName"));
                    Request.setlastName(nodeRequests.getAttributeValue("lastName"));
                    Request.settxnDate(nodeRequests.getAttributeValue("txnDate"));
                    Request.setMoneyRequest(nodeRequests.getValue());
                    listMoneyRequestList.add(Request);
                }
                MoneyRequests wal = new MoneyRequests();
                wal.setMoneyRequest(listMoneyRequestList);
                responsedetails.setMoneyRequests(wal);
            } catch (Exception e) {
                e.printStackTrace();
            }

            List listDocuments = nodeResponseDetails.getChildren("Documents");
            try {
                Element nodeDocuments = (Element) listDocuments.get(0);
                List listDocument = nodeDocuments.getChildren("Document");

                ArrayList<in.intellifox.mwallet.parser.Document> DocumentList = new ArrayList<in.intellifox.mwallet.parser.Document>();
                for (int i = 0; i < listDocument.size(); i++) {
                    Element nodeDocument = (Element) listDocument.get(i);
                    in.intellifox.mwallet.parser.Document documentres = new in.intellifox.mwallet.parser.Document();
                    documentres.setid(nodeDocument.getAttributeValue("id"));
                    documentres.setname(nodeDocument.getAttributeValue("name"));
                    documentres.setDocument(nodeDocument.getValue());
                    DocumentList.add(documentres);
                }
                Documents documents = new Documents();
                documents.setDocument(DocumentList);
                responsedetails.setDocuments(documents);
            } catch (Exception e) {
                e.printStackTrace();
            }

            List listRegions = nodeResponseDetails.getChildren("Regions");
            try {
                Element nodeRegions = (Element) listRegions.get(0);
                List listRegion = nodeRegions.getChildren("Region");

                ArrayList<Region> Regionlist = new ArrayList<Region>();
                for (int i = 0; i < listRegion.size(); i++) {
                    Element nodeRegion = (Element) listRegion.get(i);
                    Region region = new Region();
                    region.setid(nodeRegion.getAttributeValue("id"));
                    region.setlevel(nodeRegion.getAttributeValue("level"));
                    region.setparentId(nodeRegion.getAttributeValue("parentId"));
                    region.setname(nodeRegion.getAttributeValue("name"));

                    region.setRegion(nodeRegion.getValue());
                    Regionlist.add(region);
                }
                Regions regions = new Regions();
                regions.setRegion(Regionlist);
                responsedetails.setRegions(regions);
            } catch (Exception e) {
                e.printStackTrace();
            }

            List bankdetaillist = nodeResponseDetails.getChildren("Banks");
            try {
                Element nodebankdetails = (Element) bankdetaillist.get(0);

                List listBankDetail = nodebankdetails.getChildren("Bank");
                ArrayList<Bank> bankList = new ArrayList<Bank>();
                for (int i = 0; i < listBankDetail.size(); i++) {

                    Element nodeBank = (Element) listBankDetail.get(i);
                    Bank bank = new Bank();
                    bank.setAccountNumber(nodeBank.getAttributeValue("accountNumber"));
                    bank.setBranchName(nodeBank.getAttributeValue("branchName"));
                    bank.setBankName(nodeBank.getAttributeValue("bankName"));
                    bank.setBranchId(nodeBank.getAttributeValue("branchId"));
                    bank.setBranchAddress(nodeBank.getAttributeValue("branchAddress"));
                    bank.setIdentificationCode(nodeBank.getAttributeValue("identificationCode"));
                    bank.setIsPrimary(nodeBank.getAttributeValue("isPrimary"));
                    bankList.add(bank);

                }
                Banks banks = new Banks();
                banks.setBank(bankList);
                responsedetails.setBanks(banks);

            } catch (Exception e) {
                e.printStackTrace();
            }
            mpsxml.setResponsedetails(responsedetails);
        } catch (IOException io) {
            System.out.println(io.getMessage());
        } catch (JDOMException jdomex) {
            System.out.println(jdomex.getMessage());
        }
        return mpsxml;
    }
}