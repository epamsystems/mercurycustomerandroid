package in.intellifox.mwallet.parser;

import java.io.Serializable;

/**
 * Created by Raju on 08/10/2015.
 */
public class Document implements Serializable{

    private String Name;
    private String Id;
    private String Type;
    private String id;
    private String name;
    public byte[] documentFile = null;
    private String Document;

    public byte[] getDocumentFile() {
        return documentFile;
    }

    public void setDocumentFile(byte[] documentFile) {
        this.documentFile = documentFile;
    }

    public String getName() {
        return Name;
    }
    public void setName(String Name) {
        this.Name = Name;
    }
    public String getId() {
        return Id;
    }
    public void setId(String Id) {
        this.Id = Id;
    }

    public String getname() {
        return name;
    }
    public void setname(String name) {
        this.name = name;
    }
    public String getid() {
        return id;
    }
    public void setid(String id) {
        this.id = id;
    }

    public String getType() {
        return Type;
    }
    public void setType(String Type) {
        this.Type = Type;
    }

    public String getDocument() {
        return Document;
    }

    public void setDocument(String Document) {
        this.Document = Document;
    }
}
