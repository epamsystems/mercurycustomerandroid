package in.intellifox.mwallet.parser;

import android.util.Log;

/**
 * Created by Raju on 09/10/2015.
 */
public class Transaction {

    private String id;

    private String txnType;
    private String type;

    private String txnDate;


    private String amount;

    private String remark;
    private OtherDetails otherDetails;

//    private OtherDetails OtherDetails;

    public OtherDetails getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(OtherDetails otherDetails) {
        Log.v("TAG"," Setting other details");
        this.otherDetails = otherDetails;
    }







    public String getType(){
        return type;
    }

    public void setType(String type){
        this.type = type;
    }


    public String getTxnDate(){
        return txnDate;
    }
    public void setTxnDate(String txnDate){
        this.txnDate = txnDate;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }



}
