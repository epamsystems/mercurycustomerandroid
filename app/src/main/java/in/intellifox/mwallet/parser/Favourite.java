package in.intellifox.mwallet.parser;

/**
 * Created by Raju on 09/10/2015.
 */
public class Favourite {

    private String Amount;
    private String LastTransactionDate;
    private String MobileNumber;
    private String FirstName;
    private String LastName;
    private String EmailId;



    public String getamount() {
        return Amount;
    }
    public void setAmount(String Amount) {
        this.Amount = Amount;
    }
    public String getLastTransactionDate() {
        return LastTransactionDate;
    }
    public void setLastTransactionDate(String LastTransactionDate) {
        this.LastTransactionDate = LastTransactionDate;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }
    public void setMobileNumber(String MobileNumber) {
        this.MobileNumber = MobileNumber;
    }

    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }
    public String getLastName() {
        return LastName;
    }
    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getEmailId() {
        return EmailId;
    }
    public void setEmailId(String EmailId) {
        this.EmailId = EmailId;
    }



}
