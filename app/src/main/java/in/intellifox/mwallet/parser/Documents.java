package in.intellifox.mwallet.parser;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Raju on 08/10/2015.
 */
public class Documents implements Serializable {

    private ArrayList<Document> document;

    public ArrayList<Document> getDocument() {
        return document;
    }

    public void setDocument(ArrayList<Document> document) {
        this.document = document;
    }
}
