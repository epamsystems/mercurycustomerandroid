package in.intellifox.mwallet.services;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.Map;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.apicalls.RetrofitTask;
import in.intellifox.mwallet.security.AESecurityNew;
import in.intellifox.mwallet.utilities.AppSettings;
import in.intellifox.mwallet.utilities.GenericResponseHandler;
import in.intellifox.mwallet.utilities.LogUtils;
import in.intellifox.mwallet.utilities.Utils;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onCreate() {
        super.onCreate();

        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        LogUtils.Verbose(TAG, "refreshedToken" + refreshedToken);

        if (refreshedToken != null) {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    sendRegistrationToServer(refreshedToken);
                }
            };

            new Thread(r).start();
        }

    }
//    @Override
//    public void onTokenRefresh() {
//        // Get updated InstanceID token.
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        LogUtils.Verbose(TAG, "Refreshed token: " + refreshedToken);
//
//       // Utils.showToast(getApplicationContext(),"refreshedToken"+refreshedToken);
//        // If you want to send messages to this application instance or
//        // manage this apps subscriptions on the server side, send the
//        // Instance ID token to your app server.
//        Runnable r=new Runnable() {
//            @Override
//            public void run() {
//                sendRegistrationToServer(refreshedToken);
//            }
//        };
//
//        new Thread(r).start();
//    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.

        String xmlParam = "<DeviceToken>" + token + "</DeviceToken>\n" +
                "\t\t<DeviceOS>AND</DeviceOS>\t" +
                "\t\t<MobileNumber>" + MyApplication.getInstance().getmProfile_Mobile_Number() + "</MobileNumber>\t";
        xmlParam = Utils.getReqXML("SaveUpdateDeviceToken", xmlParam);

        LogUtils.Verbose(TAG, "sendRegistrationToServer" + xmlParam);

        //Utils.showToast(getApplicationContext(),"sendRegistrationToServer"+token);

        try {
            xmlParam = AESecurityNew.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.showToast(getApplicationContext(), e.toString());
            return;
        }


        RetrofitTask retrofitTask = RetrofitTask.getInstance();

        retrofitTask.executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {


                if (!isSuccess) {
                    // Constants.showToast(RegistrationIntentService.this, response);

                    return;
                }

                try {
                    String decrypted = AESecurityNew.getInstance().decryptString(response);
                    LogUtils.Verbose("TAG", " Decrypted " + decrypted);

                    //Utils.showToast(getApplicationContext()," Decrypted Response " + decrypted);

                    Map<String, String> responseMap = GenericResponseHandler.handleResponse(decrypted);

                    if (responseMap.get("ResponseType").equals("Success")) {

                        //   Constants.showToast(RegistrationIntentService.this, responseMap.get("Message"));
                        //Utils.showToast(getApplicationContext(),"success"+responseMap.get("Message"));
                        AppSettings.putData(MyFirebaseInstanceIDService.this, AppSettings.isTOKENSENDTOSERVER, "true");

                    }
                } catch (Exception e) {
                    LogUtils.Exception(e);
                    //  Constants.showToast(RegistrationIntentService.this, e.toString());
                }


            }
        });

    }
}