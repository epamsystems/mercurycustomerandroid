package in.intellifox.mwallet.services;

/**
 * Created by mayurn on 15/3/17.
 */

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.models.MercuryNotification;
import in.intellifox.mwallet.utilities.AppSettings;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.utilities.LogUtils;
import in.intellifox.mwallet.views.activities.DrawerMain;
import in.intellifox.mwallet.views.activities.NotificationActivity;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        LogUtils.Verbose(TAG, "From: " + remoteMessage.getFrom() + " BODY "+remoteMessage.getData().get("body"));




        MercuryNotification mercuryNotification = new MercuryNotification();
        mercuryNotification.setMessage(remoteMessage.getData().get("body"));

        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd", Locale.ENGLISH);
        SimpleDateFormat sdf3 = new SimpleDateFormat("MMM", Locale.ENGLISH);
        SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        SimpleDateFormat sdf5 = new SimpleDateFormat("HH:mm ", Locale.ENGLISH);

        Date d1 = null;
        try {
            LogUtils.Verbose("TAG", " Notification time " + remoteMessage.getData().get("timestamp"));
            d1 = sdf1.parse(remoteMessage.getData().get("timestamp"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mercuryNotification.setTimestamp(sdf2.format(d1) + " " + sdf3.format(d1) + "\n" + sdf4.format(d1));
        mercuryNotification.setTime(sdf5.format(d1) + "HRS");
        saveMessage(mercuryNotification);




    }

    private void saveMessage(MercuryNotification mercuryNotification) {
        Gson gson = new Gson();
        String json = AppSettings.getData(this, AppSettings.NOTIFICATIONS);
        Type type = new TypeToken<ArrayList<MercuryNotification>>() {
        }.getType();
        List<MercuryNotification> mercuryNotificationList = null;

        try {
            mercuryNotificationList = gson.fromJson(json, type);
        } catch (Exception e) {
            mercuryNotificationList = new ArrayList<>();
        }

        if (mercuryNotificationList == null)
            mercuryNotificationList = new ArrayList<>();

        mercuryNotificationList.add(0, mercuryNotification);

        String c = (AppSettings.getData(this, AppSettings.NOTIFICATION_COUNT).equals("")) ? "0" : AppSettings.getData(this, AppSettings.NOTIFICATION_COUNT);
        int count = Integer.parseInt(c);
        count = count + 1;

        AppSettings.putData(this, AppSettings.NOTIFICATION_COUNT, count + "");


        json = gson.toJson(mercuryNotificationList);
        LogUtils.Verbose("TAG", " Notification JSON " + json);
        AppSettings.putData(this, AppSettings.NOTIFICATIONS, json);
        LogUtils.Verbose("TAG", " mercuryNotification.getMessage " + mercuryNotification.getMessage());
        handleDataMessage(mercuryNotification.getMessage());

        Intent intent = new Intent("message");
        intent.putExtra("type", 2);
        sendBroadcast(intent);



    }

    private void handleDataMessage(String messageBody) {

        LogUtils.Verbose(TAG, "push json: " + messageBody);
        Intent intent = null;

        if (!isAppIsInBackground(this)) {
            intent = new Intent(this, MainActivity.class);
        } else {

            intent = new Intent(this, NotificationActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this);


        Notification notification = mBuilder.setSmallIcon(R.mipmap.mwallet_logo).setTicker(GlobalVariables.NOTIFICATION_TITLE).setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle(GlobalVariables.NOTIFICATION_TITLE)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentIntent(pendingIntent)
                .setGroup(GlobalVariables.NOTIFICATION_TITLE)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.mwallet_logo))
                .setContentText(messageBody).build();



//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.mipmap.mwallet_logo)
//                .setContentTitle(GlobalVariables.NOTIFICATION_TITLE)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(((int) System.currentTimeMillis()), notification);

    }





    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = true;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = true;
            }
        }

        return isInBackground;
    }
}
