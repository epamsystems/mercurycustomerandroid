package in.intellifox.mwallet.utilities;

import android.content.Context;
import android.content.res.Resources;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by bruce on 14-11-6.
 */
public class Utils {
    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp) {
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    public static void showToast(Context context, String msg) {
        if (context != null)
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static String getReqXML(String reqType, String reqDetails) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS", Locale.ENGLISH);

        String xml = "<?xml version='1.0' encoding='UTF-8'?>\n" +
                "<MpsXml>\n" +
                "\t<Header>\n" +
                "\t\t<ChannelId>APP</ChannelId>\n" +
                "\t\t<Timestamp>" + sdf.format(calendar.getTime()) + "</Timestamp>\n" +
                "\t\t<SessionId>" + GlobalVariables.sessionId + "</SessionId>\n" +
                "\t\t<ServiceProvider>" + GlobalVariables.SPID + "</ServiceProvider>\n" +
                "\t</Header>\n" +
                "\t<Request>\n" +
                "\t\t<RequestType>" + reqType + "</RequestType>\n" +
                "\t\t<UserType>CU</UserType>\n" +
                "\t\t</Request>\n" +
                "\t<RequestDetails>\n" +
                reqDetails +
                "\n\t\t<ResponseURL>{ResponseURL}</ResponseURL>\n" +
                "\t\t<ResponseVar>{ResponseVar}</ResponseVar>\n" +
                "\t</RequestDetails>\n" +
                "</MpsXml>\n";

        Log.v("XML Req ", xml);
        return xml;
    }

    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }


    public static String getHash(byte[] bytes) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(bytes);
            return bytesToHexString(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String decodeString(String input) {

        if (input == null || input.equalsIgnoreCase("null"))
            input = "N.A.";

        if (input.length() < 4) {
            return input;
        }

        try {


            byte[] data = Base64.decode(input, Base64.DEFAULT);
            String temp = new String(data);
            // LogUtils.Verbose("TAG", "Decode " + temp);
            char[] ch = temp.toCharArray();
            for (Character c : ch) {
                if (String.valueOf(c).matches("[a-zA-Z0-9,.\\-\\s]+"))
                    continue;
                else
                    return input;
            }
            return temp;

        } catch (Exception e) {
            return input;
        }
    }
}
