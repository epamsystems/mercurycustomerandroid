package in.intellifox.mwallet.utilities;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import dialogs.ProgressDialog;
import in.intellifox.mwallet.parser.Beneficiary;
import in.intellifox.mwallet.parser.Card1;

/**
 * Created by Administrator on 2/28/2016.
 */
public class GlobalVariables {
    public static String mProfile_Wallet_Balance = null;
    public static ArrayList<Beneficiary> beneficiaryArrayList = new ArrayList<Beneficiary>();
    public static ArrayList<Card1> cardArrayList = new ArrayList<Card1>();
    public static Activity mActivity = null;
    public static String sessionId = "";

    public static String SPID = "mercury";
    //public static String SPID = "toml";

    public static boolean DEVICE_ID = true;
    public static String USER_TYPE = "CU";

    public static ProgressDialog progressDialog;

    //public static String sessionId = null;

    // public static String url = "http://demo.timesofmoney.com/mps-demo/mobileRequestHandler?";
    // public static String url = "http://10.158.208.171:8081/mps/mobileRequestHandler?";
    public static String url = "http://10.158.208.105:8080/mps/mobileRequestHandler?";//"https://qamovit.timesofmoney.in/mps/mobileRequestHandler?";

    //public static String retrofit_url = "http://demo.timesofmoney.com/mps-demo/";
    //public static String retrofit_url = "http://10.158.208.171:8081/mps/";
    public static String retrofit_url = "http://10.158.208.105:8080/mps/";//"https://qamovit.timesofmoney.in/mps/";


    // public static String customerRegistration_URL = "http://demo.timesofmoney.com/mps-demo/CustomerRegistration";
    public static String customerRegistration_URL = "http://10.158.208.105:8080/mps/CustomerRegistration";//"https://qamovit.timesofmoney.in/mps/CustomerRegistration";



    /*public static String url="http://demo.timesofmoney.com/mps/mobileRequestHandler?";
    public static String retrofit_url="http://demo.timesofmoney.com/mps/";
    public static String customerRegistration_URL = "http://demo.timesofmoney.com/mps/CustomerRegistration";
*/
//    public static String url="http://10.158.208.67:8080/mps/mobileRequestHandler?";
//    public static String retrofit_url="http://10.158.208.67:8080/mps/";
//    public static String customerRegistration_URL = "http://10.158.208.67:8080/mps/CustomerRegistration";
//


    public static String NOTIFICATION_TITLE = "Mercury Customer";
    /*public static String url="http://10.158.208.45:8081/mps/mobileRequestHandler?";
    public static String retrofit_url="http://10.158.208.45:8081/mps/";
    public static String customerRegistration_URL = "http://demo.timesofmoney.com/mps/CustomerRegistration";*/

    public static FragmentManager fragmentManager = null;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }


    public static String getReqXML(String reqType, String reqDetails) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        String xml = "<?xml version='1.0' encoding='UTF-8'?>\n" +
                "<MpsXml>\n" +
                "\t<Header>\n" +
                "\t\t<ChannelId>APP</ChannelId>\n" +
                "\t\t<Timestamp>" + sdf.format(calendar.getTime()) + "</Timestamp>\n" +
                "\t\t<SessionId>" + sessionId + "</SessionId>\n" +
                "\t\t<ServiceProvider>" + SPID + "</ServiceProvider>\n" +
                "\t</Header>\n" +
                "\t<Request>\n" +
                "\t\t<RequestType>" + reqType + "</RequestType>\n" +
                "\t\t<UserType>" + USER_TYPE + "</UserType>\n" +
                "\t</Request>\n" +
                "\t<RequestDetails>\n" +
                reqDetails +
                "\n\t\t<ResponseURL>{ResponseURL}</ResponseURL>\n" +
                "\t\t<ResponseVar>{ResponseVar}</ResponseVar>\n" +
                "\t</RequestDetails>\n" +
                "</MpsXml>\n";

        LogUtils.Verbose("XML Req ", xml);
        return xml;
    }


    public static void showProgress(Context mContext, String message) {
        progressDialog = new ProgressDialog(mContext, message);
        progressDialog.show();
    }

    public static void dismissProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
