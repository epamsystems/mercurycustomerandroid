package in.intellifox.mwallet.utilities;

import android.util.Log;

import in.intellifox.mwallet.BuildConfig;

/**
 * Created by vishwanathp on 16/3/17.
 */

public class LogUtils {


    public static void Verbose(String tag, String message) {

        if (BuildConfig.DEBUG)
            Log.v(tag, message);

    }

    public static void Exception(Exception e) {

        if (BuildConfig.DEBUG)
            e.printStackTrace();

        // Crashlytics.logException(e);

    }

    public static void Exception(Exception e, String request, String response) {

        if (BuildConfig.DEBUG)
            e.printStackTrace();

        if (response == null)
            response = "";
//        Crashlytics.setString("API Request", request+"\n APi Response " + response);
//        Crashlytics.setUserName(QNBConsumerApp.getInstance().getMobileNumber());
//
//        Crashlytics.logException(e);
    }

    public static void Exception(Exception e, String request) {

        if (BuildConfig.DEBUG)
            e.printStackTrace();


//        Crashlytics.setString("API Request", request);
//        Crashlytics.setUserName(QNBConsumerApp.getInstance().getMobileNumber());
//
//        Crashlytics.logException(e);

    }


}
