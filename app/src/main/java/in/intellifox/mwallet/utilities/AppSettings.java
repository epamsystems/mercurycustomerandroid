package in.intellifox.mwallet.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kunalk on 4/13/2016.
 */
public class AppSettings {

    public static final String NOTIFICATIONS = "NOTIFICATIONS" ;
    public static final String NOTIFICATION_COUNT = "NOTIFICATION_COUNT" ;
    public static String MOBILE_NUMBER="mobile";
    public static String KEY="key";
    public static String isTOKENSENDTOSERVER;

    public static void putData(Context context,String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("mercuryconsumer",context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public static String getData(Context context,String key) {
        if(context==null)
         return "";
        SharedPreferences preferences = context.getSharedPreferences("mercuryconsumer", context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }
}