package in.intellifox.mwallet.utilities;

import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;

/**
 * Created by kunalk on 2/19/2016.
 */
public class AnimationUtils {

ScaleAnimation scaleAnimation;
RotateAnimation rotateAnimation;

    public void ScaleView(final View view,float fromX,float toX,float fromY,float toY,int typeX, float pivotX,int typeY,float pivotY, final IAnimationUtils callback)
    {
        scaleAnimation=new ScaleAnimation(fromX,toX,fromY,toY,typeX,pivotX,typeY,pivotY);
        scaleAnimation.setInterpolator(new BounceInterpolator());
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setDuration(400);
        view.startAnimation(scaleAnimation);


        scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {


                if(callback!=null)
                    callback.onAnimationEnd();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

  public void stopRotateAnimation(View view)
  {
      view.setAnimation(null);
  }


    public void rotateView(final View view,int fromX,int angle ,int toX,int toY, final IAnimationUtils callback)
    {
        rotateAnimation = new RotateAnimation(fromX, 360, toX, toY);
        rotateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        rotateAnimation.setDuration(500);


        view.startAnimation(rotateAnimation);

        rotateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {


                if(callback==null)
                    callback.onAnimationEnd();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public interface IAnimationUtils
    {
        void onAnimationEnd();
    }
}
