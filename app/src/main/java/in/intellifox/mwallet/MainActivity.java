package in.intellifox.mwallet;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import dialogs.OkDialog;
import in.intellifox.mwallet.apicalls.RetrofitTask;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.parser.WalletId;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.AppSettings;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.utilities.Utils;
import in.intellifox.mwallet.views.activities.BuildProfile;
import in.intellifox.mwallet.views.activities.DrawerMain;
import in.intellifox.mwallet.views.activities.ForgotMPin;

public class MainActivity extends ActionBarActivity {

    String Shared_pref_file_name_for_profile = "ProfileDetails";
    String Shared_pref_file_name_for_bank = "BankDetails";
    String timeStamp = "", mpin = "";
    static String mobilenoTemp = "";
    String mobileno = "";
    String xmlvalue = "", xmlforBankDetails = "";
    String mpinhash = "", tpinhash = "", session = "";
    String bmobile = "9200000011";
    String btpin = "111111";
    Random sessionID;
    String key = "c292a6e6c19b7403cd87949d0ad45021";

    /*String wallet_bal = "", bank_name = "", city_name = "", account_no = "", branch_id = "", branch_address = "",
            identification_code = "", is_primary = "", customer_id = "", mobile_no = "";
    String EncryptedXML = "";
    String xmlvalueforencryptionKey = "";
    String encriptonkey;*/
    static Bundle savedInstanceState;
    String iv = "288dca8258b1dd7c";
    SharedPreferences prefSession;
    SharedPreferences.Editor editorSession;
    LinearLayout login_root_layout;
    private ProgressDialog progressDialog = null;
    Button btnLogin, btnRegister;
    EditText edtMobileNo, edtMpin;
    TextView mobileNoTextView, mpinTextview, forgotPin, settingTextview;
    ImageView mobileImageView, mpinImageView;
    private Dialog dialogChangeUrl;
    private EditText editText_Url;
    //private static String android_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        setContentView(R.layout.activity_main_login);
        final InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        LayoutInitialization();
        prefSession = getApplicationContext().getSharedPreferences("Session", 0);
        editorSession = prefSession.edit();
        //    GlobalVariables.url = prefSession.getString("currentUrl", getResources().getString(R.string.responseurl));

        //GlobalVariables.url = prefSession.getString("currentUrl", GlobalVariables.url);

        /* android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("TAG", "android_id 12=" + android_id);*/

        ImageView imgvwLogo = (ImageView) findViewById(R.id.image_view_logo);
        imgvwLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BuildConfig.DEBUG) {

                    if (GlobalVariables.DEVICE_ID) {
                        GlobalVariables.DEVICE_ID = false;
                        // changeIp.setVisibility(View.VISIBLE);
                    } else {
                        GlobalVariables.DEVICE_ID = true;
                        // changeIp.setVisibility(View.GONE);
                    }

                    Utils.showToast(MainActivity.this, GlobalVariables.DEVICE_ID ? "REAL DEVICE ID" : "TEST DEVICE ID");
                }
            }
        });


        sessionID = new Random();
        settingTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeUrlDialog();
            }
        });
        forgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forgotPin = new Intent(MainActivity.this, ForgotMPin.class);
                startActivity(forgotPin);
            }
        });
        edtMpin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 4) {
//                Intent dashBoard = new Intent(MainActivity.this, DrawerMain.class);
//                startActivity(dashBoard);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    mobileno = edtMobileNo.getText().toString();
                    mpin = edtMpin.getText().toString();
                    //   callLogin(mobileno, mobileno);
                    if (!GlobalVariables.isNetworkAvailable(MainActivity.this)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage(getResources().getString(R.string.no_internet_connection))
                                .setTitle("mWallet");
                        AlertDialog dialog = builder.create();
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();
                    } else if (mobileno.length() <= 1 && mpin.length() <= 1) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage(getResources().getString(R.string.mobile_number_field_empty))
                                .setTitle("mWallet");
                        AlertDialog dialog = builder.create();
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();

                    } else if (mobileno.length() <= 5) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage(getResources().getString(R.string.invalid_mobile_number))
                                .setTitle("mWallet");

                        AlertDialog dialog = builder.create();
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();

                    } else if (mpin.length() != 4) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage(getResources().getString(R.string.invalid_mpin))
                                .setTitle("mWallet");
                        AlertDialog dialog = builder.create();
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();

                    } else {
                        MessageDigest digest = null;
                        try {
                            digest = MessageDigest.getInstance("SHA-256");
                            digest.update(mpin.getBytes());
                            mpinhash = bytesToHexString(digest.digest());
                            Log.i("Eamorr", "result is " + mpinhash);
                        } catch (NoSuchAlgorithmException e1) {

                            e1.printStackTrace();
                        }
                        progressDialog = ProgressDialog.show(MainActivity.this, null,
                                getResources().getString(R.string.authentication_msg), true);

                        // callLogin(mobileno, mpinhash);
                        MyApplication.getInstance().setMerchantMobieNo(mobileno);
                        GetEncryptionKey(mobileno, mpinhash);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent dashBoard = new Intent(MainActivity.this, DrawerMain.class);
//                startActivity(dashBoard);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                mobileno = edtMobileNo.getText().toString();
                mpin = edtMpin.getText().toString();
                //   callLogin(mobileno, mobileno);
                if (!GlobalVariables.isNetworkAvailable(MainActivity.this)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(getResources().getString(R.string.no_internet_connection))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } else if (mobileno.length() <= 1 && mpin.length() <= 1) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(getResources().getString(R.string.mobile_number_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (mobileno.length() <= 5) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(getResources().getString(R.string.invalid_mobile_number))
                            .setTitle("mWallet");

                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (mpin.length() != 4) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(getResources().getString(R.string.invalid_mpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else {
                    MessageDigest digest = null;
                    try {
                        digest = MessageDigest.getInstance("SHA-256");
                        digest.update(mpin.getBytes());
                        mpinhash = bytesToHexString(digest.digest());
                        Log.i("Eamorr", "result is " + mpinhash);
                    } catch (NoSuchAlgorithmException e1) {

                        e1.printStackTrace();
                    }
                    progressDialog = ProgressDialog.show(MainActivity.this, null,
                            getResources().getString(R.string.authentication_msg), true);

                    // callLogin(mobileno, mpinhash);
                    MyApplication.getInstance().setMerchantMobieNo(mobileno);
                    GetEncryptionKey(mobileno, mpinhash);
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register = new Intent(MainActivity.this, BuildProfile.class);
                startActivity(register);
            }
        });

        edtMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mobilenoTemp = edtMobileNo.getText().toString();
                if (edtMobileNo.getText().toString().length() == 10)     //size as per your requirement
                {
                    edtMpin.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mobileImageView.setImageResource(R.drawable.login_mobile_selected);
                    mobileNoTextView.setVisibility(View.VISIBLE);
                    edtMobileNo.setTextColor(Color.WHITE);
                    edtMobileNo.setHint("");
                } else {
                    mobileImageView.setImageResource(R.drawable.login_mobile_deselected);
                    mobileNoTextView.setVisibility(View.INVISIBLE);
                }
            }
        });

        edtMpin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mpinImageView.setImageResource(R.drawable.login_mpin_selected);
                    edtMpin.setTextColor(Color.WHITE);
                    mpinTextview.setVisibility(View.VISIBLE);
                } else {
                    mpinImageView.setImageResource(R.drawable.login_mpin_deselected);
                    mpinTextview.setVisibility(View.INVISIBLE);
                }
            }
        });

        edtMpin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                mobileno = edtMobileNo.getText().toString();

                mpin = edtMpin.getText().toString();
                Log.d("TAG", "mobileno 1=" + mobileno);
//                callLogin(mobileno, mpin);
                if (mobileno.length() <= 1 && mpin.length() <= 1) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(getResources().getString(R.string.mobile_number_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (mobileno.length() <= 5) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(getResources().getString(R.string.invalid_mobile_number))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } else if (mpin.length() != 4) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(getResources().getString(R.string.invalid_mpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } else {
                    progressDialog = ProgressDialog.show(MainActivity.this, null,
                            getResources().getString(R.string.authentication_msg), true);
                    MessageDigest digest = null;
                    try {
                        digest = MessageDigest.getInstance("SHA-256");
                        digest.update(mpin.getBytes());
                        mpinhash = bytesToHexString(digest.digest());
                        Log.i("Eamorr", "result is " + mpinhash);
                    } catch (NoSuchAlgorithmException e1) {
                        e1.printStackTrace();
                    }
                    // callLogin(mobileno, mpinhash);
                    MyApplication.getInstance().setMerchantMobieNo(mobileno);
                    GetEncryptionKey(mobileno, mpinhash);
                }
                return false;
            }
        });


        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_PHONE_STATE},
                    2);
        }
    }

    private void LayoutInitialization() {
        btnLogin = (Button) findViewById(R.id.button_login);
        btnRegister = (Button) findViewById(R.id.button_newUser);
        edtMobileNo = (EditText) findViewById(R.id.edit_text_mobile_number);
        edtMpin = (EditText) findViewById(R.id.edit_text_mpin);
        mobileNoTextView = (TextView) findViewById(R.id.text_view_mobile_number);
        mpinTextview = (TextView) findViewById(R.id.text_view_mpin);
        forgotPin = (TextView) findViewById(R.id.textView);
        mobileImageView = (ImageView) findViewById(R.id.image_view_of_mobile_number);
        mpinImageView = (ImageView) findViewById(R.id.image_view_of_mpin);
        settingTextview = (TextView) findViewById(R.id.textView_setting);
        login_root_layout = (LinearLayout) findViewById(R.id.login_root_layout);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        edtMobileNo.setText("");
        edtMpin.setText("");
        //  flag=false;
    }

    // utility function for encryption of MPIN
    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public String GenerateSessionId() {
        char[] chars1 = "ABCDEF012GHIJKL345MNOPQR678STUVWXYZ9".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 6; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        String random_string = sb1.toString();
        return random_string;
    }

    void ChangeUrlDialog() {
        dialogChangeUrl = new Dialog(MainActivity.this);
        dialogChangeUrl.setCancelable(false);
        WindowManager.LayoutParams wmlp = dialogChangeUrl.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;
        dialogChangeUrl.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogChangeUrl.setContentView(R.layout.dialog_changeurl);
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        wmlp = new WindowManager.LayoutParams();
        wmlp.copyFrom(dialogChangeUrl.getWindow().getAttributes());
        wmlp.width = (int) (width - (width * 0.05));

        dialogChangeUrl.getWindow().setAttributes(wmlp);
        editText_Url = (EditText) dialogChangeUrl.findViewById(R.id.editText_Url);
        String statusUrl = prefSession.getString("statusUrl", "0");
        if (statusUrl.equalsIgnoreCase("0")) {
            editText_Url.setText(getResources().getString(R.string.responseurl));
        } else {
            String urlString = prefSession.getString("currentUrl", getResources().getString(R.string.responseurl));
            editText_Url.setText(urlString);
        }
        Button button_Save = (Button) dialogChangeUrl.findViewById(R.id.button_Save);
        button_Save.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (editText_Url.getText().length() > 0) {
                    GlobalVariables.retrofit_url = editText_Url.getText().toString().trim();
                    editorSession.putString("currentUrl", "" + editText_Url.getText().toString().trim());
                    editorSession.putString("statusUrl", "" + 1);
                    editorSession.commit();
                    dialogChangeUrl.dismiss();
                    Toast.makeText(MainActivity.this, "Url changed successfully!!!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MainActivity.this, "Please enter Url!!!", Toast.LENGTH_LONG).show();
                }

            }
        });
        Button button_Cancel = (Button) dialogChangeUrl.findViewById(R.id.button_Cancel);
        button_Cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogChangeUrl.dismiss();

            }
        });

        dialogChangeUrl.show();

    }

    public void GetEncryptionKey(String Mob, String Mpin) {

        RetrofitTask.getInstance().executeGetKey(Mob, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                if (isSuccess) {
                    MyApplication.getInstance().setKey(response);

                    //TODO  SAVING KEY IN STOREGE NEED TO REMOVE
                    AppSettings.putData(MainActivity.this, AppSettings.KEY, response);

                    //TODO STORING MOBILE NUMBER In PREFERENCES
                    AppSettings.putData(MainActivity.this, AppSettings.MOBILE_NUMBER, mobileno);


                    callLogin(mobileno, mpinhash);
                } else {
                    progressDialog.dismiss();
                    new OkDialog(MainActivity.this, response, "", null);
                }
            }
        });
    }

    public void callLogin(final String mobilenumber, String Mpin) {
        System.out.println("mpin : " + Mpin);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
//        session = String.valueOf(sessionID.nextInt()).toString();
//        System.out.println("sessionID : " + session);
//        Log.d("TAG", "session 2=" + session);
        /*<SPID>|<USER_TYPE>|<MOBILE_NO>|<EncryptedXML>*/

        xmlvalue =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>" + timeStamp + "</Timestamp>" +
                        "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                        "<ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>Login</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<MobileNumber>" + mobilenumber + "</MobileNumber>" +
                        "<Mpin>" + Mpin + "</Mpin>" +
                        "<AppVersion>" + "AND-1" + "</AppVersion>" +
                        "<DeviceId>" + MyApplication.getInstance().getIMEINo() + "</DeviceId>" +
                        "<ResponseURL>" + "" + "</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        "</RequestDetails>" +
                        "</MpsXml>";
        Log.v("xml1", xmlvalue);
        //Log.d("TAG", "xmlvalue 100=" + xmlvalue);


       /* try {
            xmlvalue = AESecurityNew.getInstance().encryptString(xmlvalue);
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            new OkDialog(MainActivity.this, e.toString(), null, null);
            return;
        }


        RetrofitTask.getInstance().executeTask(xmlvalue, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                progressDialog.dismiss();

                if (!isSuccess) {
                    new OkDialog(MainActivity.this, response, null, null);
                    return;
                }

                String decrypted = null;
                try {
                    decrypted = AESecurityNew.getInstance().decryptString(response);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                LogUtils.Verbose("TAG", "Response " + decrypted);


            }
        });
*/
        try {
            final AESecurity aes = new AESecurity();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.v("hi", "hi:" + MyApplication.getInstance().getKey());
                        aes.encrypt(xmlvalue, MyApplication.getInstance().getKey(), iv, new PostEncryption() {

                            @Override
                            public void executeLoginAsync(String mobileNumber, String encryptedXML) {

                                String final_String = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + mobilenumber + "|" + encryptedXML;
                                Log.v("Full URL ", "Final URL : " + final_String);
                                //Log.d("TAG", "final_String 101=" + encryptedXML);
                                new MyAsyncTask(mobilenumber).execute(final_String);
                            }
                        });
                    } catch (InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException |
                            BadPaddingException | IllegalBlockSizeException e) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        e.printStackTrace();


                    }
                }
            }).start();
        } catch (Exception e) {

            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            e.printStackTrace();
        }
    }


    public class MyAsyncTask extends AsyncTask<String, Integer, String> {

        String result = "";
        String mobilenumber = "";

        public MyAsyncTask(String mobilenumber) {
            this.mobilenumber = mobilenumber;
        }

        @Override
        protected String doInBackground(String... params) {
           /* HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;*/
            MyApplication.getInstance().setmProfile_Mobile_Number(mobilenumber);
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);


            // System.out.println("request" + params[0]);

            Log.v("REQUEST", params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException | ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "result 3=" + result);
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected void onPostExecute(String s1) {
            super.onPostExecute(s1);
            String decryptedString = "";
            System.out.println("Response : " + s1);

            final MpsXml[] mpsxml = new MpsXml[1];
            final ParseResponse[] parse = new ParseResponse[1];
            try {
                AESecurity aes = new AESecurity();
                if (result.equalsIgnoreCase("")) {
                    progressDialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Please enter proper mobile Number & Password!!!").setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } else {
                    try {

                        aes.decrypt(result, MyApplication.getInstance().getKey(), iv, new PostEncryption() {

                            @Override
                            public void executeLoginAsync(String mobileNumber, String decrypted) {
                                Log.d("TAG", "decrypted 101 2=" + decrypted);
                                if (decrypted.equals(" ")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                    builder.setMessage(getResources().getString(R.string.no_internet_connection))
                                            .setTitle("mWallet");

                                    AlertDialog dialog = builder.create();
                                    dialog.setCanceledOnTouchOutside(true);
                                    dialog.show();
                                    progressDialog.dismiss();
                                } else {
                                    parse[0] = new ParseResponse();
                                    InputSource is = new InputSource(new StringReader(decrypted));
                                    mpsxml[0] = parse[0].parseXML(is);
                                    System.out.println("declogin " + decrypted);
                                    if (mpsxml[0].getResponse().getResponseType().equalsIgnoreCase("Success")) {
                                        MyApplication.getInstance().setMerchantMobieNo(mobilenumber);
                                        String reason = mpsxml[0].getResponsedetails().getReason();
                                        String updateFlag = mpsxml[0].getResponsedetails().getReason();
                                        final String mChangePin = mpsxml[0].getResponsedetails().getChangeMPin();
                                        Log.d("TAG1", "getSessionId()=" + mpsxml[0].getHeader().getSessionId());
                                        GlobalVariables.sessionId = mpsxml[0].getHeader().getSessionId();
                                        editorSession.putString("SessionId", "" + GlobalVariables.sessionId);
                                        editorSession.commit();
                                        Log.d("TAG1", "mchangepib=" + mChangePin);

                                        ArrayList<WalletId> walletList = mpsxml[0].getResponsedetails().getWallet().getWalletid();
                                        for (int i = 0; i < walletList.size(); i++) {
                                            if (walletList.get(i).getIsPrimary().equalsIgnoreCase("Y")) {
                                                GlobalVariables.mProfile_Wallet_Balance = walletList.get(i).getBalance();
                                               // MyApplication.getInstance().setGeneralBalance(walletList.get(i).getBalance());
                                                MyApplication.getInstance().setmProfile_WalletID_General(walletList.get(i).getWalletId());
                                            }
                                        }
                                        MyApplication.getInstance().setmProfile_FirstName(mpsxml[0].getResponsedetails().getFirstName());
                                        MyApplication.getInstance().setmProfile_Last_Name(mpsxml[0].getResponsedetails().getLastName());

                                        MyApplication.getInstance().setOneClilckFlag(mpsxml[0].getResponsedetails().getOneClilckFlag());
                                        MyApplication.getInstance().setOneClilckAmount(mpsxml[0].getResponsedetails().getOneClilckAmount());

                                        MyApplication.getInstance().setmProfile_Last_Login(mpsxml[0].getResponsedetails().getLastLogin());
//
                                        String isChangeMPIN = "N";
                                        if (mChangePin != null && mChangePin.equalsIgnoreCase("Y"))
                                            isChangeMPIN = "Y";
                                        Intent dashboard = new Intent(MainActivity.this, DrawerMain.class);


                                        dashboard.putExtra("changeMpin", isChangeMPIN);
                                        if (getIntent().hasExtra("isNFCDetected"))
                                            dashboard.putExtra("isNFCDetected", getIntent().getBooleanExtra("isNFCDetected", false));

                                        startActivity(dashboard);
                                        finish();

                                    } else if (mpsxml[0].getResponse().getResponseType().equalsIgnoreCase("Fail")) {

                                        mobileno = MyApplication.getInstance().getMerchantMobieNo();
                                        Log.d("TAG", "mobileNo 11=" + mobileno);
                                        if (mobileno.length() <= 1) {

                                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                            builder.setMessage("" + mpsxml[0].getResponsedetails().getReason()).setTitle("mWallet");
                                            AlertDialog dialog = builder.create();
                                            dialog.setCanceledOnTouchOutside(true);
                                            dialog.show();
                                            progressDialog.dismiss();

                                        } else {
                                            progressDialog.dismiss();
//                                        CallLogout(mobileNo);
                                            View popupView = getLayoutInflater().inflate(R.layout.custom_popup, null);
                                            final PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                                            TextView tv1 = (TextView) popupView.findViewById(R.id.txt_note1);
                                            tv1.setText("" + mpsxml[0].getResponsedetails().getReason());
                                            TextView tv2 = (TextView) popupView.findViewById(R.id.txt_note2);
                                            tv2.setText("");
                                            TextView tv3 = (TextView) popupView.findViewById(R.id.txt_note3);
                                            tv3.setText("");
                                            tv3.setTextSize(12);
                                            ImageView cancel = (ImageView) popupView.findViewById(R.id.image_view_cancel_on_popup);
                                            cancel.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    popupWindow.dismiss();
                                                }
                                            });
                                            Button btnDismiss = (Button) popupView.findViewById(R.id.button_ok_on_popup);
                                            btnDismiss.setOnClickListener(new Button.OnClickListener() {

                                                @Override
                                                public void onClick(View v) {
                                                    popupWindow.dismiss();
                                                }
                                            });

                                            popupWindow.showAtLocation(login_root_layout, Gravity.CENTER, 0, 0);

                                        }
                                    } else {
                                        progressDialog.dismiss();

                                        String reason = mpsxml[0].getResponsedetails().getReason();
                                        Log.d("TAG", "reason 12=" + reason);
                                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                        builder.setMessage("Something went wrong")
                                                .setTitle("mWallet");
                                        AlertDialog dialog = builder.create();
                                        dialog.setCanceledOnTouchOutside(true);
                                        dialog.show();

                                    }
                                    Log.d("TAG", "reason 14=");
                                    progressDialog.dismiss();
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                }


            } catch (Exception e) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                e.printStackTrace();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        // if (id == R.id.action_settings) {
        //   return true;
        // }
        return super.onOptionsItemSelected(item);
    }

    public interface PostEncryption {
        void executeLoginAsync(String mobileNumber, String encryptedXML);
    }
}
