package in.intellifox.mwallet.views.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Beneficiary;

/**
 * Created by Owner on 9/15/2015.
 */
public class CustomListFavouriteRequestMoney extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    TextView textViewFname,textViewLName,textViewDate,textViewAmount;
    private ArrayList<Beneficiary> beneficiaryArrayList=new ArrayList<Beneficiary>();

    public CustomListFavouriteRequestMoney(Context mContext, ArrayList<Beneficiary> beneficiaryArrayList) {
        this.mContext = mContext;
        this.beneficiaryArrayList=beneficiaryArrayList;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return beneficiaryArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        view = new View(mContext);
        view = mInflater.inflate(R.layout.request_money_favourite_listview_item,null);
        textViewFname=(TextView)view.findViewById(R.id.textView_favourite_name);
        textViewLName=(TextView)view.findViewById(R.id.textView_favourite_name1);
        textViewDate=(TextView)view.findViewById(R.id.textView_favourite_transactionDate);
        textViewAmount=(TextView)view.findViewById(R.id.textView_favourite_amount);
      try {
          textViewFname.setText("Name");
          textViewLName.setText(beneficiaryArrayList.get(position).getNickName());
      }catch (Exception e)
      {
        Log.d("Exception", "CustomListFavouriteRequestMoney="+e.toString());
      }
        Log.d("TAG","Mobile no="+beneficiaryArrayList.get(position).getValue());
        textViewDate.setText(beneficiaryArrayList.get(position).getValue());
//        textViewAmount.setText(amount[position]);
        return view;
    }
}
