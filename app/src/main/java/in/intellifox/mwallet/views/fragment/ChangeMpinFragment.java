package in.intellifox.mwallet.views.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.GlobalVariables;

public class ChangeMpinFragment extends Fragment{

    EditText current_pin,new_pin,confirm_pin;
    Button submit_for_change_mpin;
    EditText currentMpin, newMpin, confirmMpin;
    String current_mpin = "", new_mpin = "", confirm_mpin = "";
    String tag_for_mpin = "";
    Button confirmBtn;
    String xmlvalue_changeMpin = "", timeStamp = "", mobileNo = "",Dateofbirth="";
    String encryptedCurrentMpin = "", encryptedNewMpin = "";
    private ProgressDialog progressDialog = null;
    ScrollView scrollView;
    String iv = "288dca8258b1dd7c";
    Intent intent;
    int data;
    ImageView backArrow;
    RelativeLayout rootLay;

/*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        String mobilenumber="9000000001";
        Bundle b = this.getArguments();
        mobileNo = b.getString("9000000001").toString();
    }   */

    /** Called when the activity is first created. */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        MyApplication.resetTimer(getActivity());
        rootLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        submit_for_change_mpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }

                MyApplication.resetTimer(getActivity());
                current_mpin = current_pin.getText().toString();
                new_mpin = new_pin.getText().toString();
                confirm_mpin = confirm_pin.getText().toString();
                if(current_mpin.length()<=1 && new_mpin.length()<=1 && confirm_mpin.length()<=1){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.current_mpin_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else if(current_mpin.length()!= 4 ){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.invalid_current_mpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else if(new_mpin.length()!= 4 ){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.invalid_new_mpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else if(confirm_mpin.length()!= 4){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.invalid_confirm_mpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else if(!new_mpin.equals(confirm_mpin)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.confirm_mpin_n_new_mpin_should_match))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else  if(current_mpin.equals(new_mpin)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.current_mpin_n_new_mpin_should_not_match))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else{
                    mobileNo = MyApplication.getInstance().getMerchantMobieNo();
                    CallForOLdMPIN(current_mpin);
                    CallForNewMPIN(confirm_mpin);
                    callChangeMpin(encryptedCurrentMpin,encryptedNewMpin,mobileNo);
                }
            }
        });
    }

    private void LayoutInitialization(View view) {
        rootLay = (RelativeLayout) view.findViewById(R.id.root_relative_lay_for_chnage_mpin);
        current_pin = (EditText)view.findViewById(R.id.currentpin);
        new_pin = (EditText)view.findViewById(R.id.newpin);
        confirm_pin =(EditText)view.findViewById(R.id.cnfrmpin);
        submit_for_change_mpin = (Button)view.findViewById(R.id.changempin);
    }

    public void CallForOLdMPIN(String OLDMPIN) {

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(OLDMPIN.getBytes());
            encryptedCurrentMpin = fornewmpin(digest.digest());
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
    }

    public void CallForNewMPIN(String NEWMPIN) {

        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(NEWMPIN.getBytes());
            encryptedNewMpin = foroldmpin(digest.digest());
            Log.i("Eamorr", "result is " + encryptedNewMpin);
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
    }

    private static String foroldmpin(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    private static String fornewmpin(byte[] bytes) {

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public void callChangeMpin(String mpin_current, String mpin_confirm, final String MobileNumber) {

        progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());

        /*<MpsXml><Header><ChannelId>APP</ChannelId><ServiceProvider>mercury</ServiceProvider><Timestamp>06-08-2015 22:08:51</Timestamp><SessionId>mercury001</SessionId></Header><Request><RequestType>ChangeMpin</RequestType><UserType>ME</UserType></Request><RequestDetails><MobileNumber>909090901</MobileNumber><NewMpin>318aee3fed8c9d040d35a7fc1fa776fb31303833aa2de885354ddf3d44d8fb69</NewMpin><OldMpin>edee29f882543b956620b26d0ee0e7e950399b1c4222f5de05e06425b4c995e9</OldMpin><ResponseURL></ResponseURL><ResponseVar></ResponseVar></RequestDetails></MpsXml>*/
        xmlvalue_changeMpin =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>" + timeStamp + "</Timestamp>" +
                        "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                        "<ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>ChangeMpin</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<MobileNumber>" + MobileNumber+ "</MobileNumber>" +
                        "<OldMpin>" + mpin_current + "</OldMpin>" +
                        "<NewMpin>" + mpin_confirm + "</NewMpin>" +
                        "<ResponseURL>" + GlobalVariables.url + "</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        "</RequestDetails>" +
                        "</MpsXml>";

        try {
            AESecurity aes = new AESecurity();

            aes.encrypt(xmlvalue_changeMpin,  MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {

                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXMLforChangeMpin) {

                    String final_String_for_ChangeMPIN = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + MobileNumber + "|" + encryptedXMLforChangeMpin;
                    Log.v("Full URL ", "Final URL : " + final_String_for_ChangeMPIN);
                    new MyAsyncTaskForChangeTpin().execute(final_String_for_ChangeMPIN);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class MyAsyncTaskForChangeTpin extends AsyncTask<String, Integer, String> {
        String result = "";

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {

                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //   progressDialog = ProgressDialog.show(getActivity(), null, "Please Wait...", true);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("Response : " + s);
            progressDialog.dismiss();

            if(result.equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                progressDialog.dismiss();
            }else {

                try {
                    AESecurity aess = new AESecurity();
                    try {
                        aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                            @Override
                            public void executeLoginAsync(String mobileNumber, String decrypted) {
                                Log.v("DECRYPTED : ", "" + decrypted);
                                ParseResponse parse = new ParseResponse();
                                InputSource is = new InputSource(new StringReader(decrypted));
                                MpsXml mpsxml = parse.parseXML(is);

                                if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage(mpsxml.getResponsedetails().getMessage())
                                            .setTitle("mWallet");
                                    /*builder.setMessage(mpsxml.getResponsedetails().getFirstName())
                                            .setTitle("mWallet");
                                    builder.setMessage(mpsxml.getResponsedetails().getLastName())
                                            .setTitle("mWallet");*/
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                         //   getActivity().finish();
                                        }
                                    });
                                    AlertDialog dialog = builder.create();
                                    dialog.setCancelable(false);
                                    dialog.show();
                                    current_pin.setText("");
                                    new_pin.setText("");
                                    confirm_pin.setText("");
                                    progressDialog.dismiss();
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage(mpsxml.getResponsedetails().getReason())
                                            .setTitle("mWallet");
                                    /*builder.setMessage(mpsxml.getResponsedetails().getErrorCode())
                                            .setTitle("mWallet");*/
                                    AlertDialog dialog = builder.create();
                                    dialog.setCanceledOnTouchOutside(true);
                                    dialog.show();
                                    current_pin.setText("");
                                    new_pin.setText("");
                                    confirm_pin.setText("");
                                    progressDialog.dismiss();
                                }
                            }
                        });
                    } catch (InvalidKeyException | BadPaddingException | UnsupportedEncodingException |
                            InvalidAlgorithmParameterException | IllegalBlockSizeException e) {
                        e.printStackTrace();
                    }
                } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                    e.printStackTrace();
                }
            }
            //Parse your response here
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.change_mpin_fragment, container, false);
    }
}