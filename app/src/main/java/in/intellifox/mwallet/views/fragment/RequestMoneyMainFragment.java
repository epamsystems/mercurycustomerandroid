package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Beneficiary;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/15/2015.
 */
public class RequestMoneyMainFragment extends Fragment {

    ImageView favourite,phoneBook,email,facebook;
    RequestMoneyEmailFragment requestMoneyEmailFragment;
    RequestMoneyFacebookFragment requestMoneyFacebookFragment;
    RequestMoneyPhoneBookFragment requestMoneyPhoneBookFragment;
    RequestMoneyFavouriteFragment requestMoneyFavouriteFragment;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        GlobalVariables.fragmentManager = getFragmentManager();
        try {
            Bundle args = getArguments();
            Beneficiary beneficiary = (Beneficiary) args.getSerializable("RequestMoney");
            Log.d("TAG", "beneficiary=" + beneficiary);
            if(beneficiary!=null)
            {
                requestMoneyPhoneBookFragment=new RequestMoneyPhoneBookFragment();
                args.putSerializable("RequestMoney",beneficiary);
                requestMoneyPhoneBookFragment.setArguments(args);

                favourite.setImageResource(R.drawable.request_money_favorites_deselected);
                phoneBook.setImageResource(R.drawable.request_money_phonebook_selected);
                email.setImageResource(R.drawable.request_money_email_deselected);
                facebook.setImageResource(R.drawable.request_money_facebook_deselected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, requestMoneyPhoneBookFragment).commit();


            }else {
                //Default Fragment
                RequestMoneyFavouriteFragment rmf=new RequestMoneyFavouriteFragment();
                Bundle bundle = new Bundle();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, rmf).commit();

            }
        }catch (Exception e)
        {
            //Default Fragment
            RequestMoneyFavouriteFragment rmf=new RequestMoneyFavouriteFragment();
            Bundle bundle = new Bundle();
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, rmf).commit();

        }
        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestMoneyFavouriteFragment=new RequestMoneyFavouriteFragment();
                Bundle bundle = new Bundle();
                favourite.setImageResource(R.drawable.request_money_favorite_selected);
                phoneBook.setImageResource(R.drawable.request_money_phonebook_deselected);
                email.setImageResource(R.drawable.request_money_email_deselected);
                facebook.setImageResource(R.drawable.request_money_facebook_deselected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, requestMoneyFavouriteFragment).commit();
            }
        });

        phoneBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestMoneyPhoneBookFragment=new RequestMoneyPhoneBookFragment();
                Bundle bundle = new Bundle();
                favourite.setImageResource(R.drawable.request_money_favorites_deselected);
                phoneBook.setImageResource(R.drawable.request_money_phonebook_selected);
                email.setImageResource(R.drawable.request_money_email_deselected);
                facebook.setImageResource(R.drawable.request_money_facebook_deselected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, requestMoneyPhoneBookFragment).commit();

            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestMoneyEmailFragment=new RequestMoneyEmailFragment();
                Bundle bundle = new Bundle();
                favourite.setImageResource(R.drawable.request_money_favorites_deselected);
                phoneBook.setImageResource(R.drawable.request_money_phonebook_deselected);
                email.setImageResource(R.drawable.request_money_email_selected);
                facebook.setImageResource(R.drawable.request_money_facebook_deselected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, requestMoneyEmailFragment).commit();
            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestMoneyFacebookFragment=new RequestMoneyFacebookFragment();
                Bundle bundle = new Bundle();
                favourite.setImageResource(R.drawable.request_money_favorites_deselected);
                phoneBook.setImageResource(R.drawable.request_money_phonebook_deselected);
                email.setImageResource(R.drawable.request_money_email_deselected);
                facebook.setImageResource(R.drawable.request_money_facebook_selected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, requestMoneyFacebookFragment).commit();
            }
        });

    }

    private void LayoutInitialization(View view) {

        favourite=(ImageView) view.findViewById(R.id.request_money_layout_position_0);
        phoneBook=(ImageView) view.findViewById(R.id.request_money_layout_position_1);
        email=(ImageView) view.findViewById(R.id.request_money_layout_position_2);
        facebook=(ImageView) view.findViewById(R.id.request_money_layout_position_3);
        final RelativeLayout frag_container = (RelativeLayout) view.findViewById(R.id.request_money_main_fragment_container);
    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false,  true, "Request Money",false,"",false,"");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.request_money_main, container, false);
    }
}
