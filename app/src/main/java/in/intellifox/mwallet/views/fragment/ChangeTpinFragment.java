package in.intellifox.mwallet.views.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.activities.DrawerMain;

public class ChangeTpinFragment extends Fragment {

    EditText current_pin,new_pin,confirm_pin;
    Button submit_for_change_tpin;
    String current_tpin="",new_tpin="",confirm_tpin="";
    String tag_for_tpin ="";
    String xmlvalue_changeTpin="",timeStamp="";
    TextView link_forgot_tpin;
    String encryptedCurrentTpin="",encryptedNewTpin="",mobileNo="";
    private ProgressDialog progressDialog = null;
    RelativeLayout rootLay;
    ScrollView scrollView;
    String iv = "288dca8258b1dd7c";

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.setRetainInstance(true);
        View v =inflater.inflate(R.layout.change_tpin_fragment,container,false);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
        // Bundle b = this.getArguments();
        // mobileNo = b.getString("mobilenumber").toString();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        MyApplication.resetTimer(getActivity());
        rootLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });
        submit_for_change_tpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
                current_tpin = current_pin.getText().toString();
                new_tpin = new_pin.getText().toString();
                confirm_tpin = confirm_pin.getText().toString();

                if(current_tpin.length()<=1 && new_tpin.length()<=1 && confirm_tpin.length()<=1){

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.current_tpin_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else if(current_tpin.length()!= 6 ){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.invalid_current_tpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else if(new_tpin.length()!= 6 ){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.invalid_new_tpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else if(confirm_tpin.length()!= 6){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.invalid_confirm_tpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else if(!new_tpin.equals(confirm_tpin)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.confirm_tpin_n_new_tpin_should_match))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else if (current_tpin.equals(new_tpin)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.current_tpin_n_new_tpin_should_not_match))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else{
                    // Toast.makeText(getActivity(),"AsncTask",Toast.LENGTH_LONG).show();
                    mobileNo = MyApplication.getInstance().getMerchantMobieNo();
                    CallForOLdTPIN(current_tpin);
                    CallForNewTPIN(new_tpin);
                    //   encryptedCurrentTpin="",encryptedNewTpin="",mobileNo=""
                    callChangeTpin(encryptedCurrentTpin,encryptedNewTpin,mobileNo);
                }
            }
        });
    }

    private void LayoutInitialization(View view) {
        rootLay = (RelativeLayout) view.findViewById(R.id.root_relative_lay_for_chnage_tpin);
        current_pin = (EditText)view.findViewById(R.id.currentpin);
        new_pin = (EditText)view.findViewById(R.id.newtpin);
        confirm_pin =(EditText)view.findViewById(R.id.cnfrmtpin);
        submit_for_change_tpin = (Button)view.findViewById(R.id.changetpin);
    }

    public void CallForOLdTPIN(String OLDTPIN){

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(OLDTPIN.getBytes());
            encryptedCurrentTpin = fornewtpin(digest.digest()).toUpperCase();
            Log.i("Eamorr", "result is " + encryptedCurrentTpin);
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
    }

    public void CallForNewTPIN(String NEWTPIN){

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(NEWTPIN.getBytes());
            encryptedNewTpin = foroldtpin(digest.digest()).toUpperCase();
            Log.i("Eamorr", "result is " + encryptedNewTpin);
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
    }

    private static  String foroldtpin(byte[] bytes){
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    private static String fornewtpin(byte[] bytes) {

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public void   callChangeTpin(String tpin_current,String tpin_new,final String mobile){

        progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());

        xmlvalue_changeTpin =
                "            <MpsXml>" +
                        "            <Header>" +
                        "            <ChannelId>APP</ChannelId>" +
                        "            <Timestamp>"+timeStamp+"</Timestamp>" +
                        "            <SessionId>"+ GlobalVariables.sessionId+"</SessionId>" +
                        "            <ServiceProvider>"+getResources().getString(R.string.serviceprovider)+"</ServiceProvider>" +
                        "            </Header>" +
                        "            <Request>" +
                        "            <RequestType>ChangeTpin</RequestType>" +
                        "            <UserType>CU</UserType>" +
                        "            </Request>" +
                        "            <RequestDetails>" +
                        "            <MobileNumber>"+mobile+"</MobileNumber>" +
                        "            <OldTpin>"+tpin_current+"</OldTpin>" +
                        "            <NewTpin>"+tpin_new+"</NewTpin>" +
                        "            <ResponseURL>"+ GlobalVariables.url+"</ResponseURL>" +
                        "            <ResponseVar>mobileResponseXML</ResponseVar>" +
                        "            </RequestDetails>" +
                        "            </MpsXml>";
        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlvalue_changeTpin, MyApplication.getInstance().getKey(),iv,new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXMLforChangeTpin) {
                    String final_String_for_ChangeTPIN = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + mobile + "|" + encryptedXMLforChangeTpin;
                    new MyAsyncTaskForChangeTpin().execute(final_String_for_ChangeTPIN);
                }
            });
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public class MyAsyncTaskForChangeTpin extends AsyncTask<String, Integer, String> {
        String result = "";

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("Response : " + s);

            progressDialog.dismiss();

            //Parse your response here
            if(result.equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                progressDialog.dismiss();
            }else {

                try {
                    AESecurity aess = new AESecurity();
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted) {
                            Log.v("DECRYPTED : ", "" + decrypted);
                            ParseResponse parse = new ParseResponse();
                            InputSource is = new InputSource(new StringReader(decrypted));
                            MpsXml mpsxml = parse.parseXML(is);

                            if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success")) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getMessage())
                                        .setTitle("mWallet");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                       // getActivity().finish();
                                    }
                                });
                                AlertDialog dialog = builder.create();
                                dialog.setCancelable(false);
                                dialog.show();
                                current_pin.setText("");
                                new_pin.setText("");
                                confirm_pin.setText("");

                   /* mDashboardFragment = DashboardFragment.newInstance("","","");
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container,mDashboardFragment).commit();*/
                                progressDialog.dismiss();
                                DrawerMain.isOnDashboard = true;
                                ((DrawerMain)getActivity()).onBackPressed();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getReason())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                current_pin.setText("");
                                new_pin.setText("");
                                confirm_pin.setText("");
                                progressDialog.dismiss();
                            }
                        }
                    });
                } catch (NoSuchAlgorithmException | UnsupportedEncodingException | NoSuchPaddingException | BadPaddingException |
                        InvalidAlgorithmParameterException | InvalidKeyException | IllegalBlockSizeException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}