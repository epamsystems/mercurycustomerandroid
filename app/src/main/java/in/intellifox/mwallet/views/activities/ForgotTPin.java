package in.intellifox.mwallet.views.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.GlobalVariables;

/**
 * Created by Raju on 23/10/2015.
 */
public class ForgotTPin extends ActionBarActivity {

    EditText MobileNumber, Mpin,  edtEmailId,edtPinCode;
    String mobile_number = "", mpin = "", EmailId= "",PinCode="";
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    Button Confirm;
    String timeStamp = "",encryptedMpin = "",xmlforForgotTpin = "";
    LinearLayout rootLay;
    ScrollView scrollView;
    String iv = "288dca8258b1dd7c";
    ImageView backArrow;
    String key="c292a6e6c19b7403cd87949d0ad45021";
    TextView actionBarTitleText,actionBarPersonText,actionBarPersonTextValue;
    ImageView actionBarAppLogoImage,actionBarBackImage,actionBarNotificationImage,actionBarTagImage,actionBarQrcodeImage,
            actionBarSearchImage,actionBarNavagationImage;


    RelativeLayout llNotification;
    TextView txtNotificationCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_tpin);
        MyApplication.resetTimer(ForgotTPin.this);
        LayoutInitialization();
        setUpActionBar();

        rootLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(ForgotTPin.this);
            }
        });

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) ForgotTPin.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                MyApplication.resetTimer(ForgotTPin.this);
                mobile_number =MyApplication.getInstance().getMerchantMobieNo();;
                mpin = Mpin.getText().toString();
                EmailId = edtEmailId.getText().toString();
                PinCode = edtEmailId.getText().toString();

                if (mobile_number.length() < 1 && mpin.length() < 1 && EmailId.length() < 1) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotTPin.this);
                    builder.setMessage(getResources().getString(R.string.invalid_mobile_number))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if (mobile_number.length()<=5) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotTPin.this);
                    builder.setMessage(getResources().getString(R.string.invalid_mobile_number))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if (mpin.length() != 4) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotTPin.this);
                    builder.setMessage(getResources().getString(R.string.invalid_mpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } /*else if (!(EmailId.matches(emailPattern))) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotTPin.this);
                    builder.setMessage(getResources().getString(R.string.invalid_Doc_Verification_number))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }*/else {

                    MessageDigest digest = null;

                    try {
                        digest = MessageDigest.getInstance("SHA-256");
                        digest.update(mpin.getBytes());
                        encryptedMpin = bytesToHexString(digest.digest()).toUpperCase();
                        Log.i("Eamorr", "result is " + encryptedMpin);
                    } catch (NoSuchAlgorithmException e1) {
                        e1.printStackTrace();
                    }
                    callForgotTpin(mobile_number, encryptedMpin, PinCode, EmailId);
                }
            }
        });
    }

    private void LayoutInitialization() {
        MobileNumber = (EditText) findViewById(R.id.editext_mobile_number);
        Mpin = (EditText) findViewById(R.id.edittext_mpin);
        edtEmailId = (EditText) findViewById(R.id.edit_text_emailId_tpin);
        Confirm = (Button) findViewById(R.id.button_submit_for_forget_tpin);
        rootLay = (LinearLayout) findViewById(R.id.root_relative_lay_for_forget_tpin);
    }

    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public void callForgotTpin(final String MobileN, String Mpin, String PINCODE, String emailid) {

        System.out.println("MOBILE NO " + MobileN + "MPIN " + Mpin + "PIN CODE " + PINCODE + "EMAIL ID " + emailid);
        xmlforForgotTpin = "<MpsXml>" +
                "<Header>" +
                "<ChannelId>APP</ChannelId>" +
                "<Timestamp>" + timeStamp + "</Timestamp>" +
                "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                "<ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                "</Header>" +
                "<Request>" +
                "<RequestType>ForgotTpin</RequestType>" +
                "<UserType>CU</UserType>" +
                "</Request>" +
                "<RequestDetails>" +
                "<MobileNumber>" + MobileN + "</MobileNumber>" +
                "<Mpin>" + Mpin + "</Mpin>" +
                "<PinCode>"+PINCODE+"</PinCode>" +
                "<EmailId>" + emailid + "</EmailId>" +
                "<ResponseURL>" + GlobalVariables.url + "</ResponseURL>" +
                "<ResponseVar></ResponseVar>" +
                "</RequestDetails>" +
                "</MpsXml>";

        System.out.println("Request xml"+xmlforForgotTpin);
        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforForgotTpin, key, iv, new MainActivity.PostEncryption() {

                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXML) {
                    System.out.println("MOB "+MobileN);
                    String final_string_forgotTPIn = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + MobileN + "|" + encryptedXML;
                    new MyAsyncTaskForgotTPin().execute(final_string_forgotTPIn);
                }
            });
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    //Set custom Action bar
    private void setUpActionBar() {
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_dashboard);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));

        //Initialization of Action bar components
        actionBarNavagationImage= (ImageView) findViewById(R.id.action_bar_navagation_image);
        actionBarNavagationImage.setVisibility(View.GONE);
        actionBarBackImage=(ImageView)findViewById(R.id.action_bar_back);
        actionBarTitleText = (TextView) findViewById(R.id.action_bar_title);
        actionBarTitleText.setText("Forgot TPIN");

        //actionBarNotificationImage = (ImageView) findViewById(R.id.actionbar_notification);
        //actionBarNotificationImage.setVisibility(View.GONE);
        llNotification = (RelativeLayout) findViewById(R.id.toolbar_ll_notification);
        txtNotificationCount = (TextView) findViewById(R.id.txtCount);
        llNotification.setVisibility(View.GONE);

      //  actionBarTagImage = (ImageView) findViewById(R.id.actionbar_tag);
//        actionBarTagImage.setVisibility(View.GONE);
        actionBarQrcodeImage = (ImageView) findViewById(R.id.actionbar_qrcode);
        actionBarQrcodeImage.setVisibility(View.GONE);
        actionBarAppLogoImage = (ImageView) findViewById(R.id.action_bar_imageview_app_logo);
        actionBarAppLogoImage.setVisibility(View.GONE);
        actionBarSearchImage = (ImageView) findViewById(R.id.actionbar_search);
        actionBarSearchImage.setVisibility(View.GONE);
        actionBarPersonText= (TextView) findViewById(R.id.action_bar_PersonID);
        actionBarPersonText.setVisibility(View.GONE);
        actionBarPersonTextValue= (TextView) findViewById(R.id.action_bar_PersonAmount);
        actionBarPersonTextValue.setVisibility(View.GONE);

        actionBarBackImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public class MyAsyncTaskForgotTPin extends AsyncTask<String, Integer, String> {

        String result = "";
        private ProgressDialog progressDialog = null;

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ForgotTPin.this, null, getResources().getString(R.string.please_wait_msg), true);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("Response : " + s);
            progressDialog.dismiss();

            if (result.equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ForgotTPin.this);
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {

                try {
                    AESecurity aess = new AESecurity();
                    aess.decrypt(result, key, iv, new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted) {
                            Log.v("decrypted:",decrypted);

                            ParseResponse parse = new ParseResponse();
                            InputSource is = new InputSource(new StringReader(decrypted));
                            MpsXml mpsxml = parse.parseXML(is);

                            if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(ForgotTPin.this);
                                builder.setMessage(mpsxml.getResponsedetails().getMessage())
                                        .setTitle("mWallet");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });
                                AlertDialog dialog = builder.create();
                                dialog.show();

                                /*Intent intentToLogin = new Intent(forgotTPin.this,MainActivity.class);
                                startActivity(intentToLogin);*/
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(ForgotTPin.this);
                                builder.setMessage(mpsxml.getResponsedetails().getReason())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException |
                        UnsupportedEncodingException | InvalidAlgorithmParameterException | InvalidKeyException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

