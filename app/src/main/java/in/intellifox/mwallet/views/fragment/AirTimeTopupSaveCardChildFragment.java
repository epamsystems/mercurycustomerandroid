package in.intellifox.mwallet.views.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.adapter.ExpandableMyCardListAdapter;

/**
 * Created by Raju on 18/09/2015.
 */
public class AirTimeTopupSaveCardChildFragment extends Fragment {

    private AirTimeTopupSuccess airTimeTopupSuccess;
    Button paybysavecard;
    Button sendMoney;
    LinearLayout mainLayout;
    ExpandableMyCardListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> accountnumber;
    List<String> bankofname;
    HashMap<String, List<String>> listDataChild;
    HashMap<String, List<String>> listDataChild_Amount;
    String recentxml = "";
    String timeStamp = "";
    LinearLayout rootlay;
    String key = "YhBCOSXLjOP8xrJpRg9jZq3HjzZwxnmwU5DJfuWTLAA=";
    private ProgressDialog progressDialog = null;

    String walletid = "", mobileno = "", tpin = "333333", encryptedCurrentTpin = "";
    String Mob_No = "", Wallet_id = "";

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        sendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View popupView = getLayoutInflater(savedInstanceState).inflate(R.layout.custom_popup, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);

                TextView tv1 = (TextView) popupView.findViewById(R.id.txt_note1);
                tv1.setText("You will be redirected to third party");
                TextView tv2 = (TextView) popupView.findViewById(R.id.txt_note2);
                tv2.setText("payment site.");

                Button btnDismiss = (Button) popupView.findViewById(R.id.button_ok_on_popup);
                btnDismiss.setOnClickListener(new Button.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        popupWindow.dismiss();
                    }
                });

                popupWindow.showAtLocation(mainLayout, Gravity.CENTER, 0, 0);
            }
        });
        accountnumber= new ArrayList<String>();
        bankofname = new ArrayList<String>();

        listDataChild = new HashMap<String, List<String>>();
        listDataChild_Amount = new HashMap<String, List<String>>();

        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {

                        return true;
                    }
                }
                return false;
            }
        });

        // Adding child data
        accountnumber.add("1234 xxxx xxxx xxxx");
        accountnumber.add("1234 xxxx xxxx xxxx");
        bankofname.add("Bank Of abcd");
        bankofname.add("Bank of abcd");

        List<String> top250 = new ArrayList<String>();
        top250.add(" ");

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("");

        listDataChild.put(accountnumber.get(0), top250); // Header, Child data
        listDataChild.put(accountnumber.get(1), nowShowing);
        listDataChild_Amount = new HashMap<String, List<String>>();
        listDataChild_Amount.put(accountnumber.get(0),top250);
        listDataChild_Amount.put(accountnumber.get(1),nowShowing);
        listAdapter = new ExpandableMyCardListAdapter(getActivity(), accountnumber, bankofname, listDataChild, listDataChild_Amount);
        expListView.setAdapter(listAdapter);// Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

                /* Toast.makeText(getApplicationContext(),
                listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();*/
            }
        });
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                /*Toast.makeText(getActivity().getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();*/

            }
        });
        paybysavecard=(Button)view.findViewById(R.id.button_childrow_sendmoney);

        paybysavecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airTimeTopupSuccess = new AirTimeTopupSuccess();
                Bundle bundle = new Bundle();
                airTimeTopupSuccess.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupSuccess).commit();
            }
        });
    }

    private void LayoutInitialization(View view) {
        rootlay=(LinearLayout)view.findViewById(R.id.root_layout);
        sendMoney=(Button)view.findViewById(R.id.button_childrow_sendmoney);
        sendMoney.setText("Pay Now");
        mainLayout=(LinearLayout)view.findViewById(R.id.root_layout);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pay_merchant_child_savecard, container, false);
    }
}
