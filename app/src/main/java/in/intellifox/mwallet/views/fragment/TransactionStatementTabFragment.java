package in.intellifox.mwallet.views.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.parser.Transactions;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.adapter.ExpandableTransactionListAdapter;


public class TransactionStatementTabFragment extends Fragment {

   TextView form_date, to_date, cal_info;
    private TransactionCalendarViewFragment mTransactionCalendarViewFragment;
    RelativeLayout calander_container;
    ExpandableTransactionListAdapter listAdapter;

    private boolean undo = false;
    private CaldroidFragment caldroidFragment;
    private CaldroidFragment dialogCaldroidFragment;
    ImageView editIcon;
    int dat = 0, mon = 0, yea = 0;
    String month_name = "";

    String timeStamp = "";

    LinearLayout Calender_lay;
    ImageView right_arrow,arrow;
    ExpandableListView expListView;
    String iv = "288dca8258b1dd7c";
    String walletid = "", mobileno = "", tpin = "",tpinhash="", encryptedCurrentTpin = "";
    String Mob_No = "", Wallet_id = "",xmlforSendMoney="";
    Date date1;
    Date date2;
    RelativeLayout rootLay;
    Transactions transactions=new Transactions();
    String key = "YhBCOSXLjOP8xrJpRg9jZq3HjzZwxnmwU5DJfuWTLAA=";
    private ProgressDialog progressDialog = null;
    private String final_to_date="";
    private String final_from_date="";

    public static TransactionStatementTabFragment newInstance(String param1, String param2, String MobileNo, String WalletId) {
        TransactionStatementTabFragment fragment = new TransactionStatementTabFragment();
        Bundle args = new Bundle();
        args.putString("MobileNo", MobileNo);
        args.putString("WalletId", WalletId);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
//            mobileno = getArguments().getString("MobileNo");
//            walletid = getArguments().getString("WalletId");
//
        }
    }


    private void setCustomResourceForDates() {
        Calendar cal = Calendar.getInstance();

        // Min date is last 7 days
        cal.add(Calendar.DATE, -7);
        Date blueDate = cal.getTime();

        // Max date is next 7 days
        cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        Date greenDate = cal.getTime();

        if (caldroidFragment != null) {
            caldroidFragment.setBackgroundResourceForDate(R.color.white,
                    blueDate);
            caldroidFragment.setBackgroundResourceForDate(R.color.white,
                    greenDate);
            caldroidFragment.setTextColorForDate(R.color.black, blueDate);
            caldroidFragment.setTextColorForDate(R.color.black, greenDate);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.transaction_fragment_statement_tab, container, false);
    }





    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*rootLay = (RelativeLayout) view.findViewById(R.id.root_linear_lay_for_statement_tab);
        MyApplication.resetTimer(getActivity());
        rootLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });*/

        arrow = (ImageView) view.findViewById(R.id.Transaction_leftarrow);
        expListView = (ExpandableListView) view.findViewById(R.id.expandableListViewStatement);


        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {

                        return true;
                    }
                }
                return false;
            }
        });
        expListView.setAdapter(listAdapter);// Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

                /* Toast.makeText(getApplicationContext(),
                listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();*/
            }
        });
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                /*Toast.makeText(getActivity().getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();*/

            }
        });





        form_date = (TextView) view.findViewById(R.id.text_view_for_form_date);
        to_date = (TextView) view.findViewById(R.id.text_view_for_to_date);
        cal_info = (TextView) view.findViewById(R.id.text_view_one);
        Calender_lay = (LinearLayout) view.findViewById(R.id.calendar1);
        right_arrow = (ImageView) view.findViewById(R.id.right_arrow_for_statement);
        editIcon = (ImageView) view.findViewById(R.id.imageView12);


        final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");

        caldroidFragment = new CaldroidFragment();

        // //////////////////////////////////////////////////////////////////////
        // **** This is to show customized fragment. If you want customized
        // version, uncomment below line ****
//		 caldroidFragment = new CaldroidSampleCustomFragment();

        // Setup arguments

        // If Activity is created after rotation
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState,
                    "CALDROID_SAVED_STATE");
        }

        // If activity is created from fresh
        else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);

            // Uncomment this to customize startDayOfWeek
            // args.putInt(CaldroidFragment.START_DAY_OF_WEEK,
            // CaldroidFragment.TUESDAY); // Tuesday

            // Uncomment this line to use Caldroid in compact mode
            // args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, false);

            // Uncomment this line to use dark theme
//            args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefaultDark);
            // args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefaultDark);
            caldroidFragment.setArguments(args);
        }

        setCustomResourceForDates();

       /* if(!form_date.equals("")){
            Calender_lay.setBackground(getResources().getDrawable(R.drawable.from_date));
            FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
            t.replace(R.id.calendar1, caldroidFragment);
            t.commit();

            final CaldroidListener listener = new CaldroidListener() {

                @Override
                public void onSelectDate(Date date, View view) {
               *//* Toast.makeText(getActivity(), formatter.format(date),
                        Toast.LENGTH_SHORT).show();*//*
                    Calendar c = Calendar.getInstance();
                    c.setTime(date);
                    dat = c.get(Calendar.DATE);
                    mon = c.get(Calendar.MONTH);
                    yea = c.get(Calendar.YEAR);
                    SimpleDateFormat month_date = new SimpleDateFormat("MMM");
                    month_name = month_date.format(c.getTime());
                    System.out.println(""+dat+":"+month_name+":"+yea);
                    form_date.setText(dat+" "+month_name+" "+yea);
                }

                @Override
                public void onChangeMonth(int month, int year) {
                    String text = "month: " + month + " year: " + year;
              *//*  Toast.makeText(getActivity(), text,
                        Toast.LENGTH_SHORT).show();*//*
                }

                @Override
                public void onLongClickDate(Date date, View view) {
               *//* Toast.makeText(getActivity(),
                        "Long click " + formatter.format(date),
                        Toast.LENGTH_SHORT).show();*//*
                }

                @Override
                public void onCaldroidViewCreated() {
                    if (caldroidFragment.getLeftArrowButton() != null) {
                  *//*  Toast.makeText(getActivity(),
                            "Caldroid view is created", Toast.LENGTH_SHORT)
                            .show();*//*
                    }
                }

            };
            caldroidFragment.setCaldroidListener(listener);
        }
*/


     /*   form_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Attach to the activity

                FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                t.replace(R.id.calendar1, caldroidFragment);
                t.commit();
                final CaldroidListener listener = new CaldroidListener() {

                    @Override
                    public void onSelectDate(Date date, View view) {
               *//* Toast.makeText(getActivity(), formatter.format(date),
                        Toast.LENGTH_SHORT).show();*//*
                        Calendar c = Calendar.getInstance();
                        c.setTime(date);
                        dat = c.get(Calendar.DATE);
                        mon = c.get(Calendar.MONTH);
                        yea = c.get(Calendar.YEAR);
                        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
                        month_name = month_date.format(c.getTime());
                        System.out.println(""+dat+":"+month_name+":"+yea);
                        form_date.setText(dat+" "+month_name+" "+yea);

                    }

                    @Override
                    public void onChangeMonth(int month, int year) {
                        String text = "month: " + month + " year: " + year;
              *//*  Toast.makeText(getActivity(), text,
                        Toast.LENGTH_SHORT).show();*//*
                    }

                    @Override
                    public void onLongClickDate(Date date, View view) {
               *//* Toast.makeText(getActivity(),
                        "Long click " + formatter.format(date),
                        Toast.LENGTH_SHORT).show();*//*
                    }

                    @Override
                    public void onCaldroidViewCreated() {
                        if (caldroidFragment.getLeftArrowButton() != null) {
                  *//*  Toast.makeText(getActivity(),
                            "Caldroid view is created", Toast.LENGTH_SHORT)
                            .show();*//*

                        }
                    }

                };
                caldroidFragment.setCaldroidListener(listener);
            }
        });*/


        /*to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (form_date.getText().equals("From Date")) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please Select FromDate First ")
                            .setTitle("Error !!!");
                    AlertDialog dialog = builder.create();
                    dialog.show();

                } else {

                    FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                    t.replace(R.id.calendar1, caldroidFragment);
                    t.commit();
                    final CaldroidListener listener = new CaldroidListener() {

                        @Override
                        public void onSelectDate(Date date, View view) {
               *//* Toast.makeText(getActivity(), formatter.format(date),
                        Toast.LENGTH_SHORT).show();*//*
                            Calendar c = Calendar.getInstance();
                            c.setTime(date);
                            dat = c.get(Calendar.DATE);
                            mon = c.get(Calendar.MONTH);
                            yea = c.get(Calendar.YEAR);
                            SimpleDateFormat month_date = new SimpleDateFormat("MMM");
                            month_name = month_date.format(c.getTime());
                            System.out.println(""+dat+":"+month_name+":"+yea);
                            to_date.setText(dat+" "+month_name+" "+yea);

                        }

                        @Override
                        public void onChangeMonth(int month, int year) {
                            String text = "month: " + month + " year: " + year;
              *//*  Toast.makeText(getActivity(), text,
                        Toast.LENGTH_SHORT).show();*//*
                            to_date.setText("To Date");
                            form_date.setText("From Date");
                        }

                        @Override
                        public void onLongClickDate(Date date, View view) {
               *//* Toast.makeText(getActivity(),
                        "Long click " + formatter.format(date),
                        Toast.LENGTH_SHORT).show();*//*
                        }

                        @Override
                        public void onCaldroidViewCreated() {
                            if (caldroidFragment.getLeftArrowButton() != null) {
                  *//*  Toast.makeText(getActivity(),
                            "Caldroid view is created", Toast.LENGTH_SHORT)
                            .show();*//*
                            }
                        }
                    };
                    caldroidFragment.setCaldroidListener(listener);
                }


            }
        });*/

        // Attach to the activity
        FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();

// Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override

            public void onSelectDate(Date date, View view) {
                DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
                String Current_date = dateFormat1.format(date);

                if (form_date.getText().equals("From Date"))
                {
                    arrow.setImageResource(R.drawable.pay_merchant_right_arrow);

                    if (date.after(new Date())) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("From date cannot be future date.")
                                .setTitle("mWallet");
                        AlertDialog dialog = builder.create();
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();

                    } else {
                        Calendar c = Calendar.getInstance();
                        c.setTime(date);
                        dat = c.get(Calendar.DATE);
                        mon = c.get(Calendar.MONTH) + 1;
                        yea = c.get(Calendar.YEAR);
                        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
                        month_name = month_date.format(c.getTime());
                        System.out.println("" + dat + ":" + month_name + ":" + yea);
                        form_date.setText(dat + " " + month_name + " " + yea);


                        form_date.setTypeface(null, Typeface.BOLD);
                        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        final_from_date = dateFormat.format(date);
                        try {

                            date1 = dateFormat.parse(dat + "-" + mon + "-" + yea);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(getActivity(),"date : "+final_from_date,Toast.LENGTH_SHORT).show();

                    }
                } else if (date.after(new Date())) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("To Date cannot be future date.")
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } else {
                   DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    final_to_date = dateFormat.format(date);
                    try {

                        Calendar c = Calendar.getInstance();
                        c.setTime(date);
                        dat = c.get(Calendar.DATE);
                        mon = c.get(Calendar.MONTH) + 1;
                        yea = c.get(Calendar.YEAR);
                        date2 = dateFormat.parse(dat + "-" + mon + "-" + yea);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (date1.before(date2) || date1.equals(date2)) {
                        //Toast.makeText(getActivity(),"Please enter valid date 4", Toast.LENGTH_SHORT).show();
                        Calendar c = Calendar.getInstance();
                        c.setTime(date);
                        dat = c.get(Calendar.DATE);
                        mon = c.get(Calendar.MONTH) + 1;
                        yea = c.get(Calendar.YEAR);

                        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
                        month_name = month_date.format(c.getTime());
                        System.out.println("" + dat + ":" + month_name + ":" + yea);

                        to_date.setText(dat + " " + month_name + " " + yea);
                        to_date.setTypeface(null, Typeface.BOLD);
                        Calender_lay.setVisibility(View.GONE);
                        arrow.setVisibility(View.GONE);
                        cal_info.setText("Showing transactions");
                        right_arrow.setVisibility(View.VISIBLE);
                        editIcon.setVisibility(View.VISIBLE);
                        expListView.setAdapter(listAdapter);
                        mobileno= MyApplication.getInstance().getMerchantMobieNo();
                        walletid= MyApplication.getInstance().getmProfile_WalletID_General();
                        MessageDigest digest = null;
                        try
                        {
                            digest = MessageDigest.getInstance("SHA-256");
                            digest.update(tpin.getBytes());
                            tpinhash = bytesToHexString(digest.digest());
                            Log.d("TAD", " tpin result is " + tpinhash);
                        } catch (NoSuchAlgorithmException e1) {

                            e1.printStackTrace();
                        }
                        Format formatter = new SimpleDateFormat("dd-MM-yyyy");
                        String sDate1 = formatter.format(date1);
                        String sDate2 = formatter.format(date2);
                        callRecentTransaction(mobileno,walletid,tpinhash,sDate1,sDate2);
                        editIcon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                              //  Toast.makeText(getActivity(),"Please enter valid date 5", Toast.LENGTH_SHORT).show();
                                // MyApplication.resetTimer(getActivity());
                                FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                                t.replace(R.id.calendar1, caldroidFragment);
                                t.commit();
                                arrow.setVisibility(View.VISIBLE);
                                arrow.setImageResource(R.drawable.pay_merchant_left_arrow);
                                form_date.setText("From Date");
                                to_date.setText("To Date");
                                Calender_lay.setVisibility(View.VISIBLE);
                                cal_info.setText("Choose the dates to view statements");
                                right_arrow.setVisibility(View.INVISIBLE);
                                if(listAdapter!=null) {
                                    listAdapter.notifyDataSetChanged();
                                }
                                editIcon.setVisibility(View.INVISIBLE);
                            }
                        });

                  /*list.setVisibility(View.VISIBLE);*/



                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("To Date must be same or greater than From Date.")
                                .setTitle("mWallet");
                        AlertDialog dialog = builder.create();
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();
                    }

                }
            }


               /* Calendar c = Calendar.getInstance();
                c.setTime(date);
                dat = c.get(Calendar.DATE);
                mon = c.get(Calendar.MONTH);
                yea = c.get(Calendar.YEAR);
                SimpleDateFormat month_date = new SimpleDateFormat("MMM");
                month_name = month_date.format(c.getTime());
                System.out.println(""+dat+":"+month_name+":"+yea);
                to_date.setText(dat+" "+month_name+" "+yea);*/


            @Override
            public void onChangeMonth(int month, int year) {
                String text = "month: " + month + " year: " + year;
                /* to_date.setText("To Date");
                 form_date.setText("From Date");*/
            }

            @Override
            public void onLongClickDate(Date date, View view) {

            }

            @Override
            public void onCaldroidViewCreated() {
                if (caldroidFragment.getLeftArrowButton() != null) {
                   /*  Toast.makeText(getActivity(),
                    "Caldroid view is created", Toast.LENGTH_SHORT)
                    .show();*/
                }
            }
        };


        /*form_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                t.replace(R.id.calendar1, caldroidFragment);
                t.addToBackStack(null);
                t.commit();
                form_date.setText("From Date");
                to_date.setText("To Date");
                Calender_lay.setVisibility(View.VISIBLE);
                cal_info.setText("Choose the dates to view statements");
                right_arrow.setVisibility(View.INVISIBLE);

                listAdapter.notifyDataSetChanged();
            }
        });
*/
        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);
    }
    public class MyAsyncTaskForRecentTransaction extends AsyncTask<String, Integer, String> {
        String result = "";

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            System.out.println("Response : " + s);

            progressDialog.dismiss();

            //Parse your response here
            if(result.equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                progressDialog.dismiss();
            }else {

                try {
                    AESecurity aess = new AESecurity();
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted)
                        {
                            Log.d("TAG", "decrypted Transaction200=" + decrypted);
                            Log.d("TAG","mobileNumber Pay200="+mobileNumber);
                            ParseResponse parse = new ParseResponse();
                            InputSource is = new InputSource(new StringReader(decrypted));
                            MpsXml mpsxml = parse.parseXML(is);

                            if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success")) {

                                transactions.setTransaction(mpsxml.getResponsedetails().getTransactions().getTransaction());
                                listAdapter = new ExpandableTransactionListAdapter(getActivity(),transactions.getTransaction());
                                expListView.setAdapter(listAdapter);// Listview Group expanded listener
                                progressDialog.dismiss();

                                /*Intent toLogin = new Intent(ChangeTPin.this,MainActivity.class);
                                startActivity(toLogin);
*/
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getReason())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                //progressDialog.dismiss();
                            }
                        }
                    });
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException |
                        InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    // utility function for encryption of MPIN
    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }
    public void  callRecentTransaction(final String mobileno, String walletid, String tpinhash, String sDate1, String sDate2){

        progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
        xmlforSendMoney =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>"+timeStamp+"</Timestamp>" +
                        "<SessionId>"+ GlobalVariables.sessionId+"</SessionId>" +
                        "<ServiceProvider>"+getResources().getString(R.string.serviceprovider)+"</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>TXNHIST</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<MobileNumber>"+mobileno+"</MobileNumber>" +
                        "<WalletId>"+walletid+"</WalletId>" +
                        "<FromDate>"+sDate1+"</FromDate>" +
                        "<ToDate>"+sDate2+"</ToDate>" +
                        "<ResponseURL>"+ GlobalVariables.url+"</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        " </RequestDetails>" +
                        "</MpsXml>";
        Log.d("TAG","xmlforSendMoney="+xmlforSendMoney);
        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforSendMoney, MyApplication.getInstance().getKey(),iv,new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXMLforSendMoney)
                {
                    String final_String_for_SendMoney = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + mobileno + "|" + encryptedXMLforSendMoney;
                    new MyAsyncTaskForRecentTransaction().execute(final_String_for_SendMoney);
                }
            });
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }
}