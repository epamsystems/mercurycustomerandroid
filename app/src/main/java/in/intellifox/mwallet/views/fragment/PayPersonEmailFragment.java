package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import in.intellifox.mwallet.R;

/**
 * Created by Owner on 9/15/2015.
 */
public class PayPersonEmailFragment extends Fragment {

    EditText edtName,edtEmailId,edtAmount,edtTPin;
    TextView tvMarkFavourite;
    Button sendMoney;
    LinearLayout requestMoneyLayout;
    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);

        sendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View popupView = getLayoutInflater(savedInstanceState).inflate(R.layout.custom_popup, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);

                TextView tv1=(TextView)popupView.findViewById(R.id.txt_note1);
                tv1.setText("Send AED 500 to");
                TextView tv2=(TextView)popupView.findViewById(R.id.txt_note2);
                tv2.setText("Abcdefg Hijk Lmnaop");
                TextView tv3=(TextView)popupView.findViewById(R.id.txt_note3);
                tv3.setText("Ok to proceed");
                tv3.setTextSize(12);

                Button btnDismiss = (Button)popupView.findViewById(R.id.button_ok_on_popup);
                btnDismiss.setOnClickListener(new Button.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        popupWindow.dismiss();
                        PayPersonSuccess rms=new PayPersonSuccess();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, rms).commit();
                    }});

                popupWindow.showAtLocation(requestMoneyLayout, Gravity.CENTER,0,0);
            }
        });


    }

    private void LayoutInitialization(View view) {
        sendMoney=(Button)view.findViewById(R.id.button_requestmoney_sendmoney);
        requestMoneyLayout=(LinearLayout)view.findViewById(R.id.request_money_email_second_part);
        edtName=(EditText)view.findViewById(R.id.edit_text_name_reqMoney);
        edtEmailId=(EditText)view.findViewById(R.id.edit_text_email_reqMoney);
        edtAmount=(EditText)view.findViewById(R.id.edit_text_amount_reqMoney);
        edtTPin=(EditText)view.findViewById(R.id.edit_text_tpin_reqMoney);
        tvMarkFavourite=(TextView)view.findViewById(R.id.text_view_favourite_reqMoney);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.request_money_email_fragment, container, false);
    }
}
