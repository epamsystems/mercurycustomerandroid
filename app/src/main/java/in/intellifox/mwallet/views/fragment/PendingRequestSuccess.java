package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/23/2015.
 */
public class PendingRequestSuccess extends Fragment {

    private DashBoard dashBoard;
    ImageView dashboard;
    TextView txtTxnID, txtName, txtMessage, txtAMount, txtDate;

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dashboard = (ImageView) view.findViewById(R.id.airtime_topup_succes_dashboard);

        txtName = (TextView) view.findViewById(R.id.txtName);
        txtMessage = (TextView) view.findViewById(R.id.txtMessage);
        txtTxnID = (TextView) view.findViewById(R.id.txtTxnID);
        txtAMount = (TextView) view.findViewById(R.id.txtAmount);
        txtDate = (TextView) view.findViewById(R.id.txtDate);


        txtName.setText(getArguments().getString("Name"));
        txtMessage.setText(getArguments().getString("Message"));
        txtTxnID.setText(getArguments().getString("TxnId"));
        txtAMount.setText(getArguments().getString("Amount"));
        txtDate.setText(getArguments().getString("Timestamp"));

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dashBoard = new DashBoard();
                Bundle bundle = new Bundle();
                dashBoard.setArguments(bundle);
                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                ((DrawerMain) getActivity()).setUpActionBar();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, dashBoard).commit();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.request_money_success, container, false);
    }
}
