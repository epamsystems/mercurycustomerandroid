package in.intellifox.mwallet.views.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/10/2015.
 */
public class PayMerchantMainFragment extends Fragment {

    private PayMerchantMyCardFragment m_mycards_tab;
    private PayMerchantWalletFragment m_wallet_tab;
    private PayMerchantQrCodeMain m_qrcode_tab;
    ImageView mywallet, mycards, qrcode;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mywallet = (ImageView) view.findViewById(R.id.mywallet_layout_position_0);
        mycards = (ImageView) view.findViewById(R.id.mycards_layout_position_1);
        qrcode = (ImageView) view.findViewById(R.id.qrcode_layout_position_2);
        GlobalVariables.fragmentManager = getFragmentManager();
        final RelativeLayout frag_container = (RelativeLayout) view.findViewById(R.id.paymerchantmain_fragment_container1);
        PayMerchantWalletFragment mp = new PayMerchantWalletFragment();

        if (getArguments()!=null &&!TextUtils.isEmpty(getArguments().getString("merchantID"))) {
            Bundle bundle = new Bundle();
            bundle.putString("merchantID", getArguments().getString("merchantID"));
            bundle.putString("merchantName", getArguments().getString("merchantName"));

            mp.setArguments(bundle);
        }
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.paymerchantmain_fragment_container1, mp).commitAllowingStateLoss();

        mywallet.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                m_wallet_tab = new PayMerchantWalletFragment();
                Bundle bundle = new Bundle();

                m_wallet_tab.setArguments(bundle);
                mywallet.setImageResource(R.drawable.pay_merchant_mywallet_selected);
                mycards.setImageResource(R.drawable.pay_merchant_paybycard_deselected);
                qrcode.setImageResource(R.drawable.pay_merchant_qrcode_deselected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.paymerchantmain_fragment_container1, m_wallet_tab).commitAllowingStateLoss();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });

        mycards.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                m_mycards_tab = new PayMerchantMyCardFragment();
                Bundle bundle1 = new Bundle();
                m_mycards_tab.setArguments(bundle1);
                mywallet.setImageResource(R.drawable.pay_merchant_mywallet_deselected);
                mycards.setImageResource(R.drawable.pay_merchant_paybycard_selected);
                qrcode.setImageResource(R.drawable.pay_merchant_qrcode_deselected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.paymerchantmain_fragment_container1, m_mycards_tab).commitAllowingStateLoss();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });

        qrcode.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);

                }
                m_qrcode_tab = new PayMerchantQrCodeMain();
                Bundle bundle1 = new Bundle();
                m_qrcode_tab.setArguments(bundle1);
                mywallet.setImageResource(R.drawable.pay_merchant_mywallet_deselected);
                mycards.setImageResource(R.drawable.pay_merchant_paybycard_deselected);
                qrcode.setImageResource(R.drawable.pay_merchant_qrcode_selected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.paymerchantmain_fragment_container1, m_qrcode_tab).commitAllowingStateLoss();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }

            }
        });
    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain) getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Pay a Merchant", false, "", false, "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pay_merchant_main, container, false);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {


                m_qrcode_tab = new PayMerchantQrCodeMain();
                Bundle bundle1 = new Bundle();
                m_qrcode_tab.setArguments(bundle1);
                mywallet.setImageResource(R.drawable.pay_merchant_mywallet_deselected);
                mycards.setImageResource(R.drawable.pay_merchant_paybycard_deselected);
                qrcode.setImageResource(R.drawable.pay_merchant_qrcode_selected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.paymerchantmain_fragment_container1, m_qrcode_tab).commitAllowingStateLoss();

            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
