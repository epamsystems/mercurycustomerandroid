package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.intellifox.mwallet.R;

/**
 * Created by Owner on 9/10/2015.
 */
public class PayMerchantMyCardFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){

        View v=inflater.inflate(R.layout.pay_merchant_mycard_fragment,container,false);

        return v;

    }

}
