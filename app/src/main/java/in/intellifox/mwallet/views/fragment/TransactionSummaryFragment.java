package in.intellifox.mwallet.views.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;


/**
 * Created by utkarsh on 7/21/2015.
 */
public class TransactionSummaryFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String CONTAINER_ID = "container_id";

    ViewPager pager;
    CharSequence Titles[]={"Recent","Statement"};
    int Numboftabs =2;
    private TransactionRecentTabFragment m_recent_tab_fragment;
    private TransactionStatementTabFragment m_statement_tab_fragment;
    ImageView for_recent_tab,for_statement_tab,arrow;
    RelativeLayout rootLay;

    // TODO: Rename and change types of parameters
    private String mParam1,mParam2;
    private int mParam3;
    String wallet_id="",Mobile_no="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getInt(ARG_PARAM3);
            wallet_id = getArguments().getString("walletid");
            Mobile_no = getArguments().getString("mobileno");
        }
    }
    public static TransactionSummaryFragment newInstance(int param1, String param2,int param3,String MobileNo,String WalletId) {
        TransactionSummaryFragment fragment = new TransactionSummaryFragment();
        Bundle args = new Bundle();
        args.putInt(CONTAINER_ID, param1);
        args.putString(ARG_PARAM2, param2);
        args.putInt(ARG_PARAM3,param3);
        args.putString("mobileno",MobileNo);
        args.putString("walletid", WalletId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        for_recent_tab = (ImageView)view.findViewById(R.id.layout_position_0);
        for_statement_tab = (ImageView)view.findViewById(R.id.layout_position_1);
        final RelativeLayout frag_container = (RelativeLayout)view.findViewById(R.id.fragment_container);

        if(mParam3 == 0){
            for_recent_tab.setImageResource(R.drawable.transaction_recent_selected);
            for_statement_tab.setImageResource(R.drawable.transaction_statement_deselected);
//            m_recent_tab_fragment =new TransactionRecentTabFragment();
//            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, m_recent_tab_fragment).commit();

            //TODO PRAVIN
            m_recent_tab_fragment =new TransactionRecentTabFragment();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
//            fragmentTransaction.addToBackStack(m_recent_tab_fragment.getClass()
//                    .getName());
            fragmentTransaction.replace(R.id.fragment_container, m_recent_tab_fragment);
            fragmentTransaction.commit();
        }else if(mParam3 == 1){
            for_recent_tab.setImageResource(R.drawable.transaction_recent_deselected);
            for_statement_tab.setImageResource(R.drawable.transaction_statement_selected);
            m_statement_tab_fragment =new TransactionStatementTabFragment();
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, m_statement_tab_fragment).commit();
        }

        if(mParam3==3){
            TransactionRecentTabFragment re =new TransactionRecentTabFragment();
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,re).commit();
        }

        for_recent_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyApplication.resetTimer(getActivity());
                for_recent_tab.setImageResource(R.drawable.transaction_recent_selected);
                for_statement_tab.setImageResource(R.drawable.transaction_statement_deselected);
                m_recent_tab_fragment = new TransactionRecentTabFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, m_recent_tab_fragment).commit();
            }
        });

        for_statement_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyApplication.resetTimer(getActivity());
                for_recent_tab.setImageResource(R.drawable.transaction_recent_deselected);
                for_statement_tab.setImageResource(R.drawable.transaction_statement_selected);
                m_statement_tab_fragment = new TransactionStatementTabFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, m_statement_tab_fragment).commit();
            }

        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Transaction Summary",false,"",false,"");
    }

    // set your layout here for this fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.transaction_summary_fragment, container, false);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        // mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
