package in.intellifox.mwallet.views.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by SADIQ on 7/23/2015.
 */
public class EditTextOpensansRegular extends EditText {
    public EditTextOpensansRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditTextOpensansRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextOpensansRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/opensansregular.ttf");
        setTypeface(tf ,1);

    }
}
