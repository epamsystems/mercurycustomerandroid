package in.intellifox.mwallet.views.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Beneficiarys;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.adapter.CustomListFavouriteRequestMoney;

/**
 * Created by Owner on 9/15/2015.
 */
public class RequestMoneyFavouriteFragment extends Fragment {

    ListView favouriteList;
    ImageView tick;
    LinearLayout mainLayout;
    private String timeStamp= "";
    private String xmlforBeneficiary= "";
    String bmobile="9200000011";
    String btpin="111111";
    String iv = "288dca8258b1dd7c";
    RequestMoneyFacebookListInfoFragment requestMoneyFacebookListInfoFragment;
    private ProgressDialog progressDialog;
    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        bmobile= MyApplication.getInstance().getMerchantMobieNo();
        CallBeneficiaryDetails(bmobile, btpin);

        // Listview on item click listener
        favouriteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tick=(ImageView)view.findViewById(R.id.image_view_of_favourite_tick);
                tick.setImageResource(R.drawable.request_money_selected_tick);
                requestMoneyFacebookListInfoFragment =new RequestMoneyFacebookListInfoFragment();
                Bundle bundle=new Bundle();
                bundle.putInt("position", position);
                requestMoneyFacebookListInfoFragment.setArguments(bundle);
                //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, requestMoneyFacebookListInfoFragment).commit();

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.addToBackStack(requestMoneyFacebookListInfoFragment.getClass()
                        .getName());
                fragmentTransaction.add(R.id.request_money_main_fragment_container, requestMoneyFacebookListInfoFragment);
                fragmentTransaction.commit();

            }
        });
    }

    public void CallBeneficiaryDetails( final String Nomobile,String tpin) {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
        xmlforBeneficiary = "<MpsXml>" +
                "<Header>" +
                "<ChannelId>APP</ChannelId>" +
                "<Timestamp>" + timeStamp + "</Timestamp>" +
                "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                "<ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                "</Header>" +
                "<Request>" +
                "<RequestType>ListBene</RequestType>" +
                "<UserType>CU</UserType>" +
                "</Request>" +
                "<RequestDetails>" +
                "<MobileNumber>" + Nomobile + "</MobileNumber>" +
                "<ResponseURL>" + GlobalVariables.url + "</ResponseURL>" +
                "<ResponseVar>mobileResponseXML</ResponseVar>" +
                "</RequestDetails>" +
                "</MpsXml>";
        Log.v("xml", xmlforBeneficiary);
        Log.d("TAG", "xmlforBeneficiary 100=" + xmlforBeneficiary.toString());
        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforBeneficiary, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption()
            {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXML)
                {
                    String final_String_for_beneficiary = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + Nomobile + "|" + encryptedXML;
                    Log.v("Full URL ", "Final URL For bank details : " + final_String_for_beneficiary);
                    Log.d("TAG", "final_String_for_beneficiary 101=" + final_String_for_beneficiary.toString());
                    new MyAsyncTaskforBeneficiary().execute(final_String_for_beneficiary);
                }
            } );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public class MyAsyncTaskforBeneficiary extends AsyncTask<String,Integer,String> {
        String result = "";
//         private ProgressDialog progressDialog = null;

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException | ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT Bank: " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);
        }

        @Override
        protected void onPostExecute(String s1) {
            super.onPostExecute(s1);
            String decryptedString = "";

            System.out.println("Response Beneficiary: " + s1);
            final MpsXml[] mpsxml = new MpsXml[1];
            final ParseResponse[] parse = new ParseResponse[1];
            try {
                AESecurity aess = new AESecurity();
                try {
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption()
                    {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted)
                        {
                            if (decrypted.equals(""))
                            {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplicationContext());
                                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                //dialog.setCanceledOnTouchOutside(true);
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                            }
                            else {
                                Log.d("TAG", "decrypted 12=" + decrypted);
                                parse[0] = new ParseResponse();
                                InputSource is = new InputSource(new StringReader(decrypted));
                                mpsxml[0] = parse[0].parseXML(is);
                                if (mpsxml[0].getResponse().getResponseType().equalsIgnoreCase("Success"))
                                {
                                    String reason=mpsxml[0].getResponsedetails().getReason();
                                    Log.d("TAG", "reason 19=" + reason);
                                    MyApplication.getInstance().setGeneralBalance(mpsxml[0].getResponsedetails().getBalance());
                                    Beneficiarys beneficiarys = new Beneficiarys();
                                    beneficiarys.setBeneficiary(mpsxml[0].getResponsedetails().getBeneficiarys().getBeneficiary());
                                    GlobalVariables.beneficiaryArrayList=beneficiarys.getBeneficiary();
                                    CustomListFavouriteRequestMoney navListAdaptercenter=new CustomListFavouriteRequestMoney(getActivity().getApplicationContext(), GlobalVariables.beneficiaryArrayList);
                                    favouriteList.setAdapter(navListAdaptercenter);
                                    progressDialog.dismiss();
                                }
                                else {
                                    String reason=mpsxml[0].getResponsedetails().getReason();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage(mpsxml[0].getResponsedetails().getReason())
                                            .setTitle("mWallet");
                                    AlertDialog dialog = builder.create();
                                    dialog.setCanceledOnTouchOutside(true);
                                    dialog.show();
                                }
                                progressDialog.dismiss();
//                                Intent dashBoard = new Intent(getActivity(), DrawerMain.class);
//                                startActivity(dashBoard);
                                //mWalletBalance,bankName,cityName,accountNo
                            }
                        }
                    });
                } catch (InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException |
                        BadPaddingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                e.printStackTrace();
            }
        }
    }

    private void LayoutInitialization(View view) {
        favouriteList=(ListView)view.findViewById(R.id.request_money_favourite_listview);
        mainLayout=(LinearLayout)view.findViewById(R.id.main_layout);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.request_money_favourite_fragment, container, false);
    }
}
//RequestMoneySuccess rms=new RequestMoneySuccess();
//                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, rms).commit();
