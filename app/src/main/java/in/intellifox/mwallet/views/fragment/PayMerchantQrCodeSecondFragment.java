package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.utilities.DecimalDigitsInputFilter;

/**
 * Created by Owner on 9/12/2015.
 */
public class PayMerchantQrCodeSecondFragment extends Fragment {

    Button byWallet,byCard;
    EditText merchantID,amount;
    String sMerchantId,sAmount;
    PayMerchantQrCodeWalletFragment payMerchantQrCodeWalletFragment;
    PayMerchantQrCodeMyCardFragment payMerchantQrCodeMyCardFragment;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        Log.d("TAG","Enter  to QR code");
        try {
            sMerchantId=getArguments().getString("merchantId");
            sAmount=getArguments().getString("amount");
            Log.d("TAG","sMerchantId  to QR code"+sMerchantId);
            Log.d("TAG","sAmount  to QR code"+sAmount);
            merchantID.setText(sMerchantId);
            amount.setText(sAmount);
        }catch (Exception e)
        {

        }
        byCard.setVisibility(View.GONE);
        byCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payMerchantQrCodeMyCardFragment=new PayMerchantQrCodeMyCardFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.paymerchantmain_fragment_container1, payMerchantQrCodeMyCardFragment).commit();

                FragmentManager fragmentManager =getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(payMerchantQrCodeMyCardFragment.getClass().getName());
                fragmentTransaction.replace(R.id.paymerchantmain_fragment_container1, payMerchantQrCodeMyCardFragment);
                fragmentTransaction.commit();
            }
        });

        byWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payMerchantQrCodeWalletFragment=new PayMerchantQrCodeWalletFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.paymerchantmain_fragment_container1, payMerchantQrCodeWalletFragment).commit();
                Bundle bundle=new Bundle();
                bundle.putString("merchantId",sMerchantId);
                bundle.putString("amount",amount.getText().toString());
                payMerchantQrCodeWalletFragment.setArguments(bundle);
                FragmentManager fragmentManager =getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(payMerchantQrCodeWalletFragment.getClass().getName());
                fragmentTransaction.replace(R.id.dashboard_container, payMerchantQrCodeWalletFragment);
                fragmentTransaction.commit();
            }
        });
    }

    private void LayoutInitialization(View view) {
        byCard=(Button)view.findViewById(R.id.qrcode_paybycard);
        byWallet=(Button)view.findViewById(R.id.qrcode_paybywallet);
        merchantID=(EditText)view.findViewById(R.id.edit_text_qrcode_merchantId);
        amount=(EditText)view.findViewById(R.id.edit_text_qrcode_amount);
        amount.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(15,2)});
        final RelativeLayout frag_container = (RelativeLayout) view.findViewById(R.id.paymerchantmain_fragment_container1);

        LinearLayout main_layout= (LinearLayout) view.findViewById(R.id.main_layout);
        main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pay_merchant_qrcode_second_fragment, container, false);
    }
}
