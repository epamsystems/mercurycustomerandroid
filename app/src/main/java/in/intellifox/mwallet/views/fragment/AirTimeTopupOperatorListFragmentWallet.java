package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.adapter.CustomListAirTime;


public class AirTimeTopupOperatorListFragmentWallet extends Fragment {

    private AirTimeTopupMain airTimeTopupMain;
    private AirTimeTopupLocationListFragmentWallet airTimeTopupLocationListFragmentWallet;
    ListView OperatorList;
    ImageView paybycardscross;
    LinearLayout rootlay;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        paybycardscross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airTimeTopupMain = new AirTimeTopupMain();
                Bundle bundle = new Bundle();
                airTimeTopupMain.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupMain).commit();
            }
        });

        CustomListAirTime navListAdaptercenter=new CustomListAirTime(getActivity().getApplicationContext());
        OperatorList.setAdapter(navListAdaptercenter);
        OperatorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                airTimeTopupLocationListFragmentWallet=new AirTimeTopupLocationListFragmentWallet();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupLocationListFragmentWallet).commit();
            }
        });
    }

    private void LayoutInitialization(View view) {
        OperatorList=(ListView)view.findViewById(R.id.airtimetopup_list_operator);
        paybycardscross = (ImageView)view.findViewById(R.id.airtimetopup_list_operator_cross);
        rootlay=(LinearLayout)view.findViewById(R.id.root_linear_lay_for_airtime_listoperator);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.airtime_topup_listoperator, container, false);
    }
}