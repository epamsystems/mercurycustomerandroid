package in.intellifox.mwallet.views.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.List;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Owner on 9/10/2015.
 */
public class PayMerchantQrCodeMain extends Fragment implements ZXingScannerView.ResultHandler {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private static final String FLASH_STATE = "FLASH_STATE";
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private static final String SELECTED_FORMATS = "SELECTED_FORMATS";
    private static final String CAMERA_ID = "CAMERA_ID";
    String QR_Amount = "", QR_Name = "", QR_Merchant_Id = "", MOB_Number = "";
    String merchantId = "", merchant_name = "", mobile_No = "", amount1 = "";
    private ZXingScannerView mScannerView;
    private boolean mFlash;
    private boolean mAutoFocus;
    private ArrayList<Integer> mSelectedIndices;
    private int mCameraId = -1;
    PayMerchantQrCodeSecondFragment qrCodeChildFragment;
    ImageView qrCodeImage;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReceivePaymentCamera.
     */
    // TODO: Rename and change types and number of parameters
    public static PayMerchantQrCodeMain newInstance(String param1, String param2) {
        PayMerchantQrCodeMain fragment = new PayMerchantQrCodeMain();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {


        } else {
            mScannerView.setResultHandler(this);
            mScannerView.startCamera();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.v("TAG"," Camera is stopped");

    }

    @Override
    public void onStop() {
        super.onStop();
        
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED)
            mScannerView.stopCamera();


    }

    public PayMerchantQrCodeMain() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        qrCodeImage = (ImageView) view.findViewById(R.id.qrcode_image);
/*        qrCodeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrCodeChildFragment=new PayMerchantQrCodeSecondFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.paymerchantmain_fragment_container1,qrCodeChildFragment).commit();

            }
        });*/


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mScannerView = new ZXingScannerView(getActivity());
        if (savedInstanceState != null) {
            mFlash = savedInstanceState.getBoolean(FLASH_STATE, false);
            mAutoFocus = savedInstanceState.getBoolean(AUTO_FOCUS_STATE, true);
            mSelectedIndices = savedInstanceState.getIntegerArrayList(SELECTED_FORMATS);
            mCameraId = savedInstanceState.getInt(CAMERA_ID, -1);
        } else {
            mFlash = false;
            mAutoFocus = true;
            mSelectedIndices = null;
            mCameraId = -1;
        }
        setUpFormats();
        return mScannerView;
        //return inflater.inflate(R.layout.fragment_receive_payment_camera, container, false);
    }

    private void setUpFormats() {
        List<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
        formats.add(BarcodeFormat.QR_CODE);
        if (mScannerView != null) {
            mScannerView.setFormats(formats);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void handleResult(Result rawResult) {


        System.out.println("Result : " + rawResult);
     /*  MerchantID=1000001211,MerchantName=Balchandran,Amount=100,MobileNo=8000000171
     * MerchantID=19,MerchantName=Test
     * */
        try {

            String R1 = rawResult.getText().toString();
            String[] s = R1.split(",");

            try {
                if (s != null && s.length > 0) {
                    String[] firstname = s[0].split("=");
                    merchantId = firstname[1].toString();
                }
            } catch (Exception e) {

            }
            try {
                if (s != null && s.length > 1) {
                    String[] lastname = s[1].split("=");
                    merchant_name = lastname[1].toString();
                }
            } catch (Exception e) {

            }
            try {
                if (s != null && s.length > 2) {
                    String[] amount = s[2].split("=");
                    amount1 = amount[1].toString();
                }
            } catch (Exception e) {

            }
            try {
                if (s != null && s.length > 3) {
                    String[] mobileno = s[3].split("=");
                    mobile_No = mobileno[1].toString();
                }
            } catch (Exception e) {

            }

            MyApplication.resetTimer(getActivity());
            qrCodeChildFragment = new PayMerchantQrCodeSecondFragment();
            //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, qrCodeChildFragment).addToBackStack(null).commit();
            Bundle bundle = new Bundle();
            bundle.putString("merchantId", merchantId);
            bundle.putString("amount", amount1);
            Log.d("TAG", "sMerchantId  to QR code" + merchantId);
            Log.d("TAG", "sAmount  to QR code" + amount1);
            qrCodeChildFragment.setArguments(bundle);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.addToBackStack(qrCodeChildFragment.getClass().getName());
            fragmentTransaction.replace(R.id.dashboard_container, qrCodeChildFragment);
            fragmentTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

  /*      try{

            String R1 = rawResult.getText().toString();
            String [] s = R1.split(",");
            System.out.println("Result : "+s);
            String [] merchantid = s[0].split("=");
            QR_Merchant_Id =   merchantid[1].toString();
            String[]merchantname = s[1].split("=");
            QR_Name = merchantname[1].toString();
            String []amount = s[2].split("=");
            QR_Amount = amount[1].toString();
            String [] mob = s[3].split("=");
            MOB_Number = mob[1].toString();
            System.out.println("MOB_Number: "+MOB_Number);

            mReceivePaymentFragment = ReceivePaymentFragment.newInstance(QR_Merchant_Id,QR_Name,QR_Amount,MOB_Number);



            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, mReceivePaymentFragment).addToBackStack(null).commit();
        }catch(Exception e){
            e.printStackTrace();
        }*/

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
