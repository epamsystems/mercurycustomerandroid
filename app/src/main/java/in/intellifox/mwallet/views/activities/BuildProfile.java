package in.intellifox.mwallet.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.fragment.BuildProfileMainFragment;

/**
 * Created by Owner on 9/30/2015.
 */
public class BuildProfile extends ActionBarActivity {

    Fragment buildProfileMainFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment);
        GlobalVariables.mActivity=BuildProfile.this;
        buildProfileMainFragment=new BuildProfileMainFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.main_fragment,buildProfileMainFragment).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG1", "requestCode 4=" + requestCode);
        Log.d("TAG1", "resultCode 41=" + resultCode);
        if (resultCode == Activity.RESULT_OK)
        {
            BuildProfileMainFragment.activityResult(requestCode,data);
        }
    }
}
