package in.intellifox.mwallet.views.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Owner on 9/2/2015.
 */
public class EditTextMuseosansThreeHundred extends EditText {

    public EditTextMuseosansThreeHundred(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public EditTextMuseosansThreeHundred(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextMuseosansThreeHundred(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/museo_sans_three.otf");
        setTypeface(tf, 1);

    }
}
