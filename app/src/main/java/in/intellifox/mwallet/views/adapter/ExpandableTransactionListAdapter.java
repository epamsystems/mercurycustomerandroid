package in.intellifox.mwallet.views.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Transaction;

/**
 * Created by utkarsh on 7/22/2015.
 */
public class ExpandableTransactionListAdapter extends BaseExpandableListAdapter {

    private Activity _context;
    //    private List<String> _date = null;
//    private List<String> _transaction_id = null;
//    private List<String> _amount = null;
    private ArrayList<Transaction> _transactionArrayList = new ArrayList<Transaction>();
    LayoutInflater infalInflater;

    // child data in format of header title, child title
//    private HashMap<String, List<String>> _listDataChild = null;
//    private HashMap<String, List<String>> _listDataChild_amount = null;

    public ExpandableTransactionListAdapter(Activity activity, ArrayList<Transaction> transaction) {
        this._context = activity;
        this._transactionArrayList = transaction;
        infalInflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        try {
            return _transactionArrayList.get(groupPosition).getOtherDetails().getMerchant().get(childPosititon);
        } catch (Exception e) {
            return null;
        }


    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

//        final String childText = (String) getChild(groupPosition, childPosition);
        ChildViewHolder childViewHolder;
        if (convertView == null) {

            convertView = infalInflater.inflate(R.layout.transaction_childrow, null);

            childViewHolder = new ChildViewHolder();

            childViewHolder.nameTitle = (TextView) convertView.findViewById(R.id.text_view_title_customer_name);
            childViewHolder.mobileTitle = (TextView) convertView.findViewById(R.id.text_view_title_mobile_number);
            childViewHolder.nameValue = (TextView) convertView.findViewById(R.id.text_view_customer_name);
            childViewHolder.mobileValue = (TextView) convertView.findViewById(R.id.text_view_mobile_number);

            convertView.setTag(childViewHolder);
        } else {
            childViewHolder = (ChildViewHolder) convertView.getTag();
        }

        Log.v("TAG", " Type is " + _transactionArrayList.get(groupPosition).getTxnType());

        try {
            if (_transactionArrayList.get(groupPosition).getTxnType().equals("P2M") || _transactionArrayList.get(groupPosition).getTxnType().equals("P2MREV")) {
                // Log.v("TAG", " P2M");
                childViewHolder.nameTitle.setText("Merchant Name");
                childViewHolder.mobileTitle.setText("Mobile Number");
                childViewHolder.nameValue.setText((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getMerchant().get(0).getFirstName() + " " + ((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getMerchant().get(0).getLastName()));
                childViewHolder.mobileValue.setText((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getMerchant().get(0).getMobileNumber());
            } else if (_transactionArrayList.get(groupPosition).getTxnType().equals("WITH")) {
                //Log.v("TAG", " WITH");
                childViewHolder.nameTitle.setText("Agent Name");
                childViewHolder.mobileTitle.setText("Mobile Number");
                childViewHolder.nameValue.setText((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getAgents().get(0).getFirstName() + " " + ((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getAgents().get(0).getLastName()));
                childViewHolder.mobileValue.setText((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getAgents().get(0).getMobileNumber());
            } else if (_transactionArrayList.get(groupPosition).getTxnType().equals("REQ")) {
                // Log.v("TAG", " REQ");
                childViewHolder.nameTitle.setText("Customer Name");
                childViewHolder.mobileTitle.setText("Mobile Number");
                childViewHolder.nameValue.setText((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getCustomer().get(0).getFirstName() + " " + ((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getCustomer().get(0).getLastName()));
                childViewHolder.mobileValue.setText((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getCustomer().get(0).getMobileNumber());
            } else if (_transactionArrayList.get(groupPosition).getTxnType().equals("CASHBACK")) {
                //Log.v("TAG", " CASHBACK");
                childViewHolder.nameTitle.setText("Cashback received");
                //childViewHolder.nameTitle.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.checkbox_on_background, 0, 0, 0);
                childViewHolder.mobileTitle.setText("");
                childViewHolder.nameValue.setText("");
                childViewHolder.mobileValue.setText("");

               /* customerName.setVisibility(View.GONE);
                mobileNumber.setVisibility(View.GONE);*/

                // Log.v("TAG", " Customer is available");
                // customerName.setText((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getCustomer().get(0).getFirstName() + " " + ((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getCustomer().get(0).getLastName()));
                // mobileNumber.setText((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getCustomer().get(0).getMobileNumber());
            }/* else if (_transactionArrayList.get(groupPosition).getOtherDetails() != null && _transactionArrayList.get(0).getOtherDetails().getCustomer() != null) {
                childViewHolder.nameTitle.setText("Customer Name");
                childViewHolder.mobileTitle.setText("Mobile Number");
                Log.v("TAG", " Customer is available");
                childViewHolder.nameValue.setText((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getCustomer().get(0).getFirstName() + " " + ((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getCustomer().get(0).getLastName()));
                childViewHolder.mobileValue.setText((String) this._transactionArrayList.get(groupPosition).getOtherDetails().getCustomer().get(0).getMobileNumber());
            }*/
        } catch (Exception e) {

        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;

    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._transactionArrayList.get(groupPosition);
    }


    @Override
    public int getGroupCount() {
        return this._transactionArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {


        if (convertView == null) {


            convertView = infalInflater.inflate(R.layout.transaction_grouprow, null);

        }
        TextView date = (TextView) convertView.findViewById(R.id.text_view_id_date);
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.imageView13);


        date.setText(_transactionArrayList.get(groupPosition).getTxnDate());

        TextView transactionId = (TextView) convertView.findViewById(R.id.text_view_id_transaction_reversal_list);
        transactionId.setText(_transactionArrayList.get(groupPosition).getId());
        TextView amount = (TextView) convertView.findViewById(R.id.text_view_merchant_name);
        String tType = "";
        if (_transactionArrayList.get(groupPosition).getType().equalsIgnoreCase("D")) {
            tType = "Dr";
        } else {
            tType = "Cr";
        }
        amount.setText(_transactionArrayList.get(groupPosition).getAmount() + " " + tType);

//
//        ImageView arrow_icon = (ImageView) convertView.findViewById(R.id.imageView13);
//        RelativeLayout topPart = (RelativeLayout) convertView.findViewById(R.id.relative_lay_top_part);
//        if (isExpanded) {
//            topPart.setBackgroundResource(R.drawable.transaction_first_half_of_expand);
//            arrow_icon.setImageResource(R.drawable.transaction_expand_onclick_icon);
//        } else {
//            topPart.setBackgroundResource(R.drawable.transaction_single_cell_back_before_expand);
//            arrow_icon.setImageResource(R.drawable.transaction_expand_onclick_icon);
//        }

        try {
            if (_transactionArrayList.get(groupPosition).getTxnType().equals("P2M") || _transactionArrayList.get(groupPosition).getTxnType().equals("P2MREV")) {
                imgIcon.setImageResource(R.drawable.p2m);
            } else if (_transactionArrayList.get(groupPosition).getTxnType().equals("WITH")) {
                imgIcon.setImageResource(R.drawable.agent_cash);
            } else if (_transactionArrayList.get(groupPosition).getTxnType().equals("REQ")) {
                imgIcon.setImageResource(R.drawable.p2p);

            } else {
                imgIcon.setImageResource(R.drawable.transaction_after_expanding_img);

                /*else if (_transactionArrayList.get(groupPosition).getOtherDetails() != null && _transactionArrayList.get(0).getOtherDetails().getCustomer() != null) {
                imgIcon.setImageResource(R.drawable.p2p);
            }*/
            }
        } catch (Exception e) {

        }
        RelativeLayout topPart = (RelativeLayout) convertView.findViewById(R.id.relative_lay_top_part);
        if (isExpanded) {
            topPart.setBackgroundResource(R.drawable.transaction_first_half_of_expand);

        } else {
            topPart.setBackgroundResource(R.drawable.transaction_single_cell_back_before_expand);

        }
        return convertView;
    }


    public static class ChildViewHolder {
        private TextView nameTitle;
        private TextView nameValue;
        private TextView mobileTitle;
        private TextView mobileValue;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
