package in.intellifox.mwallet.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.models.MercuryNotification;


/**
 * Created by vishwanathp on 17/3/17.
 */
public class NotificationAdapter extends BaseAdapter{


    Context context;
    List<MercuryNotification> mercuryNotificationList;
    LayoutInflater inflater;

    public NotificationAdapter(Context context, List<MercuryNotification> mercuryNotifications) {
        this.context = context;
        this.mercuryNotificationList = mercuryNotifications;
        inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mercuryNotificationList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if(convertView==null)
        {
            holder=new ViewHolder();

            convertView=inflater.inflate(R.layout.notification_row,parent,false);

            holder.txtDate= (TextView) convertView.findViewById(R.id.txtDate);
            holder.txtMessage= (TextView) convertView.findViewById(R.id.txtMessage);
            holder.txtTime= (TextView) convertView.findViewById(R.id.txtTime);

            convertView.setTag(holder);
        }

        holder= (ViewHolder) convertView.getTag();

        MercuryNotification qnbNotofication=mercuryNotificationList.get(position);

        holder.txtDate.setText(qnbNotofication.getTimestamp());
        holder.txtMessage.setText(qnbNotofication.getMessage());
        holder.txtTime.setText(qnbNotofication.getTime());


        return convertView;
    }


    class ViewHolder
    {
        TextView txtDate,txtMessage,txtTime;

    }

}
