package in.intellifox.mwallet.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;

/**
 * Created by Raju on 18/09/2015.
 */
public class AirTimeTopupMyCard extends Fragment {

    private AirTimeTopupMain airTimeTopupMain;
    ImageView paybywalletedit;
    LinearLayout rootlay;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        paybywalletedit = (ImageView) view.findViewById(R.id.airtimetopupmwalletbalance_editimages);
        rootlay=(LinearLayout)view.findViewById(R.id.root_linear_lay_for_airtime_mycard);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        paybywalletedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airTimeTopupMain = new AirTimeTopupMain();
                Bundle bundle = new Bundle();
                airTimeTopupMain.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupMain).commit();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.airtimemycards, container, false);
    }
}
