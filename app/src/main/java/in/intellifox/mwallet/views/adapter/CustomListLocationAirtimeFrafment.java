package in.intellifox.mwallet.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import in.intellifox.mwallet.R;

/**
 * Created by Raju on 16/09/2015.
 */
public class CustomListLocationAirtimeFrafment extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    TextView textViewNumber,textViewName;
    String name[]={"Location","Location","Location","Location","Location","Location","Location","Location","Location"};
    String mobile[]={"01","02","03","04","05","06","07","08","09"};

    public CustomListLocationAirtimeFrafment(Context mContext) {
        this.mContext = mContext;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return mobile.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        view = new View(mContext);
        view = mInflater.inflate(R.layout.airtime_topup_location_listview,null);
        textViewName=(TextView)view.findViewById(R.id.textView_locatin);
        textViewNumber=(TextView)view.findViewById(R.id.textView_locatin_number);

        textViewName.setText(name[position]);
        textViewNumber.setText(mobile[position]);

        return view;
    }
}

