package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/29/2015.
 */
public class BankAccountSuccess extends Fragment {
    private DashBoard dashBoard;
    ImageView dashboard;
    LinearLayout rootlay;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyApplication.resetTimer(getActivity());
                dashBoard = new DashBoard();
                Bundle bundle = new Bundle();
                dashBoard.setArguments(bundle);
                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                ((DrawerMain)getActivity()).setUpActionBar();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, dashBoard).commit();
            }
        });
    }

    private void LayoutInitialization(View view) {
        dashboard=(ImageView)view.findViewById(R.id.airtime_topup_succes_dashboard);
        rootlay=(LinearLayout)view.findViewById(R.id.root_linear_lay_for_bank_success);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bankaccount_success, container, false);
    }
}
