package in.intellifox.mwallet.views.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import dialogs.OkDialog;
import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.models.PendingRequest;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.activities.DrawerMain;
import in.intellifox.mwallet.views.adapter.PendingRequestAdapter;


/**
 * Created by Owner on 12/12/2015.
 */
public class PendingRequestMain extends Fragment {

    LinearLayout rootlay;
    ListView pendingList;
    private String timeStamp = "";
    private String xmlforBeneficiary = "", mWalletId = "";
    String bmobile = "9200000010";
    String btpin = "1111";
    String iv = "288dca8258b1dd7c";
    private ProgressDialog progressDialog;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pendingList = (ListView) view.findViewById(R.id.pendingrequest_listview);
        rootlay = (LinearLayout) view.findViewById(R.id.root_linear_lay_for_pending_request);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });
        bmobile = MyApplication.getInstance().getMerchantMobieNo();
        mWalletId = MyApplication.getInstance().getmProfile_WalletID_General();
        CallPendingRequest(bmobile, mWalletId);
        GlobalVariables.mActivity = getActivity();
        GlobalVariables.fragmentManager = getFragmentManager();
    }


    public void CallPendingRequest(final String Nomobile, String mWalletId) {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
        xmlforBeneficiary = "<MpsXml>" +
                "<Header>" +
                "<ChannelId>APP</ChannelId>" +
                "<Timestamp>" + timeStamp + "</Timestamp>" +
                "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                "<ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                "</Header>" +
                "<Request>" +
                "<RequestType>ListReqMoney</RequestType>" +
                "<UserType>CU</UserType>" +
                "</Request>" +
                "<RequestDetails>" +
                "<MobileNumber>" + Nomobile + "</MobileNumber>" +
                "<WalletId>" + mWalletId + "</WalletId>" +
                "<ResponseURL>" + GlobalVariables.url + "</ResponseURL>" +
                "<ResponseVar>mobileResponseXML</ResponseVar>" +
                "</RequestDetails>" +
                "</MpsXml>";
        Log.v("xml", xmlforBeneficiary);
        Log.d("TAG", "xmlforBeneficiary 100=" + xmlforBeneficiary.toString());
        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforBeneficiary, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXML) {
                    String final_String_for_beneficiary = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + Nomobile + "|" + encryptedXML;
                    new MyAsyncTaskforPendingRequest().execute(final_String_for_beneficiary);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class MyAsyncTaskforPendingRequest extends AsyncTask<String, Integer, String> {
        String result = "";
//         private ProgressDialog progressDialog = null;

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException | ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT Bank: " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);
        }

        @Override
        protected void onPostExecute(String s1) {
            super.onPostExecute(s1);
            String decryptedString = "";
            progressDialog.dismiss();

            System.out.println("Response Beneficiary: " + s1);
            final MpsXml[] mpsxml = new MpsXml[1];
            final ParseResponse[] parse = new ParseResponse[1];
            try {
                AESecurity aess = new AESecurity();
                try {
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted) {
                            if (decrypted.equals("")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplicationContext());
                                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                //dialog.setCanceledOnTouchOutside(true);
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                            } else {
                                final Serializer serializer = new Persister();
                                try {
                                    PendingRequest pendingRequest = serializer.read(PendingRequest.class, decrypted,false);

                                    if (pendingRequest.getResponse().getResponseType().equals("Success")) {

                                           pendingList.setAdapter(new PendingRequestAdapter(getActivity(),pendingRequest.getRequestDetails().getMoneyRequests().getMoneyRequestList()));
                                    } else {
                                        new OkDialog(getActivity(), pendingRequest.getRequestDetails().getReason(), null, null);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    });
                } catch (InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException |
                        BadPaddingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                e.printStackTrace();
            }
        }
    }


    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain) getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Pending Request", false, "", false, "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pending_request_main, container, false);
    }
}
