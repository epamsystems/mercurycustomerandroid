package in.intellifox.mwallet.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.intellifox.mwallet.R;

/**
 * Created by Raju on 29/09/2015.
 */
public class CustomListOperatorKycDocumentFragment extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    TextView textViewNumber,textViewName;
    private ImageView kycdocument_upload;
   ArrayList<String> fileNameArray=new ArrayList<String>();
    public CustomListOperatorKycDocumentFragment(Context mContext, ArrayList<String> fileNameArray) {
        this.mContext = mContext;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.fileNameArray=fileNameArray;
    }
    @Override
    public int getCount() {
        return fileNameArray.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       /* View view;
        view = new View(mContext);
        view = mInflater.inflate(R.layout.buildprofile_kyc_document_exp_group,null);
        textViewName=(TextView)view.findViewById(R.id.build_profile_kyc_document_document);
        textViewNumber=(TextView)view.findViewById(R.id.build_profile_kyc_document_number);
        kycdocument_upload= (ImageView) view.findViewById(R.id.kycdocument_upload);
        textViewName.setText(fileNameArray.get(position));
        textViewNumber.setText(""+position);
        final View finalView = view;
        kycdocument_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer index = (Integer) finalView.getTag();
                fileNameArray.remove(index.intValue());
                notifyDataSetChanged();
            }
        }); */

        View view;
        view = new View(mContext);
        view = mInflater.inflate(R.layout.buildprofile_kyc_document_exp_group,null);
        textViewName=(TextView)view.findViewById(R.id.build_profile_kyc_document_document);
        textViewNumber=(TextView)view.findViewById(R.id.build_profile_kyc_document_number);
        kycdocument_upload= (ImageView) view.findViewById(R.id.kycdocument_upload);
        kycdocument_upload.setTag(position);
        textViewName.setText(fileNameArray.get(position));
        textViewNumber.setText(""+position);
        kycdocument_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer index = (Integer) v.getTag();
                fileNameArray.remove(index.intValue());
                notifyDataSetChanged();
            }
        });

        return view;
    }
}
