package in.intellifox.mwallet.views.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Beneficiary;
import in.intellifox.mwallet.parser.Card;
import in.intellifox.mwallet.parser.Card1;
import in.intellifox.mwallet.parser.Cards;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.parser.Transactions;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.adapter.CustomListManageCard;
import in.intellifox.mwallet.views.adapter.ExpandableTransactionListAdapter;

/**
 * Created by Owner on 9/9/2015.
 */
public class ManageSaveCardFragment extends Fragment implements View.OnClickListener {

    ListView saveCardList;
    LinearLayout main_layout;
    String key = "YhBCOSXLjOP8xrJpRg9jZq3HjzZwxnmwU5DJfuWTLAA=";
    private ProgressDialog progressDialog = null;
    String iv = "288dca8258b1dd7c";
    String walletid = "", mobileno = "", tpin = "1111",tpinhash="", encryptedCurrentTpin = "";
    String xmlforSendMoney="";
    String timeStamp = "";
    Transactions transactions=new Transactions();
    ImageView delete_card,left_arrow,right_arrow;
    TextView card_number_textView,holder_name_textView;
    int lowerLimit=0,upperLimit=0;
    private Bundle savedInstanceState;

    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){

        View v=inflater.inflate(R.layout.manage_card_savecard_fragment,container,false);
        this.savedInstanceState=savedInstanceState;
        saveCardList=(ListView)v.findViewById(R.id.savecard_listview);
        delete_card= (ImageView) v.findViewById(R.id.delete_card);
        delete_card.setOnClickListener(this);
        left_arrow= (ImageView) v.findViewById(R.id.left_arrow);
        left_arrow.setOnClickListener(this);
        right_arrow= (ImageView) v.findViewById(R.id.right_arrow);
        right_arrow.setOnClickListener(this);
        card_number_textView= (TextView) v.findViewById(R.id.card_number_textView);
        holder_name_textView= (TextView) v.findViewById(R.id.holder_name_textView);
        CustomListManageCard navListAdaptercenter=new CustomListManageCard(getActivity().getApplicationContext());
        saveCardList.setAdapter(navListAdaptercenter);

        main_layout=(LinearLayout)v.findViewById(R.id.root_linear_lay_for_manage_savecard);
        mobileno= MyApplication.getInstance().getMerchantMobieNo();
        MessageDigest digest = null;
        try
        {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(tpin.getBytes());
            tpinhash = bytesToHexString(digest.digest());
            Log.i("Eamorr", "result is " + tpinhash);
        } catch (NoSuchAlgorithmException e1) {

            e1.printStackTrace();
        }
     //   callSavedCard(mobileno,tpinhash);
        saveCardList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        return v;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.right_arrow:
                if(lowerLimit<upperLimit-1)
                {
                    lowerLimit++;
                    holder_name_textView.setText(GlobalVariables.cardArrayList.get(lowerLimit).getCardHolderName());
                    card_number_textView.setText(GlobalVariables.cardArrayList.get(lowerLimit).getNumber());
                }
                break;
            case R.id.left_arrow:
                if(lowerLimit>0)
                {
                    lowerLimit--;
                    holder_name_textView.setText(GlobalVariables.cardArrayList.get(lowerLimit).getCardHolderName());
                    card_number_textView.setText(GlobalVariables.cardArrayList.get(lowerLimit).getNumber());
                }
                break;
            case R.id.delete_card:
                View popupView = getLayoutInflater(savedInstanceState).inflate(R.layout.custom_popup, null);
                final PopupWindow popupWindow = new PopupWindow(popupView,ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);

                TextView tv1=(TextView)popupView.findViewById(R.id.txt_note1);
                tv1.setText("Do you want to delete card number "+GlobalVariables.cardArrayList.get(lowerLimit).getNumber() +"");
                TextView tv2=(TextView)popupView.findViewById(R.id.txt_note2);
                tv2.setText("");
                TextView tv3=(TextView)popupView.findViewById(R.id.txt_note3);
                tv3.setText("Ok to proceed");
                tv3.setTextSize(12);
                ImageView cancel= (ImageView) popupView.findViewById(R.id.image_view_cancel_on_popup);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });
                Button btnDismiss = (Button)popupView.findViewById(R.id.button_ok_on_popup);
                btnDismiss.setOnClickListener(new Button.OnClickListener()
                {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        popupWindow.dismiss();
                          mobileno= MyApplication.getInstance().getMerchantMobieNo();
                        MessageDigest digest = null;
                        try
                        {
                            digest = MessageDigest.getInstance("SHA-256");
                            digest.update(tpin.getBytes());
                            tpinhash = bytesToHexString(digest.digest());
                            Log.i("Eamorr", "result is " + tpinhash);
                        } catch (NoSuchAlgorithmException e1) {

                            e1.printStackTrace();
                        }
                        callRemoveCard(mobileno, tpinhash,GlobalVariables.cardArrayList.get(lowerLimit).getToken());
//                            PayPersonSuccess rms = new PayPersonSuccess();
//                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, rms).commit();
                    }
                });

                popupWindow.showAtLocation(main_layout, Gravity.CENTER, 0, 0);

                break;
        }
    }

    public class MyAsyncTaskForSavedCard extends AsyncTask<String, Integer, String> {
        String result = "";

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            System.out.println("Response : " + s);

            progressDialog.dismiss();

            //Parse your response here
            if(result.equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                progressDialog.dismiss();
            }else {

                try {
                    AESecurity aess = new AESecurity();
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted)
                        {
                            Log.d("TAG", "decrypted Listcard200=" + decrypted);
                            Log.d("TAG","mobileNumber Pay200="+mobileNumber);
                            ParseResponse parse = new ParseResponse();
                            InputSource is = new InputSource(new StringReader(decrypted));
                            MpsXml mpsxml = parse.parseXML(is);

                            if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success"))
                            {
                                Cards cards=new Cards();
                                cards.setCard(mpsxml.getResponsedetails().getCards().getCard());
                                GlobalVariables.cardArrayList=cards.getCard();
                                upperLimit= GlobalVariables.cardArrayList.size();
                                lowerLimit=0;
                                holder_name_textView.setText(GlobalVariables.cardArrayList.get(lowerLimit).getCardHolderName());
                                card_number_textView.setText(GlobalVariables.cardArrayList.get(lowerLimit).getNumber());
                                Log.d("TAG","transaction history=="+transactions.getTransaction().size());
                                progressDialog.dismiss();

                                /*Intent toLogin = new Intent(ChangeTPin.this,MainActivity.class);
                                startActivity(toLogin);
*/
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getReason())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                //progressDialog.dismiss();
                            }
                        }
                    });
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException |
                        InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    // utility function for encryption of MPIN
    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }
    public void  callSavedCard(final String mobileno, String tpinhash){

        progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
        xmlforSendMoney =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>"+timeStamp+"</Timestamp>" +
                        "<SessionId>"+GlobalVariables.sessionId+"</SessionId>" +
                        "<ServiceProvider>"+getResources().getString(R.string.serviceprovider)+"</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>ListofSavedCards</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<MobileNumber>"+mobileno+"</MobileNumber>" +
                        "<Tpin>"+tpinhash+"</Tpin>" +
                         "<ResponseURL>"+ GlobalVariables.url+"</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        " </RequestDetails>" +
                        "</MpsXml>";
        Log.d("TAG", "request Listcard200=" + xmlforSendMoney);
        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforSendMoney, MyApplication.getInstance().getKey(),iv,new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXMLforSendMoney)
                {
                    String final_String_for_SendMoney = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + mobileno + "|" + encryptedXMLforSendMoney;
                    new MyAsyncTaskForSavedCard().execute(final_String_for_SendMoney);
                }
            });
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }
    public class MyAsyncTaskForRemoveCard extends AsyncTask<String, Integer, String> {
        String result = "";

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            System.out.println("Response : " + s);

            progressDialog.dismiss();

            //Parse your response here
            if(result.equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                progressDialog.dismiss();
            }else {

                try {
                    AESecurity aess = new AESecurity();
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted)
                        {
                            Log.d("TAG", "decrypted Listcard200=" + decrypted);
                            Log.d("TAG","mobileNumber Pay200="+mobileNumber);
                            ParseResponse parse = new ParseResponse();
                            InputSource is = new InputSource(new StringReader(decrypted));
                            MpsXml mpsxml = parse.parseXML(is);

                            if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success"))
                            {
                                  progressDialog.dismiss();
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getMessage()).setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        dialog.dismiss();
                                    }
                                });
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getReason())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                //progressDialog.dismiss();
                            }
                        }
                    });
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException |
                        InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            }
        }
    }
      public void  callRemoveCard(final String mobileno, String tpinhash, String token){

        progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
        xmlforSendMoney =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>"+timeStamp+"</Timestamp>" +
                        "<SessionId>"+GlobalVariables.sessionId+"</SessionId>" +
                        "<ServiceProvider>"+getResources().getString(R.string.serviceprovider)+"</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>DeLinkCard</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<MobileNumber>"+mobileno+"</MobileNumber>" +
                        "<CardToken>"+token+"</CardToken>" +
                        "<Tpin>"+tpinhash+"</Tpin>" +
                        "<ResponseURL>"+ GlobalVariables.url+"</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        " </RequestDetails>" +
                        "</MpsXml>";
        Log.d("TAG", "request Remove card200=" + xmlforSendMoney);
        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforSendMoney, MyApplication.getInstance().getKey(),iv,new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXMLforSendMoney)
                {
                    String final_String_for_SendMoney = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + mobileno + "|" + encryptedXMLforSendMoney;
                    new MyAsyncTaskForRemoveCard().execute(final_String_for_SendMoney);
                }
            });
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }
}
