package in.intellifox.mwallet.views.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by SADIQ on 7/17/2015.
 */
public class TextViewMuseosansHundred extends TextView  {
    public TextViewMuseosansHundred(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextViewMuseosansHundred(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewMuseosansHundred(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/museo_sans_hundred.otf");
        setTypeface(tf ,1);

    }


}
