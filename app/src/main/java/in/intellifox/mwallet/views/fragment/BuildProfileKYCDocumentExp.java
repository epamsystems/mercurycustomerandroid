package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.adapter.ExpandableDocumentsListAdapter;

/**
 * Created by Owner on 9/1/2015.
 */
public class BuildProfileKYCDocumentExp extends Fragment {
    private BuildProfileWelcome buildmain;
    ExpandableDocumentsListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> document;
    List<String> number;
    Button addCard;
    LinearLayout login_root_layout;
    ImageView upload;
    Button skipheader,skipbottom;
    HashMap<String, List<String>> listDataChild;
    HashMap<String, List<String>> listDataChild_Amount;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final    Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.buildprofile_kyc_document_exp,container,false);
        login_root_layout=(LinearLayout)v.findViewById(R.id.addcard_second_part);
        expListView = (ExpandableListView) v.findViewById(R.id.buildprofile_kycdocuments_lvExp);
        expListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                View popupView = getLayoutInflater(savedInstanceState).inflate(R.layout.buildprofile_custom_popup_kyc_document, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                popupWindow.showAtLocation(login_root_layout, Gravity.CENTER, 0,0);
            }
        });
        return v;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*rootLay = (LinearLayout) view.findViewById(R.id.root_linear_lay_for_recent_tab);
        MyApplication.resetTimer(getActivity());
        rootLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });
*/      skipheader=(Button)view.findViewById(R.id.kyc_actionbar_header_skip_exp);
        skipbottom=(Button)view.findViewById(R.id.button_skip_exp);
        skipheader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildmain = new BuildProfileWelcome();
                Bundle bundle = new Bundle();

                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, buildmain).commit();
            }
        });
        skipbottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildmain = new BuildProfileWelcome();
                Bundle bundle = new Bundle();

                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, buildmain).commit();
            }
        });

        document = new ArrayList<String>();
        number = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        listDataChild_Amount = new HashMap<String, List<String>>();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {

                        return true;
                    }
                }
                return false;
            }
        });

        document.add("Document");
        document.add("Document");
        document.add("Document");
        document.add("Document");
        document.add("Document");
        document.add("Document");

        number.add("01");
        number.add("02");
        number.add("03");
        number.add("04");
        number.add("05");
        number.add("06");

        List<String> top250 = new ArrayList<String>();
        top250.add(" My-Document01.jpg");

        listDataChild.put(document.get(0), top250); // Header, Child data
        listDataChild_Amount = new HashMap<String, List<String>>();
        listDataChild_Amount.put(document.get(0),top250);
        listAdapter = new ExpandableDocumentsListAdapter(getActivity(), document, number, listDataChild, listDataChild_Amount);

        expListView.setAdapter(listAdapter);// Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

          @Override
            public void onGroupExpand(int groupPosition) {
                /* Toast.makeText(getApplicationContext(),
                listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();*/
            }
        });
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {

                /*Toast.makeText(getActivity().getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();*/

            }
        });
    }
}
