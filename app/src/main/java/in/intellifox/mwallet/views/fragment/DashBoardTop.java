package in.intellifox.mwallet.views.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.apicalls.RetrofitTask;
import in.intellifox.mwallet.security.AESecurityNew;
import in.intellifox.mwallet.utilities.AnimationUtils;
import in.intellifox.mwallet.utilities.GenericResponseHandler;
import in.intellifox.mwallet.utilities.Utils;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/3/2015.
 */
public class DashBoardTop extends Fragment {
    TextView amount, tv;
    Button addMoney;
    RelativeLayout payPerson, payMerchant, cashOut;

    TextView textView_mwallet_balance, textView_balance;
    String walletBalance = "";
    private ImageView imgRefresh;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        walletBalance = MyApplication.getInstance().getTotalBalance();
        textView_mwallet_balance.setText(walletBalance);

        textView_balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

        addMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerMain) getActivity()).OpenAddMoney();
            }
        });

        payPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerMain) getActivity()).OpenPayPerson();
            }
        });

        payMerchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerMain) getActivity()).OpenPayMerchant();
            }
        });
        cashOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerMain) getActivity()).CashOut();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callAPI();
            }
        }, 400);

        imgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callAPI();
            }
        });

    }


    public void openDialog() {
       /* final Dialog dialog = new Dialog(getActivity()); // Context, this, etc.
        dialog.setContentView(R.layout.dialog_balance);
        dialog.setTitle("Balance");
        dialog.show();*/


        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setCancelable(false);
        dialog.setTitle("Balance");
        dialog.setMessage("General Balance: AED " + MyApplication.getInstance().getGeneralBalance() +
                "\nPromo Balance: AED " + MyApplication.getInstance().getPromotionalBalance());
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
            }
        });
               /* .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".
                    }
                });*/

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dashboard_top, container, false);
    }

    private void LayoutInitialization(View view) {

        textView_balance = (TextView) view.findViewById(R.id.textView_balance);

        textView_mwallet_balance = (TextView) view.findViewById(R.id.textView_amount);
        tv = (TextView) view.findViewById(R.id.textView);
        addMoney = (Button) view.findViewById(R.id.button_addMoney);
        payPerson = (RelativeLayout) view.findViewById(R.id.relative_lay_pay_person);
        payMerchant = (RelativeLayout) view.findViewById(R.id.relative_lay_pay_merchant);
        cashOut = (RelativeLayout) view.findViewById(R.id.relative_lay_cash_out);
        imgRefresh = (ImageView) view.findViewById(R.id.refresh);
    }

    private void callAPI() {

        String xmlParam = "\t\t<MobileNumber>" + MyApplication.getInstance().getmProfile_Mobile_Number() + "</MobileNumber>\n" +
                "\t\t<WalletId>" + MyApplication.getInstance().getmProfile_WalletID_General() + "</WalletId>";


        xmlParam = Utils.getReqXML("BalanceInquiry", xmlParam);
        try {
            xmlParam = AESecurityNew.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.showToast(getActivity(), e.toString());
            return;
        }

        final AnimationUtils rotateanimation = new AnimationUtils();
        rotateanimation.rotateView(imgRefresh, 0, 360, imgRefresh.getWidth() / 2, imgRefresh.getHeight() / 2, null);

        RetrofitTask.getInstance().executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                if (getActivity() != null)
                    rotateanimation.stopRotateAnimation(imgRefresh);

                if (!isSuccess) {
                    Utils.showToast(getActivity(), response);
                    return;
                }
                String decrypted = null;
                try {
                    decrypted = AESecurityNew.getInstance().decryptString(response);
                    Log.v("TAG", "Response " + decrypted);

                    ArrayList<String> elements = new ArrayList<String>();
                    elements.add("ResponseType");
                    elements.add("Reason");
                    elements.add("Balance");
                    elements.add("PromotionalBalance");

                    Map<String, String> responseMap = GenericResponseHandler.parseElements(decrypted, elements);

                    if (responseMap.get("ResponseType").equals("Success")) {
                        MyApplication.getInstance().setGeneralBalance(responseMap.get("Balance"));
                        MyApplication.getInstance().setPromotionalBalance(responseMap.get("PromotionalBalance"));


                        BigDecimal general_bal = BigDecimal.ZERO;
                        BigDecimal promotional_bal = BigDecimal.ZERO;

                        if (!TextUtils.isEmpty(responseMap.get("Balance"))) {
                            general_bal = new BigDecimal(responseMap.get("Balance"));
                        }

                        if (!TextUtils.isEmpty(responseMap.get("PromotionalBalance"))) {
                            promotional_bal = new BigDecimal(responseMap.get("PromotionalBalance"));
                        }

                        BigDecimal result = general_bal.add(promotional_bal);

                        textView_mwallet_balance.setText("" + result);
                        MyApplication.getInstance().setTotalBalance("" + result);


                    } else {

                        MyApplication.getInstance().setGeneralBalance(null);
                        MyApplication.getInstance().setPromotionalBalance(null);
                        MyApplication.getInstance().setTotalBalance(null);

                        Utils.showToast(getActivity(), responseMap.get("Reason"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    // Utils.showToast(getActivity(), e.getMessage());
                }
            }
        });
    }
}
