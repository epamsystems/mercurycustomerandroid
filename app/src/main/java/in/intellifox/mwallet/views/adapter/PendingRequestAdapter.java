package in.intellifox.mwallet.views.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.models.MoneyRequest;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.fragment.CashOutSuccess;
import in.intellifox.mwallet.views.fragment.PendingRequestSendMoneyFragment;

/**
 * Created by kunalk on 8/11/2016.
 */
public class PendingRequestAdapter extends BaseAdapter {

    List<MoneyRequest> moneyRequestList;
    Context context;
    LayoutInflater inflater;

    public PendingRequestAdapter(Context context, List<MoneyRequest> moneyRequestList) {
        this.context = context;
        this.moneyRequestList = moneyRequestList;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return moneyRequestList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.pending_request_listview_item, parent, false);

            holder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            holder.txtTxnID = (TextView) convertView.findViewById(R.id.txtTxnID);
            holder.txtDate = (TextView) convertView.findViewById(R.id.txtDate);
            holder.txtAMount = (TextView) convertView.findViewById(R.id.stramount);
            holder.layout_accept = (LinearLayout) convertView.findViewById(R.id.layout_accept);
            holder.layout_decline = (LinearLayout) convertView.findViewById(R.id.layout_decline);
            holder.layout_expand = (LinearLayout) convertView.findViewById(R.id.layout_expand);
            holder.layout_top= (RelativeLayout) convertView.findViewById(R.id.rl_top);
            holder.layout_behind= (LinearLayout) convertView.findViewById(R.id.rl_behind);
            convertView.setTag(holder);

        }

        holder = (ViewHolder) convertView.getTag();

        final MoneyRequest moneyRequest=moneyRequestList.get(position);
        holder.layout_top.setTag(R.id.IS_OPENED, false);


        final ViewHolder finalHolder = holder;
        holder.layout_top.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {

                                                     toggleRow(finalHolder.layout_top, parent);
                                                 }
                                             }
        );

        holder.layout_accept.setOnClickListener(new View.OnClickListener()

                                                {
                                                    @Override
                                                    public void onClick(View v) {

                                                        PendingRequestSendMoneyFragment pendingRequestSendMoneyFragment = new PendingRequestSendMoneyFragment();
                                                        Bundle bundle = new Bundle();
                                                        bundle.putSerializable("moneyRequest",moneyRequest);
                                                        bundle.putString("status","Authorize");
                                                        pendingRequestSendMoneyFragment.setArguments(bundle);
                                                        FragmentTransaction fragmentTransaction = GlobalVariables.fragmentManager
                                                                .beginTransaction();
                                                        fragmentTransaction.addToBackStack(pendingRequestSendMoneyFragment.getClass()
                                                                .getName());
                                                        fragmentTransaction.add(R.id.dashboard_container, pendingRequestSendMoneyFragment);
                                                        fragmentTransaction.commit();
                                                    }
                                                }

        );


        holder.layout_decline.setOnClickListener(new View.OnClickListener()

                                                 {
                                                     @Override
                                                     public void onClick(View v) {

                                                         PendingRequestSendMoneyFragment pendingRequestSendMoneyFragment = new PendingRequestSendMoneyFragment();
                                                         Bundle bundle = new Bundle();
                                                         bundle.putSerializable("moneyRequest",moneyRequest);
                                                         bundle.putString("status","Reject");
                                                         pendingRequestSendMoneyFragment.setArguments(bundle);
                                                         FragmentTransaction fragmentTransaction = GlobalVariables.fragmentManager
                                                                 .beginTransaction();
                                                         fragmentTransaction.addToBackStack(pendingRequestSendMoneyFragment.getClass()
                                                                 .getName());
                                                         fragmentTransaction.add(R.id.dashboard_container, pendingRequestSendMoneyFragment);
                                                         fragmentTransaction.commit();
                                                     }
                                                 }

        );

        holder.txtName.setText(moneyRequest.getFirstName()+" "+moneyRequest.getLastName());
        holder.txtTxnID.setText(moneyRequest.getId());
        holder.txtDate.setText(moneyRequest.getTxnDate());
        holder.txtAMount.setText("AED "+moneyRequest.getAmount());


        return convertView;
    }


    private void toggleRow(View v, ViewGroup parent) {

        boolean isOpened = (Boolean) v.getTag(R.id.IS_OPENED);

        Log.v("TAG", " isOpened " + isOpened);

        if (isOpened) {
            v.animate().translationX(0).start();
            v.setTag(R.id.IS_OPENED, false);

        } else {
            int ANIM_OFFSET = 0;
            RelativeLayout rl_top = null;
            for (int i = 0; i < parent.getChildCount(); i++) {
                View view = parent.getChildAt(i);

                rl_top = (RelativeLayout) view.findViewById(R.id.rl_top);
                if (rl_top != null) {
                    rl_top.setTranslationX(0);
                    rl_top.setTag(R.id.IS_OPENED, false);

                    if (ANIM_OFFSET == 0)
                        ANIM_OFFSET = view.findViewById(R.id.rl_behind).getWidth();
                }
            }


            v.animate().translationX(-ANIM_OFFSET).start();
            v.setTag(R.id.IS_OPENED, true);
        }
    }

    class ViewHolder {
        RelativeLayout layout_top;
        TextView txtName, txtTxnID, txtDate, txtAMount;
        LinearLayout layout_accept, layout_decline, layout_expand,layout_behind;
    }

}
