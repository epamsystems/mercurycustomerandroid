package in.intellifox.mwallet.views.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.xml.sax.InputSource;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import dialogs.OkDialog;
import dialogs.YesNoDialog;
import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.apicalls.RetrofitTask;
import in.intellifox.mwallet.models.Document;
import in.intellifox.mwallet.models.DocumentRequest;
import in.intellifox.mwallet.models.DocumentRequestDetails;
import in.intellifox.mwallet.models.DocumentResponse;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.parser.Profile;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.security.AESecurityNew;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.utilities.LogUtils;

/**
 * Created by Raju on 29/09/2015.
 */
public class BuildProfileKYCDocumentList extends Fragment {
    LinearLayout login_root_layout;
    private static final int CAMERA_REQUEST = 1888;
    private int PICK_IMAGE_REQUEST = 1;
    private BuildProfileWelcome buildmain;
    ImageView proceed;
    Button skipheader, skipbottom;
    Profile profile = new Profile();
    String count = "";
    String iv = "288dca8258b1dd7c";
    private ProgressDialog progressDialog = null;
    Spinner spinner_documents;
    RelativeLayout rl_docs;
    private static String file_url = "http://demo.timesofmoney.com/mps/CustomerRegistration";
    private static ArrayList<String> fileNameArray = new ArrayList<String>();
    private static Activity activity;
    private static Bundle savedInstanceState;
    private Dialog dialogSaveFile;
    LayoutInflater inflater;
    LinearLayout layout_preview;
    private BuildProfileKYCDocumentList kycdocument;
    private String encryptedXML;
    Map<String, Bitmap> documentMap;
    Map<Document, String> uploadedDocuments;
    Document selectedDocument;
    ArrayList<Document> documents;
    int DEVICE_WIDTH = 0;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        skipheader = (Button) view.findViewById(R.id.kyc_actionbar_header_skip_list);
        skipbottom = (Button) view.findViewById(R.id.button_skip_list);
        LinearLayout camera = (LinearLayout) view.findViewById(R.id.buildprofile_camera_kyc);
        LinearLayout scan = (LinearLayout) view.findViewById(R.id.gallery_imageView);
        spinner_documents = (Spinner) view.findViewById(R.id.spinnerdocuments);
        rl_docs = (RelativeLayout) view.findViewById(R.id.rl_docs);
        layout_preview = (LinearLayout) view.findViewById(R.id.layout_preview);
        documentMap = new HashMap<>();
        uploadedDocuments = new HashMap<>();

        this.activity = getActivity();
        this.savedInstanceState = savedInstanceState;

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        DEVICE_WIDTH = metrics.widthPixels;
        try {
            Bundle args = getArguments();
            profile = (Profile) args.getSerializable("Profile");

        } catch (Exception e) {

        }

        getKey(profile.getMobileNumber());

        rl_docs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                spinner_documents.performClick();
            }
        });

        skipheader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count = "" + 1;
                new UploadFileToServer(profile, count).execute(GlobalVariables.customerRegistration_URL);
            }
        });

        skipbottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count = "" + 1;

                if (documentMap.size() == 0) {
                    new OkDialog(getActivity(), "Please upload atleast one document", null, null);
                    return;
                }
                // Need to check

                new UploadFileToServer(profile, count).execute(GlobalVariables.customerRegistration_URL);
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);

                    //Note: Callback is in Activity of this fragment.

                } else {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, CAMERA_REQUEST);
                    }
                }


            }
        });
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

            }
        });

        ImageView backarrow = (ImageView) view.findViewById(R.id.backarrow);
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kycdocument = new BuildProfileKYCDocumentList();
                FragmentManager manager = getActivity().getSupportFragmentManager();
                manager.popBackStackImmediate();

            }
        });

        spinner_documents.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                selectedDocument = documents.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void callListDocimentAPI() {

        final Serializer serializer = new Persister();
        DocumentRequestDetails requestDetails = new DocumentRequestDetails();

        DocumentRequest documentRequest = new DocumentRequest("GetIdentificationDocs", requestDetails);
        StringWriter writer = new StringWriter();
        try {
            serializer.write(documentRequest, writer);
            Log.v("TAG", " XML " + writer.toString());
            encryptedXML = AESecurityNew.getInstance().encryptString(writer.toString());

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            return;
        }


        RetrofitTask.getInstance().executeTask(encryptedXML, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                if (progressDialog != null && !getActivity().isFinishing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (!isSuccess) {
                    Log.v("TAG", response);
                    Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
                    return;
                }
                try {
                    String decrypted = AESecurityNew.getInstance().decryptString(response);
                    Log.v("TAG", " Decrypted " + decrypted);


                    DocumentResponse documentResponse = serializer.read(DocumentResponse.class, decrypted, false);
                    if (documentResponse.getResponse().getResponseType().equals("Success")) {
                        documents = (ArrayList<Document>) documentResponse.getResponseDetails().getDocument().getDocuments_list();
                        ArrayList<Document> documents = (ArrayList<Document>) documentResponse.getResponseDetails().getDocument().getDocuments_list();
                        ArrayAdapter<Document> dataAdapter = new ArrayAdapter<Document>(getActivity(), android.R.layout.simple_spinner_item, documents);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_documents.setAdapter(dataAdapter);

                    } else {

                        new OkDialog(getActivity(), documentResponse.getResponseDetails().getReason(), null, null);
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.v("TAG", e.toString());

                    Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        });

    }

    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = activity.getContentResolver().query(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        String selectedImagePath = cursor.getString(column_index);
        fileNameArray.add(selectedImagePath);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.inflater = inflater;
        View v = inflater.inflate(R.layout.buildprofile_kyc_document_list_listoperator, container, false);
        login_root_layout = (LinearLayout) v.findViewById(R.id.login_root_layout);
        return v;
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            Bitmap bitmap = null;
            if (requestCode == CAMERA_REQUEST) {
                Bundle extras = data.getExtras();
                Log.v("TAG", " I am at onResult");
                bitmap = (Bitmap) extras.get("data");
            } else {
                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    Log.v("TAG", " I am at Gallery image upload");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            int count = 0;
            if (!uploadedDocuments.containsKey(selectedDocument)) {
                uploadedDocuments.put(selectedDocument, "0");
            } else {
                count = Integer.parseInt(uploadedDocuments.get(selectedDocument)) + 1;
                uploadedDocuments.remove(selectedDocument);
                uploadedDocuments.put(selectedDocument, count + "");
            }
            String img_id = selectedDocument.getId() + "_" + count;
            Log.v("TAG", " Image ID " + img_id);

            bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, false);
            documentMap.put(img_id, bitmap);
            drawPreview(documentMap);
        }
    }


    // Async Task Class
    private class UploadFileToServer extends AsyncTask<String, String, String> {

        Profile profile = new Profile();
        String countDoucment;

        public UploadFileToServer(Profile profile, String count) {
            this.profile = profile;
            this.countDoucment = count;
        }

        // Show Progress bar before downloading Music
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Need to check
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getResources().getString(R.string.please_wait_msg));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();


            // progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);
        }

        // Download Music File from Internet
        @Override
        protected String doInBackground(String... f_url) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();

            HttpPost httppost = new HttpPost(GlobalVariables.customerRegistration_URL);

            String boundary = "-------------" + System.currentTimeMillis();
            //httppost.setHeader("Content-type", "multipart/form-data; boundary="+boundary);

            try {
                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                StringBody firstName = new StringBody(profile.getFirstName(), ContentType.TEXT_PLAIN);
                StringBody lastName = new StringBody(profile.getLastName(), ContentType.TEXT_PLAIN);
                StringBody gender = new StringBody(profile.getGender(), ContentType.TEXT_PLAIN);
                StringBody dob = new StringBody(profile.getBirthDate(), ContentType.TEXT_PLAIN);
                StringBody emailId = new StringBody(profile.getEmailId(), ContentType.TEXT_PLAIN);
                StringBody mobileNo = new StringBody(profile.getMobileNumber(), ContentType.TEXT_PLAIN);
                StringBody address = new StringBody(profile.getAddress(), ContentType.TEXT_PLAIN);
                StringBody city = new StringBody(profile.getCity(), ContentType.TEXT_PLAIN);
                StringBody userType = new StringBody("CU", ContentType.TEXT_PLAIN);
                StringBody requestType = new StringBody("CUSTREG", ContentType.TEXT_PLAIN);
                StringBody spid = new StringBody(GlobalVariables.SPID, ContentType.TEXT_PLAIN);

                if (profile.getProfileImage() != null) {

                    Log.v("TAG", " I am adding customer image");
                    ByteArrayBody imageBody = new ByteArrayBody(profile.getProfileImage(), "customerImage.png");
                    builder.addPart("customerImage", imageBody);

                }

                for (Document document : uploadedDocuments.keySet()) {
                    int count1 = Integer.parseInt(uploadedDocuments.get(document)) + 1;
                    String countStr = document.getId() + "_count";
                    Log.v("TAG", " doc_Count " + countStr + " " + count1);
                    StringBody doc_count_Body = new StringBody(count1 + "", ContentType.TEXT_PLAIN);
                    builder.addPart(countStr, doc_count_Body);


                }
                for (String img_name : documentMap.keySet()) {

                    Log.v("TAG", " Image Name " + img_name);
                    Bitmap bitmap = documentMap.get(img_name);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    ByteArrayBody imageBody = new ByteArrayBody(byteArray, img_name + ".png");
                    builder.addPart(img_name, imageBody);
                    bitmap.recycle();
                }

                builder.addPart("firstName", firstName);
                builder.addPart("lastName", lastName);
                builder.addPart("gender", gender);
                builder.addPart("dob", dob);
                builder.addPart("emailId", emailId);
                builder.addPart("mobileNo", mobileNo);
                builder.addPart("address", address);
                builder.addPart("city", city);
                builder.addPart("userType", userType);
                builder.addPart("requestType", requestType);
                builder.addPart("spid", spid);
                builder.setBoundary(boundary);

                final HttpEntity entity = builder.build();
                Log.v("Entity ", entity.toString());
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();


                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {

                    responseString = EntityUtils.toString(r_entity);

                    //  EntityUtilsHC4.consume(r_entity);
                    Log.d("TAG67", "responseString=" + responseString);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }


                if (r_entity != null) {
                    try {
                        r_entity.consumeContent();
                    } catch (IOException e) {
                        Log.e("TAG", "", e);
                    }
                }

                // Safe close

               /* if (httpclient != null && httpclient.getConnectionManager() != null) {

                    httpclient.getConnectionManager().shutdown();
                }*/


            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }
            return responseString;

        }

        // Once Successfully Uploaded
        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);

            if (progressDialog != null && !getActivity().isFinishing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            if (res.startsWith("Error")) {
                Log.v("TAG", " Response " + res);
                new OkDialog(getActivity(), res, null, null);
                return;
            }
            Log.v("Encrypted xml: ", res);
            final MpsXml[] mpsxml = new MpsXml[1];
            final ParseResponse[] parse = new ParseResponse[1];

            try {
                AESecurity aes = new AESecurity();
                try {
                    aes.decrypt(res, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {

                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted) {
                            Log.d("TAG67", "decrypted registered=" + decrypted);
                            if (decrypted.equals(" ")) {

                            } else {

                                parse[0] = new ParseResponse();
                                InputSource is = new InputSource(new StringReader(decrypted));
                                mpsxml[0] = parse[0].parseXML(is);
                                if (mpsxml[0].getResponse().getResponseType().equalsIgnoreCase("Success")) {
                                    String reason = mpsxml[0].getResponsedetails().getReason();

                                    buildmain = new BuildProfileWelcome();
                                    Bundle bundle = new Bundle();
                                    String name = profile.getFirstName() + " " + profile.getLastName();
                                    bundle.putString("reason", reason);
                                    bundle.putString("name", name);
                                    buildmain.setArguments(bundle);
                                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, buildmain).commit();

                                } else {
                                    String reason = mpsxml[0].getResponsedetails().getReason();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage(mpsxml[0].getResponsedetails().getReason())
                                            .setTitle("mWallet");
                                    AlertDialog dialog = builder.create();
                                    dialog.setCanceledOnTouchOutside(true);
                                    dialog.show();
                                }
                            }
                        }
                    });
                } catch (InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException |
                        InvalidAlgorithmParameterException | BadPaddingException e) {
                    e.printStackTrace();
                    new OkDialog(getActivity(), e.getMessage(), null, null);
                }
            } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                e.printStackTrace();
                new OkDialog(getActivity(), e.getMessage(), null, null);
            }
            //Note that this response is encrypted xml. You will need to decrypt it to view response
        }
    }

    private void getKey(String strMobile) {


        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        RetrofitTask.getInstance().executeGetKey(strMobile, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                if (isSuccess) {
                    MyApplication.getInstance().setKey(response);
                    LogUtils.Verbose("executeGetKey Build KYC", response);
                    callListDocimentAPI();
                } else {
                    if (progressDialog != null && !getActivity().isFinishing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                    new OkDialog(getActivity(), response, "", null);
                }
            }
        });

    }


    private void drawPreview(final Map<String, Bitmap> bitmapHashMap) {

        if (bitmapHashMap.size() == 0) {
            layout_preview.removeAllViews();
            return;
        }

        layout_preview.removeAllViews();
        int no_of_preview = bitmapHashMap.size() <= 3 ? bitmapHashMap.size() : 3;
        int preview_width = (int) (DEVICE_WIDTH / no_of_preview);

        Log.v("TAG", " parant width " + DEVICE_WIDTH + " preview " + preview_width);

        Set keySet = bitmapHashMap.keySet();
        Iterator iterator = keySet.iterator();

        while (iterator.hasNext()) {
            View view = inflater.inflate(R.layout.preview_documents, layout_preview, false);
            RelativeLayout preview_root = (RelativeLayout) view.findViewById(R.id.parant_preview);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(preview_width, LinearLayout.LayoutParams.WRAP_CONTENT);
            preview_root.setLayoutParams(layoutParams);
            TextView txtName = (TextView) view.findViewById(R.id.txtname);
            LinearLayout delete = (LinearLayout) view.findViewById(R.id.delete);
            final String imageName = (String) iterator.next();
            String doc_id = imageName.charAt(0) + "";
            txtName.setText(getDocumentNameByID(doc_id) + imageName.substring(1, imageName.length()));
            ImageView img = (ImageView) view.findViewById(R.id.img_doc);
            img.setImageBitmap(bitmapHashMap.get(imageName));
            img.setTag(imageName);


            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    new YesNoDialog(getActivity(), "Are you sure you want to delete this document?", null, new YesNoDialog.IYesNoDialogCallback() {
                        @Override
                        public void handleResponse(int responsecode) {

                            if (responsecode == 1) {
                                bitmapHashMap.remove(imageName);
                                drawPreview(bitmapHashMap);
                            }
                        }
                    });

                }
            });


            layout_preview.addView(view);
        }
    }

    private String getDocumentNameByID(String id) {

        Log.v("TAG", " Doc ID " + id);
        for (Document document : documents) {
            if (document.getId().equals(id))
                return document.getName();
        }

        return id;
    }
}