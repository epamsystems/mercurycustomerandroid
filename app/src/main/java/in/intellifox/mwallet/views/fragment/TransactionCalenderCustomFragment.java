package in.intellifox.mwallet.views.fragment;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidGridAdapter;

import in.intellifox.mwallet.views.adapter.CalenderCustomAdapter;


/**
 * Created by pvaigankar on 7/25/2015.
 */
 public class TransactionCalenderCustomFragment extends CaldroidFragment {

    @Override
    public CaldroidGridAdapter getNewDatesGridAdapter(int month,int year) {
        // TODO Auto-generated method stub
        return new CalenderCustomAdapter(getActivity(), month,year, getCaldroidData(), extraData);
    }

}