package in.intellifox.mwallet.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import in.intellifox.mwallet.R;

/**
 * Created by Raju on 28/09/2015.
 */
public class ExpandableDocumentsListAdapter extends BaseExpandableListAdapter{


    private Context _context;
    private List<String> _document = null;

    private List<String> _number = null;


    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild = null;
    private HashMap<String, List<String>> _listDataChild_amount = null;

    public ExpandableDocumentsListAdapter(Context context, List<String> document, List<String> number,
                                          HashMap<String, List<String>> listChildData, HashMap<String, List<String>> child_amount) {
        this._context = context;
        this._document = document;
        this._number = number;
        this._listDataChild = listChildData;
        this._listDataChild_amount=child_amount;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._document.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.buildprofile_kyc_document_exp_child, null);
        }

        TextView cvvnumber = (TextView) convertView.findViewById(R.id.document_images_text);
        cvvnumber.setText(childText);




        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._document.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._document.get(groupPosition);
    }


    @Override
    public int getGroupCount() {
        return this._document.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {



        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.buildprofile_kyc_document_exp_group, null);

        }
        TextView document = (TextView) convertView.findViewById(R.id.build_profile_kyc_document_document);
        document.setText(headerTitle);

        TextView bankofname = (TextView) convertView.findViewById(R.id.build_profile_kyc_document_number);
        bankofname.setText(_number.get(groupPosition));


        View view=(View)convertView.findViewById(R.id.grouprow_view);
        ImageView upload = (ImageView)convertView.findViewById(R.id.kycdocument_upload);




        RelativeLayout topPart = (RelativeLayout) convertView.findViewById(R.id.relative_lay_top_part_mycards);
        if(isExpanded){
            view.setVisibility(View.GONE);


        }else{
            view.setVisibility(View.VISIBLE);


        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

