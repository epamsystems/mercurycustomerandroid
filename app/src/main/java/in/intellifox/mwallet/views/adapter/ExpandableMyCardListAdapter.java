package in.intellifox.mwallet.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import in.intellifox.mwallet.R;

/**
 * Created by utkarsh on 7/22/2015.
 */
public class ExpandableMyCardListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _accountnumber = null;

    private List<String> _bankofname = null;


    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild = null;
    private HashMap<String, List<String>> _listDataChild_amount = null;

    public ExpandableMyCardListAdapter(Context context, List<String> accountnumber, List<String> bankofname,
                                       HashMap<String, List<String>> listChildData, HashMap<String, List<String>> child_amount) {
        this._context = context;
        this._accountnumber = accountnumber;
        this._bankofname = bankofname;
        this._listDataChild = listChildData;
        this._listDataChild_amount=child_amount;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._accountnumber.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.pay_merchant_child_savecard_child, null);
        }

        EditText cvvnumber = (EditText) convertView.findViewById(R.id.edittext_view_customer_name);
        cvvnumber.setText(childText);




        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._accountnumber.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._accountnumber.get(groupPosition);
    }


    @Override
    public int getGroupCount() {
        return this._accountnumber.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {



        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.pay_merchant_child_savecard_group, null);

        }
        TextView accountnumber = (TextView) convertView.findViewById(R.id.grouprow_visatextviewnew1);
        accountnumber.setText(headerTitle);

        TextView bankofname = (TextView) convertView.findViewById(R.id.grouprow_visatextviewnew2);
        bankofname.setText(_bankofname.get(groupPosition));


        View view=(View)convertView.findViewById(R.id.grouprow_view);
        ImageView cards = (ImageView)convertView.findViewById(R.id.grouprow_visacardsnew);
        ImageView arrow_icon = (ImageView)convertView.findViewById(R.id.grouprow_tick1);
        RelativeLayout topPart = (RelativeLayout) convertView.findViewById(R.id.relative_lay_top_part_mycards);
        if(isExpanded){
            view.setVisibility(View.GONE);
            arrow_icon.setImageResource(R.drawable.pay_merchant_expand_tick);
        }else{
            view.setVisibility(View.VISIBLE);
            arrow_icon.setImageResource(R.drawable.pay_merchant_collapsed_tick);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
