package in.intellifox.mwallet.views.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.parser.ResponseDetails;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.activities.ForgotTPin;

/**
 * Created by Owner on 9/10/2015.
 */
public class PayMerchantQrCodeWalletFragment extends Fragment {

    Button payByCard;
    LinearLayout mainLayout;
    TextView merchantID,amount,tpin;
    String sMerchantId,sAmount;
    String iv = "288dca8258b1dd7c";
    String timeStamp = "",mobileno = "",amount1 = "";
    String merchantId = "", xmlforSendMoney = "";
    String mWalletId = "",tpinhash="",session = "";
    String bmobile="9200000010";
    String btpin="2211";
    private ProgressDialog progressDialog;
    private TextView mWalletAmount;
    private String wAmount="";
    private TextView textViewforgottpin;

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        String walletBalance = MyApplication.getInstance().getTotalBalance();
        if(walletBalance!=null)
        {
            wAmount=walletBalance;
        }
        mWalletAmount.setText("AED "+wAmount);
        try {
            Bundle bundle = new Bundle();
            sMerchantId=getArguments().getString("merchantId");
            sAmount=getArguments().getString("amount");
            merchantID.setText(sMerchantId);
            amount.setText(sAmount);
        }catch (Exception e)
        {

        }
        textViewforgottpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ForgotTPin.class);
                startActivity(intent);
            }
        });
        payByCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                merchantId=merchantID.getText().toString().trim();
                amount1=amount.getText().toString().trim();
                btpin=tpin.getText().toString().trim();
                if (merchantId.length() < 1 )
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.merchantid_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else
                if (amount.length() <1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.amount_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                }
                else if (btpin.length() <6) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.invalid_tpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else
                {
                    View popupView = getLayoutInflater(savedInstanceState).inflate(R.layout.custom_popup, null);
                    final PopupWindow popupWindow = new PopupWindow(popupView,ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);

                    TextView tv1=(TextView)popupView.findViewById(R.id.txt_note1);
                    tv1.setText("Send AED "+sAmount +" to");
                    TextView tv2=(TextView)popupView.findViewById(R.id.txt_note2);
                    tv2.setText("Merchant Id "+sMerchantId);
                    TextView tv3=(TextView)popupView.findViewById(R.id.txt_note3);
                    tv3.setText("Ok to proceed");
                    tv3.setTextSize(12);
                    ImageView cancel= (ImageView) popupView.findViewById(R.id.image_view_cancel_on_popup);
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                        }
                    });
                    Button btnDismiss = (Button)popupView.findViewById(R.id.button_ok_on_popup);
                    btnDismiss.setOnClickListener(new Button.OnClickListener()
                    {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            popupWindow.dismiss();
                            MessageDigest digest = null;
                            try
                            {
                                digest = MessageDigest.getInstance("SHA-256");
                                digest.update(btpin.getBytes());
                                tpinhash = bytesToHexString(digest.digest());
                                Log.i("Eamorr", "result is " + tpinhash);
                            } catch (NoSuchAlgorithmException e1) {

                                e1.printStackTrace();
                            }
                            bmobile= MyApplication.getInstance().getMerchantMobieNo();
                            mWalletId= MyApplication.getInstance().getmProfile_WalletID_General();
                            callPayMerchant(bmobile, merchantId,amount1, tpinhash, mWalletId);

                    }
                    });

                    popupWindow.showAtLocation(mainLayout, Gravity.CENTER, 0, 0);
                }
            }
        });
    }
    public void  callPayMerchant(final String bmobile, String merchantId, String amount, String tpinhash, String mWalletId){

        progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
        xmlforSendMoney =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>"+timeStamp+"</Timestamp>" +
                        "<SessionId>"+ GlobalVariables.sessionId+"</SessionId>" +
                        "<ServiceProvider>"+getResources().getString(R.string.serviceprovider)+"</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>P2M</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<MobileNumber>"+bmobile+"</MobileNumber>" +
                        "<MerchantId>"+ merchantId +"</MerchantId>" +
                        "<Amount>"+ amount +"</Amount>" +
                        "<Tpin>"+ tpinhash +"</Tpin>" +
                        "<WalletId>"+ mWalletId +"</WalletId>" +
                        "<ResponseURL>"+ GlobalVariables.url+"</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        " </RequestDetails>" +
                        "</MpsXml>";

        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforSendMoney, MyApplication.getInstance().getKey(),iv,new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXMLforSendMoney)
                {
                    String final_String_for_SendMoney = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + bmobile + "|" + encryptedXMLforSendMoney;
                    new MyAsyncTaskForPayMerchant().execute(final_String_for_SendMoney);
                }
            });
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public class MyAsyncTaskForPayMerchant extends AsyncTask<String, Integer, String> {
        String result = "";

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            System.out.println("Response : " + s);

            progressDialog.dismiss();

            //Parse your response here
            if(result.equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                progressDialog.dismiss();
            }else {

                try {
                    AESecurity aess = new AESecurity();
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted)
                        {
                            Log.d("TAG","Pay a merchant Pay200="+decrypted);
                            Log.d("TAG","mobileNumber Pay200="+mobileNumber);
                            ParseResponse parse = new ParseResponse();
                            InputSource is = new InputSource(new StringReader(decrypted));
                            MpsXml mpsxml = parse.parseXML(is);

                            if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success")) {

//                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                                builder.setMessage(mpsxml.getResponsedetails().getMessage())
//                                        .setTitle("mWallet");
//                                AlertDialog dialog = builder.create();
//                                dialog.setCanceledOnTouchOutside(true);
//                                dialog.show();


                                ResponseDetails responseDetails=new ResponseDetails();
                                responseDetails=mpsxml.getResponsedetails();
                                MyApplication.getInstance().setGeneralBalance(""+responseDetails.getBalance());

                                PayPersonSuccess rms = new PayPersonSuccess();
                                Bundle bundle = new Bundle();
                                bundle.putString("id",sMerchantId);
                                bundle.putString("time",mpsxml.getHeader().getTimestamp());
                                bundle.putSerializable("responseDetails", responseDetails);
                                rms.setArguments(bundle);
                                FragmentTransaction fragmentTransaction = GlobalVariables.fragmentManager
                                        .beginTransaction();
                                fragmentTransaction.addToBackStack(rms.getClass()
                                        .getName());
                                fragmentTransaction.add(R.id.dashboard_container, rms);
                                fragmentTransaction.commit();
                                progressDialog.dismiss();

                                /*Intent toLogin = new Intent(ChangeTPin.this,MainActivity.class);
                                startActivity(toLogin);
*/
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getReason())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                //progressDialog.dismiss();
                            }
                        }
                    });
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException |
                        InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    // utility function for encryption of MPIN
    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }
    private void LayoutInitialization(View view) {
        payByCard=(Button)view.findViewById(R.id.button_childrow_sendmoney);
        mainLayout=(LinearLayout)view.findViewById(R.id.root_layout);
        merchantID= (TextView) view.findViewById(R.id.edit_text_merchandid);
        amount= (TextView) view.findViewById(R.id.edit_text_amount);
        tpin= (EditText) view.findViewById(R.id.edit_text_childrow);
        mWalletAmount= (TextView) view.findViewById(R.id.mWalletAmount);
        textViewforgottpin= (TextView) view.findViewById(R.id.textViewforgottpin);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pay_merchant_qrcode_payby_wallet_fragment, container, false);
    }
}
