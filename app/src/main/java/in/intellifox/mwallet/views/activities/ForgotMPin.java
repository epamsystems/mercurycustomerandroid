package in.intellifox.mwallet.views.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.AppSettings;
import in.intellifox.mwallet.utilities.GlobalVariables;

/**
 * Created by Owner on 9/8/2015.
 */
public class ForgotMPin extends ActionBarActivity {

    private ProgressDialog progressDialog = null;
    EditText mobile_no,date_of_birth,emailID,edtPinCode;
    String mobileNumber = "",Dateofbirth= "",EmailId = "",PinCode="";
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String xmlvalue = "",timeStamp="";
    ImageView back_to_login;
    Button confirm ;

    String iv = "288dca8258b1dd7c";
    String xmlvalueforencryptionKey="";
    String key="";
    TextView actionBarTitleText,actionBarPersonText,actionBarPersonTextValue;
    ImageView actionBarAppLogoImage,actionBarBackImage,actionBarNotificationImage,actionBarTagImage,actionBarQrcodeImage,
            actionBarSearchImage,actionBarNavagationImage;

    RelativeLayout llNotification;
    TextView txtNotificationCount;

    private Calendar cal;
    private int day;
    private int month;
    private int year;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_mpin);
        LayoutInitialization();
        setUpActionBar();

        date_of_birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(0);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }

                mobileNumber = mobile_no.getText().toString();
                Dateofbirth = date_of_birth.getText().toString();
                EmailId = emailID.getText().toString();
                PinCode = emailID.getText().toString();


                if (mobileNumber.length() < 1 && Dateofbirth.length() < 1 && EmailId.length() < 1) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotMPin.this);
                    builder.setMessage(getResources().getString(R.string.mobile_number_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } else if (mobileNumber.length() <=5) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotMPin.this);
                    builder.setMessage(getResources().getString(R.string.invalid_mobile_number))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } else if (Dateofbirth.length() < 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotMPin.this);
                    builder.setMessage(getResources().getString(R.string.DOB_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } /*else if (!(EmailId.matches(emailPattern))) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotMPin.this);
                    builder.setMessage(getResources().getString(R.string.invalid_Doc_Verification_number))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } */else {

                    callGetEncryptionKey(mobileNumber);
                }
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        handleNotificationCount();
    }


    private void handleNotificationCount() {
        String c = (AppSettings.getData(this, AppSettings.NOTIFICATION_COUNT).equals("")) ? "0" : AppSettings.getData(this, AppSettings.NOTIFICATION_COUNT);
        int count = Integer.parseInt(c);
        if (count > 0) {
            txtNotificationCount.setText(count + "");
            txtNotificationCount.setVisibility(View.VISIBLE);
        } else
            txtNotificationCount.setVisibility(View.GONE);
    }

    private void LayoutInitialization() {
        mobile_no=(EditText)findViewById(R.id.edit_text_mobile_number);
        date_of_birth=(EditText)findViewById(R.id.edit_text_birthdate);
        emailID=(EditText)findViewById(R.id.edit_text_emailID);
        confirm=(Button)findViewById(R.id.button_submit);
        TimeZone cst=TimeZone.getTimeZone("Asia/Kolkata");
        cal=Calendar.getInstance(cst);

        day=cal.get(Calendar.DAY_OF_MONTH);
        month=cal.get(Calendar.MONTH);
        year=cal.get(Calendar.YEAR);
    }

    protected Dialog onCreateDialog(int id)
    {
        Log.d("TAG","id="+id);
        DatePickerDialog dialog = new DatePickerDialog(this, datePickerListener, year, month, day);
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        return dialog;

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear,int selectedMonth, int selectedDay)
        {
            if(validatePastDate(getApplicationContext(),selectedDay,selectedMonth,selectedYear))
            {
                String sDay=""+selectedDay;
                String smonth=""+(selectedMonth+1);
                if(selectedDay<10)
                {
                    sDay="0"+sDay;
                }
                if(selectedMonth<9)
                {
                    smonth="0"+smonth;
                }
                date_of_birth.setText(sDay + "-" + smonth + "-"+ selectedYear);
            }else {
                date_of_birth.setText("");
                AlertDialog.Builder builder = new AlertDialog.Builder(ForgotMPin.this);
                builder.setMessage("Date of Birth will not accept future date.")
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
            }

        }
    };

    public static boolean validatePastDate(Context mContext,int day,int month,int year){
        final Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR);
        int currentMonth = c.get(Calendar.MONTH);
        int currentDay = c.get(Calendar.DAY_OF_MONTH);
        if (day > currentDay && year == currentYear && month == currentMonth) {
            return false;
        } else if (month > currentMonth && year == currentYear) {
            return false;
        } else if (year > currentYear) {
            return false;
        }

        return true;
    }

    public void  callForgotMpin(final String MOBILE,String DATEOFBIRTH,String PINCODE,String EMAILID){

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp=dateFormat.format(cal.getTime());

        System.out.println("MOBILE NO " + MOBILE + "DATEOFBIRTH " + DATEOFBIRTH + "PIN CODE " + PINCODE + "EMAIL ID " + EMAILID);
        xmlvalue = "<?xml version='1.0' encoding='UTF-8'?>" +
                "<MpsXml>" +
                "<Header>" +
                "<ChannelId>APP</ChannelId>" +
                "<Timestamp>"+timeStamp+"</Timestamp>" +
                "<SessionId>"+ GlobalVariables.sessionId+"</SessionId>" +
                "<ServiceProvider>"+getResources().getString(R.string.serviceprovider)+"</ServiceProvider>" +
                "</Header>" +
                "<Request>" +
                "<RequestType>ForgotMpin</RequestType>" +
                "<UserType>CU</UserType>" +
                "</Request>" +
                "<RequestDetails>" +
                "<MobileNumber>"+MOBILE+"</MobileNumber>" +
                "<DateOfBirth>"+DATEOFBIRTH+"</DateOfBirth>" +
                "<PinCode>"+PINCODE+"</PinCode>" +
              /*  "<EmailId>" + EMAILID + "</EmailId>" +*/
                "<ResponseURL>"+GlobalVariables.url+"</ResponseURL>" +
                "<ResponseVar></ResponseVar>" +
                "</RequestDetails>" +
                "</MpsXml>";
        System.out.println("Request xml"+xmlvalue);
        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlvalue, MyApplication.getInstance().getKey(),iv,new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXML) {
                    String  final_string = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + MOBILE + "|" + encryptedXML;
                    Log.v("Plain Request ", " " + final_string);
                    new MyAsyncTaskForgotPin().execute(final_string);
                }
            });
        } catch (NoSuchAlgorithmException | IllegalBlockSizeException | NoSuchPaddingException | InvalidAlgorithmParameterException |
                BadPaddingException | UnsupportedEncodingException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public void callGetEncryptionKey(String MobileNo){

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());

        xmlvalueforencryptionKey = "<MpsXml> " +
                "   <Header> " +
                "      <ChannelId>APP</ChannelId> " +
                "      <Timestamp>" + timeStamp + "</Timestamp> " +
                "      <SessionId>TEST06AUG15</SessionId> " +
                "      <ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider> " +
                "   </Header> " +
                "   <Request> " +
                "      <RequestType>GetKey</RequestType> " +
                "      <UserType>CU</UserType> " +
                "   </Request> " +
                "   <RequestDetails> " +
                "      <MobileNumber>" + MobileNo + "</MobileNumber> " +
                "      <ResponseURL>" + GlobalVariables.url + "</ResponseURL> " +
                "      <ResponseVar>mobileResponseXML</ResponseVar> " +
                "   </RequestDetails> " +
                "</MpsXml> ";

        String final_String_for_getKey = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + MobileNo;
        //String final_String_for_getKey = "<"+getResources().getString(R.string.serviceprovider)+">|<"+getResources().getString(R.string.usertype)+">|<"+Mob+">|<"+EncryptedXML+">";
        new MyAsyncTaskForGetEncryptionKey().execute(final_String_for_getKey);
    }

    public class MyAsyncTaskForGetEncryptionKey extends AsyncTask<String, Integer, String> {

        String result = "";
        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException | ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ForgotMPin.this, null, getResources().getString(R.string.please_wait_msg), true);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("Response : " + s);

            if(result.equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
            }else{
                MyApplication.getInstance().setKey(s);
                key = MyApplication.getInstance().getKey();
                //  callLogin(mobileno, mpinhash);
                callForgotMpin(mobileNumber,Dateofbirth,PinCode,EmailId);
            }
        }
    }

    //Set custom Action bar
    private void setUpActionBar() {
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_dashboard);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));

        //Initialization of Action bar components
        actionBarNavagationImage= (ImageView) findViewById(R.id.action_bar_navagation_image);
        actionBarNavagationImage.setVisibility(View.GONE);
        actionBarBackImage=(ImageView)findViewById(R.id.action_bar_back);
        actionBarTitleText = (TextView) findViewById(R.id.action_bar_title);
        actionBarTitleText.setText("Forgot mPIN");

        //actionBarNotificationImage = (ImageView) findViewById(R.id.actionbar_notification);
        //actionBarNotificationImage.setVisibility(View.GONE);
        llNotification = (RelativeLayout) findViewById(R.id.toolbar_ll_notification);
        txtNotificationCount = (TextView) findViewById(R.id.txtCount);
        llNotification.setVisibility(View.GONE);

      //  actionBarTagImage = (ImageView) findViewById(R.id.actionbar_tag);
//        actionBarTagImage.setVisibility(View.GONE);
        actionBarQrcodeImage = (ImageView) findViewById(R.id.actionbar_qrcode);
        actionBarQrcodeImage.setVisibility(View.GONE);
        actionBarAppLogoImage = (ImageView) findViewById(R.id.action_bar_imageview_app_logo);
        actionBarAppLogoImage.setVisibility(View.GONE);
        actionBarSearchImage = (ImageView) findViewById(R.id.actionbar_search);
        actionBarSearchImage.setVisibility(View.GONE);
        actionBarPersonText= (TextView) findViewById(R.id.action_bar_PersonID);
        actionBarPersonText.setVisibility(View.GONE);
        actionBarPersonTextValue= (TextView) findViewById(R.id.action_bar_PersonAmount);
        actionBarPersonTextValue.setVisibility(View.GONE);

        actionBarBackImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public class MyAsyncTaskForgotPin extends AsyncTask<String,Integer,String> {

        String result = "";

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
           // HttpPost httppost = new HttpPost(getResources().getString(R.string.requesturl));
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            Log.v("Request","" + params[0]);
            List<BasicNameValuePair> pairs=new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML",  params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));

                HttpResponse response= httpclient.execute(httppost);

                Log.v("Response1","" + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : "+result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("Response : "+s);

            if(result.equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(ForgotMPin.this);
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.show();

            }else {
                try {
                    AESecurity aess = new AESecurity();
                    aess.decrypt(result,MyApplication.getInstance().getKey(),iv,new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted) {
                            ParseResponse parse = new ParseResponse();
                            InputSource is = new InputSource(new StringReader(decrypted));
                            MpsXml mpsxml = parse.parseXML(is);

                            if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(ForgotMPin.this);
                                builder.setMessage(mpsxml.getResponsedetails().getMessage())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();

                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        mobile_no.setText("");
                                        date_of_birth.setText("");
                                        emailID.setText("");
                                        finish();
                                    }
                                });

                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(ForgotMPin.this);
                                builder.setMessage(mpsxml.getResponsedetails().getReason())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();

                            }
                            progressDialog.dismiss();

                            progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    mobile_no.setText("");
                                    date_of_birth.setText("");
                                    emailID.setText("");
                                }
                            });
                        }
                    });
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException
                        | InvalidKeyException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
