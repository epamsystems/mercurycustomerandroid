package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Beneficiary;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/18/2015.
 */
public class PayPersonMain extends Fragment {

    ImageView favourite,phoneBook,email,facebook;
    PayPersonEmailFragment payPersonEmailFragment;
    PayPersonFacebookFragment payPersonFacebookFragment;
    PayPersonPhoneBookFragment payPersonPhoneBookFragment;
    PayPersonFavouriteFragment payPersonFavouriteFragment;
    static String wAmount="";
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        PayPersonMain payPersonMain = new PayPersonMain();
        GlobalVariables.fragmentManager = getFragmentManager();
        try {
            Bundle args = getArguments();
            Beneficiary beneficiary = (Beneficiary) args.getSerializable("SendMoney");
            Log.d("TAG", "beneficiary=" + beneficiary);
            if(beneficiary!=null)
            {
                payPersonPhoneBookFragment=new PayPersonPhoneBookFragment();
                args.putSerializable("SendMoney",beneficiary);
                payPersonPhoneBookFragment.setArguments(args);
                favourite.setImageResource(R.drawable.request_money_favorites_deselected);
                phoneBook.setImageResource(R.drawable.request_money_phonebook_selected);
                email.setImageResource(R.drawable.request_money_email_deselected);
                facebook.setImageResource(R.drawable.request_money_facebook_deselected);

                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, payPersonPhoneBookFragment).commit();

            }else {
                //Default Fragment
                PayPersonFavouriteFragment rmf=new PayPersonFavouriteFragment();
                Bundle bundle = new Bundle();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, rmf).commit();

            }
        }catch (Exception e)
        {
            //Default Fragment
            PayPersonFavouriteFragment rmf=new PayPersonFavouriteFragment();
            Bundle bundle = new Bundle();
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, rmf).commit();

        }
        favourite.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                payPersonFavouriteFragment=new PayPersonFavouriteFragment();
                Bundle bundle = new Bundle();
                favourite.setImageResource(R.drawable.request_money_favorite_selected);
                phoneBook.setImageResource(R.drawable.request_money_phonebook_deselected);
                email.setImageResource(R.drawable.request_money_email_deselected);
                facebook.setImageResource(R.drawable.request_money_facebook_deselected);
                GlobalVariables.fragmentManager = getFragmentManager();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, payPersonFavouriteFragment).commit();
            }
        });

        phoneBook.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                payPersonPhoneBookFragment=new PayPersonPhoneBookFragment();
                Bundle bundle = new Bundle();
                favourite.setImageResource(R.drawable.request_money_favorites_deselected);
                phoneBook.setImageResource(R.drawable.request_money_phonebook_selected);
                email.setImageResource(R.drawable.request_money_email_deselected);
                facebook.setImageResource(R.drawable.request_money_facebook_deselected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, payPersonPhoneBookFragment).commit();

            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payPersonEmailFragment=new PayPersonEmailFragment();
                Bundle bundle = new Bundle();
                favourite.setImageResource(R.drawable.request_money_favorites_deselected);
                phoneBook.setImageResource(R.drawable.request_money_phonebook_deselected);
                email.setImageResource(R.drawable.request_money_email_selected);
                facebook.setImageResource(R.drawable.request_money_facebook_deselected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, payPersonEmailFragment).commit();
            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payPersonFacebookFragment=new PayPersonFacebookFragment();
                Bundle bundle = new Bundle();
                favourite.setImageResource(R.drawable.request_money_favorites_deselected);
                phoneBook.setImageResource(R.drawable.request_money_phonebook_deselected);
                email.setImageResource(R.drawable.request_money_email_deselected);
                facebook.setImageResource(R.drawable.request_money_facebook_selected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, payPersonFacebookFragment).commit();
            }
        });

    }

    private void LayoutInitialization(View view) {

        favourite=(ImageView) view.findViewById(R.id.request_money_layout_position_0);
        phoneBook=(ImageView) view.findViewById(R.id.request_money_layout_position_1);
        email=(ImageView) view.findViewById(R.id.request_money_layout_position_2);
        facebook=(ImageView) view.findViewById(R.id.request_money_layout_position_3);
        final RelativeLayout frag_container = (RelativeLayout) view.findViewById(R.id.request_money_main_fragment_container);
    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String walletBalance = MyApplication.getInstance().getTotalBalance();
        if(walletBalance!=null)
        {
            wAmount=walletBalance;

        }
        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Pay a Person", true,"mWallet Balance", true,"AED "+wAmount);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.request_money_main, container, false);
    }
}
