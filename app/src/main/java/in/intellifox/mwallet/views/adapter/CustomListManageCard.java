package in.intellifox.mwallet.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Card;

/**
 * Created by Owner on 9/3/2015.
 */
public class CustomListManageCard extends BaseAdapter {

    private Context mContext;
    private String[] mItemTitles;
    private LayoutInflater mInflater;
    private int[] mImageid ;
    String date[]={"05 JUN 15","05 JUN 15"};
    String traId[]={"1234567890","1234567890"};
    String amount[]={"AED 120","AED 120"};
    public CustomListManageCard(Context mContext) {
        this.mContext = mContext;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CustomListManageCard(Context mContext, int[] mImageid, String[] mItemTitles) {
        this.mContext = mContext;
        this.mItemTitles = mItemTitles;
        this.mImageid=mImageid;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return date.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        view = new View(mContext);
        view = mInflater.inflate(R.layout.manage_savecard_listview_item,null);
        TextView text = (TextView) view.findViewById(R.id.savecard_textView_date);
        TextView text2 = (TextView) view.findViewById(R.id.savecard_textView_transactionID);
        TextView text3 = (TextView) view.findViewById(R.id.savecard_textView_amount);
        text.setText(date[position]);
        text2.setText(traId[position]);
        text3.setText(amount[position]);

        return view;
    }
}
