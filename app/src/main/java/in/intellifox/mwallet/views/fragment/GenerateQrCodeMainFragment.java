package in.intellifox.mwallet.views.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.utilities.DecimalDigitsInputFilter;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/14/2015.
 */
public class GenerateQrCodeMainFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Button confirm;
    GenerateQrCodeFinalFragment generateQrCodeFinalFragment;
    EditText enterAmount;
    private OnFragmentInteractionListener mListener;
    RelativeLayout parent_lat_generate_qr;
    String qrAmount="",merchantName="",merchantId="",mobileNo="";
    String FirstName = "", LastName = "";

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GenerateQRCodeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GenerateQrCodeMainFragment newInstance(String param1, String param2,String MerchantName,String MerchantId,String MobileNo) {
        GenerateQrCodeMainFragment fragment = new GenerateQrCodeMainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString("FirstName",MerchantName);
        args.putString("LastName",MerchantId);
        args.putString("mobile",MobileNo);
//        args.putString("FirstName",FirstName);
//        args.putString("LastName",MobileNo);
//        args.putString("MobileNumber",MobileNo);


        fragment.setArguments(args);
        return fragment;
    }

    public GenerateQrCodeMainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            merchantName = getArguments().getString("FirstName");
            merchantId =  getArguments().getString("LastName");
            mobileNo = getArguments().getString("mobile");


        }
    }

    public void setData(Context context,View view){

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        MyApplication.resetTimer(getActivity());


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
                qrAmount = enterAmount.getText().toString();
                if (enterAmount.length() < 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.amount_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.show();

                } else {
                    // System.out.println("amount"+qrAmount+"merchant name "+merchantName+"merchantId"+merchantId);
                    enterAmount.setText("");
                    //generateQrCodeFinalFragment = GenerateQrCodeFinalFragment.newInstance("", qrAmount, merchantName, merchantId, mobileNo);
                    generateQrCodeFinalFragment = GenerateQrCodeFinalFragment.newInstance("", qrAmount);
                    //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, generateQrCodeFinalFragment).addToBackStack(null).commit();

                    FragmentManager fragmentManager =getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(generateQrCodeFinalFragment.getClass().getName());
                    fragmentTransaction.replace(R.id.dashboard_container, generateQrCodeFinalFragment);
                    fragmentTransaction.commit();
                }
            }
        });

        parent_lat_generate_qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        });
    }

    private void LayoutInitialization(View view) {
        enterAmount = (EditText) view.findViewById(R.id.edit_text_enter_amount_qrcode);
        enterAmount.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(15,2)});
        confirm=(Button)view.findViewById(R.id.button_confirm_generate_qrcode);
        parent_lat_generate_qr = (RelativeLayout)view.findViewById(R.id.parent_lat_generate_qr);
    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Generate QR code", false, "", false, "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.generate_qrcode_fragment, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onPause() {
        super.onPause();
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
