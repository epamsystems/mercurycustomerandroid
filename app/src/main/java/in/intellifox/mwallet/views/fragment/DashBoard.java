package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/3/2015.
 */
public class DashBoard extends Fragment {

    Fragment dashBoardTop, dashBoardBottom;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    View v;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    String walletBalance = "";

    @Nullable
    public static DashBoard newInstance(String param1, String param2, String WalletBal) {
        DashBoard fragment = new DashBoard();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString("walletBal", WalletBal);
        fragment.setArguments(args);
        return fragment;
    }

    public DashBoard() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            walletBalance = getArguments().getString("walletBal");
        }
       /* Bundle b = this.getArguments();
        walletBalance = b.getString("walletBal").toString();*/
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        dashBoardTop = new DashBoardTop();
        dashBoardBottom = new DashBoardBottom();
        fragmentTransaction.add(R.id.dashboard_first_part, dashBoardTop);
        fragmentTransaction.add(R.id.dashboard_second_part, dashBoardBottom);
        fragmentTransaction.commit();


    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain) getActivity()).setUpActionBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dashboard_main, container, false);
    }


}
