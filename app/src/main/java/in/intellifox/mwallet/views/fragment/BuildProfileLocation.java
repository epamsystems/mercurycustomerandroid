package in.intellifox.mwallet.views.fragment;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Beneficiary;
import in.intellifox.mwallet.parser.Profile;

/**
 * Created by Owner on 9/4/2015.
 */
public class BuildProfileLocation extends Fragment {


    Button proceed;
    EditText location,city,state;
    private BuildProfileKYCDocumentList kycdocument;
    BuildProfileMainFragment buildProfileMainFragment;
    Profile profile=new Profile();
    private String sLocation="",sCity="",sState="";
    ImageView backarrow;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        proceed=(Button)view.findViewById(R.id.build_profile_loc_proceed_button);
        location= (EditText) view.findViewById(R.id.country_buildprofile_location);
        city= (EditText) view.findViewById(R.id.address_buildprofile_location);
        state= (EditText) view.findViewById(R.id.pincode_buildprofile_location);
        backarrow= (ImageView) view.findViewById(R.id.backarrow);
        final RelativeLayout frag_container = (RelativeLayout) view.findViewById(R.id.fragment_container);
        try{
            Bundle args = getArguments();
            profile=(Profile) args.getSerializable("Profile");

        }catch (Exception e)
        {

        }
        backarrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                buildProfileMainFragment = new BuildProfileMainFragment();
//                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, buildProfileMainFragment).commit();
                FragmentManager manager = getActivity().getSupportFragmentManager();

                for(int i = 0; i< manager.getBackStackEntryCount(); i++) {
                    manager.popBackStackImmediate();
                }

            }
        });
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                sLocation=location.getText().toString();
                sCity=city.getText().toString();
                sState=state.getText().toString();
                if (sLocation.length() <= 1 )
                {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter street name!!!")
                            .setTitle("Alert");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                }else if(sCity.length()<=1)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter city name!!!")
                            .setTitle("Alert");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }
                else if(sState.length()<=1)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter state name!!!")
                            .setTitle("Alert");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }
                else
                {
                    profile.setAddress(sLocation);
                    profile.setCity(sCity);
                    profile.setState(sState);
                    kycdocument = new BuildProfileKYCDocumentList();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Profile", profile);
                    kycdocument.setArguments(bundle);

                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(kycdocument.getClass().getName());
                    //fragmentTransaction.add(R.id.build_profile_location, kycdocument);
                    fragmentTransaction.add(R.id.build_profile_location, kycdocument);
                    fragmentTransaction.commit();

                    //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, kycdocument).commit();
                }
            }
        });

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.buildprofile_location, container, false);
    }


}
