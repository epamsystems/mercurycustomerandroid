package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/9/2015.
 */
public class ManageCardMainFragment extends Fragment {

    View v;
    ImageView saveCard, addCard;
    private ManageSaveCardFragment saveCardFragment;
    private ManageAddCardFragment addCardFragment;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        saveCard = (ImageView) view.findViewById(R.id.layout_position_0);
        addCard = (ImageView) view.findViewById(R.id.layout_position_1);
        final RelativeLayout frag_container = (RelativeLayout) view.findViewById(R.id.fragment_container);

        //Default Fragment
        ManageSaveCardFragment sc=new ManageSaveCardFragment();
        Bundle b = new Bundle();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, sc).commit();


        saveCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveCardFragment = new ManageSaveCardFragment();
                Bundle bundle = new Bundle();
                saveCard.setImageResource(R.drawable.manage_card_saved_cards_selected);
                addCard.setImageResource(R.drawable.manage_card_add_card_deselected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, saveCardFragment).commit();
            }
        });

        addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCardFragment=new ManageAddCardFragment();
                Bundle bundle = new Bundle();
                saveCard.setImageResource(R.drawable.manage_card_saved_cards_deselected);
                addCard.setImageResource(R.drawable.manage_card_add_card_selected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, addCardFragment).commit();
            }
        });
    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Manage Cards",false,"",false,"");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.manage_card_main, container, false);
    }
}
