package in.intellifox.mwallet.views.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Beneficiary;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.fragment.BeneficiariesMain;
import in.intellifox.mwallet.views.fragment.PayPersonMain;
import in.intellifox.mwallet.views.fragment.RequestMoneyMainFragment;

/**
 * Created by Owner on 12/12/2015.
 */
public class CustomListFavourite extends BaseSwipeAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    TextView textViewNumber,textViewName,textViewEmail,sTextViewNumber,sTextViewName;
    ImageView profileImage,deleteBeneficiary;
    private String timeStamp= "";
    private String xmlforBeneficiary= "";
    String bmobile="9200000011";
    String btpin="111111";
    String iv = "288dca8258b1dd7c";
    private ProgressDialog progressDialog;

    int image[]={R.drawable.beneficiaries_profile,R.drawable.beneficiaries_profile,R.drawable.beneficiaries_profile,R.drawable.beneficiaries_profile};
    private ArrayList<Beneficiary> beneficiaryArrayList=new ArrayList<Beneficiary>();
    public CustomListFavourite(Context mContext, ArrayList<Beneficiary> beneficiaryArrayList) {
        this.mContext = mContext;
        this.beneficiaryArrayList=beneficiaryArrayList;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getSwipeLayoutResourceId(int i)
    {
        return R.id.swipe;
    }

    @Override
    public View generateView(final int i, ViewGroup viewGroup) {
        View view;
        view = new View(mContext);
        view = mInflater.inflate(R.layout.favourites_listview_item,null);
        SwipeLayout swipeLayout = (SwipeLayout)view.findViewById(R.id.swipe);
        swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                super.onOpen(layout);

            }
        });
        textViewName=(TextView)view.findViewById(R.id.textView_beneficiaries_name);
        textViewName.setText(beneficiaryArrayList.get(i).getNickName());
        textViewNumber=(TextView)view.findViewById(R.id.textView_beneficiaries_mobile);
        textViewNumber.setText(beneficiaryArrayList.get(i).getValue());
        sTextViewNumber=(TextView)view.findViewById(R.id.textView_beneficiaries_mobile1);
        sTextViewNumber.setText(beneficiaryArrayList.get(i).getValue());
        sTextViewName=(TextView)view.findViewById(R.id.textView_beneficiaries_name1);
        sTextViewName.setText(beneficiaryArrayList.get(i).getNickName());
        ImageView deleteBeneficiary=(ImageView)view.findViewById(R.id.image_view_of_beneficiaries_trash);
        deleteBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                new AlertDialog.Builder(GlobalVariables.mActivity)
                        .setTitle("Please Note!!!")
                        .setMessage("Are you sure you want to remove this beneficiary?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                bmobile= MyApplication.getInstance().getMerchantMobieNo();
                                RemoveBeneficiary(bmobile, beneficiaryArrayList.get(i).getBeneficiaryid());
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
        ImageView planeBeneficiary=(ImageView)view.findViewById(R.id.image_view_of_beneficiaries_plane);
        planeBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Beneficiary beneficiary = new Beneficiary();
                beneficiary = beneficiaryArrayList.get(i);
                Bundle args = new Bundle();
                args.putSerializable("SendMoney", beneficiary);
                PayPersonMain payPersonMain = new PayPersonMain();
                payPersonMain.setArguments(args);
                FragmentTransaction fragmentTransaction = GlobalVariables.fragmentManager
                        .beginTransaction();
                fragmentTransaction.addToBackStack(payPersonMain.getClass()
                        .getName());
                fragmentTransaction.add(R.id.dashboard_container, payPersonMain);
                fragmentTransaction.commit();
            }
        });
        ImageView moneyBeneficiary=(ImageView)view.findViewById(R.id.image_view_of_beneficiaries_money);
        moneyBeneficiary.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Beneficiary beneficiary=new Beneficiary();
                beneficiary=beneficiaryArrayList.get(i);
                Bundle args = new Bundle();
                args.putSerializable("RequestMoney",beneficiary);
                RequestMoneyMainFragment requestMoneyMainFragment = new RequestMoneyMainFragment();
                requestMoneyMainFragment.setArguments(args);
                FragmentTransaction fragmentTransaction = GlobalVariables.fragmentManager
                        .beginTransaction();
                fragmentTransaction.addToBackStack(requestMoneyMainFragment.getClass()
                        .getName());
                fragmentTransaction.add(R.id.dashboard_container, requestMoneyMainFragment);
                fragmentTransaction.commit();
            }
        });
        return view;
    }

    @Override
    public void fillValues(int i, View view) {

    }

    @Override
    public int getCount() {
        return beneficiaryArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    public void RemoveBeneficiary( final String Nomobile,String beneficiaryId) {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
        xmlforBeneficiary = "<MpsXml>" +
                "<Header>" +
                "<ChannelId>APP</ChannelId>" +
                "<Timestamp>" + timeStamp + "</Timestamp>" +
                "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                "<ServiceProvider>" +  mContext.getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                "</Header>" +
                "<Request>" +
                "<RequestType>RemoveBene</RequestType>" +
                "<UserType>CU</UserType>" +
                "</Request>" +
                "<RequestDetails>" +
                "<MobileNumber>" + Nomobile + "</MobileNumber>" +
                "<Beneficiary>" + beneficiaryId + "</Beneficiary>" +
                "<ResponseURL>" +  GlobalVariables.url+ "</ResponseURL>" +
                "<ResponseVar>mobileResponseXML</ResponseVar>" +
                "</RequestDetails>" +
                "</MpsXml>";
        Log.v("xml", xmlforBeneficiary);
        Log.d("TAG", "xmlforBeneficiary 100=" + xmlforBeneficiary.toString());
        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforBeneficiary, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption()
            {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXML)
                {
                    String final_String_for_beneficiary =  mContext.getResources().getString(R.string.serviceprovider) + "|" +  mContext.getResources().getString(R.string.usertype) + "|" + Nomobile + "|" + encryptedXML;
                    Log.v("Full URL ", "Final URL For bank details : " + final_String_for_beneficiary);
                    Log.d("TAG", "final_String_for_beneficiary 101=" + final_String_for_beneficiary.toString());
                    new MyAsyncTaskforRemoveBeneficiary().execute(final_String_for_beneficiary);
                }
            } );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class MyAsyncTaskforRemoveBeneficiary extends AsyncTask<String,Integer,String>
    {
        String result = "";
//         private ProgressDialog progressDialog = null;

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException | ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT Bank: " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(GlobalVariables.mActivity, null, mContext.getResources().getString(R.string.please_wait_msg), true);
        }

        @Override
        protected void onPostExecute(String s1) {
            super.onPostExecute(s1);
            String decryptedString = "";

            System.out.println("Response Remove Beneficiary: " + s1);
            final MpsXml[] mpsxml = new MpsXml[1];
            final ParseResponse[] parse = new ParseResponse[1];
            try {
                AESecurity aess = new AESecurity();
                try {
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption()
                    {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted)
                        {
                            if (decrypted.equals(""))
                            {
                                AlertDialog.Builder builder = new AlertDialog.Builder(GlobalVariables.mActivity);
                                builder.setMessage(mContext.getResources().getString(R.string.no_internet_connection))
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                //dialog.setCanceledOnTouchOutside(true);
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                            }
                            else {
                                Log.d("TAG", "Remove beneficiary=" + decrypted);
                                parse[0] = new ParseResponse();
                                InputSource is = new InputSource(new StringReader(decrypted));
                                mpsxml[0] = parse[0].parseXML(is);
                                if (mpsxml[0].getResponse().getResponseType().equalsIgnoreCase("Success"))
                                {
                                     MyApplication.getInstance().setGeneralBalance(mpsxml[0].getResponsedetails().getBalance());

                                      progressDialog.dismiss();
                                    BeneficiariesMain beneficiariesMain = new BeneficiariesMain();
//                                    FragmentTransaction fragmentTransaction = GlobalVariables.fragmentManager
//                                            .beginTransaction();
//                                    fragmentTransaction.replace(R.id.dashboard_container, beneficiariesMain);
//                                    fragmentTransaction.commit();
                                    GlobalVariables.fragmentManager.beginTransaction().replace(R.id.dashboard_container, beneficiariesMain).addToBackStack(null).commit();
                                }
                                else {
                                    String reason=mpsxml[0].getResponsedetails().getReason();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(GlobalVariables.mActivity);
                                    builder.setMessage(mpsxml[0].getResponsedetails().getReason())
                                            .setTitle("mWallet");
                                    AlertDialog dialog = builder.create();
                                    dialog.setCanceledOnTouchOutside(true);
                                    dialog.show();
                                }
                                progressDialog.dismiss();
                            }
                        }
                    });
                } catch (InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException |
                        BadPaddingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                e.printStackTrace();
            }
        }
    }

}

