package in.intellifox.mwallet.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import in.intellifox.mwallet.R;

/**
 * Created by Owner on 9/15/2015.
 */
public class CustomListFacebookRequestMoney extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    TextView textViewName,textViewMobile,textViewEmail;
    String name[]={"Abcdef Ghijklmn","Abcdef Ghijklmn","Abcdef Ghijklmn","Abcdef Ghijklmn","Abcdef Ghijklmn"};
    String mobile[]={"0123456789","0123456789","0123456789","0123456789","0123456789"};
    String email[]={"abcdefgh@abcdefghijk.com","abcdefgh@abcdefghijk.com","abcdefgh@abcdefghijk.com","abcdefgh@abcdefghijk.com","abcdefgh@abcdefghijk.com"};

    public CustomListFacebookRequestMoney(Context mContext) {
        this.mContext = mContext;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return mobile.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        view = new View(mContext);
        view = mInflater.inflate(R.layout.request_money_facebook_listview_item,null);
        textViewName=(TextView)view.findViewById(R.id.textView_facebook_name);
        textViewMobile=(TextView)view.findViewById(R.id.textView_facebook_mobile);
        textViewEmail=(TextView)view.findViewById(R.id.textView_facebook_email);
        textViewName.setText(name[position]);
        textViewMobile.setText(mobile[position]);
        textViewEmail.setText(email[position]);
        return view;
    }
}
