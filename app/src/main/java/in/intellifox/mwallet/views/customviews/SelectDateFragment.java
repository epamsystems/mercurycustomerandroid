package in.intellifox.mwallet.views.customviews;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

/**
 * Created by Owner on 10/28/2015.
 */
public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

   public EditText activity_edittext;
//    public SelectDateFragment(EditText edit_text) {
//        activity_edittext = edit_text;
//    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        activity_edittext.setText(day + "-" + (month+1) + "-" + year);
    }
}
