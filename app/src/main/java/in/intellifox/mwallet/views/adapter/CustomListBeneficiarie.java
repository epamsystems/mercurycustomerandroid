package in.intellifox.mwallet.views.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.fragment.AirTimeTopupMain;

/**
 * Created by Owner on 9/19/2015.
 */
public class CustomListBeneficiarie extends BaseSwipeAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    TextView textViewNumber,textViewName,textViewEmail;
    ImageView profileImage;
    String name[]={"Abcdef Ghijklmn","Abcdef Ghijklmn","Abcdef Ghijklmn","Abcdef Ghijklmn"};
    String mobile[]={"0123456789","0123456789","0123456789","0123456789"};
    String email[]={"abcdefgh@abcdefghijk.com","abcdefgh@abcdefghijk.com","abcdefgh@abcdefghijk.com","abcdefgh@abcdefghijk.com"};
    int image[]={R.drawable.beneficiaries_profile,R.drawable.beneficiaries_profile,R.drawable.beneficiaries_profile,R.drawable.beneficiaries_profile};

    public CustomListBeneficiarie(Context mContext) {
        this.mContext = mContext;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getSwipeLayoutResourceId(int i) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int i, ViewGroup viewGroup) {
        View view;
        view = new View(mContext);
        view = mInflater.inflate(R.layout.beneficiaries_listview_item,null);
        SwipeLayout swipeLayout = (SwipeLayout)view.findViewById(R.id.swipe);
        swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                super.onOpen(layout);

            }
        });
        ImageView d=(ImageView)view.findViewById(R.id.image_view_of_beneficiaries_trash);
        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AirTimeTopupMain airTimeTopupMain = new AirTimeTopupMain();
                Bundle bundle = new Bundle();
                airTimeTopupMain.setArguments(bundle);
               // getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_content, airTimeTopupMain).commit();
            }
        });
        return view;
    }

    @Override
    public void fillValues(int i, View view) {

    }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
