package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import in.intellifox.mwallet.R;

/**
 * Created by Owner on 9/15/2015.
 */
public class RequestMoneyFacebookFragment extends Fragment {

    ImageView connectFB;
    RequestMoneyFacebookListFragment requestMoneyFacebookListFragment;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        connectFB=(ImageView)view.findViewById(R.id.facebook_image_connectFB);
        connectFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestMoneyFacebookListFragment=new RequestMoneyFacebookListFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container,requestMoneyFacebookListFragment).commit();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.request_money_facebook_fragment, container, false);
    }
}
