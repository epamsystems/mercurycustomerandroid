package in.intellifox.mwallet.views.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Profile;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.utilities.Utility;
import in.intellifox.mwallet.utilities.ValidationUtils;
import in.intellifox.mwallet.views.activities.BuildProfile;
import in.intellifox.mwallet.views.fragment.BuildProfileLocation;
import in.intellifox.mwallet.views.activities.DrawerMain;


/**
 * Created by Owner on 8/31/2015.
 */
public class BuildProfileMainFragment extends Fragment {

    EditText fName, lName, birthdate, email, mobileNumber;
    Button proceedButton;
    private BuildProfileLocation location;
    private ImageView imageView_ClickImage;
    private static ImageView imageView_ProfilePic;
    private static String profilePath = "";
    private ImageView imageView_UploadImage;
    private static final String DOCUMENT_FOLDER = "Movit";
    private static int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private static Activity activity;
    private ToggleButton switch1;
    TextView txtMale, txtFemale;
    Bitmap profile_bitmap;

    private String sFirstName = "", sLastName = "", sEmail = "", sGender = "", sBirthDate = "", sMobileNumber = "";
    Calendar myCalendar = Calendar.getInstance();

    Fragment locationFragment;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = getActivity();
        proceedButton = (Button) view.findViewById(R.id.proceed_button_buildprofile);
        imageView_ClickImage = (ImageView) view.findViewById(R.id.imageView_ClickImage);
        imageView_ProfilePic = (ImageView) view.findViewById(R.id.imageView_ProfilePic);
        imageView_UploadImage = (ImageView) view.findViewById(R.id.imageView_UploadImage);
        fName = (EditText) view.findViewById(R.id.name_buildprofile);
        lName = (EditText) view.findViewById(R.id.name_buildprofile1);
        birthdate = (EditText) view.findViewById(R.id.birth_buildprofile);
        email = (EditText) view.findViewById(R.id.email_buildprofile);
        mobileNumber = (EditText) view.findViewById(R.id.mobilephone_buildprofile);
        txtMale = (TextView) view.findViewById(R.id.txtMale);
        txtFemale = (TextView) view.findViewById(R.id.txtFemale);

        switch1 = (ToggleButton) view.findViewById(R.id.switch1);
        final RelativeLayout frag_container = (RelativeLayout) view.findViewById(R.id.fragment_container);
        ImageView backarrow = (ImageView) view.findViewById(R.id.backarrow);

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    txtMale.setTextColor(Color.parseColor("#808080"));
                    txtFemale.setTextColor(Color.parseColor("#000000"));

                } else {
                    txtFemale.setTextColor(Color.parseColor("#808080"));
                    txtMale.setTextColor(Color.parseColor("#000000"));

                }
            }
        });
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        imageView_UploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);



            }
        });
        imageView_ClickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    requestPermissions(new String[]{Manifest.permission.CAMERA},1);

                }else
                {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, 1);
                    }
                }
            }
        });

        txtMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch1.setChecked(false);
            }
        });

        txtFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch1.setChecked(true);
            }
        });
        birthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility utility = new Utility();
                utility.InputMethodManager(getActivity());
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sFirstName = fName.getText().toString();
                sLastName = lName.getText().toString();
                sEmail = email.getText().toString().trim();
                sMobileNumber = mobileNumber.getText().toString();
                sBirthDate = birthdate.getText().toString();
                if (switch1.isChecked()) {
                    sGender = "F";
                } else {
                    sGender = "M";
                }
                if (sFirstName.length() <= 1) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter first name!!!")
                            .setTitle("Alert");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (sEmail.length() <= 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter email address!!!")
                            .setTitle("Alert");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } else if (!ValidationUtils.validateEmailAddress(sEmail)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter proper email address!!!")
                            .setTitle("Alert");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } else if (sMobileNumber.length() <= 1) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.mobile_number_field_empty))
                            .setTitle("Alert");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (sMobileNumber.length() <= 5) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.invalid_mobile_number))
                            .setTitle("Alert");

                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (sBirthDate.length() <= 4) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter BirthDate!!!").setTitle("Alert");

                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else {
                    Profile profile = new Profile();
                    profile.setFirstName(sFirstName);
                    profile.setLastName(sLastName);
                    profile.setGender(sGender);
                    profile.setEmailId(sEmail);
                    profile.setMobileNumber(sMobileNumber);
                    profile.setBirthDate(sBirthDate);
                    Bundle args = new Bundle();
                    if (profile_bitmap != null) {

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        profile_bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        profile.setProfileImage(byteArray);
                    }
                    args.putSerializable("Profile", profile);
                    location = new BuildProfileLocation();
                    location.setArguments(args);


                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(location.getClass().getName());
                    fragmentTransaction.add(R.id.login_root_layout, location);
                    fragmentTransaction.commit();
                    //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, location).commit();
                }
            }
        });


    }

    private static void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, DOCUMENT_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }
        String targetFile = file.getAbsolutePath() + "/" + "User" + ".png";
        profilePath = targetFile;
        File destination = new File(targetFile);
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(targetFile, options);
        final int REQUIRED_SIZE = 100;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(targetFile, options);
        imageView_ProfilePic.setImageBitmap(bm);
    }

    private static void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = activity.getContentResolver().query(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        String selectedImagePath = cursor.getString(column_index);
        profilePath = selectedImagePath;
        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);
        imageView_ProfilePic.setImageBitmap(bm);
    }

    public static void activityResult(int resultCode, Intent data) {
        Log.d("TAG1", "requestCode 2=" + resultCode);
        if (resultCode == 0) {
            onCaptureImageResult(data);
        } else if (resultCode == 1) {
            onSelectFromGalleryResult(data);
        }
    }


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Log.d("TAG67", "myCalendar.getTime()=" + myCalendar.getTime());
        Log.d("TAG67", "myCalendar.getTime88()=" + sdf.format(myCalendar.getTime()));
        birthdate.setText(sdf.format(myCalendar.getTime()));
    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((BuildProfile) getActivity()).getSupportActionBar().hide();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.buildprofile_main, container, false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {


            if (requestCode == 1) {
                Bundle extras = data.getExtras();
                Log.v("TAG", " I am at onResult");
                profile_bitmap = (Bitmap) extras.get("data");
            } else {
                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                    profile_bitmap = BitmapFactory.decodeStream(inputStream);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

            profile_bitmap = Bitmap.createScaledBitmap(profile_bitmap, 200, 200, false);
            imageView_ProfilePic.setImageBitmap(profile_bitmap);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, 1);
                    }

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
