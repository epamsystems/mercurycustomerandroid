package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;

/**
 * Created by Raju on 16/09/2015.
 */
public class AirTimeTopupWallet extends Fragment {

    private AirTimeTopupMain airTimeTopupMain;
    private AirTimeTopupSuccess airTimeTopupSuccess;
    Button paybywallet;
    ImageView  paybywalletedit;
    LinearLayout rootlay;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        paybywalletedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyApplication.resetTimer(getActivity());
                airTimeTopupMain = new AirTimeTopupMain();
                Bundle bundle = new Bundle();
                airTimeTopupMain.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupMain).commit();
            }
        });

        paybywallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airTimeTopupSuccess = new AirTimeTopupSuccess();
                Bundle bundle = new Bundle();
                airTimeTopupSuccess.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupSuccess).commit();
            }
        });
    }

    private void LayoutInitialization(View view) {
        paybywalletedit = (ImageView)view.findViewById(R.id.airtimetopupmwalletbalance_editimages);
        paybywallet=(Button)view.findViewById(R.id.button_airtime_paybywallet);
        paybywallet.setText("Pay Now");
        rootlay=(LinearLayout)view.findViewById(R.id.root_linear_lay_for_airtime_wallet);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.airtimemwalletbalance, container, false);
    }
}


