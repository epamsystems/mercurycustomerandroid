package in.intellifox.mwallet.views.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/14/2015.
 */
public class BankAccountFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    TextView Bankname,AccountNo,BankAddress,Codeno;
    String bank_name="",acc_no="",bankadd="",codeno="";

    String timeStamp="", xmlforBankDetails="";
    String merchantId="",MobileNo="";
    RelativeLayout rootLay;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BankAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BankAccountFragment newInstance(String param1, String param2) {
        BankAccountFragment fragment = new BankAccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);

        fragment.setArguments(args);
        return fragment;
    }

    public BankAccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    Button addMoney,transferMoney;
    LinearLayout rootlay;
    BankAccountTransferMoneyFragment bankAccountTransferMoneyFragment;
    BankAccountAddMoneyFragment bankAccountAddMoneyFragment;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addMoney=(Button)view.findViewById(R.id.bankaccount_addmoney);
        transferMoney=(Button)view.findViewById(R.id.bankaccount_tarnsfermoney);
        rootlay=(LinearLayout)view.findViewById(R.id.bankaccount_main);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });


        Bankname = (TextView)view.findViewById(R.id.bankname_bankaccount);
        BankAddress = (TextView)view.findViewById(R.id.bankaccount_address);
        AccountNo = (TextView)view.findViewById(R.id.banknumber_bankaccount);
        Codeno = (TextView)view.findViewById(R.id.Codeno);
//        Bankname.setText(MyApplication.getInstance().getmBankDetails_Bank_Name());
//        AccountNo.setText(MyApplication.getInstance().getmBankDetails_Account_Number());
//        BankAddress.setText(MyApplication.getInstance().getmBankDetails_Branch_Address());
//        Codeno.setText(MyApplication.getInstance().getmBankDetails_Identification_Code());

        addMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bankAccountAddMoneyFragment=new BankAccountAddMoneyFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment,bankAccountAddMoneyFragment).commit();
                FragmentManager fragmentManager =getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.addToBackStack(bankAccountAddMoneyFragment.getClass()
                        .getName());
                fragmentTransaction.replace(R.id.main_fragment, bankAccountAddMoneyFragment);
                fragmentTransaction.commit();
            }
        });

        transferMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bankAccountTransferMoneyFragment=new BankAccountTransferMoneyFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment,bankAccountTransferMoneyFragment).commit();
                FragmentManager fragmentManager =getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.addToBackStack(bankAccountTransferMoneyFragment.getClass()
                        .getName());
                fragmentTransaction.replace(R.id.main_fragment, bankAccountTransferMoneyFragment);
                fragmentTransaction.commit();
            }
        });
    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Bank Account",false,"",false,"");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bankaccount, container, false);
    }
}

