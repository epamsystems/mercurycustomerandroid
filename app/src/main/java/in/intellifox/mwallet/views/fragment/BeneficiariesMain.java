package in.intellifox.mwallet.views.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Beneficiarys;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.activities.DrawerMain;
import in.intellifox.mwallet.views.adapter.CustomListFavourite;

/**
 * Created by Owner on 9/19/2015.
 */
public class BeneficiariesMain extends Fragment {

    LinearLayout rootlay;
    ListView beneficiarieList;
    private String timeStamp= "";
    private String xmlforBeneficiary= "";
    String bmobile="9200000011";
    String btpin="1111";
    String iv = "288dca8258b1dd7c";
    private ProgressDialog progressDialog;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        beneficiarieList=(ListView)view.findViewById(R.id.beneficiaries_listview);
        rootlay=(LinearLayout)view.findViewById(R.id.root_linear_lay_for_beneficiaries);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });
        bmobile= MyApplication.getInstance().getMerchantMobieNo();
        CallBeneficiaryDetails(bmobile, btpin);
        GlobalVariables.mActivity=getActivity();
    }
    public void CallBeneficiaryDetails( final String Nomobile,String tpin) {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
        xmlforBeneficiary = "<MpsXml>" +
                "<Header>" +
                "<ChannelId>APP</ChannelId>" +
                "<Timestamp>" + timeStamp + "</Timestamp>" +
                "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                "<ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                "</Header>" +
                "<Request>" +
                "<RequestType>ListBene</RequestType>" +
                "<UserType>CU</UserType>" +
                "</Request>" +
                "<RequestDetails>" +
                "<MobileNumber>" + Nomobile + "</MobileNumber>" +
                "<ResponseURL>" + GlobalVariables.url + "</ResponseURL>" +
                "<ResponseVar>mobileResponseXML</ResponseVar>" +
                "</RequestDetails>" +
                "</MpsXml>";
        Log.v("xml", xmlforBeneficiary);
        Log.d("TAG", "xmlforBeneficiary 100=" + xmlforBeneficiary.toString());
        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforBeneficiary, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption()
            {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXML)
                {
                    String final_String_for_beneficiary = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + Nomobile + "|" + encryptedXML;
                    Log.v("Full URL ", "Final URL For bank details : " + final_String_for_beneficiary);
                    Log.d("TAG", "final_String_for_beneficiary 101=" + final_String_for_beneficiary.toString());
                    new MyAsyncTaskforBeneficiary().execute(final_String_for_beneficiary);
                }
            } );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public class MyAsyncTaskforBeneficiary extends AsyncTask<String,Integer,String>
    {
        String result = "";
//         private ProgressDialog progressDialog = null;

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException | ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT Bank: " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);
        }

        @Override
        protected void onPostExecute(String s1) {
            super.onPostExecute(s1);
            String decryptedString = "";

            System.out.println("Response Beneficiary: " + s1);
            final MpsXml[] mpsxml = new MpsXml[1];
            final ParseResponse[] parse = new ParseResponse[1];
            try {
                AESecurity aess = new AESecurity();
                try {
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption()
                    {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted)
                        {
                            if (decrypted.equals(""))
                            {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplicationContext());
                                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                //dialog.setCanceledOnTouchOutside(true);
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                            }
                            else {
                                Log.d("TAG", "decrypted 12=" + decrypted);
                                parse[0] = new ParseResponse();
                                InputSource is = new InputSource(new StringReader(decrypted));
                                mpsxml[0] = parse[0].parseXML(is);
                                if (mpsxml[0].getResponse().getResponseType().equalsIgnoreCase("Success"))
                                {
                                    String reason=mpsxml[0].getResponsedetails().getReason();
                                    Log.d("TAG", "reason 19=" + reason);
                                    MyApplication.getInstance().setGeneralBalance(mpsxml[0].getResponsedetails().getBalance());
                                    Beneficiarys beneficiarys = new Beneficiarys();
                                    beneficiarys.setBeneficiary(mpsxml[0].getResponsedetails().getBeneficiarys().getBeneficiary());
                                    GlobalVariables.beneficiaryArrayList=beneficiarys.getBeneficiary();
                                    GlobalVariables.fragmentManager = getFragmentManager();
                                    CustomListFavourite navListAdaptercenter=new CustomListFavourite(getActivity().getApplicationContext(),GlobalVariables.beneficiaryArrayList);
                                    beneficiarieList.setAdapter(navListAdaptercenter);
                                    progressDialog.dismiss();
                                }
                                else {
                                    String reason=mpsxml[0].getResponsedetails().getReason();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage(mpsxml[0].getResponsedetails().getReason())
                                            .setTitle("mWallet");
                                    AlertDialog dialog = builder.create();
                                    dialog.setCanceledOnTouchOutside(true);
                                    dialog.show();
                                }
                                progressDialog.dismiss();
                            }
                        }
                    });
                } catch (InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException |
                        BadPaddingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                e.printStackTrace();
            }
        }
    }
    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "My Beneficiaries",false,"",false,"");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.beneficiaries_main, container, false);
    }
}
