package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/5/2015.
 */
public class DashBoardBottom extends Fragment {
    View v;
    RelativeLayout beneficiariesLayout,bankAccountLayout,manageCardLayout,payBillLayout,airtimeLayout,inviteFriendLayout,transactionHistory;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        beneficiariesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerMain)getActivity()).OpenBeneficiaries();
            }
        });
        bankAccountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerMain)getActivity()).OpenBankAccount();
            }
        });
        manageCardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerMain)getActivity()).OpenCard();
            }
        });
        airtimeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerMain)getActivity()).OpenAirtimeTopup();
            }
        });
        inviteFriendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerMain)getActivity()).OpenInviteFriends();
            }
        });
        transactionHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerMain)getActivity()).OpenTransactionHistory();
            }
        });
    }

    private void LayoutInitialization(View view) {
        beneficiariesLayout=(RelativeLayout)view.findViewById(R.id.relative_lay_managebeneficiaries);
        bankAccountLayout=(RelativeLayout)view.findViewById(R.id.relative_lay_managebankaccount);
        manageCardLayout=(RelativeLayout)view.findViewById(R.id.relative_lay_managecards);
        payBillLayout=(RelativeLayout)view.findViewById(R.id.relative_lay_paybills);
        airtimeLayout=(RelativeLayout)view.findViewById(R.id.relative_lay_airtimetopup);
        inviteFriendLayout=(RelativeLayout)view.findViewById(R.id.relative_lay_invitefriends);
        transactionHistory=(RelativeLayout)view.findViewById(R.id.relative_lay_transaction_summary);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dashboard_bottom, container, false);
    }

}
