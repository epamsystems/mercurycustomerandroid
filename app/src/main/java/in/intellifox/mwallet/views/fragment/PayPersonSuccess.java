package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.ResponseDetails;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/28/2015.
 */
public class PayPersonSuccess extends Fragment {
    private DashBoard dashBoard;
    ImageView dashboard;
    ResponseDetails responseDetails=new ResponseDetails();
    TextView txtmerchantId,txtamount,txtTransactionId,txtTransactionDate, txtSuccessMessage, txtCashbackMsg, txtCashbacktitle;
    String merchantId,time;
    ImageView imageView_transaction_history;
    private TransactionSummaryFragment transactionSummaryFragment;

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtmerchantId= (TextView) view.findViewById(R.id.txtmerchantId);
        txtamount= (TextView) view.findViewById(R.id.txtamount);
        txtTransactionId= (TextView) view.findViewById(R.id.txtTransactionId);
        txtTransactionDate= (TextView) view.findViewById(R.id.txtTransactionDate);
        txtSuccessMessage  = (TextView) view.findViewById(R.id.txtSuccessMessage);
        imageView_transaction_history= (ImageView) view.findViewById(R.id.imageView_transaction_history);
        txtCashbackMsg  = (TextView) view.findViewById(R.id.cashback_mesg);
        txtCashbacktitle  = (TextView) view.findViewById(R.id.cashback_title);

        try{
            Bundle args = getArguments();
            responseDetails=(ResponseDetails) args.getSerializable("responseDetails");
            merchantId=getArguments().getString("id");
            time=getArguments().getString("time");
            txtTransactionDate.setText(time);
            txtmerchantId.setText(merchantId);
            txtamount.setText(responseDetails.getBalance());
            //txtTransactionId.setText(responseDetails.getTxnId()+"\n"+responseDetails.getMessage());
            txtTransactionId.setText(responseDetails.getTxnId());
            txtSuccessMessage.setText(responseDetails.getMessage());
            if(responseDetails.getCashbackAmount().equalsIgnoreCase("") ||responseDetails.getCashbackAmount() !=null){
                txtCashbackMsg.setVisibility(View.VISIBLE);
                txtCashbacktitle.setVisibility(View.VISIBLE);
                txtCashbackMsg.setText("AED "+responseDetails.getCashbackAmount());
            }else {
                txtCashbackMsg.setVisibility(View.INVISIBLE);
                txtCashbacktitle.setVisibility(View.INVISIBLE);
            }

        }catch (Exception e)
        {

        }
        dashboard=(ImageView)view.findViewById(R.id.airtime_topup_succes_dashboard);
        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyApplication.resetTimer(getActivity());
                dashBoard = new DashBoard();
                Bundle bundle = new Bundle();
                dashBoard.setArguments(bundle);

                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                ((DrawerMain)getActivity()).setUpActionBar();


                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, dashBoard).commit();
            }
        });
        imageView_transaction_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                transactionSummaryFragment = new TransactionSummaryFragment();
                Bundle bundle = new Bundle();
                transactionSummaryFragment.setArguments(bundle);
                ((DrawerMain)getActivity()).setUpActionBar();
                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                dashBoard = new DashBoard();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, dashBoard).commit();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(transactionSummaryFragment.getClass().getName());
                fragmentTransaction.add(R.id.dashboard_container, transactionSummaryFragment);
                fragmentTransaction.commit();

                //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, transactionSummaryFragment).commit();
            }
        });
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String walletBalance = MyApplication.getInstance().getTotalBalance();

        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Payment Success", true,"mWallet Balance", true,"AED"+responseDetails.getBalance());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pay_person_success, container, false);
    }
}
