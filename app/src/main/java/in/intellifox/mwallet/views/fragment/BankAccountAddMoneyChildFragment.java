package in.intellifox.mwallet.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
/**
 * Created by Owner on 9/10/2015.
 */
public class BankAccountAddMoneyChildFragment extends Fragment {

    ImageView savecards,arrow,addcards;
    LinearLayout rootlay;
    private BankAccountAddMoneySaveCardChildFragment m_savecards_tab;
    private BankAccountAddMoneyAddCardChildFragment m_addcards_tab;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        savecards = (ImageView) view.findViewById(R.id.fragment_main_save_savecard);
        addcards= (ImageView) view.findViewById(R.id.fragment_main_save_addcard);
        arrow = (ImageView) view.findViewById(R.id.leftarrow);
        final RelativeLayout frag_container = (RelativeLayout) view.findViewById(R.id.fragment_main_savecards_container);
        rootlay=(LinearLayout)view.findViewById(R.id.root_relative_lay_for_changepins);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        BankAccountAddMoneyAddCardChildFragment mp = new BankAccountAddMoneyAddCardChildFragment();
        Bundle b = new Bundle();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_savecards_container, mp).commit();

        savecards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                m_savecards_tab = new BankAccountAddMoneySaveCardChildFragment();
                Bundle bundle = new Bundle();
                arrow.setImageResource(R.drawable.pay_merchant_left_arrow);
                addcards.setImageResource(R.drawable.pay_merchant_addcard_deselected);
                savecards.setImageResource(R.drawable.pay_merchant_saved_selected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_savecards_container, m_savecards_tab).commit();

                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });

        addcards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addcards.setImageResource(R.drawable.pay_merchant_addcard_selected);
                savecards.setImageResource(R.drawable.pay_merchant_saved_deselected);
                arrow.setImageResource(R.drawable.pay_merchant_right_arrow);
                m_addcards_tab = new BankAccountAddMoneyAddCardChildFragment();
                Bundle bundle1 = new Bundle();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_savecards_container,
                        m_addcards_tab).commit();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pay_merchant_child_fragment_main, container, false);
    }

}
