package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/23/2015.
 */
public class RequestMoneySuccess extends Fragment {

    private DashBoard dashBoard;
    ImageView dashboard;
    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dashboard=(ImageView)view.findViewById(R.id.airtime_topup_succes_dashboard);
        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyApplication.resetTimer(getActivity());
                dashBoard = new DashBoard();
                Bundle bundle = new Bundle();
                dashBoard.setArguments(bundle);
                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                ((DrawerMain)getActivity()).setUpActionBar();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, dashBoard).commit();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.request_money_success, container, false);
    }
}
