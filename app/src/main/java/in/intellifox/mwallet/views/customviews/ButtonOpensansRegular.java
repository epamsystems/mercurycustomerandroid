package in.intellifox.mwallet.views.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by SADIQ on 7/21/2015.
 */
public class ButtonOpensansRegular extends Button {
    public ButtonOpensansRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ButtonOpensansRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonOpensansRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/opensansregular.ttf");
        setTypeface(tf ,1);

    }

}
