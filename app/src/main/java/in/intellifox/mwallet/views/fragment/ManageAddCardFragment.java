package in.intellifox.mwallet.views.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.GlobalVariables;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

/**
 * Created by Owner on 9/9/2015.
 */
public class ManageAddCardFragment extends Fragment {
    Button addCard;
    EditText edtCardNumber, edtExpMonth,edtExpYear, edtBankName,edtTpin;
    LinearLayout main_layout;
    String timeStamp = "", expDate = "", bankName = "";
    String cardNumber = "", xmlforSendMoney = "", nickName = "";
    String mWalletId = "", tpinhash = "", session = "";
    String bmobile = "9200000010";
    String btpin = "";
    private ProgressDialog progressDialog;
    String iv = "288dca8258b1dd7c";
    ImageView managecameraimageView;
    private int MY_SCAN_REQUEST_CODE = 100; // arbitrary int
    final Pattern CODE_PATTERN = Pattern.compile("([0-9]{0,4})|([0-9]{4}-)+|([0-9]{4}-[0-9]{0,4})+");
    final Pattern DATE_PATTERN = Pattern.compile("([0-9]{0,2})|([0-9]{2}-)+|([0-9]{2}-[0-9]{0,2})+");


    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        edtCardNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                Log.w("", "input" + s.toString());

                if (s.length() > 0 && !CODE_PATTERN.matcher(s).matches()) {
                    String input = s.toString();
                    String numbersOnly = keepNumbersOnly(input);
                    String code = formatNumbersAsCode(numbersOnly);

                    Log.w("", "numbersOnly" + numbersOnly);
                    Log.w("", "code" + code);

                    edtCardNumber.removeTextChangedListener(this);
                    edtCardNumber.setText(code);
                    // You could also remember the previous position of the cursor
                    edtCardNumber.setSelection(code.length());
                    edtCardNumber.addTextChangedListener(this);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            private String keepNumbersOnly(CharSequence s) {
                return s.toString().replaceAll("[^0-9]", ""); // Should of course be more robust
            }

            private String formatNumbersAsCode(CharSequence s) {
                int groupDigits = 0;
                String tmp = "";
                for (int i = 0; i < s.length(); ++i) {
                    tmp += s.charAt(i);
                    ++groupDigits;
                    if (groupDigits == 4) {
                        tmp += "-";
                        groupDigits = 0;
                    }
                }
                return tmp;
            }
        });


        edtExpMonth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.toString().length()==2)
                    edtExpYear.requestFocus();
            }
        });


        edtExpYear.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.toString().length()==0)
                {
                    edtExpMonth.requestFocus();
                    edtExpMonth.setSelection(2);
                }
            }
        });

        managecameraimageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scanIntent = new Intent(getActivity(), CardIOActivity.class);
                // customize these values to suit your needs.
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

                // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
                startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
            }
        });
        addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expDate = edtExpMonth.getText().toString().trim()+"/"+edtExpYear.getText().toString().trim();
                bankName = edtBankName.getText().toString().trim();
                cardNumber = edtCardNumber.getText().toString().trim();
                cardNumber = cardNumber.replaceAll("\\D", "");
                btpin = edtTpin.getText().toString().trim();;

                Log.d("TAG", "cardNumber=" + cardNumber);


                if (cardNumber.length() <= 1) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter card number")
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (expDate.length() <= 1) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please enter expiry date.")
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (bankName.length() <= 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Bank Name field empty")
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                }
                else if (bankName.length() <= 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Bank Name field empty")
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                }
                else {

                    MessageDigest digest = null;
                    try {
                        digest = MessageDigest.getInstance("SHA-256");
                        digest.update(btpin.getBytes());
                        tpinhash = bytesToHexString(digest.digest());
                        Log.i("Eamorr", "result is " + tpinhash);
                    } catch (NoSuchAlgorithmException e1) {

                        e1.printStackTrace();
                    }
                    bmobile = MyApplication.getInstance().getMerchantMobieNo();
                    mWalletId = MyApplication.getInstance().getmProfile_WalletID_General();
                    callAddCard(bmobile, cardNumber, expDate, bankName, nickName, tpinhash, mWalletId);

//                            PayPersonSuccess rms = new PayPersonSuccess();
//                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, rms).commit();

                }
            }
        });
    }

    public class MyAsyncTaskForAddCard extends AsyncTask<String, Integer, String> {
        String result = "";

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("Response : " + s);

            progressDialog.dismiss();

            //Parse your response here
            if (result.equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                progressDialog.dismiss();
            } else {

                try {
                    AESecurity aess = new AESecurity();
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted) {
                            Log.d("TAG", "decrypted Pay200=" + decrypted);
                            Log.d("TAG", "mobileNumber Pay200=" + mobileNumber);
                            ParseResponse parse = new ParseResponse();
                            InputSource is = new InputSource(new StringReader(decrypted));
                            MpsXml mpsxml = parse.parseXML(is);

                            if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success")) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getMessage())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                progressDialog.dismiss();

                                /*Intent toLogin = new Intent(ChangeTPin.this,MainActivity.class);
                                startActivity(toLogin);
*/
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getReason())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                //progressDialog.dismiss();
                            }
                        }
                    });
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException |
                        InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // utility function for encryption of MPIN
    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public void callAddCard(final String bmobile, String cardNumber, String expDate, String cardName, String nickName, String tpinhash, String mWalletId) {

        progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
        xmlforSendMoney =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>" + timeStamp + "</Timestamp>" +
                        "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                        "<ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>AddCard</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<MobileNumber>" + bmobile + "</MobileNumber>" +
                        "<CardNumber>" + cardNumber + "</CardNumber>" +
                        "<ExpiryDate>" + expDate + "</ExpiryDate>" +
                        "<NameOnCard>" + cardName + "</NameOnCard>" +
                        "<NickName>" + nickName + "</NickName>" +
                        "<Tpin>" + tpinhash + "</Tpin>" +
                        "+<WalletId>" + mWalletId + "</WalletId>" +
                        "<ResponseURL>" + GlobalVariables.url + "</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        " </RequestDetails>" +
                        "</MpsXml>";

        Log.v("TAG", " Request " + xmlforSendMoney);

        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforSendMoney, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXMLforSendMoney) {
                    String final_String_for_SendMoney = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + bmobile + "|" + encryptedXMLforSendMoney;
                    new MyAsyncTaskForAddCard().execute(final_String_for_SendMoney);
                }
            });
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG", "requestCode=" + requestCode);
        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );

                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }
            } else {
                resultDisplayStr = "Scan was canceled.";
            }
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
        }
        // else handle other activity results
    }

    private void LayoutInitialization(View view) {
        main_layout = (LinearLayout) view.findViewById(R.id.addcard_second_part);
        addCard = (Button) view.findViewById(R.id.managecard_button_addCard);
        edtBankName = (EditText) view.findViewById(R.id.edit_text_bank_name);
        edtExpMonth = (EditText) view.findViewById(R.id.edit_text_expMonth);
        edtExpYear = (EditText) view.findViewById(R.id.edit_text_expyear);
        edtCardNumber = (EditText) view.findViewById(R.id.edit_text_card_number);
        edtTpin = (EditText) view.findViewById(R.id.edtTpin);
        managecameraimageView = (ImageView) view.findViewById(R.id.manage_camera_imageView);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.manage_card_addcard_fragment, container, false);
    }
}
