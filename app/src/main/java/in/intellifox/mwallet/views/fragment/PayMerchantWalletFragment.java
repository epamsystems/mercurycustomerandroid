package in.intellifox.mwallet.views.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import dialogs.OkDialog;
import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.apicalls.RetrofitTask;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.parser.ResponseDetails;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.security.AESecurityNew;
import in.intellifox.mwallet.utilities.DecimalDigitsInputFilter;
import in.intellifox.mwallet.utilities.GenericResponseHandler;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.utilities.LogUtils;
import in.intellifox.mwallet.views.activities.ForgotTPin;

/**
 * Created by Owner on 9/10/2015.
 */
public class PayMerchantWalletFragment extends Fragment {
    Button sendMoney;
    LinearLayout mainLayout;
    RelativeLayout relativeLayoutmain, relativelayforenteringcvv;
    private EditText editTextMerchandId, editTextAmount, editTextTpin;
    String iv = "288dca8258b1dd7c";
    String timeStamp = "", mobileno = "", amount = "";
    String merchantId = "", xmlforSendMoney = "";
    String mWalletId = "", tpinhash = "", session = "";
    String bmobile = "9200000010";
    String btpin = "2211";
    static String wAmount = "";
    private ProgressDialog progressDialog;
    private TextView mWalletAmount, txtvwApplyPromocode, txtvwPromocode, txtvwPromocodeAppliedMsg;
    private ImageView imgvwRemovePromocode;
    private TextView textViewforgottpin;

    private LinearLayout linlayAppliedPromocodeContainer;
    private String strPromoCode = "";

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        String walletBalance = MyApplication.getInstance().getTotalBalance();
        mWalletAmount.setText("AED " + walletBalance);
        relativeLayoutmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        textViewforgottpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ForgotTPin.class);
                startActivity(intent);
            }
        });

        if (getArguments() != null && !TextUtils.isEmpty(getArguments().getString("merchantID"))) {
            TextView txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            EditText edtTtile = (EditText) view.findViewById(R.id.edit_text_merchandid_title);
            edtTtile.setVisibility(View.VISIBLE);
            txtTitle.setText("Merchant Name: " + getArguments().getString("merchantName"));
            editTextMerchandId.setText(getArguments().getString("merchantID"));
            editTextMerchandId.setTextColor(Color.parseColor("#000000"));
            editTextMerchandId.setEnabled(false);
            editTextAmount.requestFocus();
        }
        sendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                merchantId = editTextMerchandId.getText().toString().trim();
                amount = editTextAmount.getText().toString().trim();
                btpin = editTextTpin.getText().toString().trim();
                if (merchantId.length() < 1) {
                    editTextMerchandId.requestFocus();
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.merchantid_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (amount.length() < 1) {
                    editTextAmount.requestFocus();
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.amount_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (btpin.length() < 6) {
                    if (MyApplication.getInstance().getOneClilckFlag().equalsIgnoreCase("Y")
                            && Float.parseFloat(amount) <= Float.parseFloat(MyApplication.getInstance().getOneClilckAmount().toString())) {
                        btpin = "";
                        bmobile = MyApplication.getInstance().getMerchantMobieNo();
                        mWalletId = MyApplication.getInstance().getmProfile_WalletID_General();
                        callPayMerchant(bmobile, merchantId, amount, tpinhash, mWalletId);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage(getResources().getString(R.string.invalid_tpin))
                                .setTitle("mWallet");
                        AlertDialog dialog = builder.create();
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();
                    }

                } else {
                    MessageDigest digest = null;
                    try {
                        digest = MessageDigest.getInstance("SHA-256");
                        digest.update(btpin.getBytes());
                        tpinhash = bytesToHexString(digest.digest());
                        Log.i("Eamorr", "result is " + tpinhash);
                    } catch (NoSuchAlgorithmException e1) {

                        e1.printStackTrace();
                    }
                    bmobile = MyApplication.getInstance().getMerchantMobieNo();
                    mWalletId = MyApplication.getInstance().getmProfile_WalletID_General();
                    callPayMerchant(bmobile, merchantId, amount, tpinhash, mWalletId);

                }
            }
        });
    }


    public void callPayMerchant(final String bmobile, String merchantId, String amount, String tpinhash, String mWalletId) {

        progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
       /* xmlforSendMoney =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>" + timeStamp + "</Timestamp>" +
                        "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                        "<ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>P2M</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<MobileNumber>" + bmobile + "</MobileNumber>" +
                        "<MerchantId>" + merchantId + "</MerchantId>" +
                        "<Amount>" + amount + "</Amount>" +
                        "<Tpin>" + tpinhash + "</Tpin>" +
                        "<WalletId>" + mWalletId + "</WalletId>" +
                        "<ResponseURL>" + GlobalVariables.url + "</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        " </RequestDetails>" +
                        "</MpsXml>";*/


        xmlforSendMoney =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>" + timeStamp + "</Timestamp>" +
                        "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                        "<ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>P2M</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "<Paymode>W2W</Paymode>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<MobileNumber>" + bmobile + "</MobileNumber>" +
                        "<MerchantId>" + merchantId + "</MerchantId>" +
                        "<Amount>" + amount + "</Amount>" +
                        "<Tpin>" + tpinhash + "</Tpin>" +
                        "<WalletId>" + mWalletId + "</WalletId>" +
                        "<PromoCode>" + strPromoCode + "</PromoCode>" +
                        "<ResponseURL>" + GlobalVariables.url + "</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        " </RequestDetails>" +
                        "</MpsXml>";

        try {
            Log.d("TAG", "xmlforSendMoney=" + xmlforSendMoney.toString());
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforSendMoney, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXMLforSendMoney) {
                    Log.v("TAG", "bmobile=" + bmobile);
                    String final_String_for_SendMoney = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + bmobile + "|" + encryptedXMLforSendMoney;
                    new MyAsyncTaskForPayMerchant().execute(final_String_for_SendMoney);
                }
            });
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public class MyAsyncTaskForPayMerchant extends AsyncTask<String, Integer, String> {
        String result = "";

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("Response : " + s);

            progressDialog.dismiss();

            //Parse your response here
            if (result.equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                progressDialog.dismiss();
            } else {

                try {
                    AESecurity aess = new AESecurity();
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted) {
                            Log.d("TAG", "Pay a merchant Pay200=" + decrypted);
                            Log.d("TAG", "mobileNumber Pay200=" + mobileNumber);
                            ParseResponse parse = new ParseResponse();
                            InputSource is = new InputSource(new StringReader(decrypted));
                            MpsXml mpsxml = parse.parseXML(is);

                            if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success")) {


                                progressDialog.dismiss();
                                ResponseDetails responseDetails = new ResponseDetails();
                                responseDetails = mpsxml.getResponsedetails();
                                MyApplication.getInstance().setGeneralBalance("" + responseDetails.getBalance());

                                PayPersonSuccess rms = new PayPersonSuccess();
                                Bundle bundle = new Bundle();
                                bundle.putString("id", merchantId);
                                bundle.putString("time", mpsxml.getHeader().getTimestamp());
                                bundle.putSerializable("responseDetails", responseDetails);
                                rms.setArguments(bundle);
                                FragmentTransaction fragmentTransaction = GlobalVariables.fragmentManager
                                        .beginTransaction();
                                fragmentTransaction.addToBackStack(rms.getClass()
                                        .getName());
                                fragmentTransaction.add(R.id.dashboard_container, rms);
                                fragmentTransaction.commit();
                                progressDialog.dismiss();
                                /*Intent toLogin = new Intent(ChangeTPin.this,MainActivity.class);
                                startActivity(toLogin);
*/
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getReason())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                //progressDialog.dismiss();
                            }
                        }
                    });
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException |
                        InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // utility function for encryption of MPIN
    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    private void LayoutInitialization(View view) {
        sendMoney = (Button) view.findViewById(R.id.button_childrow_sendmoney);
        mainLayout = (LinearLayout) view.findViewById(R.id.root_layout);
        mainLayout.setEnabled(false);
        editTextMerchandId = (EditText) view.findViewById(R.id.edit_text_merchandid);
        editTextAmount = (EditText) view.findViewById(R.id.edit_text_amount);
        editTextAmount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(8, 2)});
        mWalletAmount = (TextView) view.findViewById(R.id.mWalletAmount);
        textViewforgottpin = (TextView) view.findViewById(R.id.textViewforgottpin);
        editTextTpin = (EditText) view.findViewById(R.id.edit_text_childrow);
        relativeLayoutmain = (RelativeLayout) view.findViewById(R.id.relativeLayoutmain);

       // txtvwApplyPromocode = (TextView) view.findViewById(R.id.txtvwApplyPromoCode);
        txtvwPromocode = (TextView) view.findViewById(R.id.txtvwPromoCode);
        txtvwPromocodeAppliedMsg = (TextView) view.findViewById(R.id.txtvwPromoCodeAppliedMessage);
        linlayAppliedPromocodeContainer = (LinearLayout) view.findViewById(R.id.linlayAppliedPromocodeContainer);
        imgvwRemovePromocode = (ImageView) view.findViewById(R.id.imgvwRemovePromoCode);

        relativelayforenteringcvv = (RelativeLayout) view.findViewById(R.id.relative_lay_for_entering_cvv);

//        txtvwApplyPromocode.setPaintFlags(txtvwApplyPromocode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//
//        txtvwApplyPromocode.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                customDialog = new CustomDialog(getActivity());
//                customDialog.show();
//
///*
//                if (edtxtPromoCode.getVisibility() == View.GONE) {
//                    edtxtPromoCode.setVisibility(View.VISIBLE);
//                    txtvwApplyPromocode.setVisibility(View.GONE);
//                } else {
//                    edtxtPromoCode.setVisibility(View.GONE);
//                    txtvwApplyPromocode.setVisibility(View.VISIBLE);
//                }*/
//            }
//        });

        imgvwRemovePromocode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtvwPromocode.setText("");
                txtvwPromocodeAppliedMsg.setText("");
                strPromoCode = "";
                linlayAppliedPromocodeContainer.setVisibility(View.GONE);
                txtvwApplyPromocode.setVisibility(View.VISIBLE);
            }
        });

        if (MyApplication.getInstance().getOneClilckFlag() != null) {
            if (MyApplication.getInstance().getOneClilckFlag().equalsIgnoreCase("N")) {
                relativelayforenteringcvv.setVisibility(View.VISIBLE);

            } else {
                relativelayforenteringcvv.setVisibility(View.GONE);
                editTextAmount.addTextChangedListener(passwordWatcher);
            }
        }

    }

//    public class CustomDialog extends Dialog implements
//            android.view.View.OnClickListener {
//
//        public Activity c;
//        public Dialog d;
//        public Button btnApplyPromoCode;
//        public EditText edtxtPromoCode;
//
//        private ImageView imgvwClose;
//
//        public CustomDialog(Activity a) {
//            super(a);
//            // TODO Auto-generated constructor stub
//            this.c = a;
//        }
//
//        @Override
//        protected void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            requestWindowFeature(Window.FEATURE_NO_TITLE);
//            setContentView(R.layout.dialog_apply_promocode);
//
//            edtxtPromoCode = (EditText) findViewById(R.id.edtxtPromoCode);
//            btnApplyPromoCode = (Button) findViewById(R.id.btnAppyPromoCode);
//            imgvwClose = (ImageView) findViewById(R.id.imgvwClose);
//
//            edtxtPromoCode.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
//            btnApplyPromoCode.setOnClickListener(this);
//            imgvwClose.setOnClickListener(this);
//
//
//        }
//
//        @Override
//        public void onClick(View v) {
//            switch (v.getId()) {
//                case R.id.btnAppyPromoCode:
//
//                    checkPromoValidation(edtxtPromoCode.getText().toString());
//
//                    break;
//                case R.id.imgvwClose:
//                    dismiss();
//
//                    break;
//
//                default:
//                    break;
//            }
//
//        }
//    }
//
//    private void checkPromoValidation(final String promoCode) {
//        strPromoCode = promoCode;
//
//
//        amount = editTextAmount.getText().toString().trim();
//
//        if (TextUtils.isEmpty(amount)) {
//
//            customDialog.dismiss();
//            editTextAmount.requestFocus();
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//            builder.setMessage(getResources().getString(R.string.amount_field_empty))
//                    .setTitle("mWallet");
//            AlertDialog dialog = builder.create();
//            dialog.setCanceledOnTouchOutside(true);
//            dialog.show();
//
//            return;
//        }
//
//        String xmlParam = "\t\t<MobileNumber>" + MyApplication.getInstance().getMerchantMobieNo() + "</MobileNumber>\n" +
//                "\t\t<Amount>" + editTextAmount.getText().toString() + "</Amount>\n" +
//                "\t\t<PromoCode>" + promoCode + "</PromoCode>";
//
//
//        xmlParam = GlobalVariables.getReqXML("Offervalid", xmlParam);
//
//        try {
//            xmlParam = AESecurityNew.getInstance().encryptString(xmlParam);
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            // GlobalVariables.showToast(this, e.toString());
//            return;
//        }
//
//        GlobalVariables.showProgress(getActivity(), "Validating promocode...");
//
//
//        RetrofitTask retrofitTask = RetrofitTask.getInstance();
//        retrofitTask.executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
//
//            @Override
//            public void handleResponse(boolean isSuccess, String response) {
//
//                GlobalVariables.dismissProgress();
//
//                if (!isSuccess) {
//                    new OkDialog(getActivity(), response, null, null);
//                    return;
//                }
//
//                String decrypted = null;
//                try {
//                    decrypted = AESecurityNew.getInstance().decryptString(response);
//                    LogUtils.Verbose("TAG", " Decrypted " + decrypted);
//
//                    ArrayList<String> list = new ArrayList<>();
//                    list.add("ResponseType");
//                    list.add("Message");
//                    list.add("Reason");
//
//                    Map<String, String> responseMap = GenericResponseHandler.parseElements(decrypted, list);
//
//
//                    if (responseMap.get("ResponseType") != null && responseMap.get("ResponseType").equalsIgnoreCase("Success")) {
//
//                        customDialog.dismiss();
//
//                        txtvwApplyPromocode.setVisibility(View.GONE);
//
//                        linlayAppliedPromocodeContainer.setVisibility(View.VISIBLE);
//
//                        txtvwPromocode.setText(promoCode);
//
//                        txtvwPromocodeAppliedMsg.setText(responseMap.get("Message"));
//                    } else {
//                        //customDialog.edtxtPromoCode.setText("");
//                        customDialog.edtxtPromoCode.setError(responseMap.get("Reason"));
//                    }
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        });
//
//
//    }
//
    private final TextWatcher passwordWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (!s.toString().equalsIgnoreCase("")) {
                if (Float.parseFloat(s.toString()) <= Float.parseFloat(MyApplication.getInstance().getOneClilckAmount().toString())) {
                    relativelayforenteringcvv.setVisibility(View.GONE);
                } else {
                    relativelayforenteringcvv.setVisibility(View.VISIBLE);
                }
            } else {
                relativelayforenteringcvv.setVisibility(View.GONE);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pay_merchant_mywallet_fragment, container, false);
    }
}
