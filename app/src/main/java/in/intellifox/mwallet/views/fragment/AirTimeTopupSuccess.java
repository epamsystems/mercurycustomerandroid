package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Raju on 18/09/2015.
 */
public class AirTimeTopupSuccess extends Fragment{

    private DashBoard dashBoard;
    ImageView dashboard;
    LinearLayout rootlay;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dashboard=(ImageView)view.findViewById(R.id.airtime_topup_succes_dashboard);
        rootlay=(LinearLayout)view.findViewById(R.id.root_linear_lay_for_airtime_success);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyApplication.resetTimer(getActivity());
                dashBoard = new DashBoard();
                Bundle bundle = new Bundle();
                dashBoard.setArguments(bundle);
                ((DrawerMain)getActivity()).setUpActionBar();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, dashBoard).commit();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.airtime_topup_success, container, false);
    }
}
