package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.adapter.CustomListFacebookRequestMoney;

/**
 * Created by Owner on 9/15/2015.
 */
public class PayPersonFacebookListFragment extends Fragment {

    ListView facebookList;
    PayPersonFacebookListInfoFragment payPersonFacebookListInfoFragment;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        facebookList=(ListView)view.findViewById(R.id.request_money_facebook_listview);

        CustomListFacebookRequestMoney navListAdaptercenter=new CustomListFacebookRequestMoney(getActivity().getApplicationContext());
        facebookList.setAdapter(navListAdaptercenter);

        // Listview on item click listener
        facebookList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                payPersonFacebookListInfoFragment =new PayPersonFacebookListInfoFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.request_money_main_fragment_container, payPersonFacebookListInfoFragment).commit();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.request_money_facebook_list_fragment, container, false);
    }
}
