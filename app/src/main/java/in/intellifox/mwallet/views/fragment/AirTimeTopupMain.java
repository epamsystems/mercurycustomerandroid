package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;


/**
 * Created by Raju on 16/09/2015.
 */
public class AirTimeTopupMain extends Fragment {

    private AirTimeTopupOperatorListFragmentWallet airTimeTopupOperatorListFragmentWallet;
    private AirTimeTopupOperatorListFragmentMyCards airTimeTopupOperatorListFragmentMyCards;
    Button paybywallet,paybycard;
    LinearLayout rootlay;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        paybywallet=(Button)view.findViewById(R.id.button_airtime_paybywallet);
        paybycard=(Button)view.findViewById(R.id.button_airtime_paybycard);
        rootlay=(LinearLayout)view.findViewById(R.id.root_linear_lay_for_airtime_main);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        paybywallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airTimeTopupOperatorListFragmentWallet = new AirTimeTopupOperatorListFragmentWallet();
                Bundle bundle = new Bundle();
                airTimeTopupOperatorListFragmentWallet.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupOperatorListFragmentWallet).commit();
            }
        });

        paybycard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airTimeTopupOperatorListFragmentMyCards = new AirTimeTopupOperatorListFragmentMyCards();
                Bundle bundle = new Bundle();
                airTimeTopupOperatorListFragmentMyCards.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupOperatorListFragmentMyCards).commit();
            }
        });
    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Airtime Topup",false,"",false,"");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.airtime_topup_main, container, false);
    }
}
