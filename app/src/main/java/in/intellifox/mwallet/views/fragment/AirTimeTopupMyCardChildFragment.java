package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;

/**
 * Created by Raju on 18/09/2015.
 */
public class AirTimeTopupMyCardChildFragment extends Fragment {

    ImageView savecards,addcards,arrow;
    LinearLayout rootlay;
    private AirTimeTopupSaveCardChildFragment airTimeTopupSaveCardChildFragment;
    private AirTimeTopupAddCardChildFragment airTimeTopupAddCardChildFragment;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        AirTimeTopupSaveCardChildFragment mp = new AirTimeTopupSaveCardChildFragment();
        Bundle b = new Bundle();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_savecards_container, mp).commit();

        savecards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airTimeTopupSaveCardChildFragment = new AirTimeTopupSaveCardChildFragment();
                Bundle bundle = new Bundle();
                arrow.setImageResource(R.drawable.pay_merchant_left_arrow);
                addcards.setImageResource(R.drawable.pay_merchant_addcard_deselected);
                savecards.setImageResource(R.drawable.pay_merchant_saved_selected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_savecards_container, airTimeTopupSaveCardChildFragment).commit();

            }
        });

        addcards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addcards.setImageResource(R.drawable.pay_merchant_addcard_selected);
                savecards.setImageResource(R.drawable.pay_merchant_saved_deselected);
                arrow.setImageResource(R.drawable.pay_merchant_right_arrow);
                airTimeTopupAddCardChildFragment = new AirTimeTopupAddCardChildFragment();
                Bundle bundle1 = new Bundle();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_savecards_container,
                        airTimeTopupAddCardChildFragment).commit();
            }
        });
    }

    private void LayoutInitialization(View view) {
        savecards = (ImageView) view.findViewById(R.id.fragment_main_save_savecard);
        addcards= (ImageView) view.findViewById(R.id.fragment_main_save_addcard);
        arrow = (ImageView) view.findViewById(R.id.leftarrow);
        final RelativeLayout frag_container = (RelativeLayout) view.findViewById(R.id.fragment_main_savecards_container);
        rootlay=(LinearLayout)view.findViewById(R.id.root_relative_lay_for_changepins);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pay_merchant_child_fragment_main, container, false);
    }
}
