package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;

/**
 * Created by Raju on 18/09/2015.
 */
public class AirTimeTopupAddCardChildFragment extends Fragment {

    private AirTimeTopupSuccess airTimeTopupSuccess;
    Button paybyaddcard;
    EditText edtCardNumber,edtExpDate,edtCvvNumber;
    LinearLayout rootlay;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        paybyaddcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyApplication.resetTimer(getActivity());
                airTimeTopupSuccess = new AirTimeTopupSuccess();
                Bundle bundle = new Bundle();
                airTimeTopupSuccess.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupSuccess).commit();
            }
        });
    }

    private void LayoutInitialization(View view) {
        paybyaddcard=(Button)view.findViewById(R.id.button_childrow_sendmoney);
        paybyaddcard.setText("Pay Now");
        rootlay=(LinearLayout)view.findViewById(R.id.root_layout);
        edtCardNumber=(EditText)view.findViewById(R.id.edit_text_card_number);
        edtExpDate=(EditText)view.findViewById(R.id.edit_text_expiry);
        edtCvvNumber=(EditText)view.findViewById(R.id.edit_text_cvv_number);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pay_merchant_child_addcard, container, false);
    }
}
