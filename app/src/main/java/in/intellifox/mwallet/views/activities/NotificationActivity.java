package in.intellifox.mwallet.views.activities;


import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import dialogs.YesNoDialog;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.models.MercuryNotification;
import in.intellifox.mwallet.utilities.AppSettings;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.views.adapter.NotificationAdapter;

public class NotificationActivity extends AppCompatActivity {

    RelativeLayout rootlay;
    ListView notificationsList;
    TextView tvNoData;
    Button btnClear;
    NotificationAdapter notificationAdapter;

    TextView actionBarTitleText,actionBarPersonText,actionBarPersonTextValue;
    ImageView actionBarAppLogoImage,actionBarBackImage,actionBarNotificationImage,actionBarTagImage,actionBarQrcodeImage,
            actionBarSearchImage,actionBarNavagationImage;


    RelativeLayout llNotification;
    TextView txtNotificationCount;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        setUpActionBar();
        MyApplication.resetTimer(NotificationActivity.this);
        notificationsList=(ListView)findViewById(R.id.frag_notifications_lv_content);
        rootlay=(RelativeLayout)findViewById(R.id.frag_notifications_root_layout);
        tvNoData=(TextView)findViewById(R.id.frag_notifications_tv_no_data);
        btnClear=(Button)findViewById(R.id.frag_notifications_btn_clear);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        AppSettings.putData(NotificationActivity.this, AppSettings.NOTIFICATION_COUNT, "0");

        Gson gson = new Gson();
        String json = AppSettings.getData(NotificationActivity.this, AppSettings.NOTIFICATIONS);

        Type type = new TypeToken<ArrayList<MercuryNotification>>() {
        }.getType();
        final List<MercuryNotification> mercuryNotifications = gson.fromJson(json, type);

        if (mercuryNotifications != null) {
            notificationAdapter = new NotificationAdapter(NotificationActivity.this, mercuryNotifications);
            notificationsList.setAdapter(notificationAdapter);
            if (mercuryNotifications.size() > 0)
                tvNoData.setVisibility(View.GONE);
            else
                tvNoData.setVisibility(View.VISIBLE);
        } else {
            tvNoData.setVisibility(View.VISIBLE);
            btnClear.setVisibility(View.GONE);
        }


        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new YesNoDialog(NotificationActivity.this, getString(R.string.alertdeleteNotification), null, new YesNoDialog.IYesNoDialogCallback() {
                    @Override
                    public void handleResponse(int responsecode) {

                        if (responsecode == 1) {
                            AppSettings.putData(NotificationActivity.this, AppSettings.NOTIFICATIONS, "");
                            mercuryNotifications.clear();
                            notificationAdapter.notifyDataSetChanged();
                            tvNoData.setVisibility(View.VISIBLE);
                            btnClear.setVisibility(View.GONE);
                        }
                    }
                });

            }
        });



        GlobalVariables.mActivity=NotificationActivity.this;

    }

    //Set custom Action bar
    private void setUpActionBar() {
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_dashboard);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));

        //Initialization of Action bar components
        actionBarNavagationImage= (ImageView) findViewById(R.id.action_bar_navagation_image);
        actionBarNavagationImage.setVisibility(View.GONE);
        actionBarBackImage=(ImageView)findViewById(R.id.action_bar_back);
        actionBarTitleText = (TextView) findViewById(R.id.action_bar_title);
        actionBarTitleText.setText("Notifications");

       // actionBarNotificationImage = (ImageView) findViewById(R.id.actionbar_notification);
        //actionBarNotificationImage.setVisibility(View.GONE);
        llNotification = (RelativeLayout) findViewById(R.id.toolbar_ll_notification);
        txtNotificationCount = (TextView) findViewById(R.id.txtCount);
        llNotification.setVisibility(View.GONE);

        //actionBarTagImage = (ImageView) findViewById(R.id.actionbar_tag);
       // actionBarTagImage.setVisibility(View.GONE);
        actionBarQrcodeImage = (ImageView) findViewById(R.id.actionbar_qrcode);
        actionBarQrcodeImage.setVisibility(View.GONE);
        actionBarAppLogoImage = (ImageView) findViewById(R.id.action_bar_imageview_app_logo);
        actionBarAppLogoImage.setVisibility(View.GONE);
        actionBarSearchImage = (ImageView) findViewById(R.id.actionbar_search);
        actionBarSearchImage.setVisibility(View.GONE);
        actionBarPersonText= (TextView) findViewById(R.id.action_bar_PersonID);
        actionBarPersonText.setVisibility(View.GONE);
        actionBarPersonTextValue= (TextView) findViewById(R.id.action_bar_PersonAmount);
        actionBarPersonTextValue.setVisibility(View.GONE);

        actionBarBackImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();


            }
        });
    }

}