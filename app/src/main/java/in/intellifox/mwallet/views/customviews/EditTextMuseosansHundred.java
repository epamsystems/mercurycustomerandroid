package in.intellifox.mwallet.views.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Owner on 9/4/2015.
 */
public class EditTextMuseosansHundred extends EditText {

    public EditTextMuseosansHundred(Context context) {
        super(context);
        init();
    }

    public EditTextMuseosansHundred(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextMuseosansHundred(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public EditTextMuseosansHundred(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/museo_sans_hundred.otf");
        setTypeface(tf, 1);

    }
}
