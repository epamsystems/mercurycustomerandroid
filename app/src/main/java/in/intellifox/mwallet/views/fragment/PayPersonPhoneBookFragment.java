package in.intellifox.mwallet.views.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.InputFilter;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Beneficiary;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.parser.ResponseDetails;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.DecimalDigitsInputFilter;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.utilities.LogUtils;
import in.intellifox.mwallet.views.activities.ForgotTPin;

/**
 * Created by Owner on 9/15/2015.
 */
public class PayPersonPhoneBookFragment extends Fragment {

    Button sendMoney;
    LinearLayout mainLayout;
    String timeStamp = "", mobileno = "", amount = "";
    String personName = "", xmlforSendMoney = "";
    String mWalletId = "", tpinhash = "", session = "";
    String bmobile = "9200000010";
    String btpin = "2211";
    private EditText editTextName, editTextPhone, editTextAmount, editTexttpin;
    private ProgressDialog progressDialog;
    String iv = "288dca8258b1dd7c";
    TextView textViewforgottpin, txtNotify;
    ImageView imageViewofbook, imageviewoffavourite, imageviewoffavouritered;
    SimpleCursorAdapter mAdapter;
    MatrixCursor mMatrixCursor;
    private Dialog dialogContactList;

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        try {
            Bundle args = getArguments();
            Beneficiary beneficiary = (Beneficiary) args.getSerializable("SendMoney");
            if (beneficiary != null) {
                editTextName.setText(beneficiary.getNickName());
                editTextPhone.setText(beneficiary.getValue());
            }
        } catch (Exception e) {

        }
        imageviewoffavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageviewoffavourite.isSelected()) {
                    imageviewoffavourite.setSelected(false);
                    imageviewoffavourite.setImageResource(R.drawable.request_money_greystar);
                } else {
                    imageviewoffavourite.setSelected(true);
                    imageviewoffavourite.setImageResource(R.drawable.request_money_redstar);
                }
            }
        });
        textViewforgottpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ForgotTPin.class);
                startActivity(intent);
            }
        });
        imageViewofbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {

                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1);

                } else
                    DialogContacList();

            }
        });
        sendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                personName = editTextName.getText().toString().trim();
                mobileno = editTextPhone.getText().toString().trim();
                amount = editTextAmount.getText().toString().trim();
                btpin = editTexttpin.getText().toString().trim();
                if (personName.length() < 1) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.name_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (mobileno.length() <= 1) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.mobile_number_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (mobileno.length() <= 5) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.invalid_mobile_number))
                            .setTitle("mWallet");

                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                } else if (Double.parseDouble(amount) < 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.amount_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else if (btpin.length() < 6) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.invalid_tpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else {
                    View popupView = getLayoutInflater(savedInstanceState).inflate(R.layout.custom_popup, null);
                    final PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                    TextView tv1 = (TextView) popupView.findViewById(R.id.txt_note1);
                    tv1.setText("Send AED " + amount + " to");
                    TextView tv2 = (TextView) popupView.findViewById(R.id.txt_note2);
                    tv2.setText(personName);
                    TextView tv3 = (TextView) popupView.findViewById(R.id.txt_note3);
                    tv3.setText("Ok to proceed");
                    tv3.setTextSize(12);
                    ImageView cancel = (ImageView) popupView.findViewById(R.id.image_view_cancel_on_popup);
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                        }
                    });
                    Button btnDismiss = (Button) popupView.findViewById(R.id.button_ok_on_popup);
                    btnDismiss.setOnClickListener(new Button.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            popupWindow.dismiss();
                            MessageDigest digest = null;
                            try {
                                digest = MessageDigest.getInstance("SHA-256");
                                digest.update(btpin.getBytes());
                                tpinhash = bytesToHexString(digest.digest());
                                Log.i("Eamorr", "result is " + tpinhash);
                            } catch (NoSuchAlgorithmException e1) {

                                e1.printStackTrace();
                            }
                            bmobile = MyApplication.getInstance().getMerchantMobieNo();
                            mWalletId = MyApplication.getInstance().getmProfile_WalletID_General();
                            if (!imageviewoffavourite.isSelected()) {
                                personName = "";
                            }
                            callSendMoney(bmobile, mobileno, amount, tpinhash, mWalletId, personName);

//                            PayPersonSuccess rms = new PayPersonSuccess();
//                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, rms).commit();
                        }
                    });

                    popupWindow.showAtLocation(mainLayout, Gravity.CENTER, 0, 0);
                }


            }
        });
    }

    public class MyAsyncTaskForSendMoney extends AsyncTask<String, Integer, String> {
        String result = "";

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("Response : " + s);

            progressDialog.dismiss();

            //Parse your response here
            if (result.equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                progressDialog.dismiss();
            } else {

                try {
                    AESecurity aess = new AESecurity();
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted) {
                            Log.d("TAG", "decrypted Pay200=" + decrypted);
                            Log.d("TAG", "mobileNumber Pay200=" + mobileNumber);
                            ParseResponse parse = new ParseResponse();
                            InputSource is = new InputSource(new StringReader(decrypted));
                            MpsXml mpsxml = parse.parseXML(is);

                            if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success")) {

//                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                                builder.setMessage(mpsxml.getResponsedetails().getMessage())
//                                        .setTitle("mWallet");
//                                AlertDialog dialog = builder.create();
//                                dialog.setCanceledOnTouchOutside(true);
//                                dialog.show();
                                progressDialog.dismiss();
                                ResponseDetails responseDetails = new ResponseDetails();
                                responseDetails = mpsxml.getResponsedetails();
                                MyApplication.getInstance().setGeneralBalance("" + responseDetails.getBalance());
                                PayPersonSuccess payPersonMain = new PayPersonSuccess();
                                Bundle bundle = new Bundle();
                                bundle.putString("id", mWalletId);
                                bundle.putString("time", mpsxml.getHeader().getTimestamp());
                                bundle.putSerializable("responseDetails", responseDetails);
                                payPersonMain.setArguments(bundle);
                                FragmentTransaction fragmentTransaction = GlobalVariables.fragmentManager
                                        .beginTransaction();
                                fragmentTransaction.addToBackStack(payPersonMain.getClass()
                                        .getName());
                                fragmentTransaction.add(R.id.dashboard_container, payPersonMain);
                                fragmentTransaction.commit();
                                /*Intent toLogin = new Intent(ChangeTPin.this,MainActivity.class);
                                startActivity(toLogin);
*/
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getReason())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                //progressDialog.dismiss();
                            }
                        }
                    });
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException |
                        InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // utility function for encryption of MPIN
    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public void callSendMoney(final String mobileno, final String bmobile, String amount, String tpinhash, String mWalletId, String personName) {

        progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
        xmlforSendMoney =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>" + timeStamp + "</Timestamp>" +
                        "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                        "<ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>P2P</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<SenderMobileNumber>" + mobileno + "</SenderMobileNumber>" +
                        "<ReceiverMobileNumber>" + bmobile + "</ReceiverMobileNumber>" +
                        "<Amount>" + amount + "</Amount>" +
                        "<Tpin>" + tpinhash + "</Tpin>" +
                        "<WalletId>" + mWalletId + "</WalletId>" +
                        "<NickName>" + personName + "</NickName>" +
                        "<ResponseURL>" + GlobalVariables.url + "</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        " </RequestDetails>" +
                        "</MpsXml>";


        LogUtils.Verbose("xmlforSendMoney",xmlforSendMoney);


        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforSendMoney, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXMLforSendMoney) {
                    String final_String_for_SendMoney = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + mobileno + "|" + encryptedXMLforSendMoney;
                    new MyAsyncTaskForSendMoney().execute(final_String_for_SendMoney);
                }
            });
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    /**
     * An AsyncTask class to retrieve and load listview with contacts
     */
    private class ListViewContactsLoader extends AsyncTask<Void, Void, Cursor> {


        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Please wait!!!");
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Fetching contact");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();

        }

        @Override
        protected Cursor doInBackground(Void... params) {
            Uri contactsUri = ContactsContract.Contacts.CONTENT_URI;

            // Querying the table ContactsContract.Contacts to retrieve all the contacts
            Cursor contactsCursor = getActivity().getContentResolver().query(contactsUri, null, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC ");

            if (contactsCursor.moveToFirst()) {
                do {
                    long contactId = contactsCursor.getLong(contactsCursor.getColumnIndex("_ID"));
                    Uri dataUri = ContactsContract.Data.CONTENT_URI;

                    // Querying the table ContactsContract.Data to retrieve individual items like
                    // home phone, mobile phone, work email etc corresponding to each contact
                    Cursor dataCursor = getActivity().getContentResolver().query(dataUri, null,
                            ContactsContract.Data.CONTACT_ID + "=" + contactId,
                            null, null);


                    String displayName = "";
                    String nickName = "";
                    String homePhone = "";
                    String mobilePhone = "";
                    String workPhone = "";
                    String photoPath = "" + R.drawable.blank;
                    byte[] photoByte = null;
                    String homeEmail = "";
                    String workEmail = "";
                    String companyName = "";
                    String title = "";


                    if (dataCursor.moveToFirst()) {
                        // Getting Display Name
                        displayName = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
                        do {

                            // Getting NickName
                            if (dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE))
                                nickName = dataCursor.getString(dataCursor.getColumnIndex("data1"));

                            // Getting Phone numbers
                            if (dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {
                                switch (dataCursor.getInt(dataCursor.getColumnIndex("data2"))) {
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                        homePhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                        break;
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                        mobilePhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                        break;
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                        workPhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                        break;
                                }
                            }

                            // Getting EMails
                            if (dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {
                                switch (dataCursor.getInt(dataCursor.getColumnIndex("data2"))) {
                                    case ContactsContract.CommonDataKinds.Email.TYPE_HOME:
                                        homeEmail = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                        break;
                                    case ContactsContract.CommonDataKinds.Email.TYPE_WORK:
                                        workEmail = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                        break;
                                }
                            }

                            // Getting Organization details
                            if (dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)) {
                                companyName = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                title = dataCursor.getString(dataCursor.getColumnIndex("data4"));
                            }

                            // Getting Photo
                            if (dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)) {
                                photoByte = dataCursor.getBlob(dataCursor.getColumnIndex("data15"));

                                if (photoByte != null) {
                                    Bitmap bitmap = BitmapFactory.decodeByteArray(photoByte, 0, photoByte.length);

                                    // Getting Caching directory
                                    File cacheDirectory = getActivity().getBaseContext().getCacheDir();

                                    // Temporary file to store the contact image
                                    File tmpFile = new File(cacheDirectory.getPath() + "/wpta_" + contactId + ".png");

                                    // The FileOutputStream to the temporary file
                                    try {
                                        FileOutputStream fOutStream = new FileOutputStream(tmpFile);

                                        // Writing the bitmap to the temporary file as png file
                                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOutStream);

                                        // Flush the FileOutputStream
                                        fOutStream.flush();

                                        //Close the FileOutputStream
                                        fOutStream.close();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    photoPath = tmpFile.getPath();
                                }

                            }

                        } while (dataCursor.moveToNext());

                        String details = "";
                        String mob = "";

                        // Concatenating various information to single string
                        if (homePhone != null && !homePhone.equals(""))
                            details = "HomePhone : " + homePhone + "\n";
                        if (mobilePhone != null && !mobilePhone.equals(""))
                            details += "MobilePhone : " + mobilePhone + "\n";
                        mob = mobilePhone;
                        if (workPhone != null && !workPhone.equals(""))
                            details += "WorkPhone : " + workPhone + "\n";
                        if (nickName != null && !nickName.equals(""))
                            details += "NickName : " + nickName + "\n";
                        if (homeEmail != null && !homeEmail.equals(""))
                            details += "HomeEmail : " + homeEmail + "\n";
                        if (workEmail != null && !workEmail.equals(""))
                            details += "WorkEmail : " + workEmail + "\n";
                        if (companyName != null && !companyName.equals(""))
                            details += "CompanyName : " + companyName + "\n";
                        if (title != null && !title.equals(""))
                            details += "Title : " + title + "\n";

                        // Adding id, display name, path to photo and other details to cursor
                        mMatrixCursor.addRow(new Object[]{Long.toString(contactId), displayName, photoPath, mob});
                    }
                    dataCursor.close();
                } while (contactsCursor.moveToNext());
                contactsCursor.close();
            }
            return mMatrixCursor;
        }

        @Override
        protected void onPostExecute(Cursor result) {
            // Setting the cursor containing contacts to listview
            txtNotify.setVisibility(View.GONE);
            mAdapter.swapCursor(result);
            progressDialog.dismiss();
        }
    }

    void DialogContacList() {
        dialogContactList = new Dialog(getActivity());
        // Include dialog.xml file
        WindowManager.LayoutParams wmlp = dialogContactList.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;
        dialogContactList.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogContactList.setContentView(R.layout.dialog_contact_list);
        dialogContactList.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();

        wmlp = new WindowManager.LayoutParams();
        wmlp.copyFrom(dialogContactList.getWindow().getAttributes());
        wmlp.width = (int) (width - (width * 0.1));  //Tira 7% da largura total da tela para deixar um espaço na janela
        wmlp.height = (int) (height - (height * 0.11));

        dialogContactList.getWindow().setAttributes(wmlp);
        // The contacts from the contacts content provider is stored in this cursor
        mMatrixCursor = new MatrixCursor(new String[]{"_id", "name", "photo", "details"});

        // Adapter to set data in the listview
        mAdapter = new SimpleCursorAdapter(getActivity().getBaseContext(),
                R.layout.lv_layout, null,
                new String[]{"name", "photo", "details"},
                new int[]{R.id.tv_name, R.id.iv_photo, R.id.tv_details}, 0);

        // Getting reference to listview
        ListView lstContacts = (ListView) dialogContactList.findViewById(R.id.lst_contacts);
        txtNotify = (TextView) dialogContactList.findViewById(R.id.txtNotify);
        txtNotify.setVisibility(View.VISIBLE);
        // Setting the adapter to listview
        lstContacts.setAdapter(mAdapter);

        // Creating an AsyncTask object to retrieve and load listview with contacts
        ListViewContactsLoader listViewContactsLoader = new ListViewContactsLoader();

        // Starting the AsyncTask process to retrieve and load listview with contacts
        listViewContactsLoader.execute();
        lstContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = mMatrixCursor.getString(1);
                String number = mMatrixCursor.getString(3);
                editTextName.setText(name);
                editTextPhone.setText(number);
                dialogContactList.dismiss();
                Log.d("Contact", "Name=" + name);
                Log.d("Contact", "number=" + number);
            }
        });
        dialogContactList.show();
    }

    private void LayoutInitialization(View view) {
        sendMoney = (Button) view.findViewById(R.id.button_requestmoney_sendmoney);
        sendMoney.setText(R.string.send_money_btn);
        mainLayout = (LinearLayout) view.findViewById(R.id.main_layout);
        editTextName = (EditText) view.findViewById(R.id.edit_text_name);
        editTextPhone = (EditText) view.findViewById(R.id.edit_text_phone);
        editTextAmount = (EditText) view.findViewById(R.id.edit_text_amount);
        editTextAmount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(15, 2)});
        editTexttpin = (EditText) view.findViewById(R.id.edit_text_tpin);
        textViewforgottpin = (TextView) view.findViewById(R.id.textViewforgottpin);
        imageViewofbook = (ImageView) view.findViewById(R.id.image_view_of_book);
        imageviewoffavourite = (ImageView) view.findViewById(R.id.image_view_of_favourite_red);
        imageviewoffavourite.setSelected(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.request_money_phone_book_fragment, container, false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0)
            DialogContacList();
    }
}
