package in.intellifox.mwallet.views.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

import dialogs.OkDialog;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.apicalls.RetrofitTask;
import in.intellifox.mwallet.models.MoneyRequest;
import in.intellifox.mwallet.security.AESecurityNew;
import in.intellifox.mwallet.utilities.GenericResponseHandler;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.utilities.Utils;

/**
 * Created by Owner on 9/15/2015.
 */
public class PendingRequestSendMoneyFragment extends Fragment {

    Button sendMoney;
    LinearLayout mainLayout;
    String timeStamp = "", mobileno = "", amount = "";
    String personName = "", xmlforSendMoney = "";
    String mWalletId = "", tpinhash = "", session = "";
    TextView txtBalance;
    String btpin = "2211";
    private EditText editTexttpin;
    private ProgressDialog progressDialog;
    TextView textViewforgottpin, textViewName, textViewPhone, textViewAmount;
    MoneyRequest moneyRequest;
    String status, tPin;

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);

        moneyRequest = (MoneyRequest) getArguments().getSerializable("moneyRequest");
        status = getArguments().getString("status");
        textViewName.setText(moneyRequest.getFirstName() + " " + moneyRequest.getLastName());
        textViewPhone.setText(moneyRequest.getMobileNumber());
        textViewAmount.setText(moneyRequest.getAmount());

        txtBalance.setText("mWallet Balance:  AED " + MyApplication.getInstance().getTotalBalance());

        if (status.equalsIgnoreCase("Reject"))
            sendMoney.setText("Reject Request");


        sendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tPin = editTexttpin.getText().toString();

                if (tPin.length() != 6) {
                    new OkDialog(getActivity(), "Please enter valid tPin", null, null);
                    return;
                }

                callAPI();

            }
        });
    }

    private void callAPI() {

        String xmlParam = "\t\t<MobileNumber>" + MyApplication.getInstance().getmProfile_Mobile_Number() + "</MobileNumber>\n" +
                " <TxnId>" + moneyRequest.getId() + "</TxnId>\n" +
                "<Tpin>" + Utils.getHash(tPin.getBytes()) + "</Tpin>\n" +
                " <WalletId>" + MyApplication.getInstance().getmProfile_WalletID_General() + "</WalletId>\n" +
                " <Status>" + status + "</Status>";


        xmlParam = Utils.getReqXML("AuthReqMoney", xmlParam);
        try {
            xmlParam = AESecurityNew.getInstance().encryptString(xmlParam);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.showToast(getActivity(), e.toString());
            return;
        }

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.show();

        RetrofitTask.getInstance().executeTask(xmlParam, new RetrofitTask.IRetrofitTask() {
            @Override
            public void handleResponse(boolean isSuccess, String response) {

                progressDialog.dismiss();

                if (!isSuccess) {
                    new OkDialog(getActivity(), response, null, null);
                    return;
                }
                String decrypted = null;
                try {
                    decrypted = AESecurityNew.getInstance().decryptString(response);
                    Log.v("TAG", "Response " + decrypted);

                    ArrayList<String> elements = new ArrayList<String>();
                    elements.add("ResponseType");
                    elements.add("Reason");
                    elements.add("Message");
                    elements.add("Amount");
                    elements.add("Fee");
                    elements.add("Balance");
                    elements.add("Timestamp");
                    elements.add("TxnId");


                    Map<String, String> responseMap = GenericResponseHandler.parseElements(decrypted, elements);

                    if (responseMap.get("ResponseType").equals("Success")) {

                        PendingRequestSuccess pendingRequestSuccess = new PendingRequestSuccess();
                        Bundle bundle = new Bundle();
                        bundle.putString("Name",moneyRequest.getFirstName()+" "+moneyRequest.getLastName());
                        bundle.putString("TxnId",responseMap.get("TxnId"));
                        bundle.putString("Message",responseMap.get("Message"));
                        bundle.putString("Timestamp",responseMap.get("Timestamp"));
                        bundle.putString("Amount",responseMap.get("Amount"));
                        pendingRequestSuccess.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = GlobalVariables.fragmentManager
                                .beginTransaction();
                        fragmentTransaction.addToBackStack(pendingRequestSuccess.getClass()
                                .getName());
                        fragmentTransaction.add(R.id.dashboard_container, pendingRequestSuccess);
                        fragmentTransaction.commit();

                    } else
                        new OkDialog(getActivity(), responseMap.get("Reason"), null, null);

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.showToast(getActivity(), e.getMessage());
                }

            }
        });
    }


    private void LayoutInitialization(View view) {
        sendMoney = (Button) view.findViewById(R.id.btnSubmit);
        mainLayout = (LinearLayout) view.findViewById(R.id.main_layout);
        textViewName = (TextView) view.findViewById(R.id.txtName);
        textViewPhone = (TextView) view.findViewById(R.id.txtMobile);
        textViewAmount = (TextView) view.findViewById(R.id.txtamount);
        txtBalance = (TextView) view.findViewById(R.id.headertextView);

        editTexttpin = (EditText) view.findViewById(R.id.edtTpin);
//        textViewforgottpin= (TextView) view.findViewById(R.id.textViewforgottpin);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pending_request_listinfo_fragment, container, false);
    }
}
