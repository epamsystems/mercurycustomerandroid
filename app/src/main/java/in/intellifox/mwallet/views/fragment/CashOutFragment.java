package in.intellifox.mwallet.views.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.parser.ResponseDetails;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.utilities.DecimalDigitsInputFilter;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.utilities.Utility;
import in.intellifox.mwallet.views.activities.DrawerMain;
import in.intellifox.mwallet.views.activities.ForgotTPin;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CashOutFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CashOutFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CashOutFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Button sendMoney;
    LinearLayout mainLayout;
    String timeStamp = "",mobileno = "",amount = "";
    String agentID = "", xmlforSendMoney = "";
    String mWalletId = "",tpinhash="",session = "";
    String bmobile="9200000010";
    String btpin="2211";
    private EditText editTextAgentId,editTextAmount,editTexttpin;
    private ProgressDialog progressDialog;
    String iv = "288dca8258b1dd7c";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private String wAmount;
    private TextView textViewforgottpin;

    public CashOutFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CashOutFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CashOutFragment newInstance(String param1, String param2) {
        CashOutFragment fragment = new CashOutFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.cash_out_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        GlobalVariables.fragmentManager = getFragmentManager();
        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        textViewforgottpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ForgotTPin.class);
                startActivity(intent);
            }
        });
        sendMoney.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                agentID=editTextAgentId.getText().toString().trim();
                amount=editTextAmount.getText().toString().trim();
                btpin=editTexttpin.getText().toString().trim();
                if (agentID.length() <= 0 ) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("AgentID field empty")
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else
                if (amount.length() <1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.amount_field_empty))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                }
                else if (btpin.length() <6) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.invalid_tpin))
                            .setTitle("mWallet");
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                } else
                {
                    View popupView = getLayoutInflater(savedInstanceState).inflate(R.layout.custom_popup, null);
                    final PopupWindow popupWindow = new PopupWindow(popupView,ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);

                    TextView tv1=(TextView)popupView.findViewById(R.id.txt_note1);
                    tv1.setText("Cash out AED "+amount +" to");
                    TextView tv2=(TextView)popupView.findViewById(R.id.txt_note2);
                    tv2.setText(agentID);
                    TextView tv3=(TextView)popupView.findViewById(R.id.txt_note3);
                    tv3.setText("Ok to proceed");
                    tv3.setTextSize(12);
                    ImageView cancel= (ImageView) popupView.findViewById(R.id.image_view_cancel_on_popup);
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                        }
                    });
                    Button btnDismiss = (Button)popupView.findViewById(R.id.button_ok_on_popup);
                    btnDismiss.setOnClickListener(new Button.OnClickListener()
                    {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            popupWindow.dismiss();
                            MessageDigest digest = null;
                            try
                            {
                                digest = MessageDigest.getInstance("SHA-256");
                                digest.update(btpin.getBytes());
                                tpinhash = bytesToHexString(digest.digest());
                                Log.i("Eamorr", "result is " + tpinhash);
                            } catch (NoSuchAlgorithmException e1) {

                                e1.printStackTrace();
                            }
                            bmobile= MyApplication.getInstance().getMerchantMobieNo();
                            mWalletId= MyApplication.getInstance().getmProfile_WalletID_General();
                            callSendMoney(bmobile,agentID, amount, tpinhash, mWalletId);

//                            PayPersonSuccess rms = new PayPersonSuccess();
//                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, rms).commit();
                        }
                    });

                    popupWindow.showAtLocation(mainLayout, Gravity.CENTER, 0, 0);
                }



            }
        });
    }
    public void  callSendMoney(final String bmobile, String agentID, String amount, String tpinhash, String mWalletId){

        Utility.hideKeyboard(getActivity(),mainLayout);
        progressDialog = ProgressDialog.show(getActivity(), null, getResources().getString(R.string.please_wait_msg), true);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());
        xmlforSendMoney =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>"+timeStamp+"</Timestamp>" +
                        "<SessionId>"+ GlobalVariables.sessionId+"</SessionId>" +
                        "<ServiceProvider>"+getResources().getString(R.string.serviceprovider)+"</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>WithCash</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<MobileNumber>"+bmobile+"</MobileNumber>" +
                        "<AgentId>"+ agentID +"</AgentId>" +
                        "<Amount>"+ amount +"</Amount>" +
                        "<Tpin>"+ tpinhash +"</Tpin>" +
                        "<WalletId>"+ mWalletId +"</WalletId>" +
                        "<ResponseURL>"+ GlobalVariables.url+"</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        " </RequestDetails>" +
                        "</MpsXml>";

            Log.d("TAG","xmlforSendMoney="+xmlforSendMoney);
        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlforSendMoney, MyApplication.getInstance().getKey(),iv,new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXMLforSendMoney)
                {
                    String final_String_for_SendMoney = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + bmobile + "|" + encryptedXMLforSendMoney;
                    new MyAsyncTaskForSendMoney().execute(final_String_for_SendMoney);
                }
            });
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }
    public class MyAsyncTaskForSendMoney extends AsyncTask<String, Integer, String>
    {
        String result = "";

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(GlobalVariables.url);
            System.out.println("request" + params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            System.out.println("Response : " + s);

            progressDialog.dismiss();

            //Parse your response here
            if(result.equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                        .setTitle("mWallet");
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                progressDialog.dismiss();
            }else {

                try {
                    AESecurity aess = new AESecurity();
                    aess.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted)
                        {
                            Log.d("TAG","decrypted Pay200="+decrypted);
                            Log.d("TAG","mobileNumber Pay200="+mobileNumber);
                            ParseResponse parse = new ParseResponse();
                            InputSource is = new InputSource(new StringReader(decrypted));
                            MpsXml mpsxml = parse.parseXML(is);

                            if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success"))
                            {

                                progressDialog.dismiss();
                                ResponseDetails responseDetails=new ResponseDetails();
                                responseDetails=mpsxml.getResponsedetails();

                                Log.d("TAG","responseDetails"+responseDetails.getAgentId());
                                MyApplication.getInstance().setGeneralBalance(""+responseDetails.getBalance());
                                CashOutSuccess cashOutSuccess = new CashOutSuccess();
                                Bundle bundle = new Bundle();
                                bundle.putString("id",mWalletId);
                                bundle.putString("time",mpsxml.getHeader().getTimestamp());
                                bundle.putSerializable("responseDetails", responseDetails);
                                cashOutSuccess.setArguments(bundle);
                                FragmentTransaction fragmentTransaction = GlobalVariables.fragmentManager
                                        .beginTransaction();
                                fragmentTransaction.addToBackStack(cashOutSuccess.getClass()
                                        .getName());
                                fragmentTransaction.add(R.id.dashboard_container, cashOutSuccess);
                                fragmentTransaction.commit();
                                /*Intent toLogin = new Intent(ChangeTPin.this,MainActivity.class);
                                startActivity(toLogin);
*/
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(mpsxml.getResponsedetails().getReason())
                                        .setTitle("mWallet");
                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                //progressDialog.dismiss();
                            }
                        }
                    });
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException |
                        InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    // utility function for encryption of MPIN
    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String walletBalance = MyApplication.getInstance().getTotalBalance();
        Log.v("TAG","walletBalance="+MyApplication.getInstance().getTotalBalance());
        if(walletBalance!=null)
        {
            wAmount=walletBalance;
        }
        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Cash Out", true,"mWallet Balance", true,"AED"+wAmount);

    }
    private void LayoutInitialization(View view)
    {
        sendMoney=(Button)view.findViewById(R.id.button_requestmoney_sendmoney);
        mainLayout=(LinearLayout)view.findViewById(R.id.main_layout);
        editTextAgentId= (EditText) view.findViewById(R.id.edit_text_agentId);
        editTextAmount= (EditText) view.findViewById(R.id.edit_text_amount);
        editTextAmount.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(15,2)});
        editTexttpin= (EditText) view.findViewById(R.id.edit_text_tpin);
        textViewforgottpin= (TextView) view.findViewById(R.id.textViewforgottpin);
    }
}
