package in.intellifox.mwallet.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;
import in.intellifox.mwallet.views.adapter.ViewPagerAdapter;
import in.intellifox.mwallet.views.customviews.SlidingTabLayout;

/**
 * Created by Owner on 9/3/2015.
 */
public class ChangePinMainFragment extends Fragment {

    View v;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String CONTAINER_ID = "container_id";
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"M-PIN", "T-PIN"};
    ImageView mpin,tpin;
    RelativeLayout rootLay;
    private ChangeMpinFragment m_mpin_tab;
    private ChangeTpinFragment m_tpin_tab;

    String MobileNo = "";
    int Numboftabs = 2;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public static ChangePinMainFragment newInstance(int param1, String param2, String MobileNo) {
        ChangePinMainFragment fragment = new ChangePinMainFragment();
        Bundle args = new Bundle();
        args.putInt(CONTAINER_ID, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString("mobilenumber", MobileNo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            MobileNo = getArguments().getString("mobilenumber");

        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mpin = (ImageView) view.findViewById(R.id.layout_position_0);
        tpin = (ImageView) view.findViewById(R.id.layout_position_1);
        final RelativeLayout frag_container = (RelativeLayout) view.findViewById(R.id.fragment_container);
        rootLay=(RelativeLayout)view.findViewById(R.id.root_relative_lay_for_changepins);

        rootLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        ChangeMpinFragment mp = new ChangeMpinFragment();
        Bundle b = new Bundle();
        b.putString("mobilenumber", MobileNo);
        mp.setArguments(b);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mp).commit();

        mpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyApplication.resetTimer(getActivity());
                m_mpin_tab = new ChangeMpinFragment();
                Bundle bundle = new Bundle();
                bundle.putString("mobilenumber", MobileNo);
                System.out.println("MobileNo : " + MobileNo);
                m_mpin_tab.setArguments(bundle);
                tpin.setImageResource(R.drawable.change_tpin_deselected);
                mpin.setImageResource(R.drawable.change_mpin_selected);
                ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Change MPIN",false,"",false,"");
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, m_mpin_tab).commit();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                //Toast.makeText(getActivity(),"mpin click",Toast.LENGTH_LONG).show();
            }
        });

        tpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MyApplication.resetTimer(getActivity());
                mpin.setImageResource(R.drawable.change_mpin_deselected);
                tpin.setImageResource(R.drawable.change_tpin_selected);
                ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Change TPIN",false,"",false,"");
                m_tpin_tab = new ChangeTpinFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putString("mobilenumber", MobileNo);
                System.out.println("MobileNo : " + MobileNo);
                m_tpin_tab.setArguments(bundle1);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, m_tpin_tab).commit();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });
    }
    //Menu On ActionBar
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Change MPIN",false,"",false,"");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.change_pin_main, container, false);
    }

}
