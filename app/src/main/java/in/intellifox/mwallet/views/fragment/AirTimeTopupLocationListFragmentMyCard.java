package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.adapter.CustomListLocationAirtimeFrafment;

/**
 * Created by Raju on 18/09/2015.
 */
public class AirTimeTopupLocationListFragmentMyCard extends Fragment{

    private AirTimeTopupMain airTimeTopupMain;
    private AirTimeTopupOperatorListFragmentMyCards airTimeTopupOperatorListFragmentMyCards;
    private AirTimeTopupMyCard airTimeTopupMyCard;
    ListView LocationList;
    ImageView paybycardsedit,paybycardscross;
    LinearLayout rootlay;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LocationList=(ListView)view.findViewById(R.id.airtimetopup_list_location);
        paybycardsedit = (ImageView)view.findViewById(R.id.airtimetopupmwalletbalance_location_editimages);
        paybycardscross = (ImageView)view.findViewById(R.id.airtimetopupmwalletbalance_location_cross);
        rootlay=(LinearLayout)view.findViewById(R.id.root_linear_lay_for_airtime_location);

        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        paybycardscross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airTimeTopupMain = new AirTimeTopupMain();
                Bundle bundle = new Bundle();
                airTimeTopupMain.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupMain).commit();
            }
        });

        paybycardsedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airTimeTopupOperatorListFragmentMyCards = new AirTimeTopupOperatorListFragmentMyCards();
                Bundle bundle = new Bundle();
                airTimeTopupOperatorListFragmentMyCards.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupOperatorListFragmentMyCards).commit();
            }
        });

        CustomListLocationAirtimeFrafment navListAdaptercenter=new CustomListLocationAirtimeFrafment(getActivity().getApplicationContext());
        LocationList.setAdapter(navListAdaptercenter);

        LocationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                airTimeTopupMyCard = new AirTimeTopupMyCard();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airTimeTopupMyCard).commit();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.airtime_topup_location, container, false);
    }
}
