package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.utilities.DecimalDigitsInputFilter;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/18/2015.
 */
public class BankAccountAddMoneyFragment extends Fragment {

    LinearLayout hundred, five, thousand, fiftyhundred, mainLayout;
    EditText edit_text_amount;
    TextView transfermoneyaccount;
    private String wAmount = "";

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);

        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });
        String walletBalance = MyApplication.getInstance().getTotalBalance();
        if (walletBalance != null) {
            wAmount = walletBalance;
        }
        transfermoneyaccount.setText(wAmount);
        hundred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_text_amount.setText("" + 100);
            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_text_amount.setText("" + 500);
            }
        });
        thousand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_text_amount.setText("" + 1000);
            }
        });
        fiftyhundred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_text_amount.setText("" + 1500);
            }
        });
    }

    private void LayoutInitialization(View view) {
        hundred = (LinearLayout) view.findViewById(R.id.layout_hundred);
        five = (LinearLayout) view.findViewById(R.id.layout_five);
        thousand = (LinearLayout) view.findViewById(R.id.layout_thousand);
        fiftyhundred = (LinearLayout) view.findViewById(R.id.layout_fiftyhundred);
        edit_text_amount = (EditText) view.findViewById(R.id.edit_text_amount);
        edit_text_amount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(15, 2)});
        mainLayout = (LinearLayout) view.findViewById(R.id.root_linear_lay_for_bank_addmoney);
        transfermoneyaccount = (TextView) view.findViewById(R.id.transfermoney_account);
    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain) getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Add Money", false, "", false, "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bankaccount_add_money_main, container, false);
    }
}
