package in.intellifox.mwallet.views.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.glxn.qrgen.android.QRCode;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/14/2015.
 */
public class GenerateQrCodeFinalFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String qrAmount="";

    private String MerchantID="" , MerchantName="",MobileNo="";
    RelativeLayout rootLay;
    private OnFragmentInteractionListener mListener;
    TextView amount;
    ImageView editImageView,myImage;
    Fragment generateQrCodeMainFragment;

    // TODO: Rename and change types and number of parameters
    public static GenerateQrCodeFinalFragment newInstance(String param1, String AmountQr) {
        GenerateQrCodeFinalFragment fragment = new GenerateQrCodeFinalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString("Amount", AmountQr);
        args.putString("FirstName", MyApplication.getInstance().getmProfile_FirstName());
        args.putString("LastName", MyApplication.getInstance().getmProfile_Last_Name());
        args.putString("MobileNumber",MyApplication.getInstance().getMerchantMobieNo());
        System.out.println("mobile no" + MyApplication.getInstance().getmProfile_Mobile_Number());
        fragment.setArguments(args);
        return fragment;
    }

    public GenerateQrCodeFinalFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            qrAmount = getArguments().getString("Amount");
            MerchantName = getArguments().getString("FirstName");
            MerchantID = getArguments().getString("LastName");
            MobileNo = getArguments().getString("MobileNumber");

        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        MyApplication.resetTimer(getActivity());
        rootLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });
        editImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateQrCodeMainFragment = new GenerateQrCodeMainFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, generateQrCodeMainFragment).commit();
            }
        });
    }

    private void LayoutInitialization(View view) {
        editImageView=(ImageView)view.findViewById(R.id.imageView_edit_amount_icon);
        myImage=(ImageView)view.findViewById(R.id.image_view_qrcode_generation);
        rootLay = (RelativeLayout) view.findViewById(R.id.root_relative_lay_for_qrcode_final);
        amount = (TextView) view.findViewById(R.id.text_view_mWallet_balance);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        amount.setText("AED " + qrAmount);
        System.out.println("merachant name " + MerchantName);
        ((DrawerMain)getActivity()).updateActionBarData(false, false, false, false, false, false, true, "Generate QR code", false, "", false, "");
        Bitmap myBitmap = QRCode.from("FirstName=" + MerchantName + ",LastName=" + MerchantID + ",Amount=" + qrAmount + ",MobileNumber=" + MobileNo).bitmap();
        myImage.setImageBitmap(myBitmap);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.generate_qrcode_final_fragment, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //  mListener = null;
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
}
