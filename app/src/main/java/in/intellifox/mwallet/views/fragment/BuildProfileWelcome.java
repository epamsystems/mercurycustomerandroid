package in.intellifox.mwallet.views.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.Profile;

/**
 * Created by Owner on 9/4/2015.
 */
public class BuildProfileWelcome extends Fragment {

    TextView name;
    String userName;
    Button proceedButton;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        proceedButton=(Button)view.findViewById(R.id.button_login_kyc);
        name= (TextView) view.findViewById(R.id.nameWelcom_layout);
        final RelativeLayout frag_container = (RelativeLayout) view.findViewById(R.id.fragment_container);
        try{
            Bundle args = getArguments();
            userName=(String) args.getString("name");
            name.setText(userName);
        }catch (Exception e)
        {

        }


        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), MainActivity.class);
               getActivity().startActivity(intent);
            }
        });

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.buildprofile_welcome, container, false);
    }

}
