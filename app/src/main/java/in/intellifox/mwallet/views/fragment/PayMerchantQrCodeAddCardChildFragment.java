package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import in.intellifox.mwallet.R;

/**
 * Created by Owner on 9/10/2015.
 */
public class PayMerchantQrCodeAddCardChildFragment extends Fragment {

    Button payByCard;
    LinearLayout mainLayout;
    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);
        payByCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View popupView = getLayoutInflater(savedInstanceState).inflate(R.layout.custom_popup, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);

                TextView tv1=(TextView)popupView.findViewById(R.id.txt_note1);
                tv1.setText("You will be redirected to third party");
                TextView tv2=(TextView)popupView.findViewById(R.id.txt_note2);
                tv2.setText("payment site.");

                Button btnDismiss = (Button)popupView.findViewById(R.id.button_ok_on_popup);
                btnDismiss.setOnClickListener(new Button.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        popupWindow.dismiss();
                    }});
                popupWindow.showAtLocation(mainLayout, Gravity.CENTER,0,0);
            }
        });
    }

    private void LayoutInitialization(View view) {
        payByCard=(Button)view.findViewById(R.id.button_childrow_sendmoney);
        mainLayout=(LinearLayout)view.findViewById(R.id.root_layout);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pay_merchant_qrcode_payby_card_child_addcard, container, false);
    }
}
