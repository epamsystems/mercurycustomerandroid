package in.intellifox.mwallet.views.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by SADIQ on 7/21/2015.
 */
public class TextViewOpensansRegular extends TextView {
    public TextViewOpensansRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextViewOpensansRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewOpensansRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/opensansregular.ttf");
        setTypeface(tf ,1);

    }
}
