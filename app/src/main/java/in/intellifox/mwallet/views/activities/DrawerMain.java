package in.intellifox.mwallet.views.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.tech.NfcF;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.picasso.Picasso;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import dialogs.OkDialog;
import in.intellifox.mwallet.MainActivity;
import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.parser.MpsXml;
import in.intellifox.mwallet.parser.ParseResponse;
import in.intellifox.mwallet.security.AESecurity;
import in.intellifox.mwallet.services.MyFirebaseInstanceIDService;
import in.intellifox.mwallet.utilities.AppSettings;
import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.utilities.LogUtils;
import in.intellifox.mwallet.utilities.Utils;
import in.intellifox.mwallet.views.fragment.AboutFragment;
import in.intellifox.mwallet.views.fragment.AirTimeTopupMain;
import in.intellifox.mwallet.views.fragment.BankAccountAddMoneyFragment;
import in.intellifox.mwallet.views.fragment.BankAccountMainFragment;
import in.intellifox.mwallet.views.fragment.BeneficiariesMain;
import in.intellifox.mwallet.views.fragment.CashOutFragment;
import in.intellifox.mwallet.views.fragment.ChangePinMainFragment;
import in.intellifox.mwallet.views.fragment.DashBoard;
import in.intellifox.mwallet.views.fragment.FavouritesMain;
import in.intellifox.mwallet.views.fragment.GenerateQrCodeMainFragment;
import in.intellifox.mwallet.views.fragment.InviteFriendFragment;
import in.intellifox.mwallet.views.fragment.ManageCardMainFragment;
import in.intellifox.mwallet.views.fragment.PayMerchantMainFragment;
import in.intellifox.mwallet.views.fragment.PayPersonMain;
import in.intellifox.mwallet.views.fragment.PendingRequestMain;
import in.intellifox.mwallet.views.fragment.RequestMoneyMainFragment;
import in.intellifox.mwallet.views.fragment.TransactionSearchFragment;
import in.intellifox.mwallet.views.fragment.TransactionSummaryFragment;

/**
 * Created by Owner on 9/3/2015.
 */
public class DrawerMain extends ActionBarActivity {

    RelativeLayout drawerPane, rootlay, relativeLayoutChangePin, relativeLayoutTransactionHistory, relativeLayoutFavourites;
    RelativeLayout relativeLayoutPayPerson, relativeLayoutPayMerchant, relativeLayoutBeneficiaries, relativeLayoutBankAccounts;
    RelativeLayout relativeLayoutCard, relativeLayoutQrCode, relativeLayoutBill, relativeLayoutAirtimeTopup;
    RelativeLayout relativeLayoutInviteFriends, relativeLayoutPendingRequest, relativeLayoutRequestMoney, relativeLayoutAboutmWallet, relativeLayoutOffers;
    Fragment transactionSummaryFragment, payPersonMain, payMerchantMainFragment, beneficiariesMain, bankAccountMainFragment;
    Fragment cardFragment, qrCodeFragment, billFragment, airtimeTopupFragment, inviteFriendFragment, pendingRequestFragment, cashout;
    Fragment requestMoneyFragment, aboutFragment, changePinFragment, favouritesFragment, dashBoard, transactionSearchFragment, bankAccountAddMoneyFragment, offerFragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private boolean mIsTransactionSummaryClosed;
    public static boolean isOnDashboard = false;
    TextView tvname, tvinfo, tvamount, actionBarTitleText, actionBarPersonText, actionBarPersonTextValue;
    Button logout;
    ImageView actionBarNavagationImage, actionBarAppLogoImage, actionBarBackImage, actionBarNotificationImage;
    ImageView actionBarQrcodeImage, actionBarSearchImage;
    private static final String STATE_MENUDRAWER = "net.simonvt.menudrawer.samples.WindowSample.menuDrawer";
    private static final String STATE_ACTIVE_VIEW_ID = "net.simonvt.menudrawer.samples.WindowSample.activeViewId";
    String mobileNo;
    TextView user_profile_name, textView_last_login, textView_mwallet_balance;
    String userFirstName = "", userLastName = "", walletIdGen = "", lastLogin = "", walletBalance = "",
            lastTransaction = "", merChantId = "", merChantName = "", merchantType = "", address = "",
            pinCode = "", taxIdentificationNumber = "", tradingLicenceNumber = "", emailId = "", mobileNumber = "",
            businessDiscreption = "", companyName = "";
    String timeStamp = "", mobileno = "", mpin = "";
    String xmlvalue = "", xmlforBankDetails = "";
    String mpinhash = "", session = "";
    String iv = "288dca8258b1dd7c";
    private ProgressDialog progressDialog = null;
    String key = "c292a6e6c19b7403cd87949d0ad45021";
    Random sessionID;
    private int mActiveViewId;
    static SharedPreferences pref;
    static SharedPreferences.Editor editor;
    private MenuDrawer mMenuDrawer;
    ImageView profile_photo;




    RelativeLayout llNotification;
    TextView txtNotificationCount;

    private static final String TYPE = "application/toml";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



            pref = getApplicationContext().getSharedPreferences("Session", 0);
            editor = pref.edit();
            GlobalVariables.sessionId = pref.getString("SessionId", "");
            Log.d("TAG", "Session id2==" + GlobalVariables.sessionId);
            // GlobalVariables.url = pref.getString("currentUrl", getResources().getString(R.string.responseurl));
            GlobalVariables.url = pref.getString("currentUrl", GlobalVariables.url);
            sessionID = new Random();
            if (savedInstanceState != null) {
                mActiveViewId = savedInstanceState.getInt(STATE_ACTIVE_VIEW_ID);
            }
            mMenuDrawer = MenuDrawer.attach(this, MenuDrawer.Type.BEHIND, Position.LEFT, MenuDrawer.MENU_DRAG_WINDOW);
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            int width = dm.widthPixels;
            if (width >= 1000) {
                mMenuDrawer.setMenuSize(900);
            } else if (width < 1000 && width >= 720) {
                mMenuDrawer.setMenuSize(600);
            } else if (width < 720) {
                mMenuDrawer.setMenuSize(450);
            }
            mMenuDrawer.setContentView(R.layout.activity_dash_board);
            mMenuDrawer.setMenuView(R.layout.drawer_gridlayout);
            LayoutInitialization();
            setUpActionBar();
            MyApplication.resetTimer(DrawerMain.this);
            rootlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyApplication.resetTimer(DrawerMain.this);
                }
            });

            if (!AppSettings.getData(getApplicationContext(), AppSettings.isTOKENSENDTOSERVER).equals("true")) {
                LogUtils.Verbose("TAG", "startService");
                Intent intent = new Intent(getApplicationContext(), MyFirebaseInstanceIDService.class);
                startService(intent);
            }

            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction = fragmentManager.beginTransaction();
            walletBalance = MyApplication.getInstance().getTotalBalance();
            dashBoard = DashBoard.newInstance("", "", walletBalance);
            fragmentTransaction.add(R.id.dashboard_container, dashBoard);
            fragmentTransaction.commit();

            textView_mwallet_balance.setText("Balance: AED " + walletBalance);
            // tvname.setText(MyApplication.getInstance().getmProfile_FirstName() + " " + MyApplication.getInstance().getmProfile_Last_Name());
            tvname.setText(MyApplication.getInstance().getmProfile_FirstName() + " " + MyApplication.getInstance().getmProfile_Last_Name());
            Log.d("TAG", "Last Login 128=" + MyApplication.getInstance().getmProfile_Last_Login());
            textView_last_login.setText("Last Login: " + MyApplication.getInstance().getmProfile_Last_Login());


            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);


            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  Intent logout = new Intent(DrawerMain.this, MainActivity.class);
                    //  startActivity(logout);
                    mobileNo = MyApplication.getInstance().getMerchantMobieNo();
                    if (mobileNo.length() <= 1 && mpin.length() <= 1) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(DrawerMain.this);
                        builder.setMessage(getResources().getString(R.string.mobile_number_field_empty))
                                .setTitle("mWallet");
                        AlertDialog dialog = builder.create();
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();

                    } else {

                        final AlertDialog.Builder builder = new AlertDialog.Builder(DrawerMain.this);
                        builder.setMessage("Do you really want to logout?")
                                .setTitle("mWallet");
                                    /*builder.setMessage(mpsxml.getResponsedetails().getFirstName())
                                            .setTitle("mWallet");
                                    builder.setMessage(mpsxml.getResponsedetails().getLastName())
                                            .setTitle("mWallet");*/
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                progressDialog = ProgressDialog.show(DrawerMain.this, null,
                                        getResources().getString(R.string.authentication_msg), true);
                                dialog.dismiss();
                                CallLogout(mobileNo);
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.setCancelable(false);
                        AlertDialog dialog = builder.create();
                        dialog.setCancelable(true);
                        dialog.show();


                    }
                }
            });


            relativeLayoutChangePin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenChangePin();
                }
            });

            relativeLayoutTransactionHistory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenTransactionHistory();
                }
            });

            relativeLayoutFavourites.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenFavourites();
                }
            });

            relativeLayoutPayPerson.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenPayPerson();
                }
            });

            relativeLayoutPayMerchant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenPayMerchant();
                }
            });

            relativeLayoutBeneficiaries.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenBeneficiaries();
                }
            });

            relativeLayoutBankAccounts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenBankAccount();
                }
            });

            relativeLayoutCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenCard();
                }
            });

            relativeLayoutQrCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenQrCode();
                }
            });

            relativeLayoutBill.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenBill();
                }
            });

            relativeLayoutAirtimeTopup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenAirtimeTopup();
                }
            });

            relativeLayoutInviteFriends.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenInviteFriends();
                }
            });

            relativeLayoutPendingRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenPendingRequest();
                }
            });

            relativeLayoutRequestMoney.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenRequestMoney();
                }
            });

            relativeLayoutChangePin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenChangePin();
                }
            });



            if (getIntent().hasExtra("changeMpin") && (getIntent().getStringExtra("changeMpin").equals("Y"))) {
                Intent i = new Intent(DrawerMain.this, ChangeMPin.class);
                startActivity(i);
            }

//        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
//            @Override
//            public void onBackStackChanged() {
//
//
//            }
//        });





    }


    @Override
    protected void onStart() {
        super.onStart();

        handleNotificationCount();
    }

    public void handleNotificationCount() {
        String c = (AppSettings.getData(this, AppSettings.NOTIFICATION_COUNT).equals("")) ? "0" : AppSettings.getData(this, AppSettings.NOTIFICATION_COUNT);
        int count = Integer.parseInt(c);

        if (count > 0) {
            txtNotificationCount.setText(count + "");
            txtNotificationCount.setVisibility(View.VISIBLE);
        } else
            txtNotificationCount.setVisibility(View.GONE);
    }


    public void OpenChangePin() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        changePinFragment = new ChangePinMainFragment();
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, changePinFragment).addToBackStack(null).commit();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(changePinFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, changePinFragment);
        fragmentTransaction.commit();
    }

    private void OpenFavourites() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        favouritesFragment = new FavouritesMain();
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, favouritesFragment).addToBackStack(null).commit();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(favouritesFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, favouritesFragment, "favouritesFragment");
        fragmentTransaction.commit();
    }

    public void OpenTransactionHistory() {
        mMenuDrawer.closeMenu();

        // transactionSummaryFragment=TransactionSummaryFragment.newInstance(1, "",0,"","");

        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
       /* if(mIsTransactionSummaryClosed = true){
            getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, transactionSummaryFragment).addToBackStack(null).commit();
            mMenuDrawer.closeMenu();
            mIsTransactionSummaryClosed = false;
        } */


        transactionSummaryFragment = new TransactionSummaryFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(transactionSummaryFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, transactionSummaryFragment);
        fragmentTransaction.commit();
    }

    public void CashOut() {
        mMenuDrawer.closeMenu();

        // transactionSummaryFragment=TransactionSummaryFragment.newInstance(1, "",0,"","");

        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
       /* if(mIsTransactionSummaryClosed = true){
            getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, transactionSummaryFragment).addToBackStack(null).commit();
            mMenuDrawer.closeMenu();
            mIsTransactionSummaryClosed = false;
        } */


        cashout = new CashOutFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(cashout.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, cashout);
        fragmentTransaction.commit();
    }

    public void OpenPayPerson() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        payPersonMain = new PayPersonMain();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(payPersonMain.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, payPersonMain);
        fragmentTransaction.commit();
//        payPersonMain=new PayPersonMain();
//        getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, payPersonMain).addToBackStack(null).commit();
    }


    public void OpenPayMerchant() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        payMerchantMainFragment = new PayMerchantMainFragment();
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, payMerchantMainFragment).addToBackStack(null).commit();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(payMerchantMainFragment.getClass().getName());
        // fragmentTransaction.add(R.id.dashboard_container, payMerchantMainFragment);
        fragmentTransaction.replace(R.id.dashboard_container, payMerchantMainFragment);
        fragmentTransaction.commit();
    }


    public void OpenPayMerchantwithID(String merchantID, String merchantName) {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        payMerchantMainFragment = new PayMerchantMainFragment();
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, payMerchantMainFragment).addToBackStack(null).commit();
        Bundle bundle = new Bundle();
        bundle.putString("merchantID", merchantID);
        bundle.putString("merchantName", merchantName);

        FragmentManager fragmentManager = getSupportFragmentManager();
        payMerchantMainFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(payMerchantMainFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, payMerchantMainFragment);
        fragmentTransaction.commit();
    }


    public void OpenBeneficiaries() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        beneficiariesMain = new BeneficiariesMain();
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, beneficiariesMain).addToBackStack(null).commit();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(beneficiariesMain.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, beneficiariesMain);
        fragmentTransaction.commit();
    }

    public void OpenBankAccount() {

        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        /*bankAccountMainFragment=new BankAccountMainFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, bankAccountMainFragment).addToBackStack(null).commit(); */

        mMenuDrawer.closeMenu();
        bankAccountMainFragment = new BankAccountMainFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(bankAccountMainFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, bankAccountMainFragment);
        fragmentTransaction.commit();
    }

    public void OpenCard() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        cardFragment = new ManageCardMainFragment();
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, cardFragment).addToBackStack(null).commit();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(cardFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, cardFragment);
        fragmentTransaction.commit();
    }

    public void OpenQrCode() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        qrCodeFragment = GenerateQrCodeMainFragment.newInstance("", "", merChantName, merChantId, mobileNumber);
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, qrCodeFragment).addToBackStack(null).commit();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(qrCodeFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, qrCodeFragment);
        fragmentTransaction.commit();
    }

    public void OpenBill() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
    }

    public void OpenAirtimeTopup() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        airtimeTopupFragment = new AirTimeTopupMain();
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, airtimeTopupFragment).addToBackStack(null).commit();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(airtimeTopupFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, airtimeTopupFragment);
        fragmentTransaction.commit();
    }

    public void OpenInviteFriends() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        inviteFriendFragment = new InviteFriendFragment();
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, inviteFriendFragment).addToBackStack(null).commit();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(inviteFriendFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, inviteFriendFragment);
        fragmentTransaction.commit();
    }

    public void OpenPendingRequest() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        pendingRequestFragment = new PendingRequestMain();
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, pendingRequestFragment).addToBackStack(null).commit();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(pendingRequestFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, pendingRequestFragment);
        fragmentTransaction.commit();
    }

    public void OpenRequestMoney() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        /* requestMoneyFragment=new RequestMoneyMainFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, requestMoneyFragment).addToBackStack(null).commit(); */

        requestMoneyFragment = new RequestMoneyMainFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(requestMoneyFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, requestMoneyFragment);
        fragmentTransaction.commit();
    }

    public void OpenAboutmWallet() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        aboutFragment = new AboutFragment();
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, aboutFragment).addToBackStack(null).commit();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(aboutFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, aboutFragment);
        fragmentTransaction.commit();

    }

    public void OpenAddMoney() {
        mMenuDrawer.closeMenu();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        bankAccountAddMoneyFragment = new BankAccountAddMoneyFragment();
        //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, bankAccountAddMoneyFragment).addToBackStack(null).commit();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(bankAccountAddMoneyFragment.getClass().getName());
        fragmentTransaction.add(R.id.dashboard_container, bankAccountAddMoneyFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        final int drawerState = mMenuDrawer.getDrawerState();
        if (drawerState == MenuDrawer.STATE_OPEN || drawerState == MenuDrawer.STATE_OPENING) {
            mMenuDrawer.closeMenu();
            return;
        }

        super.onBackPressed();

        int count = getSupportFragmentManager().getBackStackEntryCount();
        LogUtils.Verbose("TAG", " Reset stack is " + isOnDashboard + "Count " + count);


        if (count > 1) {
            LogUtils.Verbose("TAG", "if");
            getSupportFragmentManager().popBackStackImmediate();

        } else if (count == 0) {
            LogUtils.Verbose("TAG", "if else");
            isOnDashboard = true;
            setUpActionBar();
        } else {
            LogUtils.Verbose("TAG", "else");
            isOnDashboard = false;
        }

//        if (count <= 1) {
//            isOnDashboard = true;
//setUpActionBar();
//        } else
//            isOnDashboard = false;
    }

    public void CallLogout(final String mobilenumber) {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        timeStamp = dateFormat.format(cal.getTime());


        /*<SPID>|<USER_TYPE>|<MOBILE_NO>|<EncryptedXML>*/

        xmlvalue =
                "<MpsXml>" +
                        "<Header>" +
                        "<ChannelId>APP</ChannelId>" +
                        "<Timestamp>" + timeStamp + "</Timestamp>" +
                        "<SessionId>" + GlobalVariables.sessionId + "</SessionId>" +
                        "<ServiceProvider>" + getResources().getString(R.string.serviceprovider) + "</ServiceProvider>" +
                        "</Header>" +
                        "<Request>" +
                        "<RequestType>logout</RequestType>" +
                        "<UserType>CU</UserType>" +
                        "</Request>" +
                        "<RequestDetails>" +
                        "<MobileNumber>" + mobilenumber + "</MobileNumber>" +
                        "<ResponseURL>" + GlobalVariables.url + "</ResponseURL>" +
                        "<ResponseVar>mobileResponseXML</ResponseVar>" +
                        "</RequestDetails>" +
                        "</MpsXml>";

        LogUtils.Verbose("Logout",xmlvalue);

        try {
            AESecurity aes = new AESecurity();
            aes.encrypt(xmlvalue, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {
                @Override
                public void executeLoginAsync(String mobileNumber, String encryptedXML) {
                    String final_string = getResources().getString(R.string.serviceprovider) + "|" + getResources().getString(R.string.usertype) + "|" + mobilenumber + "|" + encryptedXML;
                    Log.v("Plain Request ", " " + final_string);
                    new MyAsyncTaskLogout().execute(final_string);
                }
            });
        } catch (NoSuchAlgorithmException | IllegalBlockSizeException | NoSuchPaddingException | InvalidAlgorithmParameterException |
                BadPaddingException | UnsupportedEncodingException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }


    public class MyAsyncTaskLogout extends AsyncTask<String, Integer, String> {

        String result = "";

        @Override
        protected String doInBackground(String... params) {
           /* HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;*/
            HttpClient httpclient = new DefaultHttpClient();

            HttpPost httppost = new HttpPost(GlobalVariables.url);

            // System.out.println("request" + params[0]);

            Log.v("REQUEST", params[0]);
            List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
            pairs.add(new BasicNameValuePair("mobileRequestXML", params[0]));
            try {
                httppost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("Response1 : " + response);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    result = EntityUtils.toString(response.getEntity());
                }
            } catch (UnsupportedEncodingException | ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("RESULT : " + result);
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected void onPostExecute(String s1) {
            super.onPostExecute(s1);
            String decryptedString = "";
            System.out.println("Response : " + s1);

//            final MpsXml[] mpsxml = new MpsXml[1];
//            final ParseResponse[] parse = new ParseResponse[1];

            try {

                AESecurity aes = new AESecurity();
                try {
                    aes.decrypt(result, MyApplication.getInstance().getKey(), iv, new MainActivity.PostEncryption() {

                        @Override
                        public void executeLoginAsync(String mobileNumber, String decrypted) {

                            System.out.println("declogin " + decrypted);
                            Log.d("TAG", "logout 12=" + decrypted);
                            if (decrypted.equals(" ")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(DrawerMain.this);
                                builder.setMessage(getResources().getString(R.string.no_internet_connection))
                                        .setTitle("mWallet");

                                AlertDialog dialog = builder.create();
                                dialog.setCanceledOnTouchOutside(true);
                                dialog.show();
                                progressDialog.dismiss();
                            } else {


                                MyApplication.getInstance().setGeneralBalance(null);
                                MyApplication.getInstance().setPromotionalBalance(null);
                                MyApplication.getInstance().setTotalBalance(null);


                                ParseResponse parse = new ParseResponse();
                                InputSource is = new InputSource(new StringReader(decrypted));
                                MpsXml mpsxml = parse.parseXML(is);
                                System.out.println("declogin " + decrypted);
                                progressDialog.dismiss();
                                if (mpsxml.getResponse().getResponseType().equalsIgnoreCase("Success")) {

                                    Log.d("TAG", "logout 133=" + mpsxml.getResponsedetails().getMessage());
//                                 String reason=mpsxml.getResponsedetails().getReason();
//                                AlertDialog.Builder builder = new AlertDialog.Builder(DrawerMain.this);
//                                builder.setMessage(mpsxml[0].getResponse().getResponseType())
//                                        .setTitle("mWallet");
//                                AlertDialog dialog = builder.create();
//                                dialog.setCanceledOnTouchOutside(true);
//                                dialog.show();
                                    editor.putString("SessionId", "");
                                    editor.commit();
                                    GlobalVariables.sessionId = "";


                                    Intent logout = new Intent(DrawerMain.this, MainActivity.class);
                                    startActivity(logout);
                                    finish();

                                    DrawerMain.this.finish();
                                    /*Intent logout = new Intent(DrawerMain.this, MainActivity.class);
                                    startActivity(logout);*/

                                } else {
//                                String reason=mpsxml.getResponsedetails().getReason();
                                    /*AlertDialog.Builder builder = new AlertDialog.Builder(DrawerMain.this);
                                    builder.setMessage(mpsxml.getResponsedetails().getMessage())
                                            .setTitle("mWallet");
                                    AlertDialog dialog = builder.create();
                                    dialog.setCanceledOnTouchOutside(true);
                                    dialog.show();*/

                                    editor.putString("SessionId", "");
                                    editor.commit();
                                    GlobalVariables.sessionId = "";


                                    Intent logout = new Intent(DrawerMain.this, MainActivity.class);
                                    startActivity(logout);
                                    finish();
                                }

                            }
                        }
                    });


                } catch (InvalidKeyException | UnsupportedEncodingException | IllegalBlockSizeException |
                        InvalidAlgorithmParameterException | BadPaddingException e) {
                    e.printStackTrace();
                }
            } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                e.printStackTrace();
            }

        }
    }


    private void LayoutInitialization() {

        rootlay = (RelativeLayout) findViewById(R.id.drawer_Layout);
        relativeLayoutChangePin = (RelativeLayout) findViewById(R.id.relative_lay_change_pins);
        relativeLayoutTransactionHistory = (RelativeLayout) findViewById(R.id.relative_lay_transaction_history);
        relativeLayoutFavourites = (RelativeLayout) findViewById(R.id.relative_lay_favourites);
        relativeLayoutPayPerson = (RelativeLayout) findViewById(R.id.relative_lay_pay_person);
        relativeLayoutPayMerchant = (RelativeLayout) findViewById(R.id.relative_lay_pay_merchant);
        relativeLayoutBeneficiaries = (RelativeLayout) findViewById(R.id.relative_lay_beneficiaries);
        relativeLayoutBankAccounts = (RelativeLayout) findViewById(R.id.relative_lay_bank_accounts);
        relativeLayoutCard = (RelativeLayout) findViewById(R.id.relative_lay_cards);
        relativeLayoutQrCode = (RelativeLayout) findViewById(R.id.relative_lay_generate_qrcode);
        relativeLayoutBill = (RelativeLayout) findViewById(R.id.relative_lay_pay_bills);
        relativeLayoutAirtimeTopup = (RelativeLayout) findViewById(R.id.relative_lay_airtime_topup);
        relativeLayoutInviteFriends = (RelativeLayout) findViewById(R.id.relative_lay_invite_friends);
        relativeLayoutPendingRequest = (RelativeLayout) findViewById(R.id.relative_lay_pending_request);
        relativeLayoutRequestMoney = (RelativeLayout) findViewById(R.id.relative_lay_request_money);
        relativeLayoutAboutmWallet = (RelativeLayout) findViewById(R.id.relative_lay_about_mwallet);



        tvname = (TextView) findViewById(R.id.tvName);
        profile_photo = (ImageView) findViewById(R.id.icon);
        tvinfo = (TextView) findViewById(R.id.tvInfo);
        textView_mwallet_balance = (TextView) findViewById(R.id.tvAmount);
        textView_last_login = (TextView) findViewById(R.id.tvInfo);
        logout = (Button) findViewById(R.id.buttonLogout);
        //logout.setPadding(0,0,0,getNavigationBarHeight());
        boolean hasMenuKey = ViewConfiguration.get(this).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);

        if (!hasMenuKey && !hasBackKey) {
            // Do whatever you need to do, this device has a navigation bar
            RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) logout.getLayoutParams();
            param.setMargins(0, 0, 0, getNavigationBarHeight());
        }

        //Log.v("TAG","bottomBarHeight="+getNavigationBarHeight());
        String photoURL = "http://demo.timesofmoney.com/mps-demo/resources/images/" + MyApplication.getInstance().getMerchantMobieNo() + "_PROFILE.png";
        Log.v("TAG", " Photo " + photoURL);
        Picasso.with(this).load(photoURL).placeholder(R.drawable.mwallet_logo).error(R.drawable.mwallet_logo).into(profile_photo);
    }

    private int getNavigationBarHeight() {
        Resources resources = this.getResources();

        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    //Set custom Action bar
    public void setUpActionBar() {
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar_dashboard);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));

        //Set Menu drawer
        actionBarNavagationImage = (ImageView) findViewById(R.id.action_bar_navagation_image);
        actionBarNavagationImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MyApplication.resetTimer(DashBoard.this);
                textView_mwallet_balance.setText("Balance: AED " + MyApplication.getInstance().getTotalBalance());
                mMenuDrawer.openMenu();

                v = DrawerMain.this.getCurrentFocus();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });

        //Initialization of Action bar components
        actionBarBackImage = (ImageView) findViewById(R.id.action_bar_back);
        actionBarBackImage.setVisibility(View.GONE);
        actionBarTitleText = (TextView) findViewById(R.id.action_bar_title);
        actionBarTitleText.setVisibility(View.GONE);

        //actionBarNotificationImage = (ImageView) findViewById(R.id.actionbar_notification);
        llNotification = (RelativeLayout) findViewById(R.id.toolbar_ll_notification);
        txtNotificationCount = (TextView) findViewById(R.id.txtCount);

        handleNotificationCount();



        actionBarQrcodeImage = (ImageView) findViewById(R.id.actionbar_qrcode);
        actionBarAppLogoImage = (ImageView) findViewById(R.id.action_bar_imageview_app_logo);
        actionBarSearchImage = (ImageView) findViewById(R.id.actionbar_search);
        actionBarSearchImage.setVisibility(View.GONE);
        actionBarPersonText = (TextView) findViewById(R.id.action_bar_PersonID);
        actionBarPersonText.setVisibility(View.GONE);
        actionBarPersonTextValue = (TextView) findViewById(R.id.action_bar_PersonAmount);
        actionBarPersonTextValue.setVisibility(View.GONE);


        llNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //notificationFragment = new NotificationActivity();

                if (mMenuDrawer.isMenuVisible()) {
                    mMenuDrawer.closeMenu();
                }

                Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
                startActivity(intent);
                //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, qrCodeFragment).commit();
//                FragmentManager fragmentManager = getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager
//                        .beginTransaction();
//                fragmentTransaction.addToBackStack(notificationFragment.getClass()
//                        .getName());
//                fragmentTransaction.add(R.id.dashboard_container, notificationFragment);
//                fragmentTransaction.commit();
            }
        });





        actionBarBackImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
                LogUtils.Verbose("Fragment Name ", getSupportFragmentManager().findFragmentByTag(tag) + "");

                if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                    getSupportFragmentManager().popBackStackImmediate();
                } else {
                    onBackPressed();
                }


            }
        });

        actionBarQrcodeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrCodeFragment = new GenerateQrCodeMainFragment();

                //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, qrCodeFragment).commit();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.addToBackStack(qrCodeFragment.getClass()
                        .getName());
                fragmentTransaction.add(R.id.dashboard_container, qrCodeFragment);
                fragmentTransaction.commit();

            }
        });

        actionBarSearchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transactionSearchFragment = new TransactionSearchFragment();
                //getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container, transactionSearchFragment).commit();

                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.addToBackStack(transactionSearchFragment.getClass()
                        .getName());
                fragmentTransaction.add(R.id.dashboard_container, transactionSearchFragment);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        handleNotificationCount();

    }

    @Override
    protected void onPause() {

        super.onPause();
    }









    //Update Action bar
    public void updateActionBarData(boolean showBackImageView, boolean showAppLogoImageView, boolean showNotificationImageView,
                                    boolean showTagImageView, boolean showQrcodeImageView, boolean showSearchImageView,
                                    boolean showTitleTextView, String titleText, boolean showPersonTextView, String personText,
                                    boolean showPersonTextViewValue, String personTextValue) {

        if (showBackImageView) {
            actionBarBackImage.setVisibility(View.VISIBLE);
        } else {
            actionBarBackImage.setVisibility(View.GONE);
        }

        if (showAppLogoImageView) {
            actionBarAppLogoImage.setVisibility(View.VISIBLE);
        } else {
            actionBarAppLogoImage.setVisibility(View.GONE);
        }

        if (showNotificationImageView) {
            llNotification.setVisibility(View.VISIBLE);
            txtNotificationCount.setVisibility(View.VISIBLE);
        } else {
            llNotification.setVisibility(View.GONE);
            txtNotificationCount.setVisibility(View.GONE);
        }

        if (showTagImageView) {
            //actionBarTagImage.setVisibility(View.VISIBLE);
        } else {
          //  actionBarTagImage.setVisibility(View.GONE);
        }

        if (showQrcodeImageView) {
            actionBarQrcodeImage.setVisibility(View.VISIBLE);
        } else {
            actionBarQrcodeImage.setVisibility(View.GONE);
        }

        if (showSearchImageView) {
            actionBarSearchImage.setVisibility(View.VISIBLE);
        } else {
            actionBarSearchImage.setVisibility(View.GONE);
        }

        if (showTitleTextView) {
            actionBarTitleText.setVisibility(View.VISIBLE);
            actionBarTitleText.setText(titleText);
        } else {
            actionBarTitleText.setVisibility(View.GONE);
        }

        if (showPersonTextView) {
            actionBarPersonText.setVisibility(View.VISIBLE);
            actionBarPersonText.setText(personText);
        } else {
            actionBarPersonText.setVisibility(View.GONE);
        }

        if (showPersonTextViewValue) {
            actionBarPersonTextValue.setVisibility(View.VISIBLE);
            actionBarPersonTextValue.setText(personTextValue);
        } else {
            actionBarPersonTextValue.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the MyHome/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_tag || id == R.id.action_notification || id == R.id.action_qrcode) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }







}
