package in.intellifox.mwallet.views.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by SADIQ on 7/17/2015.
 */
public class ButtonMuseosansThreeHundred extends Button {
    public ButtonMuseosansThreeHundred(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ButtonMuseosansThreeHundred(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonMuseosansThreeHundred(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/museo_sans_three.otf");
        setTypeface(tf ,1);

    }
}
