package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import in.intellifox.mwallet.R;

/**
 * Created by Owner on 9/30/2015.
 */
public class TransactionSearchFragment extends Fragment {

    Button search;
    Fragment transactionRecentTabFragment;
    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transactionRecentTabFragment=new TransactionRecentTabFragment();
                //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.dashboard_container,transactionRecentTabFragment).commit();

                FragmentManager fragmentManager =getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(transactionRecentTabFragment.getClass().getName());
                fragmentTransaction.replace(R.id.dashboard_container, transactionRecentTabFragment);
                fragmentTransaction.commit();
            }
        });
    }

    private void LayoutInitialization(View view) {
        search=(Button)view.findViewById(R.id.transaction_search_button);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.transaction_search_fragment, container, false);
    }

}
