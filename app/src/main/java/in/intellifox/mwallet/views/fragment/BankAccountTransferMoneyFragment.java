package in.intellifox.mwallet.views.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import in.intellifox.mwallet.MyApplication;
import in.intellifox.mwallet.R;
import in.intellifox.mwallet.views.activities.DrawerMain;

/**
 * Created by Owner on 9/11/2015.
 */
public class BankAccountTransferMoneyFragment extends Fragment {

    Button transferMoney;
    LinearLayout hundred,five,thousand,fiftyhundred,mainLayout;

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInitialization(view);

        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.resetTimer(getActivity());
            }
        });

        transferMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View popupView = getLayoutInflater(savedInstanceState).inflate(R.layout.custom_popup, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);

                TextView tv1=(TextView)popupView.findViewById(R.id.txt_note1);
                tv1.setText("The amount of AED 500 will be");
                TextView tv2=(TextView)popupView.findViewById(R.id.txt_note2);
                tv2.setText("transferred to your bank account");
                TextView tv3=(TextView)popupView.findViewById(R.id.txt_note3);
                tv3.setText("linked with mWallet");

                Button btnDismiss = (Button)popupView.findViewById(R.id.button_ok_on_popup);
                btnDismiss.setOnClickListener(new Button.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        popupWindow.dismiss();
                        BankAccountSuccess rms=new BankAccountSuccess();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, rms).commit();
                    }});

                popupWindow.showAtLocation(mainLayout, Gravity.CENTER,0,0);
            }
        });
    }

    private void LayoutInitialization(View view) {
        transferMoney=(Button)view.findViewById(R.id.transfermoney_button);
        hundred=(LinearLayout)view.findViewById(R.id.layout_hundred);
        five=(LinearLayout)view.findViewById(R.id.layout_five);
        thousand=(LinearLayout)view.findViewById(R.id.layout_thousand);
        fiftyhundred=(LinearLayout)view.findViewById(R.id.layout_fiftyhundred);
        mainLayout=(LinearLayout)view.findViewById(R.id.root_linear_lay_for_bank_transfermoney);
    }

    //Custom Action bar
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((DrawerMain)getActivity()).updateActionBarData(true, false, false, false, false, false, true, "Transfer Money",false,"",false,"");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bankaccount_transfermoney, container, false);
    }
}
