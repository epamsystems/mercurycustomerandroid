package in.intellifox.mwallet.models;

import org.simpleframework.xml.Element;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import in.intellifox.mwallet.utilities.GlobalVariables;


/**
 * Created by kunalk on 1/29/2016.
 */
public class Header {
    @Element(required = false)
    String ChannelId="App",Timestamp="TestTime",SessionId="Test",ServiceProvider="mercury";

    public Header()
    {
        Calendar calendar=Calendar.getInstance();
        SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");

        Timestamp=sdf.format(calendar.getTime());

        SessionId= GlobalVariables.sessionId;

        ServiceProvider = "mercury";

        //ServiceProvider = "toml";
    }

    public String getSessionId() {
        return SessionId;
    }
}
