package in.intellifox.mwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;


@Root(name="MpsXml")
public class DocumentResponse {

    @Element(name="Header")
    Header header;

    @Element(name="Request")
    Request request;

    @Element(name="Response")
    Response response;

    @Element(name="ResponseDetails")
    DocumentResponseDetails responseDetails;

    public Response getResponse() {
        return response;
    }

    public DocumentResponseDetails getResponseDetails() {
        return responseDetails;
    }

    public Header getHeader() {
        return header;
    }
}
