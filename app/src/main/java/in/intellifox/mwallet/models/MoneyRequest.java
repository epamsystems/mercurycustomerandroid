package in.intellifox.mwallet.models;

import org.simpleframework.xml.Attribute;

import java.io.Serializable;

import in.intellifox.mwallet.utilities.Utils;

/**
 * Created by kunalk on 8/11/2016.
 */
public class MoneyRequest implements Serializable {
    @Attribute
    String id,amount,mobileNumber,firstName,lastName,txnDate;

    public String getId() {
        return id;
    }

    public String getAmount() {
        return amount;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getFirstName() {
        if(firstName==null || firstName.equalsIgnoreCase("null") || firstName.isEmpty())
            return "";

        return Utils.decodeString(firstName);
    }

    public String getLastName() {
        if(lastName==null || lastName.equalsIgnoreCase("null") || lastName.isEmpty())
            return "";

        return Utils.decodeString(lastName);
    }

    public String getTxnDate() {
        return txnDate;
    }
}
