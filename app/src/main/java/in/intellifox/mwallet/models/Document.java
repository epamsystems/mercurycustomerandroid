package in.intellifox.mwallet.models;

import org.simpleframework.xml.Attribute;

/**
 * Created by kunalk on 3/10/2016.
 */
public class Document {

    @Attribute
    String id,name;

    public Document()
    {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Document)) return false;

        Document document = (Document) o;

        return !(getId() != null ? !getId().equals(document.getId()) : document.getId() != null);

    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    @Override
    public String toString() {
        return name;
    }

    public Document(String id, String name)
    {
       this.id=id;
        this.name=name;
    }
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
