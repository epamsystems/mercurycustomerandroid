package in.intellifox.mwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by pankajp on 20-04-2016.
 */
@Root(name = "MpsXml")
public class OfferResponse {

    @Element(name = "Header")
    Header header;

    @Element(name = "Request")
    Request request;

    @Element(name = "Response")
    Response response;

    @Element(name = "ResponseDetails")
    OfferResponseDetails offerResponseDetails;



    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public OfferResponseDetails getOfferResponseDetails() {
        return offerResponseDetails;
    }

    public void setOfferResponseDetails(OfferResponseDetails offerResponseDetails) {
        this.offerResponseDetails = offerResponseDetails;
    }


}
