package in.intellifox.mwallet.models;

/**
 * Created by kunalk on 4/28/2016.
 */
public class MercuryNotification {

    private String message,timestamp,time;


    public String getMessage() {
        return message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }
}
