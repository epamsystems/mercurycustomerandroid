package in.intellifox.mwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by kunalk on 8/11/2016.
 */

@Root(name="MpsXml")
public class PendingRequest {

    @Element(name="Header")
    Header header;

    @Element(name="Request")
    Request request;

    @Element(name="Response")
    Response response;

    @Element(name = "ResponseDetails")
    PendingRequestDetails requestDetails;

    public PendingRequest(){}

    public PendingRequest(String requestType,PendingRequestDetails requestDetails)
    {
        this.header=new Header();
        this.request=new Request(requestType);
        this.requestDetails=requestDetails;
    }

    public PendingRequestDetails getRequestDetails() {
        return requestDetails;
    }

    public Response getResponse() {
        return response;
    }
}
