package in.intellifox.mwallet.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;



/**
 * Created by kunalk on 3/10/2016.
 */
@Root(name="MpsXml")
@Order(elements = {"Header","Request","RequestDetails"})
public class DocumentRequest {

    @Element(name="Header")
    Header header;

    @Element(name="Request")
    Request request;

    @Element(name = "RequestDetails")
    DocumentRequestDetails requestDetails;

    public DocumentRequest(String requestType,DocumentRequestDetails requestDetails)
    {
        this.header=new Header();
        this.request=new Request(requestType);
        this.requestDetails=requestDetails;
    }
}

