package in.intellifox.mwallet.models;

import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by pankajp on 18/4/17.
 */

public class OfferList {

    @ElementList(entry = "Offer", inline = true, required = false)
    List<Offer> listOffers;


    public List<Offer> getListOffers() {
        return listOffers;
    }

}
