package in.intellifox.mwallet.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

/**
 * Created by pankajp on 18/4/17.
 */

public class Offer implements Parcelable {

    @Attribute(required = false)
    public String OfferId = "";
    @Element(required = false)
    String OfferTypeName;
    @Element(required = false)
    String OfferName;
    @Element(required = false)
    String Description;
    @Element(required = false)
    String FromDate;
    @Element(required = false)
    String ToDate;
    @Element(required = false)
    String PromoCodeName;
    @Element(required = false)
    String Type;
    @Element(required = false)
    double Value;

    public Offer() {
    }

    protected Offer(Parcel in) {
        OfferId = in.readString();
        OfferTypeName = in.readString();
        OfferName = in.readString();
        Description = in.readString();
        FromDate = in.readString();
        ToDate = in.readString();
        PromoCodeName = in.readString();
        Type = in.readString();
        Value = in.readDouble();
    }

    public static final Creator<Offer> CREATOR = new Creator<Offer>() {
        @Override
        public Offer createFromParcel(Parcel in) {
            return new Offer(in);
        }

        @Override
        public Offer[] newArray(int size) {
            return new Offer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(OfferId);
        dest.writeString(OfferTypeName);
        dest.writeString(OfferName);
        dest.writeString(Description);
        dest.writeString(FromDate);
        dest.writeString(ToDate);
        dest.writeString(PromoCodeName);
        dest.writeString(Type);
        dest.writeDouble(Value);
    }


    public String getOfferId() {
        return OfferId;
    }

    public void setOfferId(String offerId) {
        OfferId = offerId;
    }

    public String getOfferTypeName() {
        return OfferTypeName;
    }

    public void setOfferTypeName(String offerTypeName) {
        OfferTypeName = offerTypeName;
    }

    public String getOfferName() {
        return OfferName;
    }

    public void setOfferName(String offerName) {
        OfferName = offerName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String fromDate) {
        FromDate = fromDate;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String toDate) {
        ToDate = toDate;
    }

    public String getPromoCodeName() {
        return PromoCodeName;
    }

    public void setPromoCodeName(String promoCodeName) {
        PromoCodeName = promoCodeName;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public double getValue() {
        return Value;
    }

    public void setValue(double value) {
        Value = value;
    }
}
