package in.intellifox.mwallet.models;

import org.simpleframework.xml.Element;

/**
 * Created by pankajp on 20-04-2016.
 */
public class OfferResponseDetails {


    public OfferList getOfferList() {
        return objOfferList;
    }

    public void setOfferList(Offer objOffer) {
        this.objOfferList = objOfferList;
    }

    @Element(name = "Offers", required = false)
    OfferList objOfferList;

    @Element(required = false)
    String ErrorCode;
    @Element(required = false)
    String Reason;
    @Element(name = "Message", required = false)
    String Message;


    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }


    public String getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(String errorCode) {
        ErrorCode = errorCode;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }
}
