package in.intellifox.mwallet.apicalls;


import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

/**
 * Created by kunalk on 1/20/2016.
 */
public interface IRetrofit {
    @FormUrlEncoded
    @POST("mobileRequestHandler")
    Call<ResponseBody> executeAPI(@Field("mobileRequestXML") String mobileRequestXML);


    @POST("mobileRequestHandler")
    Call<ResponseBody> getSecurityKey(@Query("mobileRequestXML") String mobileRequestXML);

    @Multipart
    @POST("CustomerRegistration")
    Call<ResponseBody> registerCustomer(@PartMap Map<String, RequestBody> map);
}
