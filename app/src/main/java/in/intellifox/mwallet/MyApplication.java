package in.intellifox.mwallet;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.IntentCompat;
import android.telephony.TelephonyManager;

import in.intellifox.mwallet.utilities.GlobalVariables;
import in.intellifox.mwallet.utilities.Utils;

/**
 * Created by admin on 8/26/2015.
 */
public class MyApplication extends Application {
    private static MyApplication mInstance;
    private String key = "";
    private String MobileNo = "";
    private static Thread mLogoutThread;
    private final String iv = "288dca8258b1dd7c";

    private String mGeneralBalance, mPromotionalBalance, mTotalBalance;

    public String getGeneralBalance() {
        return mGeneralBalance != null ? mGeneralBalance : "NA";
    }

    public void setGeneralBalance(String mGeneralBalance) {
        this.mGeneralBalance = mGeneralBalance;
    }

    public String getTotalBalance() {
        return mTotalBalance != null ? mTotalBalance : "NA";
    }

    public void setTotalBalance(String mTotalBalance) {
        this.mTotalBalance = mTotalBalance;
    }

    public String getPromotionalBalance() {
        return mPromotionalBalance != null ? mPromotionalBalance : "NA";
    }

    public void setPromotionalBalance(String mPromotionalBalance) {
        this.mPromotionalBalance = mPromotionalBalance;
    }


    private String mProfile_FirstName;
    private String mProfile_Last_Name;
    private String mProfile_Last_Login;
    private String mProfile_Last_Transaction;
    private String mProfile_WalletID_General;
    private String mProfile_Mobile_Number;
    private String mProfile_Customer_ID;
    private String mProfile_Customer_Type;
    private String mProfile_City;
    private String mProfile_Address;
    private String mProfile_Pin_Code;
    private String mProfile_EmailID;
    private String mProfile_Wallet_Balance;

    private String mBankDetails_Account_Number;
    private String mBankDetails_Bank_Name;
    private String mBankDetails_Branch_Name;
    private String mBankDetails_BranchID;
    private String mBankDetails_Branch_Address;
    private String mBankDetails_Identification_Code;
    private String mBankDetails_ISPrimary;
    private String NFC_DATA;

    //This is implemented for Oneclick
    private String OneClilckFlag;
    private String OneClilckAmount;

    public String getOneClilckFlag() {
        return OneClilckFlag;
    }

    public void setOneClilckFlag(String OneClilckFlag) {
        this.OneClilckFlag = OneClilckFlag;
    }


    public String getOneClilckAmount() {
        return OneClilckAmount;
    }

    public void setOneClilckAmount(String OneClilckAmount) {
        this.OneClilckAmount = OneClilckAmount;
    }


    public String getmProfile_City() {
        return mProfile_City;
    }

    public void setmProfile_City(String mProfile_City) {
        this.mProfile_City = mProfile_City;
    }

    public String getmProfile_FirstName() {
        if (mProfile_FirstName == null || mProfile_FirstName.equalsIgnoreCase("null") || mProfile_FirstName.isEmpty())
            return "";

        return Utils.decodeString(mProfile_FirstName);
    }

    public void setmProfile_FirstName(String mProfile_FirstName) {
        this.mProfile_FirstName = mProfile_FirstName;
    }

    public String getmProfile_Last_Name() {
        if (mProfile_Last_Name == null || mProfile_Last_Name.equalsIgnoreCase("null") || mProfile_Last_Name.isEmpty())
            return "";

        return Utils.decodeString(mProfile_Last_Name);
    }

    public void setmProfile_Last_Name(String mProfile_Last_Name) {
        this.mProfile_Last_Name = mProfile_Last_Name;
    }

    public String getmProfile_Last_Login() {
        return mProfile_Last_Login;
    }

    public void setmProfile_Last_Login(String mProfile_Last_Login) {
        this.mProfile_Last_Login = mProfile_Last_Login;
    }

    public String getNFC_DATA() {
        return NFC_DATA;
    }

    public void setNFC_DATA(String NFC_DATA) {
        this.NFC_DATA = NFC_DATA;
    }


    public String getmProfile_Wallet_Balance() {
        return mProfile_Wallet_Balance;
    }

    public void setmProfile_Wallet_Balance(String mProfile_Wallet_Balance) {
        this.mProfile_Wallet_Balance = mProfile_Wallet_Balance;
    }

    public String getmProfile_Last_Transaction() {
        return mProfile_Last_Transaction;
    }

    public void setmProfile_Last_Transaction(String mProfile_Last_Transaction) {
        this.mProfile_Last_Transaction = mProfile_Last_Transaction;
    }

    public String getmProfile_WalletID_General() {
        return mProfile_WalletID_General;
    }

    public void setmProfile_WalletID_General(String mProfile_WalletID_General) {
        this.mProfile_WalletID_General = mProfile_WalletID_General;
    }

    public String getmProfile_Mobile_Number() {
        return mProfile_Mobile_Number;
    }

    public void setmProfile_Mobile_Number(String mProfile_Mobile_Number) {
        this.mProfile_Mobile_Number = mProfile_Mobile_Number;
    }

    public String getmProfile_Customer_ID() {
        return mProfile_Customer_ID;
    }

    public void setmProfile_Customer_ID(String mProfile_Customer_ID) {
        this.mProfile_Customer_ID = mProfile_Customer_ID;
    }


    public String getmProfile_Customer_Type() {
        return mProfile_Customer_Type;
    }

    public void setmProfile_Customer_Type(String mProfile_Customer_Type) {
        this.mProfile_Customer_Type = mProfile_Customer_Type;
    }

    public String getmProfile_Address() {
        return mProfile_Address;
    }

    public void setmProfile_Address(String mProfile_Address) {
        this.mProfile_Address = mProfile_Address;
    }

    public String getmProfile_Pin_Code() {
        return mProfile_Pin_Code;
    }

    public void setmProfile_Pin_Code(String mProfile_Pin_Code) {
        this.mProfile_Pin_Code = mProfile_Pin_Code;
    }

    public String getmProfile_EmailID() {
        return mProfile_EmailID;
    }

    public void setmProfile_EmailID(String mProfile_EmailID) {
        this.mProfile_EmailID = mProfile_EmailID;
    }

    public String getmBankDetails_Account_Number() {
        return mBankDetails_Account_Number;
    }

    public void setmBankDetails_Account_Number(String mBankDetails_Account_Number) {
        this.mBankDetails_Account_Number = mBankDetails_Account_Number;
    }

    public String getmBankDetails_Bank_Name() {
        return mBankDetails_Bank_Name;
    }

    public void setmBankDetails_Bank_Name(String mBankDetails_Bank_Name) {
        this.mBankDetails_Bank_Name = mBankDetails_Bank_Name;
    }

    public String getmBankDetails_Branch_Name() {
        return mBankDetails_Branch_Name;
    }

    public void setmBankDetails_Branch_Name(String mBankDetails_Branch_Name) {
        this.mBankDetails_Branch_Name = mBankDetails_Branch_Name;
    }

    public String getmBankDetails_BranchID() {
        return mBankDetails_BranchID;
    }

    public void setmBankDetails_BranchID(String mBankDetails_BranchID) {
        this.mBankDetails_BranchID = mBankDetails_BranchID;
    }

    public String getmBankDetails_Branch_Address() {
        return mBankDetails_Branch_Address;
    }

    public void setmBankDetails_Branch_Address(String mBankDetails_Branch_Address) {
        this.mBankDetails_Branch_Address = mBankDetails_Branch_Address;
    }

    public String getmBankDetails_Identification_Code() {
        return mBankDetails_Identification_Code;
    }

    public void setmBankDetails_Identification_Code(String mBankDetails_Identification_Code) {
        this.mBankDetails_Identification_Code = mBankDetails_Identification_Code;
    }

    public String getmBankDetails_ISPrimary() {
        return mBankDetails_ISPrimary;
    }

    public void setmBankDetails_ISPrimary(String mBankDetails_ISPrimary) {
        this.mBankDetails_ISPrimary = mBankDetails_ISPrimary;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public String getIv() {
        return iv;
    }

    public static void resetTimer(final Context context) {
        if (mLogoutThread != null) {
            mLogoutThread.interrupt();
        }
        mLogoutThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(9000000);
                    Intent dashBoard = new Intent(context, MainActivity.class);
                    dashBoard.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(dashBoard);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        mLogoutThread.start();
    }

    public static void stopTimer(final Context context) {
        mLogoutThread.interrupt();
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMerchantMobieNo() {
        return MobileNo;
    }

    public void setMerchantMobieNo(String MobileNo) {
        this.MobileNo = MobileNo;
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public String getIMEINo() {

        if (GlobalVariables.DEVICE_ID) {

            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            return telephonyManager.getDeviceId();

        } else {
            return "1234";
            //}
        }
    }

}